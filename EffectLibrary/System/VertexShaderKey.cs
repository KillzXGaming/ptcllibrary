﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Threading.Tasks;
using Syroot.BinaryData;

namespace EffectLibrary
{
    public struct VertexShaderKey : IResData
    {
        public VertexTransformMode VertexTransformMode { get; set; }
        public bool VertexRotationMode { get; set; }
        public byte UserSetting { get; set; }
        public byte StripeType { get; set; }
        public bool StripeFollowEmitter { get; set; }
        public bool IsPrimitive { get; set; }
        public uint UserFlag { get; set; }
        public uint UserSwitchFlag { get; set; }
        public Memory<byte> UserMacro { get; set; }

        public void Read(BinaryFileReader reader)
        {
            VertexTransformMode = (VertexTransformMode)reader.ReadByte();
            VertexRotationMode = reader.ReadBoolean();
            UserSetting = reader.ReadByte();
            StripeType = reader.ReadByte();
            StripeFollowEmitter = reader.ReadBoolean();
            IsPrimitive = reader.ReadBoolean();
            reader.ReadUInt16();
            UserFlag = reader.ReadUInt32();
            UserSwitchFlag = reader.ReadUInt32();
            UserMacro = reader.ReadBytes(16);
        }

        public void Write(BinaryFileWriter writer)
        {
        }

        void InitializeSimple(SimpleEmitterData data)
        {
            VertexTransformMode = data.VertexTransformMode;
            VertexRotationMode = data.ParticleRotationMode != 0;
            UserSetting = data.ShaderUserSetting;
            UserFlag = data.ShaderUserFlag;
            UserSwitchFlag = data.ShaderUserSwitchFlag;
            StripeType = 0;
            IsPrimitive = data.MeshType == MeshType.Primitive;
            UserMacro = new byte[16];
        }

        void InitializeComplex(ComplexEmitterData cdata) {
            InitializeSimple(cdata);

            if (cdata.VertexTransformMode == VertexTransformMode.Stripe ||
                cdata.VertexTransformMode == VertexTransformMode.ComplexStripe)
            {
                StripeType = (byte)cdata.StripeData.type;
                if ((cdata.stripeFlags & 1) != 0) StripeFollowEmitter = true;
                else StripeFollowEmitter = false;
            }
            else
            {
                StripeFollowEmitter = false;
            }
        }

        public void Initialize(SimpleEmitterData data)
        {
            if (data.Type == EmitterType.Complex)
                InitializeComplex((ComplexEmitterData)data);
            else
                InitializeSimple(data);

            UserMacro.Span[0] = 0;
        }

        public void Initialize(ChildData data)
        {
            VertexTransformMode = data.vertexTransformMode;
            VertexRotationMode = data.rotationMode != 0;
            UserSetting = data.ShaderUserSetting;
            UserFlag = data.ShaderUserFlag;
            UserSwitchFlag = data.ShaderUserSwitchFlag;
            StripeType = 3;
            IsPrimitive = data.MeshType == MeshType.Primitive;
        }

        public static bool operator ==(VertexShaderKey c1, VertexShaderKey c2)
        {
            return c1.Equals(c2);
        }

        public static bool operator !=(VertexShaderKey c1, VertexShaderKey c2)
        {
            return !c1.Equals(c2);
        }

        public bool Equals(VertexShaderKey other)
        {
            return (VertexTransformMode == other.VertexTransformMode
                   && VertexRotationMode == other.VertexRotationMode
                   && UserSetting == other.UserSetting
                   && UserFlag == other.UserFlag
                   && UserSwitchFlag == other.UserSwitchFlag
                   && StripeType == other.StripeType
                   && StripeFollowEmitter == other.StripeFollowEmitter
                   && IsPrimitive == other.IsPrimitive
                  /* && UserMacro.Equals(other.UserMacro)*/); //Todo comparing macros doesn't work?
        }
    }
}

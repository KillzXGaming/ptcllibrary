﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using OpenTK;

namespace EffectLibrary
{
    public struct AlphaAnim
    {
        public float startValue;
        public float startDiff;
        public float endDiff;
        public int time2;
        public int time3;
    }

    public struct ScaleAnim
    {
        public Vector2 startDiff;
        public Vector2 endDiff;
        public int time2;
        public int time3;
    }

    public struct TexUVParam
    {
        public float rotate;
        public Vector2 offset;
        public Vector2 scroll;
        public Vector2 scale;
    }

    public class PtclInstance
    {
        public PtclType type;

        public float counter;
        public int lifespan;
        public Vector3 pos;
        public Vector3 posDiff;
        public Vector3 velocity;
        public Vector3 worldPos;
        public Vector3 worldPosDiff;
        public Vector3 rotation;
        public Vector3 angularVelocity;
        public float[] alpha = new float[2];
        public AlphaAnim[] alphaAnim = new AlphaAnim[2];
        public Vector2 scale;
        public ScaleAnim scaleAnim;

        public Dictionary<AnimGroupType, float> AnimData;
        public WiiU.KeyGroup[] keyGroups;

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
        public TexUVParam[] texAnimParam; // For each texture

        public Vector4[] color = new Vector4[2];

        public PtclAttributeBuffer PtclAttributeBuffer;

        public MatrixRefType matrixRefType;

        //Reference types for matrix handling
        private EmitterInstance EmitterForMatrix;
        private PtclInstance ParticleForMatrix;

        public Matrix4 matrixSRT;
        public Matrix4 matrixRT;

        public Matrix4 pMatrixSRT
        {
            get
            {
                if (matrixRefType == MatrixRefType.Particle)
                    return ParticleForMatrix.matrixSRT;
                else
                    return EmitterForMatrix.matrixSRT;
            }
        }

        public Matrix4 pMatrixRT
        {
            get
            {
                if (matrixRefType == MatrixRefType.Particle)
                    return ParticleForMatrix.matrixRT;
                else
                    return EmitterForMatrix.matrixRT;
            }
        }

        public void SetEmitterMatrixRef(EmitterInstance emitter)
        {
            matrixRefType = MatrixRefType.Emitter;
            EmitterForMatrix = emitter;
        }

        public void SetParticleMatrixRef(PtclInstance ptcl)
        {
            matrixRefType = MatrixRefType.Particle;
            ParticleForMatrix = ptcl;
        }

        public uint _140;
        public SimpleEmitterData data;

        public EmitterInstance emitter;
        public PtclStripe stripe;
        public uint particleType;
        public uint randomU32;
        public float randomF32;

        public float spawnTime;

        public Vector3 random;

        public float childEmitCounter;
        public float childPreCalcCounter;
        public float childEmitLostTime;

        public float fluctuationAlpha;
        public float fluctuationScale;

        public PtclInstance Next;
        public PtclInstance Prev;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct PtclStripeQueue
    {
        public Vector3 pos;
        public float scale;
        public Matrix4 emitterMatrixSRT;
        public Vector3 outer;
        public Vector3 dir;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public class PtclStripe
    {
        public uint queueFront;
        public uint queueRear;

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 256)]
        public PtclStripeQueue[] queue;

        public uint queueCount;
        public uint groupID;
        public ComplexEmitterData data;
        public int counter;
        public Matrix4 emitterMatrixSRT;

        public PtclInstance particle;

        public Vector3 currentSliceDir;
        public Vector3 pos0;
        public Vector3 pos1;

        public PtclStripe Next;
        public PtclStripe Prev;

        public uint drawFirstVertex;
        public uint numDraw;
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

namespace EffectLibrary.Switch
{
    public class VFXB_Header : IResData, IFileHeader
    {
        /// <summary>
        /// Gets or sets the graphic api version.
        /// </summary>
        public ushort GraphicsAPIVersion { get; set; }

        /// <summary>
        /// Gets or sets the byte order mark
        /// </summary>
        public ushort ByteOrderMark { get; private set; }

        /// <summary>
        /// Gets or sets the alignment.
        /// </summary>
        public byte Alignment { get; set; }

        /// <summary>
        /// Gets or sets the target offset.
        /// </summary>
        public byte TargetOffset;

        /// <summary>
        /// Gets or sets the header flags.
        /// </summary>
        public ushort Flag { get; set; }

        /// <summary>
        /// Gets or sets the block offset.
        /// </summary>
        public ushort BlockOffset { get; set; }

        /// <summary>
        /// Gets or sets the data alignment.
        /// </summary>
        public uint DataAlignment { get; set; }

        public PrimitiveInfo PrimitiveInfo { get; set; }
        public TextureInfo TextureInfo { get; set; }
        public ShaderInfo ShaderInfo { get; set; }

        /// <summary>
        /// Gets or sets the file version.
        /// </summary>
        public uint Version { get; set; }

        public IList<IEmitterSet> EmitterSetList => EmitterSets.Cast<IEmitterSet>().ToList();
        public IList<IPrimitive> PrimitiveList => Primitives.Cast<IPrimitive>().ToList();
        public IList<IShaderResource> ShaderList => Shaders.Cast<IShaderResource>().ToList();

        public List<EmitterSet> EmitterSets = new List<EmitterSet>();
        public List<PrimitiveResource> Primitives = new List<PrimitiveResource>();
        public List<ShaderResource> Shaders = new List<ShaderResource>();

        public FileResourceFlags Flags { get; set; }

        public void Read(BinaryFileReader reader)
        {
            reader.CheckSignature("VFXB");
            uint padding = reader.ReadUInt32();
            GraphicsAPIVersion = reader.ReadUInt16();
            Version = reader.ReadUInt16();
            ByteOrderMark = reader.ReadUInt16();
            Alignment = reader.ReadByte();
            TargetOffset = reader.ReadByte();
            uint HeaderSize = reader.ReadUInt32();
            Flag = reader.ReadUInt16();
            BlockOffset = reader.ReadUInt16();
            uint padding2 = reader.ReadUInt32();
            uint FileSize = reader.ReadUInt32();

            reader.Seek(BlockOffset, SeekOrigin.Begin);
            while (reader.Position < reader.BaseStream.Length)
            {
                long pos = reader.Position;
                var section = SectionBase.Read(reader, this);

                reader.Seek(pos + 12, SeekOrigin.Begin);
                uint nextSectionOffset = reader.ReadUInt32();

                if (nextSectionOffset != uint.MaxValue)
                    reader.Seek(pos + nextSectionOffset, SeekOrigin.Begin);
                else
                    break;
            }
            UpdateResources();
        }

        public void Write(BinaryFileWriter writer)
        {

        }

        public void UpdateResources()
        {
            Primitives.Clear();
            Shaders.Clear();

            Flags |= FileResourceFlags.UseShaderIndex;
            Flags |= FileResourceFlags.HasAlpha1;
            Flags |= FileResourceFlags.HasGPUBehavior;
            Flags |= FileResourceFlags.HasNearFarAlpha;
            Flags |= FileResourceFlags.HasTexture3;

            foreach (var prim in this.PrimitiveInfo.GetPrimitives())
                Primitives.Add(new PrimitiveResource(prim));

            foreach (var shader in this.ShaderInfo.GetShaders())
                Shaders.Add(new ShaderResource(shader));

            foreach (var emitterSet in EmitterSets)
            {
                foreach (Emitter emitter in emitterSet.ChildSections)
                {
                    emitter.UpdatePrimitiveResources(this.PrimitiveInfo);
                    emitter.UpdateTextureResources(this.TextureInfo);
                }
            }
        }
    }
}

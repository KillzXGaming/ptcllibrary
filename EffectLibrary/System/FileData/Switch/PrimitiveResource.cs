﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Numerics;
using BfresLibrary;
using BfresLibrary.Helpers;

namespace EffectLibrary
{
    public class PrimitiveResource : IPrimitive
    {
        public uint GetElementCount() => Shape.Meshes[0].IndexCount;
        public uint GetVertexCount() => (uint)numVertex;

        public bool HasNormals => VertexBuffer.Attributes.ContainsKey("_n0");
        public bool HasColors => VertexBuffer.Attributes.ContainsKey("_c0");
        public bool HasTexCoords => VertexBuffer.Attributes.ContainsKey("_u0");

        public Vector4[] GetPositionData() { return GetBufferData("_p0"); }
        public Vector4[] GetNormalsData() { return GetBufferData("_n0"); }
        public Vector4[] GetColorData() { return GetBufferData("_c0"); }
        public Vector4[] GetTexCoordData() { return GetBufferData("_u0"); }

        int numVertex = 0;

        VertexBuffer VertexBuffer;
        Shape Shape;

        public PrimitiveResource(Model model)
        {
            //Assume only one mesh is possible.
            Shape = model.Shapes[0];
            VertexBuffer = model.VertexBuffers[Shape.VertexBufferIndex];

            var helper = new VertexBufferHelper(VertexBuffer, Syroot.BinaryData.ByteOrder.LittleEndian);
            numVertex = helper.Attributes[0].Data.Length;
        }

        private Vector4[] GetBufferData(string attribute)
        {
            if (!VertexBuffer.Attributes.ContainsKey(attribute))
                return new Vector4[0];

            var helper = new VertexBufferHelper(VertexBuffer, Syroot.BinaryData.ByteOrder.LittleEndian);
            var data = helper.Attributes.FirstOrDefault(x => x.Name == attribute).Data;

            Vector4[] buffer = new Vector4[data.Length];
            for (int i = 0; i < data.Length; i++)
                buffer[i] = new Vector4(data[i].X, data[i].Y, data[i].Z, data[i].W);
            return buffer;
        }

        public uint[] GetIndexBuffer() {
            return Shape.Meshes[0].GetIndices().ToArray();
        }
    }
}

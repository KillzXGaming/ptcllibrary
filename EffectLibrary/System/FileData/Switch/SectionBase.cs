﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Toolbox.Core.IO;

namespace EffectLibrary.Switch
{
    public class SectionBase  
    {
        public string Magic { get; set; }

        public uint Size { get; set; }

        public Stream BinaryData { get; set; }

        public List<SectionBase> ChildSections = new List<SectionBase>();

        public SectionBase ParentSection { get; set; }

        public VFXB_Header ParentHeader { get; set; }

        public static SectionBase Read(BinaryFileReader reader, VFXB_Header header, SectionBase parent = null)
        {
            SectionBase section = new SectionBase();
            long pos = reader.Position;
            string magic = reader.ReadString(4, Encoding.ASCII);
            uint size = reader.ReadUInt32();
            uint SubSectionOffset = reader.ReadUInt32();
            uint NextSectionOffset = reader.ReadUInt32();
            uint AttribteOffset = reader.ReadUInt32();
            uint BinaryDataOffset = reader.ReadUInt32();
            uint Padding = reader.ReadUInt32();
            uint SubSectionCount = reader.ReadUInt16();
            ushort unk = reader.ReadUInt16(); //0x0001

            //Init the attached section
            if (Sections.ContainsKey(magic)) {
               section = (SectionBase)Activator.CreateInstance(Sections[magic]);
            }

            //Apply section info to the created sections
            section.Magic = magic;
            section.Size = size;
            section.ParentSection = parent;
            section.ParentHeader = header;

            //Read the header data after the chunk section
            section.ReadSection(reader);

            //Read the binary data
            if (BinaryDataOffset != uint.MaxValue && section.Size != 0) {
                section.BinaryData = new SubStream(reader.BaseStream, pos + BinaryDataOffset, section.Size);

                using (var binaryReader = new BinaryFileReader(reader.PTCL, section.BinaryData, true)) {
                    section.ReadBinary(binaryReader);
                }
            }

            if (section is EmitterSet)
                header.EmitterSets.Add(section as EmitterSet);
            if (section is TextureInfo)
                header.TextureInfo = section as TextureInfo;
            if (section is PrimitiveInfo)
                header.PrimitiveInfo = section as PrimitiveInfo;
            if (section is ShaderInfo)
                header.ShaderInfo = section as ShaderInfo;

            //Seek the sub section if there is any
            if (SubSectionCount > 0)
            {
                reader.Seek(pos + SubSectionOffset, SeekOrigin.Begin);
                for (int i = 0; i < SubSectionCount; i++) {
                    var childSection = SectionBase.Read(reader, header, section);
                    section.ChildSections.Add(childSection);
                }
            }

            //Seek the next section if a valid pointer has been set
            if (NextSectionOffset != uint.MaxValue)
                reader.Seek(pos + NextSectionOffset, SeekOrigin.Begin);

            return section;
        }

        //Reads a section
        public virtual void ReadSection(BinaryFileReader reader)
        {
        }

        public virtual void WriteSection(BinaryFileWriter writer)
        {
        }

        public virtual void ReadBinary(BinaryFileReader reader)
        {
        }

        public virtual void WriteBinary(BinaryFileWriter writer)
        {
        }

        static Dictionary<string, Type> Sections = new Dictionary<string, Type>()
        {
            { "GRTF", typeof(TextureInfo) },
            { "G3PR", typeof(PrimitiveInfo) },
            { "GTNT", typeof(TextureDescTable) },
            { "G3NT", typeof(PrimitiveDescTable) },
            { "GRSN", typeof(ShaderInfo) },
            { "ESET", typeof(EmitterSet) },
            { "EMTR", typeof(Emitter) },
        };
    }
}

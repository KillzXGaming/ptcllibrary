﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Syroot.NintenTools.NSW.Bntx;

namespace EffectLibrary.Switch
{
    public class TextureResource : ITextureResource
    {
        public bool HasData => TextureData != null;

        public Toolbox.Core.STGenericTexture Handle { get; set; }

        public bool Initialized => Handle != null;

        public FilterMode FilterMode { get; set; }

        public WrapMode WrapX { get; set; }
        public WrapMode WrapY { get; set; }

        public float LODMax { get; set; }
        public float LODBias { get; set; }

        public Texture TextureData { get; set; }

        public TextureResource(Texture texture) {
            TextureData = texture;
        }

        public void CreateResourceHandle()
        {
            Handle = new BntxTexture(TextureData);
        }
    }
}

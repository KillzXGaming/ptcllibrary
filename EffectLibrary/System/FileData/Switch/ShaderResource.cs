﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BfshaLibrary;

namespace EffectLibrary
{
    public class ShaderResource : IShaderResource
    {
        public VertexShaderKey VertexShaderKey { get; set; }
        public FragmentShaderKey FragmentShaderKey { get; set; }

        ShaderVariation ShaderVariation;

        public ShaderResource(ShaderVariation variation) {
            ShaderVariation = variation;
        }

        public ShaderWrapperBase CreatePlatformWrapper()
        {
            return new NXShaderWrapper(ShaderVariation);
        }
    }
}

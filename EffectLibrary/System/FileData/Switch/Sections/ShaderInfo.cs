﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Threading.Tasks;
using BfshaLibrary;

namespace EffectLibrary.Switch
{
    public class ShaderInfo : SectionBase
    {
        public BnshFile BnshFile;

        public ShaderVariation GetShader(int index)
        {
            if (index == -1) return null;

            return BnshFile.ShaderVariations[index];
        }

        public ShaderVariation[] GetShaders()
        {
            return BnshFile.ShaderVariations;
        }

        public override void ReadBinary(BinaryFileReader reader) {
            BnshFile = new BnshFile(reader.BaseStream);
        }

        public override void WriteBinary(BinaryFileWriter writer)
        {
        }
    }
}

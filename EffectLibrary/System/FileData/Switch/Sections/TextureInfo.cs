﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Syroot.NintenTools.NSW.Bntx;

namespace EffectLibrary.Switch
{
    public class TextureInfo : SectionBase
    {
        BntxFile BntxFile { get; set; }

        public List<Texture> GetTextures()
        {
            return BntxFile.Textures.ToList();
        }

        public ulong GetID(string name)
        {
            var table = this.ChildSections[0] as TextureDescTable;
            if (table == null)
                throw new Exception("Failed to find primitive descriptor table!");

            var desc = table.Textures.FirstOrDefault(x => x.Name == name);
            if (desc == null)
                return 0;

            return desc.ID;
        }

        public Texture GetTexture(ulong id)
        {
            var table = this.ChildSections[0] as TextureDescTable;
            if (table == null)
                throw new Exception("Failed to find texture descriptor table!");

            var descTex = table.Textures.FirstOrDefault(x => x.ID == id);
            if (descTex != null) {
                return BntxFile.Textures.FirstOrDefault(x => x.Name == descTex.Name);
            }
            return null;
        }

        public override void ReadBinary(BinaryFileReader reader)
        {
            BntxFile = new BntxFile(reader.BaseStream);
        }

        public override void WriteBinary(BinaryFileWriter writer)
        {
            BntxFile.Save(writer.BaseStream);
        }
    }
}

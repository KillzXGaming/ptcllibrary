﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Syroot.BinaryData;
using Toolbox.Core;

namespace EffectLibrary.Switch
{
    public class Emitter : SectionBase
    {
        public SimpleEmitterData EmitterData { get; set; }

        public bool IsVisible { get; set; } = true;

        public ulong PrimitveID { get; set; }

        public MeshType MeshType = MeshType.Particle;

        public EmitterSampler[] Samplers = new EmitterSampler[3];

        public List<Emitter> GetEmitters()
        {
            List<Emitter> emitters = new List<Emitter>();
            foreach (var child in ChildSections)
                emitters.Add(child as Emitter);
            return emitters;
        }

        public BfresLibrary.Model GetPrimitive() {
            return this.ParentHeader.PrimitiveInfo.GetPrimitive(PrimitveID);
        }

        public override void ReadBinary(BinaryFileReader reader)
        {
            long pos = reader.Position;

            EmitterData = new SimpleEmitterData(reader);
            EmitterData.Textures = new TextureResource[3];
            EmitterData.TextureEmitters = new TextureEmitter[3];

            reader.ReadBytes(16); //padding
            EmitterData.Name = reader.ReadStringFixed(64);
            reader.ReadBytes(1792);
            reader.ReadBytes(344);

            reader.ReadBytes(32);
            PrimitveID = reader.ReadUInt64();

            if (PrimitveID != ulong.MaxValue)
                MeshType = MeshType.Primitive;

            reader.SeekBegin(pos + 2036);
            EmitterData.EmissionStartFrame = reader.ReadUInt32();
            reader.ReadUInt32();
            EmitterData.EmissionEndFrame = reader.ReadUInt32();
            EmitterData.EmissionRatio = reader.ReadSingle();
            EmitterData.EmissionRatioRandomness = reader.ReadUInt32();
            EmitterData.EmissionInterval = reader.ReadUInt32();
            EmitterData.EmissionIntervalRandomness = reader.ReadInt32();
            EmitterData.PositionRandomizer = reader.ReadSingle();

            reader.SeekBegin(pos + 1904);
            EmitterData.BaseTranslation = new OpenTK.Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
            EmitterData.TranslationRandom = new OpenTK.Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
            EmitterData.BaseRotation = new OpenTK.Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
            EmitterData.RotationRandom = new OpenTK.Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
            EmitterData.BaseScale = new OpenTK.Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
            EmitterData.Color0 = new OpenTK.Vector4(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
            EmitterData.Color1 = new OpenTK.Vector4(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());

            reader.SeekBegin(pos + 2216);
            bool isInfinite = reader.ReadBoolean();
            if (isInfinite)
                EmitterData.MaxLifespan = 0x7FFFFFFF;

            reader.SeekBegin(pos + 2412);
            EmitterData.AllDirectionVelocity = reader.ReadSingle();
            EmitterData.DirectionVelocity = reader.ReadSingle();
            EmitterData.Direction = new OpenTK.Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
            EmitterData.DispersionAngle = reader.ReadSingle();
            EmitterData.ParticleAddXZVelocity = reader.ReadSingle();
            EmitterData.DiffusionVel = new OpenTK.Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
            EmitterData.DirectionVelocityRandom = reader.ReadSingle();

            reader.SeekBegin(pos + 2472); //Constant colors

            reader.SeekBegin(pos + 2324);
            EmitterData.ShaderIndex1 = reader.ReadInt32();

            reader.SeekBegin(pos + 2552);

            for (int i = 0; i < 3; i++)
            {
                EmitterData.TextureEmitters[i] = new TextureEmitter();

                EmitterSampler samplerInfo = new EmitterSampler();
                samplerInfo.Read(reader);
                Samplers[i] = samplerInfo;
            }
        }

        public void UpdatePrimitiveResources(PrimitiveInfo info)
        {
            EmitterData.PrimitiveIndex = info.GetPrimitiveIndex(PrimitveID);
        }

        public void UpdateTextureResources(TextureInfo info)
        {
            for (int i = 0; i < 3; i++)
            {
                var sampler = Samplers[i];
                var texture = info.GetTexture(sampler.TextureID);
                EmitterData.Textures[i] = new TextureResource(texture);
            }
        }

        public override void WriteBinary(BinaryFileWriter writer)
        {
            writer.Write(new byte[16]); //padding
            writer.Write(EmitterData.Name, BinaryStringFormat.ZeroTerminated);
        }
    }
}

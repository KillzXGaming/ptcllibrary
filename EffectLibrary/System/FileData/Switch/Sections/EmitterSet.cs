﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Syroot.BinaryData;
using OpenTK;

namespace EffectLibrary.Switch
{
    public class EmitterSet : SectionBase, IEmitterSet
    {
        //Instance used for spawning emitter set data
        public EffectLibrary.EmitterSet EmitterSetHandle { get; set; } = null;

        public Matrix4 MatrixSRT { get; set; } = Matrix4.Identity;

        public string Name { get; set; }

        public bool IsVisible { get; set; }

        public uint UserData { get; set; }

        public IEnumerable<IEmitter> EmitterList => this.GetEmitters();

        public List<SimpleEmitterData> GetEmitters()
        {
            List<SimpleEmitterData> emitters = new List<SimpleEmitterData>();
            foreach (Emitter child in ChildSections)
                emitters.Add((SimpleEmitterData)child.EmitterData);
            return emitters;
        }

        public override void ReadSection(BinaryFileReader reader)
        {
            reader.ReadBytes(16); //padding
            Name = reader.ReadStringFixed(64);
        }

        public override void WriteSection(BinaryFileWriter writer)
        {
            writer.Write(new byte[16]); //padding
            writer.Write(Name);
        }
    }
}

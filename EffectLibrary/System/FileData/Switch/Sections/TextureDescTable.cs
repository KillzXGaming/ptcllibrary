﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EffectLibrary.Switch
{
    public class TextureDescTable : SectionBase
    {
        public List<TextureDesc> Textures = new List<TextureDesc>();

        public override void ReadBinary(BinaryFileReader reader)
        {
            while (reader.BaseStream.Length > reader.Position)
            {
                long pos = reader.Position;

                ulong id = reader.ReadUInt64();
                uint nextDescOffset = reader.ReadUInt32();
                uint nameLength = reader.ReadUInt32();

                Textures.Add(new TextureDesc()
                {
                    ID = id,
                    Name = reader.ReadStringFixed((int)nameLength),
                });

                if (nextDescOffset == 0)
                    break;

                reader.SeekBegin(pos + nextDescOffset);
            }
        }

        public override void WriteSection(BinaryFileWriter writer)
        {

        }

        public class TextureDesc
        {
            public ulong ID { get; set; }
            public string Name { get; set; }
        }
    }
}

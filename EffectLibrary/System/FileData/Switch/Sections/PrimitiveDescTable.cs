﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EffectLibrary.Switch
{
    public class PrimitiveDescTable : SectionBase
    {
        public List<PrimitiveDesc> Primitives = new List<PrimitiveDesc>();

        public override void ReadBinary(BinaryFileReader reader)
        {
            while (reader.BaseStream.Length > reader.Position)
            {
                long pos = reader.Position;

                ulong id = reader.ReadUInt64();
                uint nextDescOffset = reader.ReadUInt32();
                uint size = reader.ReadUInt32();

                Primitives.Add(new PrimitiveDesc()
                {
                    ID = id,
                    Unknown = reader.ReadBytes((int)size),
                });

                if (nextDescOffset == 0)
                    break;

                reader.SeekBegin(pos + nextDescOffset);
            }
        }

        public override void WriteSection(BinaryFileWriter writer)
        {

        }
    }

    public class PrimitiveDesc
    {
        public ulong ID { get; set; }
        public byte[] Unknown { get; set; }
    }
}

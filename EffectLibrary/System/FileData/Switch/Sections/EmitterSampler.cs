﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Threading.Tasks;

namespace EffectLibrary.Switch
{
    public class EmitterSampler
    {
        public ulong TextureID;

        public WrapMode WrapU = WrapMode.Mirror;
        public WrapMode WrapV = WrapMode.Mirror;

        public float MaxLOD = 15.0f;
        public float LODBias = 0.0f;

        public void Read(BinaryFileReader reader)
        {
            TextureID = reader.ReadUInt64();
            WrapU = (WrapMode)reader.ReadByte();
            WrapV = (WrapMode)reader.ReadByte();
            reader.ReadByte();
            reader.ReadByte();
            MaxLOD = reader.ReadSingle();
            LODBias = reader.ReadSingle();
            reader.Seek(12, SeekOrigin.Current);
        }
    }
}

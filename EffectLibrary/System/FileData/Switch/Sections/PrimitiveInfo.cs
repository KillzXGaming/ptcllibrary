﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BfresLibrary;

namespace EffectLibrary.Switch
{
    public class PrimitiveInfo : SectionBase
    {
        ResFile ResFile { get; set; }

        public List<Model> GetPrimitives() => ResFile.Models.Values.ToList();

        public ulong GetID(int index)
        {
            var table = this.ChildSections[0] as PrimitiveDescTable;
            if (table == null)
                throw new Exception("Failed to find primitive descriptor table!");

            return table.Primitives[index].ID;
        }

        public Model GetPrimitive(ulong id)
        {
            var table = this.ChildSections[0] as PrimitiveDescTable;
            if (table == null)
                throw new Exception("Failed to find primitive descriptor table!");

            var descPrimIndex = table.Primitives.FindIndex(x => x.ID == id);
            if (descPrimIndex != -1) {
                return ResFile.Models[descPrimIndex];
            }
            return null;
        }

        public int GetPrimitiveIndex(ulong id)
        {
            var table = this.ChildSections[0] as PrimitiveDescTable;
            if (table == null)
                return -1;

            return table.Primitives.FindIndex(x => x.ID == id);
        }

        public override void ReadBinary(BinaryFileReader reader)
        {
            ResFile = new ResFile(reader.BaseStream);
        }

        public override void WriteBinary(BinaryFileWriter writer)
        {
            ResFile.Save(writer.BaseStream);
        }
    }
}

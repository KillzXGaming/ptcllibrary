﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Threading.Tasks;
using Toolbox.Core.IO;

namespace EffectLibrary.WiiU
{
    //https://github.com/kinnay/Nintendo-File-Formats/wiki/PTCL-File-Format
    public class PTCL_Header : IFileHeader, IResData
    {
        public uint Version { get; set; }

        public IList<IEmitterSet> EmitterSetList => EmitterSets.Cast<IEmitterSet>().ToList();
        public IList<IPrimitive> PrimitiveList => Primitives.Cast<IPrimitive>().ToList();
        public IList<IShaderResource> ShaderList => Shaders.Cast<IShaderResource>().ToList();

        public List<EmitterSet> EmitterSets = new List<EmitterSet>();
        public List<Primitive> Primitives = new List<Primitive>();
        public List<ShaderInfo> Shaders = new List<ShaderInfo>();

        public FileResourceFlags Flags { get; set; }

        /// <summary>
        /// Gets the string table position for reading a string value.
        /// </summary>
        internal uint StringTableOffset { get; private set; }
        internal uint KeyTableOffset { get; private set; }
        internal uint ParamTableOffset { get; private set; }
        internal uint TextureDataTableOffset { get; private set; }

        public void Read(BinaryFileReader reader)
        {
            reader.PTCL.FileHeader = this;

            reader.CheckSignature("EFTF", "SPBD");
            Version = reader.ReadUInt32();
            uint numEffects = reader.ReadUInt32();
            reader.ReadUInt32(); //padding
            StringTableOffset = reader.ReadUInt32();
            TextureDataTableOffset = reader.ReadUInt32();
            uint textureDataTblSize = reader.ReadUInt32();
            uint shaderTblOffset = reader.ReadUInt32();
            uint shaderTblSize = reader.ReadUInt32();
            KeyTableOffset = reader.ReadUInt32();
            uint keyframeTblSize = reader.ReadUInt32();
            uint primitiveTblOffset = reader.ReadUInt32();
            uint primitiveTblOSize = reader.ReadUInt32();
            ParamTableOffset = reader.ReadUInt32();
            uint unkSize = reader.ReadUInt32();
            reader.ReadUInt32();
            reader.ReadUInt32();
            reader.ReadUInt32();

            if (Version > 55)
            {
                Flags |= FileResourceFlags.HasAlpha1;
                Flags |= FileResourceFlags.HasTexture3;
                Flags |= FileResourceFlags.HasGPUBehavior;
                Flags |= FileResourceFlags.UseShaderIndex;
                Flags |= FileResourceFlags.HasNearFarAlpha;
            }

            if (Version < 55)
                reader.Seek(0x40, SeekOrigin.Begin);

            for (int i = 0; i < numEffects; i++) {
                var emitterSet = new EmitterSet();
                emitterSet.Read(this, reader);
                EmitterSets.Add(emitterSet);
            }

            reader.SeekBegin(primitiveTblOffset);
            ReadPrimitives(reader);

            reader.SeekBegin(shaderTblOffset);
            ReadShaders(reader);
        }

        public EmitterSet GetEmitterSet(string name)
        {
            for (int i = 0; i < EmitterSets.Count; i++)
                if (EmitterSets[i].Name == name)
                    return (EmitterSet)EmitterSets[i];

            return null;
        }

        private void ReadShaders(BinaryFileReader reader)
        {
            var pos = reader.Position;

            uint numShaders = reader.ReadUInt32();
            uint sectionSize = reader.ReadUInt32();
            if (reader.PTCL.FileHeader.Version < 55)
            {
                uint srcInfoOffset = reader.ReadUInt32();
                uint binaryInfoOffset = reader.ReadUInt32();

                reader.SeekBegin(pos + binaryInfoOffset);
                long fileOffset = binaryInfoOffset + (numShaders * 92);
                Shaders = reader.ReadList<ShaderInfo>((int)numShaders);

                for (int i = 0; i < Shaders.Count; i++)
                {
                    Shaders[i].Data = new SubStream(reader.BaseStream,
                        pos + fileOffset + Shaders[i].ShaderOffset, Shaders[i].ShaderSize);

                    Shaders[i].GFDFile = new GFD(Shaders[i].Data);
                }
            }
            else
            {
                reader.ReadUInt32();
                reader.ReadUInt32();
                uint fileOffset = reader.ReadUInt32();
                uint infoOffset = reader.ReadUInt32();

                reader.SeekBegin(pos + infoOffset);
                Shaders = reader.ReadList<ShaderInfo>((int)numShaders);

                for (int i = 0; i < Shaders.Count; i++)
                {
                    Shaders[i].Data = new SubStream(reader.BaseStream,
                        pos + fileOffset + Shaders[i].ShaderOffset, Shaders[i].ShaderSize);

                    Shaders[i].GFDFile = new GFD(Shaders[i].Data);
                }
            }
        }

        private void ReadPrimitives(BinaryFileReader reader)
        {
            var pos = reader.Position;

            uint numPrimitives = reader.ReadUInt32();
            uint sectionSize = reader.ReadUInt32();
            uint primitivesOffset = reader.ReadUInt32();

            reader.SeekBegin(pos + primitivesOffset);
            Primitives = reader.ReadList<Primitive>((int)numPrimitives);
            uint vertexBufferOffset = (uint)reader.Position;

            //Setup the primitive pool of data
            for (int i = 0; i < numPrimitives; i++)
            {
                Primitives[i].PositionsInfo.ReadBuffer(reader, vertexBufferOffset, 0);
                Primitives[i].NormalsInfo.ReadBuffer(reader, vertexBufferOffset,   1);
                Primitives[i].ColorsInfo.ReadBuffer(reader, vertexBufferOffset,    2);
                Primitives[i].TexCoordInfo.ReadBuffer(reader, vertexBufferOffset,  3);
                Primitives[i].IndexInfo.ReadBuffer(reader, vertexBufferOffset,     4);
            }
        }

        public void Write(BinaryFileWriter writer)
        {
            writer.WriteSignature("EFTF");
            writer.Write(EmitterSets.Count);
            //Skip header as offsets/sizes are resolved later
            writer.Seek(72, SeekOrigin.Begin);
            foreach (var emitterSet in EmitterSets)
                emitterSet.Write(writer);
            writer.Align(4096);
            //Write all the texture data
            long textureStart = writer.Position;
            foreach (var textureData in GetTextureData())
            {
                writer.Align(4096);
                writer.Write(textureData);
            }
            writer.Align(8196);
            long textureSize = writer.Position - textureStart;

            //Write string table
            writer.WriteOffset(16);
            writer.WriteStringTable();
        }

        private List<byte[]> GetTextureData()
        {
            Dictionary<uint, byte[]> textures = new Dictionary<uint, byte[]>();
            foreach (var emitterSet in EmitterSets) {
                foreach (var emitter in emitterSet.Emitters)
                {
                    CheckImageData((TextureRes)emitter.Textures[0], textures);
                    CheckImageData((TextureRes)emitter.Textures[1], textures);
                    CheckImageData((TextureRes)emitter.Textures[2], textures);
                }
            }
            return textures.Values.ToList();
        }

        private void CheckImageData(TextureRes texture, Dictionary<uint, byte[]> list)
        {
            if (texture.Width == 0)
                return;

            if (!list.ContainsKey(texture.DataPosition) && texture.Data.Length > 0) {
                list.Add(texture.DataPosition, texture.Data.ToArray());

                Console.WriteLine($"pos {texture.DataPosition} {texture.Size}");
            }
        }

        private void WritePrimitives(BinaryFileWriter writer) {
            writer.WriteList(Primitives);
            long startPos = writer.Position;
            for (int i = 0; i < Primitives.Count; i++)
            {
                Primitives[i].PositionsInfo.WriteBuffer(writer, startPos);
                Primitives[i].NormalsInfo.WriteBuffer(writer, startPos);
                Primitives[i].ColorsInfo.WriteBuffer(writer, startPos);
                Primitives[i].TexCoordInfo.WriteBuffer(writer, startPos);
                Primitives[i].IndexInfo.WriteBuffer(writer, startPos);
            }
        }

        public KeyTable ReadKeyTable(BinaryFileReader reader, uint offset)
        {
            using (reader.TemporarySeek(KeyTableOffset + offset, SeekOrigin.Begin)) {
                return reader.ReadSection<KeyTable>();
            }
        }

        public float[] ReadParameters(BinaryFileReader reader, uint offset, uint count)
        {
            using (reader.TemporarySeek(ParamTableOffset + offset, SeekOrigin.Begin)) {
                return reader.ReadSingles((int)count);
            }
        }

        public string ReadStringTable(BinaryFileReader reader) {
            uint offset = reader.ReadUInt32();

            using (reader.TemporarySeek(StringTableOffset + offset, SeekOrigin.Begin)) {
                return reader.ReadString(Syroot.BinaryData.BinaryStringFormat.ZeroTerminated);
            }
        }
    }
}

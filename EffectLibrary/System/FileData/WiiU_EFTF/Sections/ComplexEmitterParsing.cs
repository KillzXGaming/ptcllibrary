﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EffectLibrary
{
    public partial class ComplexEmitterData
    {
        public void ReadComplexDataWiiU(long startPosition, BinaryFileReader reader)
        {
            childFlags = reader.ReadUInt32();
            fieldFlags = reader.ReadUInt16();
            fluctuationFlags = reader.ReadUInt16();
            stripeFlags = reader.ReadUInt16();
            _6FE = reader.ReadBytes(2);
            ushort childDataOffs = reader.ReadUInt16();
            ushort fieldDataOffs = reader.ReadUInt16();
            ushort fluctuationDataOffs = reader.ReadUInt16();
            ushort stripeDataOffs = reader.ReadUInt16();
            DataSize = reader.ReadUInt32();

            if ((childFlags & (int)ChildFlags.HasChild) != 0) {
                reader.SeekBegin(startPosition + childDataOffs);

                ChildData = new ChildData();
                ChildData.ReadWiiU(reader);
            }
            if (fieldFlags != 0)
            {
                reader.SeekBegin(startPosition + fieldDataOffs);

                if ((fieldFlags & 0x02) != 0) FieldData.Add(new FieldMagnetData());
                if ((fieldFlags & 0x04) != 0) FieldData.Add(new FieldSpinData());
                if ((fieldFlags & 0x08) != 0) FieldData.Add(new FieldCollisionData());
                if ((fieldFlags & 0x10) != 0) FieldData.Add(new FieldConvergenceData());
                if ((fieldFlags & 0x20) != 0) FieldData.Add(new FieldPosAddData());
                if ((fieldFlags & 0x40) != 0) FieldData.Add(new FieldCurlNoiseData());

                foreach (var field in FieldData)
                    field.ReadWiiU(reader);
            }
            if (fluctuationFlags != 0 && fluctuationDataOffs != 0)
            {
                reader.SeekBegin(startPosition + fluctuationDataOffs);
                FluctuationData = new FluctuationData();
                FluctuationData.ReadWiiU(reader);
            }
            if (VertexTransformMode == VertexTransformMode.Stripe ||
                VertexTransformMode == VertexTransformMode.ComplexStripe)
            {
                reader.SeekBegin(startPosition + stripeDataOffs);
                StripeData = new StripeData();
                StripeData.ReadWiiU(reader);
            }
        }
    }
}

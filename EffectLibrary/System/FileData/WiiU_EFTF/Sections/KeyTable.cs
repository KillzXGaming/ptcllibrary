﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Syroot.BinaryData;

namespace EffectLibrary.WiiU
{
    public class KeyTable : IResData
    {
        /// <summary>
        /// Gets or sets a list of groups used for the key animation.
        /// </summary>
        public List<KeyGroup> KeyGroups { get; set; } = new List<KeyGroup>();

        public void Read(BinaryFileReader reader)
        {
            reader.CheckSignature("KEYA");
            uint numGroups = reader.ReadUInt32();
            KeyGroups = reader.ReadList<KeyGroup>(numGroups);
        }

        public void Write(BinaryFileWriter writer)
        {
            writer.WriteSignature("KEYA");
            writer.Write(KeyGroups.Count);
            writer.WriteList(KeyGroups);
        }
    }

    /// <summary>
    /// Represents a key group for an animated channel type.
    /// </summary>
    public class KeyGroup : IResData
    {
        /// <summary>
        /// Gets or sets the key frames in the key group.
        /// </summary>
        public KeyFrame[] KeyFrames { get; set; }

        /// <summary>
        /// Gets or sets the group type.
        /// </summary>
        public AnimGroupType Type { get; set; }

        /// <summary>
        /// Gets or sets the interpolation type. 
        /// </summary>
        public uint Interpolation { get; set; }

        public uint LoopFlags { get; set; }
        public uint FrameCount { get; set; }
        public uint Unknown3 { get; set; }

        public void Read(BinaryFileReader reader)
        {
            uint numKeys = reader.ReadUInt32();
            Interpolation = reader.ReadUInt32();
            Type = (AnimGroupType)reader.ReadUInt32();
            LoopFlags = reader.ReadUInt32();
            FrameCount = reader.ReadUInt32(); //Frame count? Sometimes higher or lower than given frames
            if (reader.PTCL.FileHeader.Version >= 55)
                Unknown3 = reader.ReadUInt32(); //0 or 1
            uint sectionSize = reader.ReadUInt32();

            KeyFrames = new KeyFrame[numKeys];
            for (int i = 0; i < numKeys; i++)
            {
                KeyFrames[i] = new KeyFrame()
                {
                    Frame = reader.ReadSingle(),
                    Value = reader.ReadSingle(),
                };
            }
        }

        public void InsertKey(float frame, float value)
        {
            var keys = KeyFrames.ToList();
            keys.Add(new KeyFrame()
            {
                Frame = frame,
                Value = value,
            });
            KeyFrames = keys.ToArray();
        }

        public void Write(BinaryFileWriter writer)
        {
            writer.Write(KeyFrames.Length);
            writer.Write(Interpolation);
            writer.Write((uint)Type);
            writer.Write(LoopFlags);
            writer.Write(FrameCount);
            if (writer.PTCL.FileHeader.Version >= 55)
                writer.Write(Unknown3);
            //Calculate section size
            writer.Write(KeyFrames.Length * 8 + 28);
            for (int i = 0; i < KeyFrames.Length; i++) {
                writer.Write(KeyFrames[i].Frame);
                writer.Write(KeyFrames[i].Value);
            }
        }
    }

    public class KeyFrame
    {
        public float Frame { get; set; }
        public float Value { get; set; }
    }
}

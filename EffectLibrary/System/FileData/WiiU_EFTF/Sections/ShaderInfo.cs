﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Threading.Tasks;
using Syroot.BinaryData;

namespace EffectLibrary.WiiU
{
    public class ShaderInfo : IResData, IShaderResource
    {
        public VertexShaderKey VertexShaderKey { get; set; }
        public FragmentShaderKey FragmentShaderKey { get; set; }
        public uint Unknown { get; set; }
        public uint ShaderOffset { get; set; }
        public uint ShaderSize { get; set; }
        public uint Unknown2 { get; set; }
        public uint Unknown3 { get; set; }

        public GFD GFDFile { get; set; }

        public Stream Data { get; set; }

        public void Read(BinaryFileReader reader)
        {
            if (reader.PTCL.FileHeader.Version < 55)
            {
                VertexShaderKey = reader.ReadSection<VertexShaderKey>();
                FragmentShaderKey = reader.ReadSection<FragmentShaderKey>();
                Unknown = reader.ReadUInt32();
                ShaderSize = reader.ReadUInt32();
                ShaderOffset = reader.ReadUInt32();
            }
            else
            {
                VertexShaderKey = reader.ReadSection<VertexShaderKey>();
                reader.ReadBytes(8);
                FragmentShaderKey = reader.ReadSection<FragmentShaderKey>();
                reader.ReadBytes(12);

                Unknown = reader.ReadUInt32();
                ShaderSize = reader.ReadUInt32();
                ShaderOffset = reader.ReadUInt32();
                Unknown2 = reader.ReadUInt32();
                Unknown3 = reader.ReadUInt32();
            }
        }

        public void Write(BinaryFileWriter writer)
        {
            writer.WriteSection(VertexShaderKey);
            writer.WriteSection(FragmentShaderKey);
            writer.Write(Unknown);
            writer.Write(ShaderSize);
            writer.Write(ShaderOffset);
            writer.Write(Unknown2);
            writer.Write(Unknown3);
        }

        public ShaderWrapperBase CreatePlatformWrapper()
        {
            return new WiiUShaderWrapper(GFDFile);
        }
    }
}

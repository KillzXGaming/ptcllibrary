﻿using System;
using System.Collections.Generic;
using System.Linq;
using OpenTK;
using System.Threading.Tasks;
using Toolbox.Core;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json;
using EffectLibrary.WiiU;

namespace EffectLibrary
{
    public partial class SimpleEmitterData
    {
        public void ReadWiiU(BinaryFileReader reader)
        {
            StartPosition = reader.Position - 4;

            Flags = reader.ReadUInt32();
            RandomSeed = reader.ReadUInt32();
            reader.ReadUInt32();
            reader.ReadUInt32();
            reader.ReadSingles(8);
            CustomCallbackID = reader.ReadUInt32();
            Name = reader.ReadStringTable();
            reader.ReadUInt32(); //Name ptr set at runtime
            Textures[0] = reader.ReadSection<TextureRes>();
            Textures[1] = reader.ReadSection<TextureRes>();
            if (reader.PTCL.FileHeader.Version >= 55)
                Textures[2] = reader.ReadSection<TextureRes>();
            reader.ReadUInt32(); //Key table ptr set runtime
            uint keyframeTblOffset = reader.ReadUInt32();
            uint keyframeTblSize = reader.ReadUInt32();
            reader.ReadBytes(8); //unk
            PrimitiveIndex = reader.ReadInt32();
            if (reader.PTCL.FileHeader.Version >= 55)
            {
                reader.ReadBytes(8); //unk
                PrimitiveIndex2 = reader.ReadInt32();

                uint numParameters = reader.ReadUInt32();
                uint parameterOffset = reader.ReadUInt32();
                reader.ReadUInt32(); //params ptr set runtime

                if (numParameters != 0)
                    Parameters = ((PTCL_Header)reader.PTCL.FileHeader).ReadParameters(reader, parameterOffset, numParameters);

                TransformGravity = reader.ReadBoolean();
                NoEmissionDuringFade = reader.ReadBoolean();
                DisplayParent = reader.ReadBoolean();
                EquidistantParticleEmission = reader.ReadBoolean();
                reader.ReadBytes(6); //unk
                SphereDivTblIdx = reader.ReadByte();
                RandomArcStartAngle = reader.ReadBoolean();
                reader.ReadBytes(8); //unk

                ParticleRotationMode = (VertexRotationMode)reader.ReadUInt32();
                PtclFollowType = (PtclFollowType)reader.ReadUInt32();
                ColorMode = (ColorMode)reader.ReadUInt32();
                AlphaMode = (AlphaMode)reader.ReadUInt32();
                reader.ReadUInt32();
                DisplaySideType = (DisplayFaceType)reader.ReadUInt32();
                BlendType = (BlendType)reader.ReadUInt32();
                ZbufferTestType = (DepthType)reader.ReadUInt32();
                AnimScaleFunc = (AnimationFunctions)reader.ReadUInt32();
                AnimColor0Func = (AnimationFunctions)reader.ReadUInt32();
                AnimColor1Func = (AnimationFunctions)reader.ReadUInt32();
                AnimAlpha0Func = (AnimationFunctions)reader.ReadUInt32();
                AnimAlpha1Func = (AnimationFunctions)reader.ReadUInt32();
                EmitterFunc = (EmitterFunctions)reader.ReadUInt32();
                reader.ReadUInt32();
                VolumeScale = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
                ArcStartAngle = reader.ReadSingle();
                ArcLength = reader.ReadSingle();
                ArcRandomness = reader.ReadSingle();
                VolumeFillRatio = reader.ReadSingle();
                SphereLatitude = reader.ReadSingle();
                SphereLatitudeDir = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
                LineCenterPosition = reader.ReadSingle();
                EmissionShapeScale = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
                Color0 = new Vector4(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
                Color1 = new Vector4(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
                EmitterAlpha = reader.ReadSingle();
                reader.ReadBytes(96); //unk

                BaseScale = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
                BaseRotation = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
                BaseTranslation = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
                RotationRandom = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
                TranslationRandom = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());

                EmissionRatio = reader.ReadSingle();
                EmissionRatioRandomness = reader.ReadUInt32();
                EmissionStartFrame = reader.ReadUInt32();
                EmissionEndFrame = reader.ReadUInt32();
                EmissionInterval = reader.ReadUInt32(); //Emission time in frames
                EmissionIntervalRandomness = reader.ReadInt32(); //Emission time in frames
                AllDirectionVelocity = reader.ReadSingle();
                DirectionVelocityRandom = reader.ReadSingle();
                DirectionVelocity = reader.ReadSingle();
                Direction = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
                DispersionAngle = reader.ReadSingle();
                DiffusionVel = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
                AirResist = reader.ReadSingle();
                ParticleAddXZVelocity = reader.ReadSingle();
                PositionRandomizer = reader.ReadSingle();
                reader.ReadBytes(20); //unk
                Gravity = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
                MaxLifespan = reader.ReadUInt32();
                LifespanRandomizer = reader.ReadUInt32();
                MeshType = (MeshType)reader.ReadUInt32();
                VertexTransformMode = (VertexTransformMode)reader.ReadUInt32();
                BaseSpeedRandomness = reader.ReadSingle();
                RotationPivot = new Vector2(reader.ReadSingle(), reader.ReadSingle());
                reader.ReadUInt32();
                CameraOffset = reader.ReadSingle();
                TextureEmitters[0] = reader.ReadSection<TextureEmitter>();
                TextureEmitters[1] = reader.ReadSection<TextureEmitter>();
                TextureEmitters[2] = reader.ReadSection<TextureEmitter>();
                Color0Source = (ColorSource)reader.ReadUInt32(); 
                Color1Source = (ColorSource)reader.ReadUInt32();

                ColorKeys = new Vector4[2, 8];
                for (int i = 0; i < 8; i++)
                {
                    ColorKeys[0, i] = new Vector4(
                         reader.ReadSingle(), reader.ReadSingle(),
                         reader.ReadSingle(), reader.ReadSingle());
                }
                for (int i = 0; i < 8; i++)
                {
                    ColorKeys[1, i] = new Vector4(
                         reader.ReadSingle(), reader.ReadSingle(),
                         reader.ReadSingle(), reader.ReadSingle());
                }

                ColorKey4Time2[0] = reader.ReadInt32();
                ColorKey4Time2[1] = reader.ReadInt32();
                ColorKey4Time3[0] = reader.ReadInt32();
                ColorKey4Time3[1] = reader.ReadInt32();
                ColorKey4Time4[0] = reader.ReadInt32();
                ColorKey4Time4[1] = reader.ReadInt32();

                ColorRepeatCount[0] = reader.ReadUInt32();
                ColorRepeatCount[1] = reader.ReadUInt32();
                ColorRandomCount[0] = reader.ReadUInt32();
                ColorRandomCount[1] = reader.ReadUInt32();
                ColorScale = reader.ReadSingle();
                reader.ReadBytes(8); //unk

                for (int i = 0; i < 2; i++)
                {
                    AlphaSection alphaSection = i == 0 ? AlphaKey0 : AlphaKey1;
                    alphaSection.StartValue = reader.ReadSingle();
                    alphaSection.StartDifference = reader.ReadSingle();
                    alphaSection.EndDifference = reader.ReadSingle();
                    alphaSection.Time2 = (int)(reader.ReadSingle() * 100);
                    alphaSection.Time3 = (int)(reader.ReadSingle() * 100);
                }

                FragmentTextureColorBlendType = reader.ReadUInt32();
                reader.ReadUInt32();
                FragmentPrimitiveColorBlendType = reader.ReadUInt32();
                FragmentTextureAlphaBlendType = reader.ReadUInt32();
                reader.ReadUInt32();
                FragmentPrimitiveAlphaBlendType = reader.ReadUInt32();
                ScaleTime2 = reader.ReadInt32();
                ScaleTime3 = reader.ReadInt32();
                PtclScaleRandom =  new Vector2(reader.ReadSingle(), reader.ReadSingle());
                PtclScale = new Vector2(reader.ReadSingle(), reader.ReadSingle());
                PtclScaleStart = new Vector2(reader.ReadSingle(), reader.ReadSingle());
                PtclScaleStartDiff = new Vector2(reader.ReadSingle(), reader.ReadSingle());
                PtclScaleEndDiff = new Vector2(reader.ReadSingle(), reader.ReadSingle());
                _5DC = reader.ReadSingle();
                PtclRotate = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
                PtclRotateRandom = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
                AngularVelocity = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
                AngularVelocityRandom = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
                RotationInertia = reader.ReadSingle();
                AlphaFadeStep = reader.ReadSingle();
                FragmentShaderMode = (FragmentShaderMode)reader.ReadByte();
                ShaderUserSetting = reader.ReadByte();
                FragmentSoftEdge = reader.ReadByte();
                FragmentReflectionAlpha = reader.ReadByte();
                reader.ReadBytes(8);
                ShaderParam0 = reader.ReadSingle();
                ShaderParam1 = reader.ReadSingle();
                ShaderIndex1 = reader.ReadInt32();
                ShaderIndex2 = reader.ReadInt32();
                ShaderIndex3 = reader.ReadInt32();
                reader.ReadInt32();
                FragmentSoftEdgeFadeDistance = reader.ReadSingle();
                float unk1 = reader.ReadSingle();
                FragmentSoftEdgeVolume = reader.ReadSingle();
                FresnelAlphaMin = reader.ReadSingle();
                FresnelAlphaMax = reader.ReadSingle();
                float unk4 = reader.ReadSingle();
                NearAlphaParameters = new Vector4(
                    reader.ReadSingle(), reader.ReadSingle(),
                    reader.ReadSingle(), reader.ReadSingle());
                UserMacros1 = reader.ReadBytes(16);
                UserMacros2 = reader.ReadBytes(16);
                ShaderUserFlag = reader.ReadUInt32();
                ShaderUserSwitchFlag = reader.ReadUInt32();
                reader.ReadBytes(24);
/*
                if (reader.PTCL.FileHeader.Version >= 55)
                    reader.ReadBytes(24);
                else
                    reader.ReadBytes(16);*/
            }
            else
            {
                reader.ReadBytes(3); //unk
                TransformGravity = reader.ReadBoolean();
                reader.ReadByte(); //unk
                NoEmissionDuringFade = reader.ReadBoolean();
                SphereDivTblIdx = reader.ReadByte();
                RandomArcStartAngle = reader.ReadBoolean();
                DisplayParent = reader.ReadBoolean();
                EquidistantParticleEmission = reader.ReadBoolean();
                SphereUseLatitude = reader.ReadBoolean();
                reader.ReadByte();
                ParticleRotationMode = (VertexRotationMode)reader.ReadUInt32();
                PtclFollowType = (PtclFollowType)reader.ReadUInt32();
                ColorMode = (ColorMode)reader.ReadUInt32();
                AlphaMode = (AlphaMode)reader.ReadUInt32();
                reader.ReadUInt32();
                DisplaySideType = (DisplayFaceType)reader.ReadUInt32();
                MomentumRandom = reader.ReadSingle();
                reader.ReadBytes(96);
                BaseScale = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
                BaseRotation = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
                BaseTranslation = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
                RotationRandom = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
                TranslationRandom = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
                BlendType = (BlendType)reader.ReadUInt32();
                ZbufferTestType = (DepthType)reader.ReadUInt32();
                EmitterFunc = (EmitterFunctions)reader.ReadUInt32();
                VolumeScale = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
                ArcStartAngleUint = reader.ReadInt32();
                ArcLengthUint = reader.ReadUInt32();

                ArcStartAngle = MathTriangular.Idx2Rad(ArcStartAngleUint);
                ArcLength = MathTriangular.Idx2Rad(ArcLengthUint);

                UseArcUints = true;

                VolumeFillRatio = reader.ReadSingle();
                SphereLatitude = reader.ReadSingle();
                SphereLatitudeDir = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
                LineCenterPosition = reader.ReadSingle();
                EmissionShapeScale = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
                Color0 = new Vector4(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
                Color1 = new Vector4(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
                EmitterAlpha = reader.ReadSingle();
                EquidistantParticleEmissionUnit = reader.ReadSingle();
                PerFrameTravelDistanceMaxClamp = reader.ReadSingle();
                PerFrameTravelDistanceMinClamp = reader.ReadSingle();
                PerFrameTravelDistanceFallback = reader.ReadSingle();
                EmissionRatio = reader.ReadSingle();
                EmissionStartFrame = reader.ReadUInt32();
                EmissionEndFrame = reader.ReadUInt32();
                EmissionInterval = reader.ReadUInt32(); //Emission time in frames
                EmissionIntervalRandomness = reader.ReadInt32(); //Emission time in frames
                AllDirectionVelocity = reader.ReadSingle();
                DirectionVelocity = reader.ReadSingle();
                DirectionVelocityRandom = reader.ReadSingle();
                Direction = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
                DispersionAngle = reader.ReadSingle();
                DiffusionVel = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
                AirResist = reader.ReadSingle();
                Gravity = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
                ParticleAddXZVelocity = reader.ReadSingle();
                PositionRandomizer = reader.ReadSingle();
                MaxLifespan = reader.ReadUInt32();
                LifespanRandomizer = reader.ReadUInt32();

                MeshType = (MeshType)reader.ReadUInt32();
                VertexTransformMode = (VertexTransformMode)reader.ReadUInt32();
                RotationPivot = new Vector2(reader.ReadSingle(), reader.ReadSingle());
                CameraOffset = reader.ReadSingle();

                TextureEmitters = new TextureEmitter[2];
                TextureEmitters[0] = reader.ReadSection<TextureEmitter>();
                TextureEmitters[1] = reader.ReadSection<TextureEmitter>();
                Color0Source = (ColorSource)reader.ReadUInt32();
                Color1Source = (ColorSource)reader.ReadUInt32();

                ColorKeys = new Vector4[2, 3];
                for (int i = 0; i < 3; i++)
                {
                    ColorKeys[0, i] = new Vector4(
                         reader.ReadSingle(), reader.ReadSingle(),
                         reader.ReadSingle(), reader.ReadSingle());
                }
                for (int i = 0; i < 3; i++)
                {
                    ColorKeys[1, i] = new Vector4(
                         reader.ReadSingle(), reader.ReadSingle(),
                         reader.ReadSingle(), reader.ReadSingle());
                }

                ColorKey4Time2[0] = reader.ReadInt32();
                ColorKey4Time2[1] = reader.ReadInt32();
                ColorKey4Time3[0] = reader.ReadInt32();
                ColorKey4Time3[1] = reader.ReadInt32();
                ColorKey4Time4[0] = reader.ReadInt32();
                ColorKey4Time4[1] = reader.ReadInt32();

                ColorRepeatCount[0] = reader.ReadUInt32();
                ColorRepeatCount[1] = reader.ReadUInt32();
                ColorRandomCount[0] = reader.ReadUInt32();
                ColorRandomCount[1] = reader.ReadUInt32();
                ColorScale = reader.ReadSingle();
                for (int i = 0; i < 1; i++)
                {
                    AlphaSection alphaSection = AlphaKey0;
                    alphaSection.StartValue = reader.ReadSingle();
                    alphaSection.StartDifference = reader.ReadSingle();
                    alphaSection.EndDifference = reader.ReadSingle();
                    alphaSection.Time2 = reader.ReadInt32();
                    alphaSection.Time3 = reader.ReadInt32();
                }

                FragmentTextureColorBlendType = reader.ReadUInt32();
                FragmentPrimitiveColorBlendType = reader.ReadUInt32();
                FragmentTextureAlphaBlendType = reader.ReadUInt32();
                FragmentPrimitiveAlphaBlendType = reader.ReadUInt32();
                ScaleTime2 = reader.ReadInt32();
                ScaleTime3 = reader.ReadInt32();
                float scaleRandom = reader.ReadSingle();
                PtclScaleRandom = new Vector2(scaleRandom, scaleRandom);
                PtclScale = new Vector2(reader.ReadSingle(), reader.ReadSingle());
                PtclScaleStart = new Vector2(reader.ReadSingle(), reader.ReadSingle());
                PtclScaleStartDiff = new Vector2(reader.ReadSingle(), reader.ReadSingle());
                PtclScaleEndDiff = new Vector2(reader.ReadSingle(), reader.ReadSingle());
                PtclRotate = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
                PtclRotateRandom = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
                AngularVelocity = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
                AngularVelocityRandom = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
                RotationInertia = reader.ReadSingle();
                AlphaFadeStep = reader.ReadSingle();
                FragmentShaderMode = (FragmentShaderMode)reader.ReadByte();
                ShaderUserSetting = reader.ReadByte();
                FragmentSoftEdge = reader.ReadByte();
                FragmentReflectionAlpha = reader.ReadByte();
                ShaderParam0 = reader.ReadSingle();
                ShaderParam1 = reader.ReadSingle();
                FragmentSoftEdgeFadeDistance = reader.ReadSingle();
                FragmentSoftEdgeVolume = reader.ReadSingle();
                UserMacros1 = reader.ReadBytes(16);
                UserMacros2 = reader.ReadBytes(16);
                ShaderUserFlag = reader.ReadUInt32();
                ShaderUserSwitchFlag = reader.ReadUInt32();
                reader.ReadBytes(128);

                if (AlphaKey0.Time2 != 0 || AlphaKey0.Time3 != 100)
                    AnimAlpha0Func = AnimationFunctions.Key4Value3;
            }

            if (keyframeTblSize != 0)
                KeyTable = ((PTCL_Header)reader.PTCL.FileHeader).ReadKeyTable(reader, keyframeTblOffset);
        //    this.isVisible = ((WiiU.TextureRes)Textures[0]).OriginalSize == 0;

            this.GPUCalc = (Flags & (int)EmitterFlags.GPUParticleCalculations) != 0;
            this.ParticleSortReverse = (Flags & (int)EmitterFlags.ReverseParticleOrder) != 0;
            this.ParticleZSort = (Flags & (int)EmitterFlags.ZSort) != 0;
        }

        private long emitterKeyOfsPos;
        private long emitterParamOfsPos;

        public void Write(BinaryFileWriter writer)
        {
            writer.Write((uint)Type);
            writer.Write((uint)Flags);

        }

        public void WriteKeyTable(BinaryFileWriter writer)
        {
            writer.WriteOffset(emitterKeyOfsPos);
            writer.WriteSection(KeyTable);
        }

        public void WriteParamTable(BinaryFileWriter writer)
        {
            writer.WriteOffset(emitterParamOfsPos);
            writer.Write(Parameters);
        }

    }
}

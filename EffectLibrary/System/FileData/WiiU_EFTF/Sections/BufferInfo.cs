﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;
using System.IO;
using Toolbox.Core.IO;
using Syroot.BinaryData;

namespace EffectLibrary.WiiU
{
    public class BufferInfo : IResData
    {
        public uint ElementCount { get; set; }
        public uint Stride { get; set; }
        public uint Offset { get; set; }
        public uint Size { get; set; }

        public Vector4[] Data { get; set; }

        public int AttributeIndex { get; set; }

        public bool IsUint32 => AttributeIndex == 4;

        public void Read(BinaryFileReader reader)
        {
            ElementCount = reader.ReadUInt32();
            Stride = reader.ReadUInt32();
            Offset = reader.ReadUInt32();
            Size = reader.ReadUInt32();
        }

        public void ReadBuffer(BinaryFileReader reader, uint startOffset, int index)
        {
            reader.ByteOrder = ByteOrder.BigEndian;

            AttributeIndex = index;
            if (Size != 0)
            {
                reader.SeekBegin(startOffset + this.Offset);
                Data = GetBufferData(reader);
            }
            else
                Data = new Vector4[0];
        }

        public void Write(BinaryFileWriter writer)
        {
            writer.Write(ElementCount);
            writer.Write(Stride);
            writer.Write(Offset);
            writer.Write(Size);
        }

        public void WriteBuffer(BinaryFileWriter writer, long startPos)
        {
            Offset = (uint)(writer.Position - startPos);
            Size   = (uint)(Data.Length * Stride * 4);
            for (int i = 0; i < Data.Length; i++)
            {
                if (Stride > 0) writer.Write(IsUint32 ? (uint)Data[i].X : Data[i].X);
                if (Stride > 1) writer.Write(IsUint32 ? (uint)Data[i].Y : Data[i].Y);
                if (Stride > 2) writer.Write(IsUint32 ? (uint)Data[i].Z : Data[i].Z);
                if (Stride > 3) writer.Write(IsUint32 ? (uint)Data[i].W : Data[i].W);
            }
        }

        public float[] GetDisplayBuffer()
        {
            List<float> data = new List<float>();
            for (int i = 0; i < Data.Length; i++)
            {
                if (Stride > 0) data.Add(Data[i].X);
                if (Stride > 1) data.Add(Data[i].Y);
                if (Stride > 2) data.Add(Data[i].Z);
                if (Stride > 3) data.Add(Data[i].W);
            }
            return data.ToArray();
        }

        public Vector4[] GetBufferData(BinaryDataReader reader)
        {
            Vector4[] elements = new Vector4[ElementCount / Stride];
            for (int i = 0; i < ElementCount / Stride; i++)
            {
                var values = ReadElements(reader);
                elements[i] = new Vector4(
                    values.Length > 0 ? values[0] : 0,
                    values.Length > 1 ? values[1] : 0,
                    values.Length > 2 ? values[2] : 0,
                    values.Length > 3 ? values[3] : 0);
            }
            return elements;
        }

        private float[] ReadElements(BinaryDataReader reader)
        {
            float[] values = new float[Stride];
            for (int i = 0; i < Stride; i++)
            {
                if (!IsUint32)
                    values[i] = reader.ReadSingle();
                else
                    values[i] = reader.ReadUInt32();
            }
            return values;
        }
    }
}

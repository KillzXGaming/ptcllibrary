﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Threading.Tasks;
using Syroot.BinaryData;

namespace EffectLibrary.WiiU
{
    public class Primitive : IResData, IPrimitive
    {
        public BufferInfo PositionsInfo { get; set; }
        public BufferInfo NormalsInfo { get; set; }
        public BufferInfo ColorsInfo { get; set; }
        public BufferInfo TexCoordInfo { get; set; }
        public BufferInfo IndexInfo { get; set; }

        public bool HasNormals   => NormalsInfo.Data?.Length  > 0;
        public bool HasColors    => ColorsInfo.Data?.Length   > 0;
        public bool HasTexCoords => TexCoordInfo.Data?.Length > 0;

        public uint GetElementCount() => IndexInfo.ElementCount;
        public uint GetVertexCount() => PositionsInfo.ElementCount / 3;

        public Vector4[] GetPositionData() { return PositionsInfo.Data; }
        public Vector4[] GetNormalsData() { return NormalsInfo.Data; }
        public Vector4[] GetColorData() { return ColorsInfo.Data; }
        public Vector4[] GetTexCoordData() { return TexCoordInfo.Data; }

        public uint CalculateTotalVertexStride()
        {
            uint stride = 0;
            stride += PositionsInfo.Stride * sizeof(float);
            stride += NormalsInfo.Stride * sizeof(float);
            stride += ColorsInfo.Stride * sizeof(float);
            stride += TexCoordInfo.Stride * sizeof(float);
            return stride;
        }

        public void Read(BinaryFileReader reader)
        {
            PositionsInfo = reader.ReadSection<BufferInfo>();
            NormalsInfo = reader.ReadSection<BufferInfo>();
            ColorsInfo = reader.ReadSection<BufferInfo>();
            TexCoordInfo = reader.ReadSection<BufferInfo>();
            IndexInfo = reader.ReadSection<BufferInfo>();
        }

        public void Write(BinaryFileWriter writer)
        {
            writer.WriteSection(PositionsInfo);
            writer.WriteSection(NormalsInfo);
            writer.WriteSection(ColorsInfo);
            writer.WriteSection(TexCoordInfo);
            writer.WriteSection(IndexInfo);
        }

        public uint[] GetIndexBuffer()
        {
            uint count = IndexInfo.ElementCount;

            uint[] indexBuffer = new uint[count];
            for (int i = 0; i < count / IndexInfo.Stride; i++)
            {
                int index = i * (int)IndexInfo.Stride;
                if (IndexInfo.Stride == 3)
                {
                    indexBuffer[index + 0] = (uint)IndexInfo.Data[i].X;
                    indexBuffer[index + 1] = (uint)IndexInfo.Data[i].Y;
                    indexBuffer[index + 2] = (uint)IndexInfo.Data[i].Z;
                }
                else
                    indexBuffer[index + 0] = (uint)IndexInfo.Data[i].X;
            }
            return indexBuffer;
        }
    }
}

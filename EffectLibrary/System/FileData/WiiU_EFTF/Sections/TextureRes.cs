﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EffectLibrary.WiiU
{
    public class TextureRes : ITextureResource, IResData
    {
        public bool HasData => this.Width != 0 && this.Height != 0;

        public Toolbox.Core.STGenericTexture Handle { get; set; }

        public bool Initialized => Handle != null;

        public uint Alignment { get; set; } = 8196;
        public uint Pitch { get; set; } = 8196;

        public ushort Width { get; set; }
        public ushort Height { get; set; }
        public uint TileMode { get; set; }
        public uint Swizzle { get; set; }
        public int WrapMode { get; set; }
        public FilterMode FilterMode { get; set; }
        public uint Depth { get; set; } = 1;
        public uint Unknown { get; set; }
        public uint MipCount { get; set; }
        public byte[] CompSel { get; set; }
        public float LODMax { get; set; }
        public float LODBias { get; set; }

        public uint OriginalSize { get; set; }
        public uint OriginalPosition { get; set; }
        public GX2TexResFormat OriginalFormat { get; set; }

        public WrapMode WrapX { get; set; }
        public WrapMode WrapY { get; set; }

        public GX2TexResFormat Format { get; set; }

        public uint Size { get; set; }
        public uint DataPosition { get; set; }

        public Memory<byte> Data { get; set; }

        public void Read(BinaryFileReader reader)
        {
            Width = reader.ReadUInt16();
            Height = reader.ReadUInt16();
            TileMode = reader.ReadUInt32();
            Swizzle = reader.ReadUInt32();

            if (reader.PTCL.FileHeader.Version < 55)
            {
                Alignment = reader.ReadUInt32();
                Pitch = reader.ReadUInt32();
                WrapMode = reader.ReadByte(); //11 = repeat, 22 = mirror
                FilterMode = (FilterMode)reader.ReadByte();
                Depth = reader.ReadByte();
                Unknown = reader.ReadByte();
                MipCount = reader.ReadUInt32();
                CompSel = reader.ReadBytes(4);
                uint[] MipOffsets = reader.ReadUInt32s(13);
                LODMax = reader.ReadSingle();
                LODBias = reader.ReadSingle();
                OriginalFormat = (GX2TexResFormat)reader.ReadUInt32();
                OriginalSize = reader.ReadUInt32();
                OriginalPosition = reader.ReadUInt32();
                Format = reader.ReadEnum<GX2TexResFormat>(false);
                Size = reader.ReadUInt32();
                DataPosition = reader.ReadUInt32();
                reader.ReadUInt32();
            }
            else
            {
                WrapMode = reader.ReadByte();
                FilterMode = (FilterMode)reader.ReadByte();
                Depth = reader.ReadByte();
                Unknown = reader.ReadByte();
                MipCount = reader.ReadUInt32();
                CompSel = reader.ReadBytes(4);
                LODMax = reader.ReadSingle();
                LODBias = reader.ReadSingle();
                OriginalFormat = (GX2TexResFormat)reader.ReadUInt32();
                OriginalSize = reader.ReadUInt32();
                OriginalPosition = reader.ReadUInt32();
                Format = (GX2TexResFormat)reader.ReadUInt32();
                Size = reader.ReadUInt32();
                DataPosition = reader.ReadUInt32();
                reader.ReadUInt32();
            }

            WrapX = (WrapMode)(WrapMode & 0xF);
            WrapY = (WrapMode)(WrapMode >> 4);

            reader.ReadBytes(0x9C); //GX2 Header set at runtime

            if (!HasData)
                return;

            var dataStart = ((WiiU.PTCL_Header)reader.PTCL.FileHeader).TextureDataTableOffset;
            if (Size != 0)
            {
                using (reader.TemporarySeek(dataStart + DataPosition)) {
                    reader.SeekBegin(dataStart + DataPosition);
                    Data = reader.ReadBytes((int)Size);
                }
            }
            else if (OriginalSize != 0)
            {
                using (reader.TemporarySeek(dataStart + OriginalPosition)) {
                    reader.SeekBegin(dataStart + OriginalPosition);
                    Data = reader.ReadBytes((int)OriginalSize);
                }
            }
 
        }

        public void Write(BinaryFileWriter writer)
        {

        }

        public void CreateResourceHandle()
        {
            if (Size > 0)
                Handle = new FtexbTexureHandle(this);
            else if (OriginalSize > 0)
                Handle = new OriginalTexureHandle(this);
        }
    }
}

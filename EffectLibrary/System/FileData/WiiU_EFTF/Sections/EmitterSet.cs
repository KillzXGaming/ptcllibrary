﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Syroot.BinaryData;
using OpenTK;

namespace EffectLibrary.WiiU
{
    public class EmitterSet : IEmitterSet
    {
        //Instance used for spawning emitter set data
        public EffectLibrary.EmitterSet EmitterSetHandle { get; set; } = null;

        public Matrix4 MatrixSRT { get; set; } = Matrix4.Identity;

        public List<SimpleEmitterData> Emitters { get; set; } = new List<SimpleEmitterData>();

        public IEnumerable<IEmitter> EmitterList => this.Emitters;

        public string Name { get; set; }

        public uint UserData { get; set; }

        public bool IsVisible { get; set; } = false;

        public void Read(PTCL_Header header, BinaryFileReader reader)
        {
            reader.ReadUInt32();
            reader.ReadUInt32();
            Name = header.ReadStringTable(reader);
            reader.ReadUInt32();
            uint numEmitters = reader.ReadUInt32();
            uint emitterOffset = reader.ReadUInt32();
            reader.ReadUInt32();

            Console.WriteLine($"EmitterSet {Name}");


            //Read emitter tables.
            using (reader.TemporarySeek(emitterOffset, System.IO.SeekOrigin.Begin)) {
                for (int i = 0; i < numEmitters; i++)
                {
                    uint offset = reader.ReadUInt32();
                    reader.ReadUInt32(); //Emitter set at runtime
                    if (reader.PTCL.FileHeader.Version >= 55)
                    {
                        reader.ReadUInt32(); //Block set at runtime
                        reader.ReadUInt32(); //Block set at runtime
                    }

                    using (reader.TemporarySeek(offset, System.IO.SeekOrigin.Begin))
                    {
                        EmitterType type = (EmitterType)reader.ReadUInt32();
                        if (type == EmitterType.Complex)
                            Emitters.Add(new ComplexEmitterData(reader));
                        else
                            Emitters.Add(new SimpleEmitterData(reader));
                    }
                }
            }
        }

        private long emitterOfsPos;

        public void Write(BinaryFileWriter writer)
        {
            writer.Write(0);
            writer.Write(0);
            writer.SaveString(Name);
            writer.Write(0);
            writer.Write(Emitters.Count);
            emitterOfsPos = writer.Position;
            writer.Write(0);
            writer.Write(0);
        }

        public void WriteEmitters(BinaryFileWriter writer)
        {
            writer.WriteOffset(emitterOfsPos);
            foreach (var emitter in Emitters)
                emitter.Write(writer);
        }
    }   
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OpenTK;

namespace EffectLibrary
{
    public partial class TextureEmitter 
    {
        public void ReadWiiU(BinaryFileReader reader)
        {
            HasTexPtnAnim = reader.ReadBoolean();
            TexPtnAnimRandStart = reader.ReadBoolean();
            TexPtnAnimClamp = reader.ReadBoolean();
            TexPtnAnimIdxDiv = reader.ReadByte();
            reader.ReadByte();
            TexPtnAnimNum = reader.ReadByte();
            reader.ReadBytes(2);
            if (reader.PTCL.FileHeader.Version >= 55)
            {
                reader.ReadBytes(4);
                TexPtnAnimData = reader.ReadBytes(32);
                reader.ReadUInt32();
                TexPtnAnimPeriod = reader.ReadInt16();
                TexPtnAnimUsedSize = reader.ReadInt16();
            }
            else
            {
                TexPtnAnimPeriod = reader.ReadInt16();
                TexPtnAnimUsedSize = reader.ReadInt16();
                TexPtnAnimData = reader.ReadBytes(32);
                reader.ReadBytes(4);
            }

            UvScaleInit = new Vector2(reader.ReadSingle(), reader.ReadSingle());
            UvShiftAnimMode = reader.ReadUInt32();

            TranslateShift = new Vector2(reader.ReadSingle(), reader.ReadSingle());
            Translate = new Vector2(reader.ReadSingle(), reader.ReadSingle());
            TranslateRnd = new Vector2(reader.ReadSingle(), reader.ReadSingle());

            ScaleShift = new Vector2(reader.ReadSingle(), reader.ReadSingle());
            Scale = new Vector2(reader.ReadSingle(), reader.ReadSingle());
            ScaleRnd = new Vector2(reader.ReadSingle(), reader.ReadSingle());

            RotateShift = reader.ReadSingle();
            Rotate = reader.ReadSingle();
            RotateRnd = reader.ReadSingle();
        }
    }
}

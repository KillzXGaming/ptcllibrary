﻿using EffectLibrary.WiiU;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using OpenTK;
using System;
using System.Collections.Generic;
using Toolbox.Core;

namespace EffectLibrary
{
    public class EmitterData
    {
        [JsonConverter(typeof(StringEnumConverter))]
        public EmitterType Type { get; set; }

        public string Name { get; set; }

        public uint RandomSeed { get; set; }

        public uint CustomCallbackID { get; set; }

        public bool IsVisible { get; set; } = true;

        public ITextureResource[] Textures { get; set; } = new ITextureResource[3];

        public STGenericTexture GetTextureWrapper(int index)
        {
            if (Textures.Length > index && Textures[index] != null)
                return Textures[index].Handle;
            else
                return null;
        }
    }

    public partial class SimpleEmitterData : EmitterData, IEmitter
    {
        public long StartPosition;

        public uint Flags { get; set; }

        public int _29C;

        public bool DisplayParent { get; set; } = true;

        public TextureEmitter[] TextureEmitters { get; set; } = new TextureEmitter[3];

        public bool TransformGravity { get; set; }
        public bool NoEmissionDuringFade { get; set; }
        public byte SphereDivTblIdx { get; set; }
        public bool RandomArcStartAngle { get; set; }
        public bool isVisible { get; set; } = true;
        public bool EquidistantParticleEmission { get; set; }
        public bool SphereUseLatitude { get; set; }
        public VertexRotationMode ParticleRotationMode { get; set; }
        public PtclFollowType PtclFollowType { get; set; }

        public AlphaMode AlphaMode { get; set; }
        public ColorMode ColorMode { get; set; }

        public DisplayFaceType DisplaySideType { get; set; }
        public BlendType BlendType { get; set; }
        public DepthType ZbufferTestType { get; set; }

        public bool GPUCalc = false;
        public bool ParticleSortReverse = false;
        public bool ParticleZSort = false;

        public float MomentumRandom;

        public OpenTK.Matrix4 AnimMatrixSRT = OpenTK.Matrix4.Identity;
        public OpenTK.Matrix4 AnimMatrixRT = OpenTK.Matrix4.Identity;

        public Vector3 BaseScale { get; set; }
        public Vector3 BaseRotation { get; set; }
        public Vector3 BaseTranslation { get; set; }
        public Vector3 RotationRandom { get; set; }
        public Vector3 TranslationRandom { get; set; }

        public Vector3 VolumeScale { get; set; }
        public Vector3 EmissionShapeScale { get; set; }

        public float ArcStartAngle { get; set; }
        public float ArcLength { get; set; }
        public float ArcRandomness { get; set; }

        public bool UseArcUints = false;

        public int ArcStartAngleUint { get; set; }
        public uint ArcLengthUint { get; set; }

        public float VolumeFillRatio { get; set; }

        public float SphereLatitude { get; set; }
        public Vector3 SphereLatitudeDir { get; set; }

        public float EmitterAlpha { get; set; }
        public float EmitterAlpha1 { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public AnimationFunctions AnimScaleFunc { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public AnimationFunctions AnimColor0Func { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public AnimationFunctions AnimColor1Func { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public AnimationFunctions AnimAlpha0Func { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public AnimationFunctions AnimAlpha1Func { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public EmitterFunctions EmitterFunc { get; set; }

        public Vector4 Color0 { get; set; }
        public Vector4 Color1 { get; set; }

        public float AllDirectionVelocity { get; set; }
        public float DirectionVelocity { get; set; }
        public float DirectionVelocityRandom { get; set; }
        public Vector3 Direction { get; set; }
        public float DispersionAngle { get; set; }
        public Vector3 DiffusionVel { get; set; }
        public float AirResist { get; set; }
        public Vector3 Gravity { get; set; }
        public float ParticleAddXZVelocity { get; set; }
        public float PositionRandomizer { get; set; }

        public float LineCenterPosition;

        public float EquidistantParticleEmissionUnit { get; set; }
        public float PerFrameTravelDistanceMaxClamp { get; set; }
        public float PerFrameTravelDistanceMinClamp { get; set; }
        public float PerFrameTravelDistanceFallback { get; set; }

        public uint MaxLifespan { get; set; }
        public uint LifespanRandomizer { get; set; }

        public VertexTransformMode VertexTransformMode { get; set; }

        public uint EmissionInterval { get; set; }
        public int EmissionIntervalRandomness { get; set; }

        public float EmissionRatio { get; set; }
        public uint EmissionRatioRandomness { get; set; }
        public uint EmissionStartFrame { get; set; }
        public uint EmissionEndFrame { get; set; }

        public float BaseSpeedRandomness { get; set; }

        public float RotationAcceleration { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public MeshType MeshType { get; set; }

        public Vector2 RotationPivot { get; set; }
        public float CameraOffset { get; set; }

        public uint FragmentTextureColorBlendType { get; set; }
        public uint FragmentPrimitiveColorBlendType { get; set; }
        public uint FragmentTextureAlphaBlendType { get; set; }
        public uint FragmentPrimitiveAlphaBlendType { get; set; }

        public float AlphaFadeStep { get; set; }
        public FragmentShaderMode FragmentShaderMode { get; set; }
        public byte ShaderUserSetting { get; set; }
        public byte FragmentSoftEdge { get; set; }
        public byte FragmentReflectionAlpha { get; set; }
        public float ShaderParam0 { get; set; }
        public float ShaderParam1 { get; set; }

        public float FragmentSoftEdgeFadeDistance { get; set; }
        public float FragmentSoftEdgeVolume { get; set; }

        public byte[] UserMacros1 { get; set; }
        public byte[] UserMacros2 { get; set; }

        public uint ShaderUserFlag { get; set; }
        public uint ShaderUserSwitchFlag { get; set; }

        public byte ShaderCallbackID { get; set; }

        public int ShaderIndex1 { get; set; } = -1;
        public int ShaderIndex2 { get; set; } = -1;
        public int ShaderIndex3 { get; set; } = -1;

        public ColorSource Color0Source { get; set; }
        public ColorSource Color1Source { get; set; }

        public Vector4[,] ColorKeys { get; set; } = new Vector4[2, 8];

        public int[] ColorKey4Time2 { get; set; } = new int[2];
        public int[] ColorKey4Time3 { get; set; } = new int[2];
        public int[] ColorKey4Time4 { get; set; } = new int[2];

        public uint[] ColorRepeatCount { get; set; } = new uint[2];
        public uint[] ColorRandomCount { get; set; } = new uint[2];

        public AlphaSection AlphaKey0 { get; set; } = new AlphaSection();
        public AlphaSection AlphaKey1 { get; set; } = new AlphaSection();

        public Vector3 PtclRotate { get; set; }
        public Vector3 PtclRotateRandom { get; set; }

        public Vector3 ptclRotateDegrees
        {
            get { return PtclRotate * STMath.Rad2Deg; }
            set { PtclRotate = value * STMath.Deg2Rad; }
        }

        public Vector3 ptclRotateDegreesRandom
        {
            get { return PtclRotateRandom * STMath.Rad2Deg; }
            set { PtclRotateRandom = value * STMath.Deg2Rad; }
        }

        public Vector3 AngularVelocity { get; set; }
        public Vector3 AngularVelocityRandom { get; set; }

        public float RotationInertia { get; set; }

        public int ScaleTime2 { get; set; }
        public int ScaleTime3 { get; set; }

        public float _5DC { get; set; }

        public Vector2 PtclScale { get; set; } = new Vector2(1);

        public Vector2 PtclScaleRandom { get; set; }
        public Vector2 PtclScaleStart { get; set; }
        public Vector2 PtclScaleStartDiff { get; set; }
        public Vector2 PtclScaleEndDiff { get; set; }

        private float colorScale = 0;

        public float ColorScale
        {
            get { return colorScale; }
            set
            {
                colorScale = Math.Max(0, value);
            }
        }

        public float[] Parameters { get; set; }

        public KeyTable KeyTable { get; set; }
        public uint EmitterSize { get; set; }

        public Vector4 NearAlphaParameters = new Vector4(10, 30, 100, 80);

        public float FresnelAlphaMin;
        public float FresnelAlphaMax;

        public int PrimitiveIndex { get; set; }
        public int PrimitiveIndex2 { get; set; }

        public SimpleEmitterData(BinaryFileReader reader)
        {
            if (!reader.IsSwitch)
                ReadWiiU(reader);
        }

        public KeyGroup[] GetKeyGroups(params AnimGroupType[] types)
        {
            List<KeyGroup> groups = new List<KeyGroup>();
            if (KeyTable == null)
                return groups.ToArray();

            for (int i = 0; i < KeyTable.KeyGroups.Count; i++)
            {
                foreach (var type in types) {
                    if (KeyTable.KeyGroups[i].Type == type)
                        groups.Add(KeyTable.KeyGroups[i]);
                }
            }

            return groups.ToArray();
        }

        public bool HasAnimationGroup(AnimGroupType type)
        {
            if (KeyTable == null)
                return false;

            for (int i = 0; i < KeyTable.KeyGroups.Count; i++)
            {
                if (KeyTable.KeyGroups[i].Type == type)
                    return true;
            }
            return false;
        }

        public List<ColorKeyFrame> GetAnimatedKeys(int type)
        {
            List<ColorKeyFrame> keyFrames = new List<ColorKeyFrame>();
            switch (type)
            {
                case 0:
                    if (Color0Source == ColorSource.Key4Value3)
                    {
                        keyFrames.Add(new ColorKeyFrame(ColorKey4Time2[0] / 100.0f, ColorKeys[0, 0]));
                        keyFrames.Add(new ColorKeyFrame(ColorKey4Time3[0] / 100.0f, ColorKeys[0, 1]));
                        keyFrames.Add(new ColorKeyFrame(ColorKey4Time4[0] / 100.0f, ColorKeys[0, 2]));
                    }
                    if (Color0Source == ColorSource.Key8)
                    {
                        Dictionary<float, float[]> colors = new Dictionary<float, float[]>();

                        foreach (var group in KeyTable.KeyGroups)
                        {
                            switch (group.Type)
                            {
                                case AnimGroupType.EmitterColor0R:
                                case AnimGroupType.EmitterColor0G:
                                case AnimGroupType.EmitterColor0B:

                                    int index = (int)group.Type - 38;
                                    foreach (var key in group.KeyFrames)
                                    {
                                        float time = key.Frame / 100;
                                        if (!colors.ContainsKey(time))
                                            colors.Add(time, new float[3]);

                                        colors[time][index] = key.Value;
                                    }
                                    break;
                            }
                        }

                        foreach (var key in colors)
                        {
                            keyFrames.Add(new ColorKeyFrame(key.Key, new Vector4(
                                key.Value[0], key.Value[1],
                                key.Value[2], key.Value[2])));
                        }
                    }
                    break;
                case 1:
                    if (Color1Source == ColorSource.Key4Value3)
                    {
                        keyFrames.Add(new ColorKeyFrame(ColorKey4Time2[1] / 100.0f, ColorKeys[1, 0]));
                        keyFrames.Add(new ColorKeyFrame(ColorKey4Time3[1] / 100.0f, ColorKeys[1, 1]));
                        keyFrames.Add(new ColorKeyFrame(ColorKey4Time4[1] / 100.0f, ColorKeys[1, 2]));
                    }
                    if (Color1Source == ColorSource.Key8)
                    {
                        Dictionary<float, float[]> colors = new Dictionary<float, float[]>();

                        foreach (var group in KeyTable.KeyGroups)
                        {
                            switch (group.Type)
                            {
                                case AnimGroupType.EmitterColor1R:
                                case AnimGroupType.EmitterColor1G:
                                case AnimGroupType.EmitterColor1B:
                                    int index = (int)group.Type - 41;
                                    foreach (var key in group.KeyFrames)
                                    {
                                        float time = key.Frame / 100;
                                        if (!colors.ContainsKey(time))
                                            colors.Add(time, new float[3]);

                                        colors[time][index] = key.Value;
                                    }
                                    break;
                            }
                        }
                        foreach (var key in colors)
                        {
                            keyFrames.Add(new ColorKeyFrame(key.Key, new Vector4(
                                key.Value[0], key.Value[1],
                                key.Value[2], 1.0f)));
                        }
                    }
                    break;
                case 2:
                    if (AnimAlpha0Func == AnimationFunctions.Key4Value3 ||
                        AnimAlpha0Func == AnimationFunctions.Constant)
                    {
                        keyFrames.AddRange(AlphaKey0.GetKeys());
                    }
                    break;
                case 3:
                    if (AnimAlpha1Func == AnimationFunctions.Key4Value3 ||
                       AnimAlpha1Func == AnimationFunctions.Constant)
                    {
                        keyFrames.AddRange(AlphaKey1.GetKeys());
                    }
                    break;
            }
            return keyFrames;
        }
    }


    /// <summary>
    /// Represents a 4 key 3 value alpha animation section.
    /// </summary>
    public class AlphaSection
    {
        //The first key value
        public float StartValue { get; set; }

        //The difference between start first key frame
        public float StartDifference { get; set; }
        //The difference between the key frame to the end frame
        public float EndDifference { get; set; }

        //Times for 2 keys in the middle
        public int Time2 { get; set; }
        public int Time3 { get; set; }

        public List<ColorKeyFrame> GetKeys()
        {
            float value0 = StartValue;
            float value1 = StartValue + StartDifference;
            float value2 = value1 + EndDifference;

            List<ColorKeyFrame> keys = new List<ColorKeyFrame>();
            keys.Add(new ColorKeyFrame(0, new Vector4(value0)));
            keys.Add(new ColorKeyFrame(Time2 / 100.0f, new Vector4(value1)));
            keys.Add(new ColorKeyFrame(Time3 / 100.0f, new Vector4(value1)));
            keys.Add(new ColorKeyFrame(1, new Vector4(value2)));

            return keys;
        }
    }

    public partial class ComplexEmitterData : SimpleEmitterData
    {
        public ChildData ChildData;
        public List<IFieldData> FieldData = new List<IFieldData>();
        public FluctuationData FluctuationData;
        public StripeData StripeData;

        public uint childFlags;
        public ushort fieldFlags;
        public ushort fluctuationFlags;
        public ushort stripeFlags;

        public byte[] _6FE = new byte[2];

        public uint DataSize;

        public ComplexEmitterData(BinaryFileReader reader) : base(reader)
        {
            Type = EmitterType.Complex;

            if (!reader.IsSwitch)
                ReadComplexDataWiiU(StartPosition, reader);
        }
    }

    public class StripeData
    {
        public uint type;
        public uint crossType;
        public uint connectionType;
        public uint textureType;
        public uint numSliceHistory;
        public uint numDivisions;
        public float alphaStart;
        public float alphaEnd;
        public Vector2 _20;
        public uint sliceHistInterval;
        public float sliceInterpolation;
        public float dirInterpolation;

        public void ReadWiiU(BinaryFileReader reader)
        {
            type = reader.ReadUInt32();
            crossType = reader.ReadUInt32();
            connectionType = reader.ReadUInt32();
            textureType = reader.ReadUInt32();
            numSliceHistory = reader.ReadUInt32();
            numDivisions = reader.ReadUInt32();
            alphaStart = reader.ReadSingle();
            alphaEnd = reader.ReadSingle();
            _20 = new Vector2(reader.ReadSingle(), reader.ReadSingle());
            sliceHistInterval = reader.ReadUInt32();
            sliceInterpolation = reader.ReadSingle();
            dirInterpolation = reader.ReadSingle();
       }
    }

    public class ChildData
    {
        public string Name = "Child";

        public int numChildParticles;
        public int startFramePercent;
        public int ptclMaxLifespan;
        public int emissionInterval;
        public float VelocityInheritRatio;
        public float AllDirectionVelocity;
        public Vector3 diffusionVel;
        public float PtclPosRandom;
        public uint _28;
        public uint _2C;
        public int PrimitiveIdx = -1;
        public float MomentumRandom;
        public BlendType BlendType;
        public MeshType MeshType;
        public VertexTransformMode vertexTransformMode;
        public DepthType ZbufferTestType;
        public WiiU.TextureRes Texture;
        public DisplayFaceType DisplaySideType;
        public Vector3 PtclColor0;
        public Vector3 PtclColor1;
        public float _178;
        public uint primitiveColorBlend;
        public uint primitiveAlphaBlend;

        public float ScaleInheritRatio;
        public Vector2 PtclEmitScale;
        public float PtclScaleRandom;
        public VertexRotationMode rotationMode;
        public Vector3 PtclRotate;
        public Vector3 PtclRotateRandom;
        public Vector3 AngularVelocity;
        public Vector3 AngularVelocityRandom;
        public float RotInertia;
        public Vector2 RotBasis;
        public Vector3 Gravity;
        public int ScaleAnimTime1;
        public Vector2 PtclScaleEnd;

        public AlphaSection AlphaKey0 { get; set; } = new AlphaSection();
        public AlphaSection AlphaKey1 { get; set; } = new AlphaSection();

        public TextureEmitter TextureEmitter;

        public uint FragmentColorMode;
        public uint FragmentAlphaMode;
        public float AirResist;

        public FragmentShaderMode FragmentShaderMode { get; set; }
        public byte ShaderUserSetting { get; set; }
        public byte FragmentSoftEdge { get; set; }
        public byte FragmentReflectionAlpha { get; set; }
        public float ShaderParam0 { get; set; }
        public float ShaderParam1 { get; set; }

        public Vector4 NearAlphaParameters = new Vector4(10, 30, 100, 80);

        public float FragmentSoftEdgeFadeDistance { get; set; }
        public float FragmentSoftEdgeVolume { get; set; }

        public byte[] UserMacros1 { get; set; } = new byte[16];
        public byte[] UserMacros2 { get; set; } = new byte[16];

        public uint ShaderUserFlag { get; set; }
        public uint ShaderUserSwitchFlag { get; set; }

        public int ShaderIndex1 { get; set; } = -1;
        public int ShaderIndex2 { get; set; } = -1;
        public int ShaderIndex3 { get; set; } = -1;

        public void ReadWiiU(BinaryFileReader reader)
        {
            numChildParticles = reader.ReadInt32();
            startFramePercent = reader.ReadInt32();
            ptclMaxLifespan = reader.ReadInt32();
            emissionInterval = reader.ReadInt32();
            if (reader.PTCL.FileHeader.Version >= 55) {
                reader.ReadUInt32();
            }
            VelocityInheritRatio = reader.ReadSingle();
            AllDirectionVelocity = reader.ReadSingle();
            diffusionVel = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
            PtclPosRandom = reader.ReadSingle();
            _28 = reader.ReadUInt32();
            _2C = reader.ReadUInt32();
            PrimitiveIdx = reader.ReadInt32();
            MomentumRandom = reader.ReadSingle();
            BlendType = (BlendType)reader.ReadUInt32();
            MeshType = (MeshType)reader.ReadUInt32();
            vertexTransformMode = (VertexTransformMode)reader.ReadUInt32();
            ZbufferTestType = (DepthType)reader.ReadUInt32();
            Texture = reader.ReadSection<WiiU.TextureRes>();
            DisplaySideType = (DisplayFaceType)reader.ReadUInt32();
            PtclColor0 = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
            PtclColor1 = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
            _178 = reader.ReadSingle();
            primitiveColorBlend = reader.ReadUInt32();
            primitiveAlphaBlend = reader.ReadUInt32();

            if (reader.PTCL.FileHeader.Version >= 55) {
                reader.ReadBytes(8); //unk
                for (int i = 0; i < 2; i++)
                {
                    AlphaSection alphaSection = i == 0 ? AlphaKey0 : AlphaKey1;
                    alphaSection.StartValue = reader.ReadSingle();
                    alphaSection.StartDifference = reader.ReadSingle();
                    alphaSection.EndDifference = reader.ReadSingle();
                    alphaSection.Time2 = (int)(reader.ReadSingle() * 100);
                    alphaSection.Time3 = (int)(reader.ReadSingle() * 100);
                }
            }
            else
            {
                AlphaKey0.StartValue = reader.ReadSingle();
                AlphaKey0.EndDifference = reader.ReadSingle();
                AlphaKey0.StartDifference = reader.ReadSingle();
            }

            ScaleInheritRatio = reader.ReadSingle();
            PtclEmitScale = new Vector2(reader.ReadSingle(), reader.ReadSingle());
            PtclScaleRandom = reader.ReadSingle();
            rotationMode = (VertexRotationMode)reader.ReadUInt32();
            PtclRotate = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
            PtclRotateRandom = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
            AngularVelocity = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
            AngularVelocityRandom = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
            RotInertia = reader.ReadSingle();
            if (reader.PTCL.FileHeader.Version >= 55) {
                reader.ReadUInt32();
            }
            RotBasis = new Vector2(reader.ReadSingle(), reader.ReadSingle());
            Gravity = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());

            if (reader.PTCL.FileHeader.Version < 55)
            {
                AlphaKey0.Time3 = reader.ReadInt32();
                AlphaKey0.Time2 = reader.ReadInt32();
            }

            ScaleAnimTime1 = reader.ReadInt32();
            PtclScaleEnd = new Vector2(reader.ReadSingle(), reader.ReadSingle());

            if (reader.PTCL.FileHeader.Version >= 55)
            {
                TextureEmitter = reader.ReadSection<TextureEmitter>();
            }
            else
            {
                TextureEmitter = new TextureEmitter();
                TextureEmitter.TexPtnAnimNum = (byte)reader.ReadUInt16();
                TextureEmitter.TexPtnAnimIdxDiv = reader.ReadByte();
                TextureEmitter.UvScaleInit = new Vector2(reader.ReadSingle(), reader.ReadSingle());
                TextureEmitter.TexPtnAnimData = reader.ReadBytes(32);
                TextureEmitter.TexPtnAnimPeriod = reader.ReadInt16();
                TextureEmitter.TexPtnAnimUsedSize = reader.ReadInt16();
                TextureEmitter.TexPtnAnimClamp = reader.ReadBoolean();
                if (TextureEmitter.TexPtnAnimUsedSize == 256)
                    Console.WriteLine();
            }

            FragmentColorMode = reader.ReadUInt32();
            FragmentAlphaMode = reader.ReadUInt32();
            AirResist = reader.ReadSingle();

            //Seems to match simple emitter structure
            if (reader.PTCL.FileHeader.Version >= 65)
            {
                FragmentShaderMode = (FragmentShaderMode)reader.ReadByte();
                ShaderUserSetting = reader.ReadByte();
                FragmentSoftEdge = reader.ReadByte();
                FragmentReflectionAlpha = reader.ReadByte();
                reader.ReadBytes(8);
                ShaderParam0 = reader.ReadSingle();
                ShaderParam1 = reader.ReadSingle();
                ShaderIndex1 = reader.ReadInt32();
                ShaderIndex2 = reader.ReadInt32();
                ShaderIndex3 = reader.ReadInt32();
                reader.ReadInt32();
                FragmentSoftEdgeFadeDistance = reader.ReadSingle();
                reader.ReadSingle();
                FragmentSoftEdgeVolume = reader.ReadSingle();
                reader.ReadBytes(8); //unk
                NearAlphaParameters = new Vector4(
                    reader.ReadSingle(), reader.ReadSingle(),
                    reader.ReadSingle(), reader.ReadSingle());
                reader.ReadSingle();
                UserMacros1 = reader.ReadBytes(16);
                UserMacros2 = reader.ReadBytes(16);
                ShaderUserFlag = reader.ReadUInt32();
                ShaderUserSwitchFlag = reader.ReadUInt32();
                reader.ReadBytes(24);
            }
            else
            {
                FragmentShaderMode = (FragmentShaderMode)reader.ReadByte();
                ShaderUserSetting = reader.ReadByte();
                FragmentSoftEdge = reader.ReadByte();
                FragmentReflectionAlpha = reader.ReadByte();
                ShaderParam0 = reader.ReadSingle();
                ShaderParam1 = reader.ReadSingle();
                FragmentSoftEdgeFadeDistance = reader.ReadSingle();
                FragmentSoftEdgeVolume = reader.ReadSingle();
                UserMacros1 = reader.ReadBytes(16);
                UserMacros2 = reader.ReadBytes(16);
                ShaderUserFlag = reader.ReadUInt32();
                ShaderUserSwitchFlag = reader.ReadUInt32();
            }
        }
    }

    public interface IFieldData
    {
        void ReadWiiU(BinaryFileReader reader);
    }

    public partial class TextureEmitter : IResData
    {
        public bool HasTexPtnAnim;
        public bool TexPtnAnimRandStart;
        public bool TexPtnAnimClamp;
        public byte TexPtnAnimIdxDiv;

        public byte TexPtnAnimNum;

        public short TexPtnAnimPeriod;
        public short TexPtnAnimUsedSize;
        public byte[] TexPtnAnimData = new byte[32];

        public Vector2 UvScaleInit { get; set; }

        public uint UvShiftAnimMode { get; set; }

        public Vector2 ScaleShift { get; set; }
        public Vector2 Scale { get; set; }
        public Vector2 ScaleRnd { get; set; }

        public Vector2 TranslateShift { get; set; }
        public Vector2 Translate { get; set; }
        public Vector2 TranslateRnd { get; set; }

        public float RotateShift { get; set; }
        public float Rotate { get; set; }
        public float RotateRnd { get; set; }

        public TexturePatternType PatternType
        {
            get
            {
                if (!HasTexPtnAnim) return TexturePatternType.None;

                if (TexPtnAnimClamp) return TexturePatternType.Clamp;
                if (TexPtnAnimRandStart) return TexturePatternType.Random;
                if (TexPtnAnimPeriod == 0)
                    return TexturePatternType.FitLifespan;
                else
                    return TexturePatternType.Loop;
            }
            set
            {
                if (value != TexturePatternType.None)
                    HasTexPtnAnim = true;
                else
                {
                    HasTexPtnAnim = false;
                    return;
                }

                TexPtnAnimClamp = value == TexturePatternType.Clamp;
                TexPtnAnimRandStart = value == TexturePatternType.Random;
                if (value == TexturePatternType.FitLifespan)
                    TexPtnAnimPeriod = 0;
                else
                    TexPtnAnimPeriod = 1;
            }
        }

        public TextureRepeat RepeatMode
        {
            get
            {
                if (UvScaleInit.X == 1 && UvScaleInit.Y == 1)
                    return TextureRepeat.Repeat_1x1;
                if (UvScaleInit.X == 1 && UvScaleInit.Y == 2)
                    return TextureRepeat.Repeat_1x2;
                if (UvScaleInit.X == 2 && UvScaleInit.Y == 1)
                    return TextureRepeat.Repeat_2x1;

                return TextureRepeat.Repeat_2x2;
            }
            set
            {
                switch (value)
                {
                    case TextureRepeat.Repeat_1x1: UvScaleInit = new Vector2(1, 1); break;
                    case TextureRepeat.Repeat_1x2: UvScaleInit = new Vector2(1, 2); break;
                    case TextureRepeat.Repeat_2x1: UvScaleInit = new Vector2(2, 1); break;
                    case TextureRepeat.Repeat_2x2: UvScaleInit = new Vector2(2, 2); break;
                }
            }
        }

        public void Read(BinaryFileReader reader)
        {
            if (!reader.PTCL.IsSwitch)
                ReadWiiU(reader);
        }

        public void Write(BinaryFileWriter writer)
        {
        }
    }

    public struct FieldRandomData : IFieldData
    {
        public int period;
        public Vector3 randomVelScale;

        public void ReadWiiU(BinaryFileReader reader)
        {
            period = reader.ReadInt32();
            randomVelScale = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
        }
    }

    public struct FieldMagnetData : IFieldData
    {
        public float strength;
        public Vector3 pos;
        public uint flags;

        public void ReadWiiU(BinaryFileReader reader)
        {
            strength = reader.ReadSingle();
            pos = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
            flags = reader.ReadUInt32();
        }
    }

    public struct FieldSpinData : IFieldData
    {
        public int angle;
        public uint axis;
        public float diffusionVel;

        public void ReadWiiU(BinaryFileReader reader)
        {
            angle = reader.ReadInt32();
            axis = reader.ReadUInt32();
            diffusionVel = reader.ReadSingle();
        }
    }

    public struct FieldCollisionData : IFieldData
    {
        public ushort collisionType;
        public ushort coordSystem;
        public float y;
        public float friction;

        public void ReadWiiU(BinaryFileReader reader)
        {
            collisionType = reader.ReadUInt16();
            coordSystem = reader.ReadUInt16();
            y = reader.ReadUInt32();
            friction = reader.ReadSingle();
        }
    }

    public struct FieldConvergenceData : IFieldData
    {
        public Vector3 pos;
        public float strength;

        public void ReadWiiU(BinaryFileReader reader)
        {
            pos = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
            if (reader.PTCL.FileHeader.Version >= 55)
                reader.ReadInt32();
            strength = reader.ReadSingle();
        }
    }

    public struct FieldPosAddData : IFieldData
    {
        public Vector3 posAdd;

        public void ReadWiiU(BinaryFileReader reader)
        {
            posAdd = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
        }
    }

    public struct FieldCurlNoiseData : IFieldData
    {
        public void ReadWiiU(BinaryFileReader reader)
        {
            reader.ReadBytes(36);
        }
    }
    
    public struct FluctuationData
    {
        public float amplitude;
        public float frequency;
        public uint enableRandom;

        public void ReadWiiU(BinaryFileReader reader)
        {
            amplitude = reader.ReadSingle();
            frequency = reader.ReadSingle();
            enableRandom = reader.ReadUInt32();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EffectLibrary
{
    public class ParticleSettings
    {
        public static float EmissionSpeed = 1.0f;

        public static bool UseFixedFrameCount = false;
        public static float FixedFrameCount = 1000.0f;

        public static bool RespawnAtStartFrame = true;
    }
}

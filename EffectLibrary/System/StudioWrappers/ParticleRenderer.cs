﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GLFrameworkEngine;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace EffectLibrary
{
    public class ParticleRenderer : GenericRenderer, IPickable, IRayCastPicking
    {
        public static bool RespawnEmitterSet = false;

        public static bool FadeOnKill = false;

        public uint ResourceID = 0;

        public byte ActiveGroupID = 0;

        public Handle[] EmitterSetHandles;

        public bool IsHovered { get; set; }

        public void DrawColorPicking(GLContext context) { }

        IFileHeader Header;

        public ParticleRenderer(IFileHeader header)
        {
            Header = header;
            InitSystem(header);
        }

        public BoundingNode GetBounding()
        {
            float size = 0;

            return new BoundingNode()
            {
                Box = new BoundingBox()
                {
                    Max = new Vector3(size, size, size),
                    Min = new Vector3(-size, -size, -size),
                }
            };
        }

        private void InitSystem(IFileHeader header)
        {
            var system = EffectSystem.ActiveSystem;
            var heap = new Heap();
            var config = new Config();
            var resource = new Resource();

            ResourceID = 0;

            system.Initialize(heap, config);
            resource.Initialize(heap, header, ResourceID, system);

            system.RemovePtcl();
            system.EntryResource(null, resource, ResourceID);

            EmitterSetHandles = new Handle[header.EmitterSetList.Count];
        }

        public override void DrawModel(GLContext control, Pass pass, Vector4 highlightColor)
        {
            if (pass != Pass.TRANSPARENT)
                return;

            ScreenBufferTexture.FilterScreen(control);

            bool calculate = animFrameUpdateNum != 0;

            //Force srgb correction for the render output
            if (Header.Version != 40)
                control.UseSRBFrameBuffer = true;
            else
                control.UseSRBFrameBuffer = false;

            //Get the global system instance.
            var system = EffectSystem.ActiveSystem;
            var emissionSpeed = ParticleSettings.EmissionSpeed;

            //Begin the frame data
            system.BeginFrame();

            if (calculate)
            {
                system.SwapDoubleBuffer();

                //Calculate the emitters to spawn particle instances
                system.CalcEmitter(ActiveGroupID, emissionSpeed);
                //Calculate the particle attribute data
                system.CalcParticle(true);
                system.CalcParticleStreamOut();
            }
            else
                system.ResetStripe();

            var cam = control.Camera;
            var camPos = cam.TargetPosition;

            //Inspect camera uses the distance for Z
            if (cam.Controller is InspectCameraController)
                camPos.Z = cam.TargetDistance;

            //Begin the render and pass the camera parameters to make the view block.
            system.BeginRender(control,
                cam.ProjectionMatrix,
                cam.ViewMatrix,
                camPos, cam.ZNear, cam.ZFar);

            for (int i = 0; i < 64u; i++)
            {
                for (EmitterInstance emitter = system.emitterGroups[i]; emitter != null; emitter = emitter.Next)
                {
                    //Render out the emitter data
                    system.RenderEmitter(emitter, true, null);
                    //Reset bindings
                    GL.BindTexture(TextureTarget.Texture2D, 0);
                }
            }

            //End the render
            system.EndRender();

            if (calculate)
                system.Calc(true);

            control.CurrentShader = null;
            ResetRenderPipeline();

            if (animFrameUpdateNum > 0)
                animFrameUpdateNum -= 1;

            //DrawRayBoundary(control);
        }

        private void DrawRayBoundary(GLContext control)
        {
            var bounding = GetBounding();

            var shader = GlobalShaders.GetShader("PICKING");
            control.CurrentShader = shader;
            control.CurrentShader.SetVector4("color", new Vector4(1));

            Matrix4 transform = this.Transform.TransformMatrix;
            control.CurrentShader.SetMatrix4x4("mtxMdl", ref transform);

            BoundingBoxRender.Draw(control, bounding.Box.Min, bounding.Box.Max);
        }

        uint animFrameUpdateNum;

        public void OnNextFrame(float frame)
        {
            //Update calculations atleast twice to update properly
            animFrameUpdateNum = 2;

            if (frame == 0 && ParticleSettings.RespawnAtStartFrame)
                RespawnEmitterSet = true;
        }

        public void SpawnEmitterSet(int ID, byte groupID)
        {
            if (groupID > 63)
                throw new Exception($"Group ID must be below 63.");

            Transform = new GLTransform();

            Matrix4 matrixSRT = Header.EmitterSetList[ID].MatrixSRT;

            ActiveGroupID = groupID;

            var system = EffectSystem.ActiveSystem;
            //Check if the emitter set handle has been created.
            SetupEmitterSetHandle(system, matrixSRT, ID, ActiveGroupID);

            this.Transform.TransformUpdated += delegate
            {
              //  Header.EmitterSetList[ActiveEmitterSetID].MatrixSRT = this.Transform.TransformMatrix;
                Header.EmitterSetList[ID].EmitterSetHandle.SetMatrix(this.Transform.TransformMatrix);
            };
        }

        public void KillEmitterSet(byte groupID)
        {
            this.TransformChanged = null;
            Transform = new GLTransform();

            if (FadeOnKill)
            {
                for (int i = 0; i < EmitterSetHandles.Length; i++)
                    KillEmitterSetFade(EmitterSetHandles[i]);
            }
            else
            {
                for (int i = 0; i < EmitterSetHandles.Length; i++)
                    EmitterSetHandles[i] = null;

                var system = EffectSystem.ActiveSystem;
                system.KillEmitterGroup(groupID);
            }

            GC.Collect();
        }

        private void KillEmitterSetFade(Handle handle)
        {
            if (handle == null)
                return;

            var emitterSet = handle.emitterSet;
            if (emitterSet != null && handle.createID == emitterSet.createID && emitterSet.numEmitter >= 1)
                emitterSet.doFade = true;
            handle.emitterSet = null;
        }

        public void SetupEmitterSetHandle(EffectSystem system, Matrix4 matrixSRT, int index, byte groupID)
        {
            if (EmitterSetHandles.Length <= index)
                return;

            EmitterSetHandles[index] = new Handle();
            system.CreateEmitterSetID(EmitterSetHandles[index], Matrix4.Identity, index, ResourceID, groupID, 0xFFFFFFFF);
            if (EmitterSetHandles[index].emitterSet == null)
                return;

            this.Header.EmitterSetList[index].EmitterSetHandle = EmitterSetHandles[index].emitterSet;

            EmitterSetHandles[index].emitterSet.SetMatrix(this.Header.EmitterSetList[index].MatrixSRT);
        }

        private void ResetRenderPipeline()
        {
            GL.BindTexture(TextureTarget.Texture2D, 0);
            GL.Enable(EnableCap.DepthTest);
            GL.DepthFunc(DepthFunction.Lequal);
            GL.DepthMask(true);
            GL.Disable(EnableCap.TextureCubeMapSeamless);
            GL.AlphaFunc(AlphaFunction.Lequal, 0.5f);
            GL.Disable(EnableCap.AlphaTest);
            GL.Disable(EnableCap.Blend);
            GL.Enable(EnableCap.CullFace);
            GL.CullFace(CullFaceMode.Back);
            GL.UseProgram(0);
        }
    }
}

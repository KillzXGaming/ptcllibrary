﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;

namespace EffectLibrary
{
    public partial class EffectSystem
    {
        public void BeginFrame()
        {
            numCalcEmitter = 0;
            numCalcParticle = 0;
            numCalcStripe = 0;
            numEmittedParticle = 0;
            activeGroupsFlg = 0;

            for (int i = 0; i < (int)CpuCore.Max; i++)
                _570[i] = new uint[64];
        }

        public void SwapDoubleBuffer()
        {
            for (int i = 0; i < 64u; i++)
            {
                var emitter = emitterGroups[i];
                if (emitter == null)
                    continue;

                emitter.IsCalculated = false;
                emitter.PtclAttributeBuffer = null;
                emitter.ChildPtclAttributeBuffer = null;
                emitter.StripeVertexBuffer = null;
                emitter.DynamicUniformBlock = null;
                emitter.ChildDynamicUniformBlock = null;
            }

            for (int i = 0; i < (int)CpuCore.Max; i++)
                renderers[i].SwapDoubleBuffer();

            doubleBufferSwapped = 1;
        }

        public void ResetStripe()
        {
            for (int i = 0; i < (int)CpuCore.Max; i++)
                renderers[i].ResetStripe();
        }

        public void CalcEmitter(byte groupID, float emissionSpeed = 1.0f)
        {
            activeGroupsFlg |= 1UL << groupID;
            for (EmitterInstance emitter = emitterGroups[groupID]; emitter != null; emitter = emitter.Next)
                CalcEmitter(emitter, emissionSpeed);
        }

        public void CalcParticleStreamOut()
        {
            for (int i = 0; i < 64u; i++)
            {
                for (EmitterInstance emitter = emitterGroups[i]; emitter != null; emitter = emitter.Next)
                {
                    renderers[(int)CpuCore._1].DrawStreamOut(emitter);
                }
            }
        }

        public void CalcParticle(EmitterInstance emitter, CpuCore core)
        {
            if (emitter == null || emitter.calc == null)
                return;

           /* if (emitter.PositionStreamOut.StreamOutIDs[0] != -1) {
                renderers[(int)CpuCore._1].SetupStreamout(emitter);
            }*/

            bool noCalcBehavior = false;
            if ((activeGroupsFlg & (1UL << emitter.groupID)) == 0)
                noCalcBehavior = true;
            if (emitter.emitterSet.noCalc != 0)
                noCalcBehavior = true;

            var callback = GetCustomShaderEmitterPostCalcCallback(emitter.data.ShaderUserSetting);
            if (callback != null)
            {
                ShaderEmitterPostCalcArg arg = new ShaderEmitterPostCalcArg()
                {
                    emitter = emitter,
                    noCalcBehavior = noCalcBehavior,
                    childParticle = false,
                };
                callback(arg);
            }

            numCalcParticle += emitter.calc.CalcParticle(emitter, core, noCalcBehavior, false);
            _570[(int)core][emitter.groupID] |= (uint)(1 << emitter.data._29C);
        }

        public void CalcChildParticle(EmitterInstance emitter, CpuCore core)
        {
            if (emitter == null || emitter.calc == null)
                return;

            bool noCalcBehavior = false;
            if ((activeGroupsFlg & (1UL << emitter.groupID)) == 0)
                noCalcBehavior = true;
            if (emitter.emitterSet.noCalc != 0)
                noCalcBehavior = true;

            if (emitter.HasChild())
            {
                var callback = GetCustomShaderEmitterPostCalcCallback(emitter.data.ShaderUserSetting);
                if (callback != null)
                {
                    ShaderEmitterPostCalcArg arg = new ShaderEmitterPostCalcArg()
                    {
                        emitter = emitter,
                        noCalcBehavior = noCalcBehavior,
                        childParticle = false,
                    };
                    callback(arg);
                }
            }
            if (emitter.HasChild())
                numCalcParticle += emitter.calc.CalcChildParticle(emitter, core, noCalcBehavior, false);
        }

        public void FlushCache()
        {
            for (int i = 0; i < (int)CpuCore.Max; i++)
                renderers[i].FlushCache();
        }

        public void FlushGpuCache()
        {

        }

        public void CalcEmitter(EmitterInstance emitter, float emissionSpeed)
        {
            if (emitter.emitterSet.noCalc == 0)
            {
                if (emitter.emissionSpeed != emissionSpeed)
                {
                    emitter.emitCounter = 0.0f;
                    emitter.preCalcCounter = emitter.counter;
                    emitter.emitLostTime = 0.0f;
                    emitter.emissionSpeed = emissionSpeed;
                }

                if (GetCurrentCustomActionEmitterPreCalcCallback(emitter) != null)
                {
                    EmitterPreCalcArg arg = new EmitterPreCalcArg{ emitter = emitter };
                    GetCurrentCustomActionEmitterPreCalcCallback(emitter)(arg);
                }
                else
                {
                    emitter.calc.CalcEmitter(emitter);
                }

                if (GetCurrentCustomActionEmitterPostCalcCallback(emitter) != null)
                {
                    EmitterPostCalcArg arg = new EmitterPostCalcArg { emitter = emitter };
                    GetCurrentCustomActionEmitterPostCalcCallback(emitter)(arg);
                }

                numCalcEmitter++;
            }
        }

        public void CalcParticle(bool flushCache)
        {
            for (int i = 0; i < 64u; i++)
            {
                for (EmitterInstance emitter = emitterGroups[i]; emitter != null; emitter = emitter.Next)
                {
                    CalcParticle(emitter, CpuCore._1);
                    _570[(int)CpuCore._1][emitter.groupID] |= (uint)(1 << emitter.data._29C);

                    if (emitter.HasChild())
                    {
                        EmitChildParticle();
                        CalcChildParticle(emitter, CpuCore._1);
                    }
                }
            }

            RemovePtcl();

            if (flushCache)
            {
                FlushCache();
                FlushGpuCache();
            }
        }

        public void Calc(bool flushCache)
        {
            if (doubleBufferSwapped == 0)
            {
                SwapDoubleBuffer();
                if (activeGroupsFlg != 0)
                {
                    activeGroupsFlg = 0;
                    CalcParticle(flushCache);
                }
            }
            doubleBufferSwapped = 0;
        }

        public void BeginRender(GLFrameworkEngine.GLContext context, Matrix4 projection, Matrix4 view, Vector3 cameraWorldPos, float zNear, float zFar)
        {
            this.view[OSGetCoreId()] = view;
            renderers[OSGetCoreId()].BeginRender(context, projection, view, cameraWorldPos, zNear, zFar);
        }

        public void RenderEmitter(EmitterInstance emitter, bool flushCache, object argData)
        {
            if (emitter == null || !emitter.data.isVisible )
                return;

            CpuCore core = (CpuCore)OSGetCoreId();

            if (!emitter.IsCalculated && (emitter.numParticles != 0 || emitter.numChildParticles != 0))
            {
                if (emitter.numParticles > 0)
                    emitter.calc.CalcParticle(emitter, core, true, false);

                if (emitter.HasChild() && emitter.numChildParticles > 0)
                    emitter.calc.CalcChildParticle(emitter, core, true, false);

                if (flushCache)
                {
                    FlushCache();
                    FlushGpuCache();
                }
            }

            if (GetCurrentCustomActionEmitterDrawOverrideCallback(emitter) != null)
            {
                EmitterDrawOverrideArg arg = new EmitterDrawOverrideArg
                {
                    emitter = emitter,
                    renderer = renderers[(int)core],
                    flushCache = flushCache,
                    argData = argData,
                };
                GetCurrentCustomActionEmitterDrawOverrideCallback(emitter)(arg);
            }
            else
            {
                renderers[(int)core].EntryParticle(emitter, flushCache, argData);
            }
        }

        public void EndRender()
        {
            renderers[OSGetCoreId()].EndRender();
        }

        public int OSGetCoreId() => (int)CpuCore._1;
    }
}

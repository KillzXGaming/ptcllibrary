﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EffectLibrary
{
    public class Config
    {
        public uint numEmitterMax;
        public uint numParticleMax;
        public uint numEmitterSetMax;
        public uint numResourceMax;
        public uint numStripeMax;
        public uint doubleBufferSize;

        private Heap heap;

        public Config() {
            heap = null;
            numEmitterMax = 0x100;
            numParticleMax = 0x1000;
            numEmitterSetMax = 0x80;
            numResourceMax = 1;
            numStripeMax = 0x100;
            doubleBufferSize = 0x20000;
        }

        public Heap GetHeap() {
            return heap;
        }

    }
}

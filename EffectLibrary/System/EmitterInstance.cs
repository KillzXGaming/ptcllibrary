﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EffectLibrary.WiiU;
using OpenTK;

namespace EffectLibrary
{
    public class EmitterInstance
    {
        public EmitterType type;

        public EmitterInstance Next;
        public EmitterInstance Prev;

        public PtclRandom random = new PtclRandom();

        public ParticleShader[] shader;
        public ParticleShader[] childShader;

        public float counter;
        public float counter2;
        public float emitCounter;
        public float preCalcCounter;
        public float emitLostTime;
        public uint numParticles;
        public uint numChildParticles;
        public int groupID;

        public Matrix4 matrixRT;
        public Matrix4 matrixSRT;

        public float emissionInterval;
        public float fadeAlpha;
        public float emissionSpeed;

        public Vector3 scaleRandom;
        public Vector3 rotateRandom;
        public Vector3 translateRandom;

        public EmitterSet? emitterSet;
        public EmitterController controller;
        public uint emitterSetCreateID;

        public Matrix4 animMatrixRT;
        public Matrix4 animMatrixSRT;

        public float emitLostDistance;

        public Dictionary<AnimGroupType, float> AnimData;
        public KeyGroup[] keyGroups;

        public PtclFollowType ptclFollowType;

        public PtclInstance particleHead;
        public PtclInstance childParticleHead;
        public PtclInstance particleTail;
        public PtclInstance childParticleTail;

        public PrimitiveRenderer primitive;
        public PrimitiveRenderer childPrimitive;

        public EmitterCalc calc;

        public SimpleEmitterData data;

        float _1EA;

        public Vector3 prevPos;

        public bool prevPosSet;

        public bool IsCalculated;
        public bool IsEmitted;

        public float emitLostRate;

        public ParticleBehavior particleBehaviorFlg;
        public ShaderAvailableAttrib shaderAvailableAttribFlg;
        public ShaderAvailableAttrib childShaderAvailableAttribFlg;
        public uint numDrawParticle;
        public uint numDrawChildParticle;
        public uint numDrawStripe;

        public PtclAttributeBuffer[] PtclAttributeBuffer;
        public PtclAttributeBuffer[] ChildPtclAttributeBuffer;
        public StripeVertexBuffer[] StripeVertexBuffer;

        public TransformFeedbackBuffer PositionStreamOut;
        public TransformFeedbackBuffer VectorStreamOut;

        public EmitterStaticUniformBlock? StaticUniformBlock;
        public EmitterStaticUniformBlock? ChildStaticUniformBlock;
        public EmitterDynamicUniformBlock? DynamicUniformBlock;
        public EmitterDynamicUniformBlock? ChildDynamicUniformBlock;

        public void Init(SimpleEmitterData data)
        {
            this.data = data;
            counter = 0.0f;
            counter2 = 0.0f;
            emitCounter = 0.0f;
            preCalcCounter = 0.0f;
            emitLostTime = 0.0f;
            numParticles = 0;
            numChildParticles = 0;

            emissionInterval = data.EmissionInterval - random.GetS32(data.EmissionIntervalRandomness);
            fadeAlpha = 1.0f;
            emissionSpeed = 1.0f;

            ptclFollowType = data.PtclFollowType;

            particleHead = null;
            childParticleHead = null;
            particleTail = null;
            childParticleTail = null;

            scaleRandom.X = 0.0f;
            scaleRandom.Y = 0.0f;
            scaleRandom.Z = 0.0f;

            rotateRandom.X = random.GetF32Range(-1.0f, 1.0f) * data.RotationRandom.X;
            rotateRandom.Y = random.GetF32Range(-1.0f, 1.0f) * data.RotationRandom.Y;
            rotateRandom.Z = random.GetF32Range(-1.0f, 1.0f) * data.RotationRandom.Z;

            translateRandom.X = random.GetF32Range(-1.0f, 1.0f) * data.TranslationRandom.X;
            translateRandom.Y = random.GetF32Range(-1.0f, 1.0f) * data.TranslationRandom.Y;
            translateRandom.Z = random.GetF32Range(-1.0f, 1.0f) * data.TranslationRandom.Z;

            _1EA = 0;

            AnimData = new Dictionary<AnimGroupType, float>();
            for (int i = 0; i < 25; i++)
                AnimData.Add((AnimGroupType)i, 0);

            emitLostRate = 0.0f;
            IsEmitted = false;
            IsCalculated = false;

            primitive = null;
            childPrimitive = null;

            animMatrixRT = OpenTK.Matrix4.Identity;
            animMatrixSRT = OpenTK.Matrix4.Identity;

            prevPosSet = false;
            emitLostDistance = 0.0f;

            if (data.KeyTable != null)
                keyGroups = data.KeyTable.KeyGroups.ToArray();
            else
                keyGroups = null;

            StaticUniformBlock = new EmitterStaticUniformBlock();
            ChildStaticUniformBlock = new EmitterStaticUniformBlock();

            PositionStreamOut = new TransformFeedbackBuffer();
            VectorStreamOut = new TransformFeedbackBuffer();

            StaticUniformBlock.Init();
            ChildStaticUniformBlock.Init();
        }

        public void UpdateChildStaticUniformBlock(EmitterStaticUniformBlock uniformBlock, ChildData data)
        {
            uniformBlock.flag = 0;
            uniformBlock.UvScaleInit = new Vector4(data.TextureEmitter.UvScaleInit.X, data.TextureEmitter.UvScaleInit.Y, 0, 0);
            uniformBlock.RotBasis = new Vector4(data.RotBasis.X, data.RotBasis.Y, 0,0);
            uniformBlock.ShaderParam = new Vector4(data.ShaderParam0, data.ShaderParam1,
                data.FragmentSoftEdgeFadeDistance, data.FragmentSoftEdgeVolume);

            for (int i = 0; i < 1; i++)
            {
                uniformBlock.Textures[i].Translation = new Vector2(
                    data.TextureEmitter.Translate.X,
                    data.TextureEmitter.Translate.Y);
                uniformBlock.Textures[i].TranslationShift = new Vector2(
                    data.TextureEmitter.TranslateShift.X,
                   data.TextureEmitter.TranslateShift.Y);
                uniformBlock.Textures[i].TranslationRnd = new Vector2(
                    data.TextureEmitter.TranslateRnd.X,
                    data.TextureEmitter.TranslateRnd.Y);

                uniformBlock.Textures[i].Scale = new Vector2(
                   data.TextureEmitter.Scale.X,
                   data.TextureEmitter.Scale.Y);
                uniformBlock.Textures[i].ScaleShift = new Vector2(
                    data.TextureEmitter.ScaleShift.X,
                    data.TextureEmitter.ScaleShift.Y);
                uniformBlock.Textures[i].ScaleRnd = new Vector2(
                    data.TextureEmitter.ScaleRnd.X,
                    data.TextureEmitter.ScaleRnd.Y);

                uniformBlock.Textures[i].Rotation = data.TextureEmitter.Rotate;
                uniformBlock.Textures[i].RotationRnd = data.TextureEmitter.RotateRnd;
                uniformBlock.Textures[i].RotationShift = data.TextureEmitter.RotateShift;

                for (int j = 0; j < 32; j++)
                {
                    if (i == 0)
                        uniformBlock.PtnAnim0_Tbl[j] = data.TextureEmitter.TexPtnAnimData[j];
                    if (i == 1)
                        uniformBlock.PtnAnim1_Tbl[j] = data.TextureEmitter.TexPtnAnimData[j];
                }

                if (i == 0)
                    uniformBlock.PtnAnim0_Prm = new Vector4(data.TextureEmitter.TexPtnAnimNum, 0, 0, 0);
                if (i == 1)
                    uniformBlock.PtnAnim1_Prm = new Vector4(data.TextureEmitter.TexPtnAnimNum, 0, 0, 0);

                uniformBlock.Textures[i].Param = new Vector4(data.TextureEmitter.UvScaleInit.X,
                    data.TextureEmitter.UvScaleInit.Y, 1, 1);
            }

            uniformBlock.GravityParam = new Vector4(data.Gravity.X, data.Gravity.Y, data.Gravity.Z, 0);
            uniformBlock.NearAlpha = new Vector4(
                data.NearAlphaParameters.X, data.NearAlphaParameters.Y,
                data.NearAlphaParameters.Z, data.NearAlphaParameters.W);
        }

        public void UpdateEmitterStaticUniformBlock(EmitterStaticUniformBlock uniformBlock, SimpleEmitterData data)
        {
            uniformBlock.flag = 0;
            for (int i = 0; i < (int)ShaderType.Max; i++)
            {
                ParticleShader shader = this.shader[i];
                if (shader == null)
                    continue;

                shader.ShaderWrapper.InitAttributeLocations(shader);

                if (shader.attrSclBuffer != -1)
                    shaderAvailableAttribFlg |= ShaderAvailableAttrib.Scale;
                if (shader.attrTexAnimBuffer != -1)
                    shaderAvailableAttribFlg |= ShaderAvailableAttrib.TexAnim;
                if (shader.attrSubTexAnimBuffer != -1)
                    shaderAvailableAttribFlg |= ShaderAvailableAttrib.SubTexAnim;
                if (shader.attrWldPosBuffer != -1)
                    shaderAvailableAttribFlg |= ShaderAvailableAttrib.WorldPos;
                if (shader.attrWldPosDfBuffer != -1)
                    shaderAvailableAttribFlg |= ShaderAvailableAttrib.WorldPosDif;
                if (shader.attrColor0Buffer != -1)
                    shaderAvailableAttribFlg |= ShaderAvailableAttrib.Color0;
                if (shader.attrColor1Buffer != -1)
                    shaderAvailableAttribFlg |= ShaderAvailableAttrib.Color1;
                if (shader.attrRotBuffer != -1)
                    shaderAvailableAttribFlg |= ShaderAvailableAttrib.Rot;
                if (shader.attrEmMat0Buffer != -1)
                    shaderAvailableAttribFlg |= ShaderAvailableAttrib.EmMat;
            }

            if (childShader != null)
            {
                for (int i = 0; i < (int)ShaderType.Max; i++)
                {
                    ParticleShader shader = this.childShader[i];
                    if (shader == null)
                        continue;

                    shader.ShaderWrapper.InitAttributeLocations(shader);

                    if (shader.attrSclBuffer != -1)
                        childShaderAvailableAttribFlg |= ShaderAvailableAttrib.Scale;
                    if (shader.attrTexAnimBuffer != -1)
                        childShaderAvailableAttribFlg |= ShaderAvailableAttrib.TexAnim;
                    if (shader.attrSubTexAnimBuffer != -1)
                        childShaderAvailableAttribFlg |= ShaderAvailableAttrib.SubTexAnim;
                    if (shader.attrWldPosBuffer != -1)
                        childShaderAvailableAttribFlg |= ShaderAvailableAttrib.WorldPos;
                    if (shader.attrWldPosDfBuffer != -1)
                        childShaderAvailableAttribFlg |= ShaderAvailableAttrib.WorldPosDif;
                    if (shader.attrColor0Buffer != -1)
                        childShaderAvailableAttribFlg |= ShaderAvailableAttrib.Color0;
                    if (shader.attrColor1Buffer != -1)
                        childShaderAvailableAttribFlg |= ShaderAvailableAttrib.Color1;
                    if (shader.attrRotBuffer != -1)
                        childShaderAvailableAttribFlg |= ShaderAvailableAttrib.Rot;
                    if (shader.attrEmMat0Buffer != -1)
                        childShaderAvailableAttribFlg |= ShaderAvailableAttrib.EmMat;
                }
            }


            particleBehaviorFlg = 0;

            if (data.AirResist != 1.0f)
                particleBehaviorFlg |= ParticleBehavior.AirResist;

            if (data.Gravity.Length != 0)
                particleBehaviorFlg |= ParticleBehavior.Gravity;

            if (data.ParticleRotationMode != VertexRotationMode.None)
                particleBehaviorFlg |= ParticleBehavior.Rotation;

            if (data.RotationInertia != 1.0f)
                particleBehaviorFlg |= ParticleBehavior.RotationInertia;

            if (shaderAvailableAttribFlg.HasFlag(ShaderAvailableAttrib.WorldPosDif))
                particleBehaviorFlg |= ParticleBehavior.WorldDiff;

            if (data.AnimAlpha0Func == AnimationFunctions.Key4Value3)
                particleBehaviorFlg |= ParticleBehavior.Alpha0Anim;

            if (data.AnimAlpha1Func == AnimationFunctions.Key4Value3)
                particleBehaviorFlg |= ParticleBehavior.Alpha1Anim;

            if (data.ScaleTime2 != -127 || data.ScaleTime3 != 100)
                particleBehaviorFlg |= ParticleBehavior.ScaleAnim;

            if (data.Color0Source == ColorSource.Key4Value3)
                particleBehaviorFlg |= ParticleBehavior.Color0Anim;

            if (data.Color1Source == ColorSource.Key4Value3)
                particleBehaviorFlg |= ParticleBehavior.Color1Anim;

            if (data.TextureEmitters[0].UvShiftAnimMode != 0) particleBehaviorFlg |= ParticleBehavior.UVShiftAnim0;
            if (data.TextureEmitters[0].HasTexPtnAnim) particleBehaviorFlg |= ParticleBehavior.PatternAnim0;

            if (data.Textures[0].HasData)
                particleBehaviorFlg |= ParticleBehavior.HasTexture1;

            if (data.TextureEmitters.Length > 1)
            {
                if (data.TextureEmitters[1].UvShiftAnimMode != 0) particleBehaviorFlg |= ParticleBehavior.UVShiftAnim1;
                if (data.TextureEmitters[1].HasTexPtnAnim) particleBehaviorFlg |= ParticleBehavior.PatternAnim1;

                if (data.Textures[1].HasData)
                    particleBehaviorFlg |= ParticleBehavior.HasTexture2;
            }
            if (data.TextureEmitters.Length > 2)
            {
                if (data.TextureEmitters[2].UvShiftAnimMode != 0) particleBehaviorFlg |= ParticleBehavior.UVShiftAnim1;
                if (data.TextureEmitters[2].HasTexPtnAnim) particleBehaviorFlg |= ParticleBehavior.PatternAnim1;

                if (data.Textures[2].HasData)
                    particleBehaviorFlg |= ParticleBehavior.HasTexture3;
            }

            uniformBlock.UvScaleInit = new Vector4(
                data.TextureEmitters[0].UvScaleInit.X,
                data.TextureEmitters[0].UvScaleInit.Y,
                data.TextureEmitters[1].UvScaleInit.X,
                data.TextureEmitters[1].UvScaleInit.Y);
            uniformBlock.RotBasis = new Vector4(data.RotationPivot.X, data.RotationPivot.Y, 0, 0);
            uniformBlock.ShaderParam = new Vector4(
                data.ShaderParam0, data.ShaderParam1,
                data.FragmentSoftEdgeFadeDistance,
                data.FragmentSoftEdgeVolume);

            if (!EffectSystem.ActiveSystem.HasResourceFlags(FileResourceFlags.HasGPUBehavior))
                return;

            //Setup static block for shader calculations used in newer versions

            for (int i = 0; i < 8; i++)
                uniformBlock.Color0[i] = data.ColorKeys[0, i];
            for (int i = 0; i < 8; i++)
                uniformBlock.Color1[i] = data.ColorKeys[1, i];

            uniformBlock.RotBasis = new Vector4(data.RotationPivot.X, data.RotationPivot.Y, 0, 0);
            uniformBlock.RotateVelocity = new Vector4(data.AngularVelocity, data.RotationInertia);
            uniformBlock.RotateVelocityRnd = new Vector4(data.AngularVelocityRandom, 0);
            uniformBlock.FresnelMinMax = new Vector4(data.FresnelAlphaMin, data.FresnelAlphaMax, 0, 1);

            uniformBlock.AlphaAnim0.X = data.AlphaKey0.StartValue;
            uniformBlock.AlphaAnim0.Y = data.AlphaKey0.StartValue + data.AlphaKey0.StartDifference;
            uniformBlock.AlphaAnim0.Z = data.AlphaKey0.Time2 / 100f;
            uniformBlock.AlphaAnim1.X = data.AlphaKey0.StartValue + data.AlphaKey0.StartDifference + data.AlphaKey0.EndDifference;
            uniformBlock.AlphaAnim1.Y = data.AlphaKey0.Time3 / 100f;

            uniformBlock.AlphaAnim1.Z = data.AlphaKey1.StartValue;
            uniformBlock.AlphaAnim1.W = data.AlphaKey1.StartValue + data.AlphaKey1.StartDifference;
            uniformBlock.AlphaAnim2.X = data.AlphaKey1.Time2 / 100f;
            uniformBlock.AlphaAnim2.Y = data.AlphaKey1.StartValue + data.AlphaKey1.StartDifference + data.AlphaKey1.EndDifference;
            uniformBlock.AlphaAnim2.Z = data.AlphaKey1.Time3 / 100f;
            uniformBlock.AlphaAnim2.W = 1;

            uniformBlock.ScaleAnim0.X = data.PtclScale.X;
            uniformBlock.ScaleAnim0.Y = data.PtclScale.Y;
            uniformBlock.ScaleAnim0.Z = data.PtclScaleStart.X;
            uniformBlock.ScaleAnim0.W = data.PtclScaleStart.Y;

            uniformBlock.ScaleAnim1.X = data.ScaleTime2 / 100f;
            uniformBlock.ScaleAnim1.Y = data.PtclScaleStart.X + data.PtclScaleStartDiff.X;
            uniformBlock.ScaleAnim1.Z = data.PtclScaleStart.Y + data.PtclScaleStartDiff.Y;
            uniformBlock.ScaleAnim1.W = data.PtclRotateRandom.X;

            uniformBlock.ScaleAnim2.X = data.ScaleTime3 / 100f;
            uniformBlock.ScaleAnim2.Y = data.PtclScaleStart.X + data.PtclScaleStartDiff.X + data.PtclScaleEndDiff.X;
            uniformBlock.ScaleAnim2.Z = data.PtclScaleStart.Y + data.PtclScaleStartDiff.Y + data.PtclScaleEndDiff.Y;
            uniformBlock.ScaleAnim2.W = data.PtclRotateRandom.Y;

            uniformBlock.ColorAnim0.X = data.ColorKey4Time2[0] / 100f;
            uniformBlock.ColorAnim0.Y = data.ColorKey4Time3[0] / 100f;
            uniformBlock.ColorAnim0.Z = data.ColorKey4Time4[0] / 100f;
            uniformBlock.ColorAnim0.W = data.ColorScale;

            uniformBlock.ColorAnim1.X = data.ColorKey4Time2[1] / 100f;
            uniformBlock.ColorAnim1.Y = data.ColorKey4Time3[1] / 100f;
            uniformBlock.ColorAnim1.Z = data.ColorKey4Time4[1] / 100f;
            uniformBlock.ColorAnim1.W = 1;

            uniformBlock.VecParam = new Vector4(data.AirResist, data.Direction.X, data.Direction.Y, data.Direction.Z);

            uniformBlock.ShaderParamAnim0 = new Vector4(1, 1, 0.25f, 0);
            uniformBlock.ShaderParamAnim1 = new Vector4(1, 0.75f, 0, 0);

            uniformBlock.GravityParam = new Vector4(data.Gravity.X, data.Gravity.Y, data.Gravity.Z, 0);
            uniformBlock.NearAlpha = new Vector4(
                data.NearAlphaParameters.X, data.NearAlphaParameters.Y,
                data.NearAlphaParameters.Z, data.NearAlphaParameters.W);

            for (int i = 0; i < data.TextureEmitters.Length; i++)
            {
                uniformBlock.Textures[i].Translation = new Vector2(
                    data.TextureEmitters[i].Translate.X,
                    data.TextureEmitters[i].Translate.Y);
                uniformBlock.Textures[i].TranslationShift = new Vector2(
                    data.TextureEmitters[i].TranslateShift.X,
                    data.TextureEmitters[i].TranslateShift.Y);
                uniformBlock.Textures[i].TranslationRnd = new Vector2(
                    data.TextureEmitters[i].TranslateRnd.X,
                    data.TextureEmitters[i].TranslateRnd.Y);

                uniformBlock.Textures[i].Scale = new Vector2(
                    data.TextureEmitters[i].Scale.X,
                    data.TextureEmitters[i].Scale.Y);
                uniformBlock.Textures[i].ScaleShift = new Vector2(
                    data.TextureEmitters[i].ScaleShift.X,
                    data.TextureEmitters[i].ScaleShift.Y);
                uniformBlock.Textures[i].ScaleRnd = new Vector2(
                    data.TextureEmitters[i].ScaleRnd.X,
                    data.TextureEmitters[i].ScaleRnd.Y);

                uniformBlock.Textures[i].Rotation = data.TextureEmitters[i].Rotate;
                uniformBlock.Textures[i].RotationRnd = data.TextureEmitters[i].RotateRnd;
                uniformBlock.Textures[i].RotationShift = data.TextureEmitters[i].RotateShift;

                for (int j = 0; j < 32; j++)
                {
                    if (i == 0)
                        uniformBlock.PtnAnim0_Tbl[j] = data.TextureEmitters[i].TexPtnAnimData[j];
                    if (i == 1)
                        uniformBlock.PtnAnim1_Tbl[j] = data.TextureEmitters[i].TexPtnAnimData[j];
                }

                if (i == 0)
                    uniformBlock.PtnAnim0_Prm = new Vector4(data.TextureEmitters[i].TexPtnAnimNum, 0, 0, 0);
                if (i == 1)
                    uniformBlock.PtnAnim1_Prm = new Vector4(data.TextureEmitters[i].TexPtnAnimNum, 0, 0, 0);

                uniformBlock.Textures[i].Param = new Vector4(data.TextureEmitters[i].UvScaleInit.X,
                    data.TextureEmitters[i].UvScaleInit.Y, 1, 1);
            }
        }

        public void UpdateResInfo()
        {
            ptclFollowType = data.PtclFollowType;

            AnimData[AnimGroupType.EmissionRatio] = data.EmissionRatio;
            AnimData[AnimGroupType.Lifespan] = data.MaxLifespan;
            AnimData[AnimGroupType.AllDirectionVelocity] = data.AllDirectionVelocity;
            AnimData[AnimGroupType.DirectionVelocity] = data.DirectionVelocity;
            AnimData[AnimGroupType.Alpha0] = data.EmitterAlpha;
            AnimData[AnimGroupType.Color0R] = data.Color0.X;
            AnimData[AnimGroupType.Color0G] = data.Color0.Y;
            AnimData[AnimGroupType.Color0B] = data.Color0.Z;
            AnimData[AnimGroupType.Color1R] = data.Color1.X;
            AnimData[AnimGroupType.Color1G] = data.Color1.Y;
            AnimData[AnimGroupType.Color1B] = data.Color1.Z;
            AnimData[AnimGroupType.EmissionShapeScaleX] = data.EmissionShapeScale.X;
            AnimData[AnimGroupType.EmissionShapeScaleY] = data.EmissionShapeScale.Y;
            AnimData[AnimGroupType.EmissionShapeScaleZ] = data.EmissionShapeScale.Z;
            AnimData[AnimGroupType.PtclScaleX] = data.PtclScale.X;
            AnimData[AnimGroupType.PtclScaleY] = data.PtclScale.Y;

            AnimData[AnimGroupType.ScaleX] = data.BaseScale.X + scaleRandom.X;
            AnimData[AnimGroupType.ScaleY] = data.BaseScale.Y + scaleRandom.Y;
            AnimData[AnimGroupType.ScaleZ] = data.BaseScale.Z + scaleRandom.Z;
            AnimData[AnimGroupType.RotationX] = data.BaseRotation.X + rotateRandom.X;
            AnimData[AnimGroupType.RotationY] = data.BaseRotation.Y + rotateRandom.Y;
            AnimData[AnimGroupType.RotationZ] = data.BaseRotation.Z + rotateRandom.Z;
            AnimData[AnimGroupType.PositionX] = data.BaseTranslation.X + translateRandom.X;
            AnimData[AnimGroupType.PositionY] = data.BaseTranslation.Y + translateRandom.Y;
            AnimData[AnimGroupType.PositionZ] = data.BaseTranslation.Z + translateRandom.Z;
            AnimData[AnimGroupType.Gravity] = 1.0f;

            particleBehaviorFlg = 0;
            shaderAvailableAttribFlg = 0;
            childShaderAvailableAttribFlg = 0;

            UpdateEmitterStaticUniformBlock(StaticUniformBlock, data);

            if (shader[0].attrStreamOutPos != -1)
                UpdateStreamOut();

            if (HasChild())
                UpdateChildStaticUniformBlock(ChildStaticUniformBlock, GetChildData());
        }

        private void UpdateStreamOut()
        {
            int numParticles = 1024;

            int size = numParticles * Vector4.SizeInBytes;

            PositionStreamOut.Dispose();
            VectorStreamOut.Dispose();

            PositionStreamOut.Init(size);
            VectorStreamOut.Init(size);
        }

        public ChildData GetChildData() {
            return ((ComplexEmitterData)data).ChildData;
        }

        public bool HasChild()
        {
            if (data is ComplexEmitterData)
                return ((((ComplexEmitterData)data).childFlags & 1) != 0);
            else
                return false;
        }
    }
}

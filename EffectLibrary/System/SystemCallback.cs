﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EffectLibrary
{
    public partial class EffectSystem
    {
        Action<ParticleRemoveArg>[] customActionParticleRemoveCallback = new Action<ParticleRemoveArg>[(int)CustomActionCallBackID.Max];
        Action<EmitterPreCalcArg>[] customActionEmitterPreCalcCallback = new Action<EmitterPreCalcArg>[(int)CustomActionCallBackID.Max];
        Action<EmitterPostCalcArg>[] customActionEmitterPostCalcCallback = new Action<EmitterPostCalcArg>[(int)CustomActionCallBackID.Max];
        Action<EmitterDrawOverrideArg>[] customActionEmitterDrawOverrideCallback = new Action<EmitterDrawOverrideArg>[(int)CustomActionCallBackID.Max];
        Action<ParticleCalcArg>[] customActionParticleCalcCallback = new Action<ParticleCalcArg>[(int)CustomActionCallBackID.Max];
        Action<ParticleMakeAttrArg>[] customActionParticleMakeAttrCallback = new Action<ParticleMakeAttrArg>[(int)CustomActionCallBackID.Max];
        Func<ParticleEmitArg, bool>[] customActionParticleEmitCallback = new Func<ParticleEmitArg, bool>[(int)CustomActionCallBackID.Max];

        Action<ParticleMakeAttrArg>[] customActionParticleMakeAttributeCallback = new Action<ParticleMakeAttrArg>[(int)CustomActionCallBackID.Max];

        Action<ShaderEmitterPostCalcArg>[] customShaderEmitterPostCalcCallback = new Action<ShaderEmitterPostCalcArg>[(int)CustomShaderCallBackID.Max];
        Action<ShaderDrawOverrideArg>[] customShaderDrawOverrideCallback = new Action<ShaderDrawOverrideArg>[(int)CustomShaderCallBackID.Max];
        Action<RenderStateSetArg>[] customShaderRenderStateSetCallback = new Action<RenderStateSetArg>[(int)CustomShaderCallBackID.Max];

        //Emitter action callbacks

        public Action<ParticleRemoveArg> GetCurrentCustomActionParticleRemoveCallback(EmitterInstance emitter)
        {
            if (_8A8 != -1 || currentCallbackID != (uint)CustomActionCallBackID.Invalid)
                return customActionParticleRemoveCallback[(int)currentCallbackID];

            uint callbackID = emitter.data.CustomCallbackID;
            if (callbackID == (uint)CustomActionCallBackID.Invalid)
                return null;

            return customActionParticleRemoveCallback[(int)callbackID];
        }

        public Action<EmitterPostCalcArg> GetCurrentCustomActionEmitterPostCalcCallback(EmitterInstance emitter)
        {
            if (_8A8 != -1 || currentCallbackID != (uint)CustomActionCallBackID.Invalid)
                return customActionEmitterPostCalcCallback[(int)currentCallbackID];

            uint callbackID = emitter.data.CustomCallbackID;
            if (callbackID == (uint)CustomActionCallBackID.Invalid)
                return null;

            return customActionEmitterPostCalcCallback[(int)callbackID];
        }

        public Action<EmitterPreCalcArg> GetCurrentCustomActionEmitterPreCalcCallback(EmitterInstance emitter)
        {
            if (_8A8 != -1 || currentCallbackID != (uint)CustomActionCallBackID.Invalid)
                return customActionEmitterPreCalcCallback[(int)currentCallbackID];

            uint callbackID = emitter.data.CustomCallbackID;
            if (callbackID == (uint)CustomActionCallBackID.Invalid)
                return null;

            return customActionEmitterPreCalcCallback[(int)callbackID];
        }

        public Action<EmitterDrawOverrideArg> GetCurrentCustomActionEmitterDrawOverrideCallback(EmitterInstance emitter)
        {
            if (_8A8 != -1 || currentCallbackID != (uint)CustomActionCallBackID.Invalid)
                return customActionEmitterDrawOverrideCallback[(int)currentCallbackID];

            uint callbackID = emitter.data.CustomCallbackID;
            if (callbackID == (uint)CustomActionCallBackID.Invalid)
                return null;

            return customActionEmitterDrawOverrideCallback[(int)callbackID];
        }

        //Particle callbacks

        public Func<ParticleEmitArg, bool> GetCurrentCustomActionParticleEmitCallback(EmitterInstance emitter)
        {
            if (_8A8 != -1 || currentCallbackID != (uint)CustomActionCallBackID.Invalid)
                return customActionParticleEmitCallback[currentCallbackID];

            uint callbackID = emitter.data.CustomCallbackID;
            if (callbackID == (uint)CustomActionCallBackID.Invalid)
                return null;

            return customActionParticleEmitCallback[(int)callbackID];
        }

        public Action<ParticleCalcArg> GetCurrentCustomActionParticleCalcCallback(EmitterInstance emitter)
        {
            if (_8A8 != -1 || currentCallbackID != (uint)CustomActionCallBackID.Invalid)
                return customActionParticleCalcCallback[currentCallbackID];

            uint callbackID = emitter.data.CustomCallbackID;
            if (callbackID == (uint)CustomActionCallBackID.Invalid)
                return null;

            return customActionParticleCalcCallback[(int)callbackID];
        }

        public Action<ParticleMakeAttrArg> GetCurrentCustomActionParticleMakeAttributeCallback(EmitterInstance emitter)
        {
            if (_8A8 != -1 || currentCallbackID != (uint)CustomActionCallBackID.Invalid)
                return customActionParticleMakeAttributeCallback[currentCallbackID];

            uint callbackID = emitter.data.CustomCallbackID;
            if (callbackID == (uint)CustomActionCallBackID.Invalid)
                return null;

            return customActionParticleMakeAttributeCallback[(int)callbackID];
        }

        //Shader callbacks

        public Action<ShaderEmitterPostCalcArg> GetCustomShaderEmitterPostCalcCallback(int callbackID)
        {
            if (callbackID > (int)CustomShaderCallBackID.Max)
                return null;

            return customShaderEmitterPostCalcCallback[callbackID];
        }

        public Action<ShaderDrawOverrideArg> GetCustomShaderDrawOverrideCallback(int callbackID)
        {
            if (callbackID > (int)CustomShaderCallBackID.Max)
                return null;

            return customShaderDrawOverrideCallback[callbackID];
        }

        public Action<RenderStateSetArg> GetCustomShaderRenderStateSetCallback(int callbackID)
        {
            if (callbackID > (int)CustomShaderCallBackID.Max)
                return null;

            return customShaderRenderStateSetCallback[callbackID];
        }
    }

    public struct ParticleEmitArg { public PtclInstance ptcl; }

    public struct ParticleCalcArg
    {
        public EmitterInstance emitter;
        public PtclInstance ptcl;
        public CpuCore core;
        public bool noCalcBehavior;
    }

    public struct ParticleMakeAttrArg
    {
        public EmitterInstance emitter;
        public PtclInstance ptcl;
        public CpuCore core;
        public bool noCalcBehavior;
    }

    public struct ParticleRemoveArg { public PtclInstance ptcl; }
    public struct EmitterPreCalcArg { public EmitterInstance emitter; }
    public struct EmitterPostCalcArg { public EmitterInstance emitter; }
    public struct EmitterDrawOverrideArg
    {
        public EmitterInstance emitter;
        public Renderer renderer;
        public bool flushCache;
        public object argData;
    }

    public struct ShaderEmitterPostCalcArg
    {
        public EmitterInstance emitter;
        public bool noCalcBehavior;
        public bool childParticle;
    }
    public struct ShaderDrawOverrideArg
    {
        public EmitterInstance emitter;
        public Renderer renderer;
        public bool flushCache;
        public object argData;
    };
    public struct RenderStateSetArg
    {
        public EmitterInstance emitter;
        public Renderer renderer;
        public bool flushCache;
        public object argData;
    };
}

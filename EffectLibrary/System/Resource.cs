﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EffectLibrary.WiiU;

namespace EffectLibrary
{
    public class Resource
    {
        public EmitterSet[] EmitterSets;
        public ParticleShader[] Shaders;
        public PrimitiveRenderer[] Primitives;
        public IFileHeader ResourceFile;

        uint numShader;
        uint numPrimitive;

        public EffectSystem system;
        public Heap heap;

        public void Initialize(Heap heap, IFileHeader resource, uint resourceID, EffectSystem system)
        {
            this.system = system;
            this.heap = heap;
            Shaders = null;
            numShader = 0;
            Primitives = null;
            numPrimitive = 0;

            ResourceFile = resource;

            var shaders = resource.ShaderList.ToList();
            var primitives = resource.PrimitiveList.ToList();
            var emitterSetData = resource.EmitterSetList.ToList();

            numShader = (uint)shaders.Count;
            Shaders = new ParticleShader[numShader];
            for (int i = 0; i < numShader; i++)
            {
                Shaders[i] = new ParticleShader();
                Shaders[i].VertexShaderKey = shaders[i].VertexShaderKey;
                Shaders[i].FragmentShaderKey = shaders[i].FragmentShaderKey;
                Shaders[i].SetupShaderResource(heap, shaders[i]);
            }

            numPrimitive = (uint)primitives.Count;

            this.Primitives = new PrimitiveRenderer[numPrimitive];
            for (int i = 0; i < numPrimitive; i++)
                this.Primitives[i] = new PrimitiveRenderer(primitives[i]);


            this.EmitterSets = new EmitterSet[emitterSetData.Count];
            for (int i = 0; i < emitterSetData.Count; i++)
            {
                EmitterSets[i] = new EmitterSet();

                EmitterSet emitterSet = EmitterSets[i];
                emitterSet.data = emitterSetData[i];
                emitterSet.name = emitterSet.data.Name;
                emitterSet.numEmitter = emitterSet.data.EmitterList.ToList().Count;
                emitterSet.userData = emitterSet.data.UserData;
                emitterSet._unused = 0;
                emitterSet.shaders = Shaders;
                emitterSet.numShader = numShader;
                emitterSet.primitives = Primitives;
                emitterSet.numPrimitive = numPrimitive;

                emitterSet.emitterRefs = new EmitterData[emitterSet.numEmitter];

                var emitters = emitterSet.data.EmitterList.ToList();
                for (int j = 0; j < emitterSet.numEmitter; j++)
                {
                    emitterSet.emitterRefs[j] = emitters[j] as EmitterData;

                    var emitter = emitterSet.emitterRefs[j];
                    for (int k = 0; k < 3; k++)
                    {
                        if (emitter.Textures[k] != null && emitter.Textures[k].HasData)
                            emitter.Textures[k].CreateResourceHandle();
                    }
                    if (emitter.Type == EmitterType.Complex && ((((ComplexEmitterData)emitter).childFlags & 1) != 0))
                    {
                        var texture = ((ComplexEmitterData)emitter).ChildData.Texture;
                        if (texture.HasData)
                            texture.CreateResourceHandle();
                    }
                }
            }
        }

        public ParticleShader GetShader(uint emitterSetID, VertexShaderKey vertexShaderKey, FragmentShaderKey fragmentShaderKey, int index = -1)
        {
            uint numShader = EmitterSets[emitterSetID].numShader;

            if (EffectSystem.ActiveSystem.HasResourceFlags(FileResourceFlags.UseShaderIndex)) {
                //Use the emitter keys as newer keys aren't fully set properly
                Shaders[index].VertexShaderKey = vertexShaderKey;
                Shaders[index].FragmentShaderKey = fragmentShaderKey;

                return Shaders[index];
            }

            for (int i = 0; i < numShader; i++) {
                if (Shaders[i].VertexShaderKey == vertexShaderKey &&
                    Shaders[i].FragmentShaderKey == fragmentShaderKey)
                {
                    return Shaders[i];
                }
            }

            return null;
        }

        public void Finalize(Heap heap)
        {

        }
    }
}

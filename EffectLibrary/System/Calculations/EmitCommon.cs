﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EffectLibrary
{
    public partial class EmitterCalc
    {
        static void EmitCommon(EmitterInstance emitter, PtclInstance ptcl)
        {
            ptcl.keyGroups = emitter.keyGroups;
            ptcl.AnimData = new Dictionary<AnimGroupType, float>();
            //26+ are particle animation types
            for (int i = 26; i < 50; i++)
                ptcl.AnimData.Add((AnimGroupType)i, 0);

            ptcl.AnimData[AnimGroupType.EmitterColor0R] = 1;
            ptcl.AnimData[AnimGroupType.EmitterColor0G] = 1;
            ptcl.AnimData[AnimGroupType.EmitterColor0B] = 1;
            ptcl.AnimData[AnimGroupType.EmitterColor1R] = 1;
            ptcl.AnimData[AnimGroupType.EmitterColor1G] = 1;
            ptcl.AnimData[AnimGroupType.EmitterColor1B] = 1;

            var data = emitter.data;
            var emitterSet = emitter.emitterSet;

            float velocityMagRandom = 1.0f - emitter.random.GetF32() * data.DirectionVelocityRandom * emitterSet.dirVelRandom;
            float velocityMag = emitter.AnimData[(AnimGroupType)16] * emitterSet.dirVel;

            if (data.PositionRandomizer != 0.0f)
            {
                var nrmVec3 = emitter.random.GetNormalizedVec3();
                ptcl.pos = new OpenTK.Vector3(nrmVec3.X, nrmVec3.Y, nrmVec3.Z) * data.PositionRandomizer + ptcl.pos;
            }

            if (emitterSet.dirSet != 0)
                ptcl.velocity = (ptcl.velocity + emitterSet.dir * velocityMag) * velocityMagRandom;
            else
            {
                float dispersionAngle = data.DispersionAngle;
                if (dispersionAngle == 0.0f)
                    ptcl.velocity = (ptcl.velocity + new OpenTK.Vector3(
                        data.Direction.X,
                        data.Direction.Y,
                        data.Direction.Z) * velocityMag) * velocityMagRandom;
                else
                {
                    dispersionAngle = 1.0f - dispersionAngle / 90.0f;

                    float sin_val = 0, cos_val = 0, angle = emitter.random.GetF32() * 2.0f * MathF.PI;
                    MathTriangular.SinCosRad(ref sin_val, ref cos_val, angle);

                    float y = emitter.random.GetF32() * (1.0f - dispersionAngle) + dispersionAngle;

                    float a = 1.0f - y * y;
                    if (a <= 0.0f)
                        a = 0.0f;
                    else
                        a = MathF.Sqrt(a);

                    var normalizedVel = new OpenTK.Vector3(a * cos_val, y, a * sin_val);

                    var mBase = new OpenTK.Vector3(0.0f, 1.0f, 0.0f);
                    var mtx = MatrixExtension.MakeVectorRotation(mBase, data.Direction);

                    normalizedVel = MatrixExtension.MultVec(normalizedVel, mtx);
                    ptcl.velocity = (ptcl.velocity + normalizedVel * velocityMag) * velocityMagRandom;
                }
            }

            var randomVec3 = emitter.random.GetVec3();
            ptcl.velocity.X += randomVec3.X * data.DiffusionVel.X;
            ptcl.velocity.Y += randomVec3.Y * data.DiffusionVel.Y;
            ptcl.velocity.Z += randomVec3.Z * data.DiffusionVel.Z;

            var addVelocity = MatrixExtension.MultMTX(emitterSet.addVelocity, emitterSet.matrixRT);
            ptcl.velocity += addVelocity;

            ptcl.posDiff = ptcl.velocity;
            ptcl.counter = 0.0f;
            ptcl.spawnTime = emitter.counter;

            //GPU calculation uses 3 random values
            ptcl.random.X = emitter.random.GetF32();
            ptcl.random.Y = emitter.random.GetF32();
            ptcl.random.Z = emitter.random.GetF32();

            ptcl.randomU32 = emitter.random.GetU32();

            if (data.MaxLifespan == 0x7FFFFFFF)
                ptcl.lifespan = 0x7FFFFFFF;
            else
                ptcl.lifespan = (int)((emitter.AnimData[AnimGroupType.Lifespan] - emitter.random.GetS32((int)data.LifespanRandomizer)) * emitter.controller.Life);

            int lifespan = ptcl.lifespan - 1;
            var scaleRandom = (new OpenTK.Vector2(1.0f) - data.PtclScaleRandom * emitter.random.GetF32()) * emitterSet.ptclEmitScale;

            if (lifespan == 0)
            {
                for (int i = 0; i < ptcl.alphaAnim.Length; i++)
                {
                    ptcl.alphaAnim[i].time2 = -1;
                    ptcl.alphaAnim[i].time3 = 0x7FFFFFFF;
                    ptcl.alphaAnim[i].startDiff = 0.0f;
                    ptcl.alphaAnim[i].endDiff = 0.0f;
                    if (i == 0)
                        ptcl.alpha[i] = data.AlphaKey0.StartValue;
                    else
                        ptcl.alpha[i] = data.AlphaKey1.StartValue;
                }

                {
                    ptcl.scaleAnim.time2 = -1;
                    ptcl.scaleAnim.time3 = 0x7FFFFFFF;
                    ptcl.scaleAnim.startDiff = new OpenTK.Vector2();
                    ptcl.scaleAnim.endDiff = new OpenTK.Vector2();

                    ptcl.scale.X = data.PtclScaleStart.X * scaleRandom.X * emitter.AnimData[AnimGroupType.PtclScaleX];
                    ptcl.scale.Y = data.PtclScaleStart.Y * scaleRandom.Y * emitter.AnimData[AnimGroupType.PtclScaleY];
                }
            }
            else
            {
                ptcl.alpha[0] = data.AlphaKey0.StartValue;
                ptcl.alpha[1] = data.AlphaKey1.StartValue;

                if (data.AnimAlpha0Func == AnimationFunctions.Key4Value3)
                    ptcl.alpha[0] = Initialize3v4kAnim(ref ptcl.alphaAnim[0], data.AlphaKey0, lifespan);
                if (data.AlphaKey1 != null && data.AnimAlpha1Func == AnimationFunctions.Key4Value3)
                    ptcl.alpha[1] = Initialize3v4kAnim(ref ptcl.alphaAnim[1], data.AlphaKey1, lifespan);

                ptcl.scaleAnim.time2 = (data.ScaleTime2 * lifespan) / 100;
                ptcl.scaleAnim.time3 = (data.ScaleTime3 * lifespan) / 100;

                var scaleAnimStartDiff = data.PtclScaleStartDiff * (ptcl.scaleAnim.time2 == 0 ? 1.0f : (1.0f / (float)ptcl.scaleAnim.time2));
                var scale = data.PtclScaleStart - scaleAnimStartDiff;
                var scaleAnimEndDiff = data.PtclScaleEndDiff * (1.0f / (float)(lifespan - ptcl.scaleAnim.time3));

                //GPU already does scale anim and base scale so ignore those and use random values from emission
                if (emitter.data.GPUCalc)
                {
                    ptcl.scale.X = scaleRandom.X;
                    ptcl.scale.Y = scaleRandom.Y;
                }
                else
                {
                    ptcl.scaleAnim.startDiff.X = scaleAnimStartDiff.X * emitter.AnimData[AnimGroupType.PtclScaleX] * scaleRandom.X;
                    ptcl.scaleAnim.startDiff.Y = scaleAnimStartDiff.Y * emitter.AnimData[AnimGroupType.PtclScaleY] * scaleRandom.Y;
                    ptcl.scaleAnim.endDiff.X = scaleAnimEndDiff.X * emitter.AnimData[AnimGroupType.PtclScaleX] * scaleRandom.X;
                    ptcl.scaleAnim.endDiff.Y = scaleAnimEndDiff.Y * emitter.AnimData[AnimGroupType.PtclScaleY] * scaleRandom.Y;
                    ptcl.scale.X = scale.X * emitter.AnimData[AnimGroupType.PtclScaleX] * scaleRandom.X;
                    ptcl.scale.Y = scale.Y * emitter.AnimData[AnimGroupType.PtclScaleY] * scaleRandom.Y;
                }
            }

            ptcl.rotation.X = data.PtclRotate.X + emitter.random.GetF32() * data.PtclRotateRandom.X + emitterSet.ptclRotate.X;
            ptcl.rotation.Y = data.PtclRotate.Y + emitter.random.GetF32() * data.PtclRotateRandom.Y + emitterSet.ptclRotate.Y;
            ptcl.rotation.Z = data.PtclRotate.Z + emitter.random.GetF32() * data.PtclRotateRandom.Z + emitterSet.ptclRotate.Z;

            ptcl.angularVelocity.X = data.AngularVelocity.X + emitter.random.GetF32() * data.AngularVelocityRandom.X;
            ptcl.angularVelocity.Y = data.AngularVelocity.Y + emitter.random.GetF32() * data.AngularVelocityRandom.Y;
            ptcl.angularVelocity.Z = data.AngularVelocity.Z + emitter.random.GetF32() * data.AngularVelocityRandom.Z;

            uint color0Idx = 0;
            uint color1Idx = 0;

            if (data.Color0Source == ColorSource.Random)
                color0Idx = ptcl.randomU32 % 3;
            if (data.Color1Source == ColorSource.Random)
                color1Idx = ptcl.randomU32 % 3;

            ptcl.color[0] = new OpenTK.Vector4(data.ColorKeys[0, color0Idx]);
            ptcl.color[1] = new OpenTK.Vector4(data.ColorKeys[1, color1Idx]);

            ptcl.texAnimParam = new TexUVParam[3];

            for (int i = 0; i < emitter.data.TextureEmitters.Length; i++)
            {
                ptcl.texAnimParam[i] = new TexUVParam();

                var tex = emitter.data.TextureEmitters[i];
                if (tex == null)
                    continue;

                ptcl.texAnimParam[i].scroll.X = tex.Translate.X - tex.TranslateRnd.X * emitter.random.GetF32Range(-1.0f, 1.0f);
                ptcl.texAnimParam[i].scroll.Y = tex.Translate.Y - tex.TranslateRnd.Y * emitter.random.GetF32Range(-1.0f, 1.0f);
                ptcl.texAnimParam[i].scale.X = tex.Scale.X - tex.ScaleRnd.X * emitter.random.GetF32Range(-1.0f, 1.0f);
                ptcl.texAnimParam[i].scale.Y = tex.Scale.Y - tex.ScaleRnd.Y * emitter.random.GetF32Range(-1.0f, 1.0f);
                ptcl.texAnimParam[i].rotate = tex.Rotate - tex.RotateRnd * emitter.random.GetF32Range(-1.0f, 1.0f);
            }

            if (emitter.ptclFollowType == PtclFollowType.SRT)
            {
                ptcl.SetEmitterMatrixRef(emitter);
            }
            else
            {
                ptcl.matrixRT = emitter.matrixRT;
                ptcl.matrixSRT = emitter.matrixSRT;
                ptcl.SetParticleMatrixRef(ptcl);
            }

            for (int i = 0; i < data.TextureEmitters.Length; i++)
            {
                if (data.TextureEmitters[i].TexPtnAnimNum <= 1)
                    ptcl.texAnimParam[i].offset = new OpenTK.Vector2();
                else // TexPtnAnim Type Random
                {
                    int texPtnAnimIdx = (int)emitter.random.GetU32(data.TextureEmitters[i].TexPtnAnimNum);
                    int texPtnAnimIdxDiv = data.TextureEmitters[i].TexPtnAnimIdxDiv;
                    int offsetX = texPtnAnimIdx % texPtnAnimIdxDiv;
                    int offsetY = texPtnAnimIdx / texPtnAnimIdxDiv;

                    ptcl.texAnimParam[i].offset.X = data.TextureEmitters[i].UvScaleInit.X * (float)offsetX;
                    ptcl.texAnimParam[i].offset.Y = data.TextureEmitters[i].UvScaleInit.Y * (float)offsetY;
                }
            }

            if (data.VertexTransformMode == VertexTransformMode.Stripe)
                ptcl.stripe = EffectSystem.ActiveSystem.AllocAndConnectStripe(emitter, ptcl);

            ptcl.fluctuationAlpha = 1.0f;
            ptcl.fluctuationScale = 1.0f;
            ptcl._140 = 0;
            ptcl.emitter = emitter;
            ptcl.childEmitCounter = 1000000.0f;
            ptcl.childPreCalcCounter = 0.0f;
            ptcl.childEmitLostTime = 0.0f;
            ptcl.randomF32 = 1.0f - data.MomentumRandom * emitter.random.GetF32Range(-1.0f, 1.0f);

            var callback = EffectSystem.ActiveSystem.GetCurrentCustomActionParticleEmitCallback(emitter);
            if (callback != null)
            {
                ParticleEmitArg arg = new ParticleEmitArg { ptcl = ptcl };
                if (!callback(arg))
                {
                    RemoveParticle(emitter, ptcl, CpuCore._1);
                    return;
                }
            }

            AddPtclToList(emitter, ptcl);
        }

        static float Initialize3v4kAnim(ref AlphaAnim anim, AlphaSection key, int lifespan)
        {
            anim.time2 = (key.Time2 * lifespan) / 100;
            anim.time3 = (key.Time3 * lifespan) / 100;

            if (anim.time2 == 0)
                anim.startDiff = 0.0f;
            else
                anim.startDiff = key.StartDifference / (float)anim.time2;

            if (key.Time3 == 100)
                anim.endDiff = 0.0f;
            else
                anim.endDiff = key.EndDifference / (float)(lifespan - anim.time3);

            return key.StartValue - anim.startDiff;
        }
    }
}

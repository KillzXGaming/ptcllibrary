﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Syroot.IOExtension;

namespace EffectLibrary
{
    public partial class EmitterCalc
    {
        static PtclInstance CalcEmitPoint(EmitterInstance emitter)
        {
            var data = emitter.data;
            var mSys = EffectSystem.ActiveSystem;

            float emissionRate = emitter.AnimData[AnimGroupType.EmissionRatio] * emitter.controller.EmissionRatio;
            emitter.emitLostRate += emissionRate;

            int counter = (int)emitter.counter;
            if (counter == 0 && emitter.emitLostRate < 1.0f && emissionRate != 0.0f)
                emitter.emitLostRate = 1.0f;

            int numEmit = (int)MathF.Floor(emitter.emitLostRate);
            if (data.EquidistantParticleEmission) numEmit = 1;
            emitter.emitLostRate -= (float)numEmit;
            if (numEmit == 0)
                return null;

            float velocityMag = emitter.AnimData[AnimGroupType.AllDirectionVelocity] * emitter.emitterSet.allDirVel;

            PtclInstance ptclFirst = null;
            for (int i = 0; i < numEmit; i++)
            {
                PtclInstance ptcl = mSys.AllocPtcl(emitter.calc.GetPtclType());
                if (ptclFirst == null)
                    ptclFirst = ptcl;
                if (ptcl == null)
                    break;

                ptcl.data = data;
                ptcl.stripe = null;

                var nrmVec = emitter.random.GetNormalizedVec3();

                ptcl.pos = new OpenTK.Vector3();
                ptcl.velocity = new OpenTK.Vector3(nrmVec.X, nrmVec.Y, nrmVec.Z) * velocityMag;

                if (data.ParticleAddXZVelocity != 0)
                {
                    var posXZ = new OpenTK.Vector3(ptcl.pos.X, 0.0f, ptcl.pos.Z);
                    if (posXZ.LengthSquared <= MathTriangular.FLT_MIN)
                    {
                        posXZ.X = emitter.random.GetF32Range(-1.0f, 1.0f);
                        posXZ.Z = emitter.random.GetF32Range(-1.0f, 1.0f);
                    }

                    if (posXZ.LengthSquared == 0.0f)
                        posXZ = new OpenTK.Vector3(0);
                    else
                        posXZ.Normalize();

                    posXZ *= data.ParticleAddXZVelocity;
                    ptcl.velocity += posXZ;
                }

                EmitCommon(emitter, ptcl);
            }

            emitter.IsEmitted = true;
            return ptclFirst;
        }

        static PtclInstance CalcEmitCircle(EmitterInstance emitter)
        {
            var data = emitter.data;
            var emitterSet = emitter.emitterSet;
            var mSys = EffectSystem.ActiveSystem;

            float emissionRate = emitter.AnimData[AnimGroupType.EmissionRatio] * emitter.controller.EmissionRatio;
            emitter.emitLostRate += emissionRate;

            int counter = (int)emitter.counter;
            if (counter == 0 && emitter.emitLostRate < 1.0f && emissionRate != 0.0f)
                emitter.emitLostRate = 1.0f;

            int numEmit = (int)MathF.Floor(emitter.emitLostRate);
            if (data.EquidistantParticleEmission) numEmit = 1;
            emitter.emitLostRate -= (int)numEmit;
            if (numEmit == 0)
                return null;

            float velocityMag = emitter.AnimData[AnimGroupType.AllDirectionVelocity] * emitter.emitterSet.allDirVel;

            float scaleX = data.VolumeScale.X * emitterSet.emitterVolumeScale.X * emitter.AnimData[(AnimGroupType)22];
            float scaleZ = data.VolumeScale.Z * emitterSet.emitterVolumeScale.Z * emitter.AnimData[(AnimGroupType)23];

            float arcLength = data.ArcLength;
            float arcStartAngle = data.ArcStartAngle;
            if (data.UseArcUints)
            {
                arcLength = MathTriangular.Idx2Rad(data.ArcLengthUint);
                arcStartAngle = MathTriangular.Idx2Rad(data.ArcStartAngleUint);
            }

            if (data.RandomArcStartAngle)
                arcStartAngle = emitter.random.GetF32() * MathF.PI * 2.0f;

            PtclInstance ptclFirst = null;
            for (int i = 0; i < numEmit; i++)
            {
                PtclInstance ptcl = mSys.AllocPtcl(emitter.calc.GetPtclType());
                if (ptclFirst == null)
                    ptclFirst = ptcl;
                if (ptcl == null)
                    break;

                ptcl.data = data;
                ptcl.stripe = null;

                float sin_val = 0, cos_val = 0, angle = emitter.random.GetF32() * arcLength + arcStartAngle;
                MathTriangular.SinCosRad(ref sin_val, ref cos_val, angle);

                ptcl.pos.X = sin_val * scaleX;
                ptcl.pos.Y = 0.0f;
                ptcl.pos.Z = cos_val * scaleZ;

                ptcl.velocity.X = sin_val * velocityMag;
                ptcl.velocity.Y = 0.0f;
                ptcl.velocity.Z = cos_val * velocityMag;

                if (data.ParticleAddXZVelocity != 0)
                {
                    var posXZ = new OpenTK.Vector3(ptcl.pos.X, 0.0f, ptcl.pos.Z);
                    if (posXZ.LengthSquared <= MathTriangular.FLT_MIN)
                    {
                        posXZ.X = emitter.random.GetF32Range(-1.0f, 1.0f);
                        posXZ.Z = emitter.random.GetF32Range(-1.0f, 1.0f);
                    }

                    if (posXZ.LengthSquared == 0.0f)
                        posXZ = new OpenTK.Vector3(0);
                    else
                        posXZ.Normalize();

                    posXZ *= data.ParticleAddXZVelocity;
                    ptcl.velocity += posXZ;
                }

                EmitCommon(emitter, ptcl);
            }

            emitter.IsEmitted = true;
            return ptclFirst;
        }

        static PtclInstance CalcEmitCircleSameDivide(EmitterInstance emitter)
        {
            var data = emitter.data;
            var emitterSet = emitter.emitterSet;
            var mSys = EffectSystem.ActiveSystem;

            float emissionRate = emitter.AnimData[AnimGroupType.EmissionRatio] * emitter.controller.EmissionRatio;
            emitter.emitLostRate += emissionRate;

            int counter = (int)emitter.counter;
            if (counter == 0 && emitter.emitLostRate < 1.0f && emissionRate != 0.0f)
                emitter.emitLostRate = 1.0f;

            int numEmit = (int)MathF.Floor(emitter.emitLostRate);
            if (data.EquidistantParticleEmission) numEmit = 1;
            emitter.emitLostRate -= (int)numEmit;
            if (numEmit == 0)
                return null;

            float velocityMag = emitter.AnimData[AnimGroupType.AllDirectionVelocity] * emitter.emitterSet.allDirVel;

            float scaleX = data.VolumeScale.X * emitterSet.emitterVolumeScale.X * emitter.AnimData[(AnimGroupType)22];
            float scaleZ = data.VolumeScale.Z * emitterSet.emitterVolumeScale.Z * emitter.AnimData[(AnimGroupType)23];

            DWord angle;
            DWord arcLengthUnit;

            if (data.UseArcUints)
            {
                uint arcStartAngle = (uint)data.ArcStartAngleUint;
                uint arcLength = data.ArcLengthUint;

                if (data.RandomArcStartAngle)
                    arcStartAngle = emitter.random.GetU32();

                if (numEmit <= 1)
                    arcLengthUnit = 0;
                else
                {
                    if (arcLength == 0xFFFFFFFF)
                        arcLengthUnit = (uint)(0xFFFFFFFF / numEmit);
                    else
                        arcLengthUnit = (uint)(arcLength / (numEmit - 1));
                }
                angle = arcStartAngle;
            }
            else
            {
                float arcStartAngle = data.ArcStartAngle;

                if (data.RandomArcStartAngle)
                    arcStartAngle = emitter.random.GetF32() * MathF.PI * 2.0f;

                if (numEmit <= 1)
                    arcLengthUnit = 0.0f;
                else
                {
                    if (data.ArcLength == MathF.PI * 2.0f)
                        arcLengthUnit = (float)(data.ArcLength / numEmit);
                    else
                        arcLengthUnit = (float)(data.ArcLength / (numEmit - 1));
                }
                angle = arcStartAngle;
            }

            PtclInstance ptclFirst = null;
            for (int i = 0; i < numEmit; i++)
            {
                PtclInstance ptcl = mSys.AllocPtcl(emitter.calc.GetPtclType());
                if (ptclFirst == null)
                    ptclFirst = ptcl;
                if (ptcl == null)
                    break;

                ptcl.data = data;
                ptcl.stripe = null;

                float sin_val = 0, cos_val = 0;
                if (!data.UseArcUints)
                {
                    float rndAngle = angle.Single;
                    if (data.ArcRandomness != 0.0f)
                        rndAngle += ((emitter.random.GetF32() - 0.5f) * 2.0f * data.ArcRandomness / 180.0f * MathF.PI);
                    MathTriangular.SinCosRad(ref sin_val, ref cos_val, rndAngle);
                }
                else
                    MathTriangular.SinCosIdx(ref sin_val, ref cos_val, angle);

                ptcl.pos.X = sin_val * scaleX;
                ptcl.pos.Y = 0.0f;
                ptcl.pos.Z = cos_val * scaleZ;

                ptcl.velocity.X = sin_val * velocityMag;
                ptcl.velocity.Y = 0.0f;
                ptcl.velocity.Z = cos_val * velocityMag;

                if (!data.UseArcUints)
                    angle.Single += arcLengthUnit.Single;
                else
                    angle.Int32 += arcLengthUnit.Int32;

                if (data.ParticleAddXZVelocity != 0)
                {
                    var posXZ = new OpenTK.Vector3(ptcl.pos.X, 0.0f, ptcl.pos.Z);
                    if (posXZ.LengthSquared <= MathTriangular.FLT_MIN)
                    {
                        posXZ.X = emitter.random.GetF32Range(-1.0f, 1.0f);
                        posXZ.Z = emitter.random.GetF32Range(-1.0f, 1.0f);
                    }

                    if (posXZ.LengthSquared == 0.0f)
                        posXZ = new OpenTK.Vector3(0);
                    else
                        posXZ.Normalize();

                    posXZ *= data.ParticleAddXZVelocity;
                    ptcl.velocity += posXZ;
                }

                EmitCommon(emitter, ptcl);
            }

            emitter.IsEmitted = true;
            return ptclFirst;
        }

        static PtclInstance CalcEmitFillCircle(EmitterInstance emitter)
        {
            var data = emitter.data;
            var emitterSet = emitter.emitterSet;
            var mSys = EffectSystem.ActiveSystem;

            float emissionRate = emitter.AnimData[AnimGroupType.EmissionRatio] * emitter.controller.EmissionRatio;
            emitter.emitLostRate += emissionRate;

            int counter = (int)emitter.counter;
            if (counter == 0 && emitter.emitLostRate < 1.0f && emissionRate != 0.0f)
                emitter.emitLostRate = 1.0f;

            int numEmit = (int)MathF.Floor(emitter.emitLostRate);
            if (data.EquidistantParticleEmission) numEmit = 1;
            emitter.emitLostRate -= (int)numEmit;
            if (numEmit == 0)
                return null;

            float velocityMag = emitter.AnimData[AnimGroupType.AllDirectionVelocity] * emitter.emitterSet.allDirVel;

            float scaleX = data.VolumeScale.X * emitterSet.emitterVolumeScale.X * emitter.AnimData[(AnimGroupType)22];
            float scaleZ = data.VolumeScale.Z * emitterSet.emitterVolumeScale.Z * emitter.AnimData[(AnimGroupType)23];

            float arcLength = data.ArcLength;
            float arcStartAngle = data.ArcStartAngle;
            if (data.UseArcUints)
            {
                arcLength = MathTriangular.Idx2Rad(data.ArcLengthUint);
                arcStartAngle = MathTriangular.Idx2Rad(data.ArcStartAngleUint);
            }

            if (data.RandomArcStartAngle)
                arcStartAngle = emitter.random.GetF32() * MathF.PI * 2.0f;

            PtclInstance ptclFirst = null;
            for (int i = 0; i < numEmit; i++)
            {
                PtclInstance ptcl = mSys.AllocPtcl(emitter.calc.GetPtclType());
                if (ptclFirst == null)
                    ptclFirst = ptcl;
                if (ptcl == null)
                    break;

                ptcl.data = data;
                ptcl.stripe = null;

                float sin_val = 0, cos_val = 0, angle = emitter.random.GetF32() * arcLength + arcStartAngle;
                MathTriangular.SinCosRad(ref sin_val, ref cos_val, angle);

                float v0 = emitter.random.GetF32();
                float v1 = 1.0f - data.VolumeFillRatio;

                float a = v0 + v1 * v1 * (1.0f - v0);
                if (a <= 0.0f)
                    a = 0.0f;
                else
                    a = MathF.Sqrt(a);

                ptcl.pos.X = (a * sin_val) * scaleX;
                ptcl.pos.Y = 0.0f;
                ptcl.pos.Z = (a * cos_val) * scaleZ;

                ptcl.velocity.X = (a * sin_val) * velocityMag;
                ptcl.velocity.Y = 0.0f;
                ptcl.velocity.Z = (a * cos_val) * velocityMag;

                if (data.ParticleAddXZVelocity != 0)
                {
                    var posXZ = new OpenTK.Vector3(ptcl.pos.X, 0.0f, ptcl.pos.Z);
                    if (posXZ.LengthSquared <= MathTriangular.FLT_MIN)
                    {
                        posXZ.X = emitter.random.GetF32Range(-1.0f, 1.0f);
                        posXZ.Z = emitter.random.GetF32Range(-1.0f, 1.0f);
                    }

                    if (posXZ.LengthSquared == 0.0f)
                        posXZ = new OpenTK.Vector3(0);
                    else
                        posXZ.Normalize();

                    posXZ *= data.ParticleAddXZVelocity;
                    ptcl.velocity += posXZ;
                }

                EmitCommon(emitter, ptcl);
            }

            emitter.IsEmitted = true;
            return ptclFirst;
        }

        static PtclInstance CalcEmitSphere(EmitterInstance emitter)
        {
            var data = emitter.data;
            var emitterSet = emitter.emitterSet;
            var mSys = EffectSystem.ActiveSystem;

            float emissionRate = emitter.AnimData[AnimGroupType.EmissionRatio] * emitter.controller.EmissionRatio;
            emitter.emitLostRate += emissionRate;

            int counter = (int)emitter.counter;
            if (counter == 0 && emitter.emitLostRate < 1.0f && emissionRate != 0.0f)
                emitter.emitLostRate = 1.0f;

            int numEmit = (int)MathF.Floor(emitter.emitLostRate);
            if (data.EquidistantParticleEmission) numEmit = 1;
            emitter.emitLostRate -= (int)numEmit;
            if (numEmit == 0)
                return null;

            float velocityMag = emitter.AnimData[AnimGroupType.AllDirectionVelocity] * emitter.emitterSet.allDirVel;

            float scaleX = data.VolumeScale.X * emitterSet.emitterVolumeScale.X * emitter.AnimData[(AnimGroupType)22];
            float scaleY = data.VolumeScale.Y * emitterSet.emitterVolumeScale.Y * emitter.AnimData[(AnimGroupType)23];
            float scaleZ = data.VolumeScale.Z * emitterSet.emitterVolumeScale.Z * emitter.AnimData[(AnimGroupType)24];

            float arcLength = data.ArcLength;
            float arcStartAngle = data.ArcStartAngle;
            if (data.UseArcUints)
            {
                arcLength = MathTriangular.Idx2Rad(data.ArcLengthUint);
                arcStartAngle = MathTriangular.Idx2Rad(data.ArcStartAngleUint);
            }

            if (data.SphereUseLatitude)
            {
                arcLength = 0.0f;
                arcStartAngle = 0.0f;
            }
            else
            {
                if (data.RandomArcStartAngle)
                    arcStartAngle = emitter.random.GetF32() * MathF.PI * 2.0f;
            }

            PtclInstance ptclFirst = null;
            for (int i = 0; i < numEmit; i++)
            {
                PtclInstance ptcl = mSys.AllocPtcl(emitter.calc.GetPtclType());
                if (ptclFirst == null)
                    ptclFirst = ptcl;
                if (ptcl == null)
                    break;

                ptcl.data = data;
                ptcl.stripe = null;

                float sin_val = 0, cos_val = 0, angle = data.SphereUseLatitude ? emitter.random.GetF32() * MathF.PI * 2.0f
                                                                              : emitter.random.GetF32() * arcLength + arcStartAngle;
                MathTriangular.SinCosRad(ref sin_val, ref cos_val, angle);

                float y;
                if (data.UseArcUints)
                    y = (data.SphereUseLatitude) ? MathTriangular.CosRad(emitter.random.GetF32() * data.SphereLatitude)
                                                : emitter.random.GetF32Range(-1.0f, 1.0f);
                else
                     y = (data.SphereUseLatitude) ? 1.0f - (1.0f - MathTriangular.CosRad(data.SphereLatitude)) * emitter.random.GetF32()
                               : emitter.random.GetF32Range(-1.0f, 1.0f);


                float a = 1.0f - y * y;
                if (a <= 0.0f)
                    a = 0.0f;
                else
                    a = MathF.Sqrt(a);

                var normalizedVel = new OpenTK.Vector3(a * sin_val, y, a * cos_val);

                if (data.SphereUseLatitude && (data.SphereLatitudeDir.X != 0.0f || data.SphereLatitudeDir.Y != 1.0f || data.SphereLatitudeDir.Z != 0.0f))
                {
                    var aBase = new OpenTK.Vector3(0.0f, 1.0f, 0.0f);
                    var mtx = MatrixExtension.MakeVectorRotation(aBase, data.SphereLatitudeDir);
                    normalizedVel = MatrixExtension.MultVec(normalizedVel, mtx);
                }

                ptcl.pos = normalizedVel * new OpenTK.Vector3(scaleX, scaleY, scaleZ);
                ptcl.velocity = normalizedVel * velocityMag;

                if (data.ParticleAddXZVelocity != 0)
                {
                    var posXZ = new OpenTK.Vector3(ptcl.pos.X, 0.0f, ptcl.pos.Z);
                    if (posXZ.LengthSquared <= MathTriangular.FLT_MIN)
                    {
                        posXZ.X = emitter.random.GetF32Range(-1.0f, 1.0f);
                        posXZ.Z = emitter.random.GetF32Range(-1.0f, 1.0f);
                    }

                    if (posXZ.LengthSquared == 0.0f)
                        posXZ = new OpenTK.Vector3(0);
                    else
                        posXZ.Normalize();

                    posXZ *= data.ParticleAddXZVelocity;
                    ptcl.velocity += posXZ;
                }

                EmitCommon(emitter, ptcl);
            }

            emitter.IsEmitted = true;
            return ptclFirst;
        }

        static PtclInstance CalcEmitSphereSameDivide(EmitterInstance emitter)
        {
            var data = emitter.data;
            var emitterSet = emitter.emitterSet;
            var mSys = EffectSystem.ActiveSystem;

            float emissionRate = emitter.AnimData[AnimGroupType.EmissionRatio] * emitter.controller.EmissionRatio;
            emitter.emitLostRate += emissionRate;

            int counter = (int)emitter.counter;
            if (counter == 0 && emitter.emitLostRate < 1.0f && emissionRate != 0.0f)
                emitter.emitLostRate = 1.0f;

            int numEmit = (int)MathF.Floor(emitter.emitLostRate);
            if (data.EquidistantParticleEmission) numEmit = 1;
            emitter.emitLostRate -= (int)numEmit;
            if (numEmit == 0)
                return null;

            float velocityMag = emitter.AnimData[AnimGroupType.AllDirectionVelocity] * emitter.emitterSet.allDirVel;

            float scaleX = data.VolumeScale.X * emitterSet.emitterVolumeScale.X * emitter.AnimData[(AnimGroupType)22];
            float scaleY = data.VolumeScale.Y * emitterSet.emitterVolumeScale.Y * emitter.AnimData[(AnimGroupType)23];
            float scaleZ = data.VolumeScale.Z * emitterSet.emitterVolumeScale.Z * emitter.AnimData[(AnimGroupType)24];

            var table = SphereVolumeDivTables.gSameDivideSphereTbl[data.SphereDivTblIdx];

            PtclInstance ptclFirst = null;
            for (int i = 0; i < numEmit; i++)
            {
                PtclInstance ptcl = mSys.AllocPtcl(emitter.calc.GetPtclType());
                if (ptclFirst == null)
                    ptclFirst = ptcl;
                if (ptcl == null)
                    break;

                ptcl.data = data;
                ptcl.stripe = null;

                int index = i * 3; //Multiply 3 for vec3, 3 floats per vec in table
                var normalizedVel = new OpenTK.Vector3(table[index], table[index + 1], table[index + 2]);

                if (data.SphereUseLatitude)
                {
                    if (data.UseArcUints)
                    {
                        if (!(MathTriangular.CosRad(data.SphereLatitude) < normalizedVel.Y))
                            continue;
                    }
                    else
                    {
                        if (!(MathF.PI - 0.0001f < data.SphereLatitude || MathTriangular.CosRad(data.SphereLatitude) < normalizedVel.Y))
                            continue;
                    }
         
                    if (data.SphereLatitudeDir.X != 0.0f || data.SphereLatitudeDir.Y != 1.0f || data.SphereLatitudeDir.Z != 0.0f)
                    {
                        var aBase = new OpenTK.Vector3(0.0f, 1.0f, 0.0f);
                        var mtx = MatrixExtension.MakeVectorRotation(aBase, data.SphereLatitudeDir);
                        normalizedVel = MatrixExtension.MultVec(normalizedVel, mtx);
                    }
                }

                ptcl.pos = normalizedVel * new OpenTK.Vector3(scaleX, scaleY, scaleZ);
                ptcl.velocity = normalizedVel * velocityMag;

                if (data.ParticleAddXZVelocity != 0)
                {
                    var posXZ = new OpenTK.Vector3(ptcl.pos.X, 0.0f, ptcl.pos.Z);
                    if (posXZ.LengthSquared <= MathTriangular.FLT_MIN)
                    {
                        posXZ.X = emitter.random.GetF32Range(-1.0f, 1.0f);
                        posXZ.Z = emitter.random.GetF32Range(-1.0f, 1.0f);
                    }

                    if (posXZ.LengthSquared == 0.0f)
                        posXZ = new OpenTK.Vector3(0);
                    else
                        posXZ.Normalize();

                    posXZ *= data.ParticleAddXZVelocity;
                    ptcl.velocity += posXZ;
                }

                EmitCommon(emitter, ptcl);
            }

            emitter.IsEmitted = true;
            return ptclFirst;
        }

        static PtclInstance CalcEmitSphereSameDivide64(EmitterInstance emitter)
        {
            var data = emitter.data;
            var emitterSet = emitter.emitterSet;
            var mSys = EffectSystem.ActiveSystem;

            float emissionRate = emitter.AnimData[AnimGroupType.EmissionRatio] * emitter.controller.EmissionRatio;
            emitter.emitLostRate += emissionRate;

            int counter = (int)emitter.counter;
            if (counter == 0 && emitter.emitLostRate < 1.0f && emissionRate != 0.0f)
                emitter.emitLostRate = 1.0f;

            int numEmit = (int)MathF.Floor(emitter.emitLostRate);
            if (data.EquidistantParticleEmission) numEmit = 1;
            emitter.emitLostRate -= (int)numEmit;
            if (numEmit == 0)
                return null;

            float velocityMag = emitter.AnimData[AnimGroupType.AllDirectionVelocity] * emitter.emitterSet.allDirVel;

            float scaleX = data.VolumeScale.X * emitterSet.emitterVolumeScale.X * emitter.AnimData[(AnimGroupType)22];
            float scaleY = data.VolumeScale.Y * emitterSet.emitterVolumeScale.Y * emitter.AnimData[(AnimGroupType)23];
            float scaleZ = data.VolumeScale.Z * emitterSet.emitterVolumeScale.Z * emitter.AnimData[(AnimGroupType)24];

            var table = SphereVolumeDivTables.gSameDivideSphere64Tbl[data.SphereDivTblIdx + 2];

            PtclInstance ptclFirst = null;
            for (int i = 0; i < numEmit; i++)
            {
                PtclInstance ptcl = mSys.AllocPtcl(emitter.calc.GetPtclType());
                if (ptclFirst == null)
                    ptclFirst = ptcl;
                if (ptcl == null)
                    break;

                ptcl.data = data;
                ptcl.stripe = null;

                int index = i * 3; //Multiply 3 for vec3, 3 floats per vec in table
                var normalizedVel = new OpenTK.Vector3(table[index], table[index + 1], table[index + 2]);

                if (data.SphereUseLatitude)
                {
                    if (data.UseArcUints)
                    {
                        if (!(MathTriangular.CosRad(data.SphereLatitude) < normalizedVel.Y))
                            continue;
                    }
                    else
                    {
                        if (!(MathF.PI - 0.0001f < data.SphereLatitude || MathTriangular.CosRad(data.SphereLatitude) < normalizedVel.Y))
                            continue;
                    }

                    if (data.SphereLatitudeDir.X != 0.0f || data.SphereLatitudeDir.Y != 1.0f || data.SphereLatitudeDir.Z != 0.0f)
                    {
                        var aBase = new OpenTK.Vector3(0.0f, 1.0f, 0.0f);
                        var mtx = MatrixExtension.MakeVectorRotation(aBase, data.SphereLatitudeDir);
                        normalizedVel = MatrixExtension.MultVec(normalizedVel, mtx);
                    }
                }

                ptcl.pos = normalizedVel * new OpenTK.Vector3(scaleX, scaleY, scaleZ);
                ptcl.velocity = normalizedVel * velocityMag;

                if (data.ParticleAddXZVelocity != 0)
                {
                    var posXZ = new OpenTK.Vector3(ptcl.pos.X, 0.0f, ptcl.pos.Z);
                    if (posXZ.LengthSquared <= MathTriangular.FLT_MIN)
                    {
                        posXZ.X = emitter.random.GetF32Range(-1.0f, 1.0f);
                        posXZ.Z = emitter.random.GetF32Range(-1.0f, 1.0f);
                    }

                    if (posXZ.LengthSquared == 0.0f)
                        posXZ = new OpenTK.Vector3(0);
                    else
                        posXZ.Normalize();

                    posXZ *= data.ParticleAddXZVelocity;
                    ptcl.velocity += posXZ;
                }

                EmitCommon(emitter, ptcl);
            }

            emitter.IsEmitted = true;
            return ptclFirst;
        }

        static PtclInstance CalcEmitFillSphere(EmitterInstance emitter)
        {
            var data = emitter.data;
            var emitterSet = emitter.emitterSet;
            var mSys = EffectSystem.ActiveSystem;

            float emissionRate = emitter.AnimData[AnimGroupType.EmissionRatio] * emitter.controller.EmissionRatio;
            emitter.emitLostRate += emissionRate;

            int counter = (int)emitter.counter;
            if (counter == 0 && emitter.emitLostRate < 1.0f && emissionRate != 0.0f)
                emitter.emitLostRate = 1.0f;

            int numEmit = (int)MathF.Floor(emitter.emitLostRate);
            if (data.EquidistantParticleEmission) numEmit = 1;
            emitter.emitLostRate -= (int)numEmit;
            if (numEmit == 0)
                return null;

            float velocityMag = emitter.AnimData[AnimGroupType.AllDirectionVelocity] * emitter.emitterSet.allDirVel;

            float scaleX = data.VolumeScale.X * emitterSet.emitterVolumeScale.X * emitter.AnimData[(AnimGroupType)22];
            float scaleY = data.VolumeScale.Y * emitterSet.emitterVolumeScale.Y * emitter.AnimData[(AnimGroupType)23];
            float scaleZ = data.VolumeScale.Z * emitterSet.emitterVolumeScale.Z * emitter.AnimData[(AnimGroupType)24];

            float arcLength = data.ArcLength;
            float arcStartAngle = data.ArcStartAngle;
            if (data.UseArcUints)
            {
                arcLength = MathTriangular.Idx2Rad(data.ArcLengthUint);
                arcStartAngle = MathTriangular.Idx2Rad(data.ArcStartAngleUint);
            }

            if (data.SphereUseLatitude)
            {
                arcLength = 0.0f;
                arcStartAngle = 0.0f;
            }
            else
            {
                if (data.RandomArcStartAngle)
                    arcStartAngle = emitter.random.GetF32() * MathF.PI * 2.0f;
            }

            PtclInstance ptclFirst = null;
            for (int i = 0; i < numEmit; i++)
            {
                PtclInstance ptcl = mSys.AllocPtcl(emitter.calc.GetPtclType());
                if (ptclFirst == null)
                    ptclFirst = ptcl;
                if (ptcl == null)
                    break;

                ptcl.data = data;
                ptcl.stripe = null;

                float sin_val = 0, cos_val = 0, angle = data.SphereUseLatitude ? emitter.random.GetF32() * MathF.PI * 2.0f
                                                                              : emitter.random.GetF32() * arcLength + arcStartAngle;
                MathTriangular.SinCosRad(ref sin_val, ref cos_val, angle);

                float y;
                if (data.UseArcUints)
                    y = (data.SphereUseLatitude) ? MathTriangular.CosRad(emitter.random.GetF32() * data.SphereLatitude)
                                                : emitter.random.GetF32Range(-1.0f, 1.0f);
                else
                    y = (data.SphereUseLatitude) ? 1.0f - (1.0f - MathTriangular.CosRad(data.SphereLatitude)) * emitter.random.GetF32()
                              : emitter.random.GetF32Range(-1.0f, 1.0f);

                float a = 1.0f - y * y;
                if (a <= 0.0f)
                    a = 0.0f;
                else
                    a = MathF.Sqrt(a);

                float extent = emitter.random.GetF32();
                if (extent <= 0.0f)
                    extent = 0.0f;
                else
                    extent = MathF.Sqrt(extent);
                extent = extent * data.VolumeFillRatio + 1.0f - data.VolumeFillRatio;

                var normalizedVel = new OpenTK.Vector3(a * sin_val, y, a * cos_val);

                if (data.SphereUseLatitude && (data.SphereLatitudeDir.X != 0.0f || data.SphereLatitudeDir.Y != 1.0f || data.SphereLatitudeDir.Z != 0.0f))
                {
                    var aBase = new OpenTK.Vector3(0.0f, 1.0f, 0.0f);
                    var mtx = MatrixExtension.MakeVectorRotation(aBase, data.SphereLatitudeDir);
                    normalizedVel = MatrixExtension.MultVec(normalizedVel, mtx);
                }

                ptcl.pos = normalizedVel * new OpenTK.Vector3(scaleX, scaleY, scaleZ) * extent;
                ptcl.velocity = normalizedVel * velocityMag;

                if (data.ParticleAddXZVelocity != 0)
                {
                    var posXZ = new OpenTK.Vector3(ptcl.pos.X, 0.0f, ptcl.pos.Z);
                    if (posXZ.LengthSquared <= MathTriangular.FLT_MIN)
                    {
                        posXZ.X = emitter.random.GetF32Range(-1.0f, 1.0f);
                        posXZ.Z = emitter.random.GetF32Range(-1.0f, 1.0f);
                    }

                    if (posXZ.LengthSquared == 0.0f)
                        posXZ = new OpenTK.Vector3(0);
                    else
                        posXZ.Normalize();

                    posXZ *= data.ParticleAddXZVelocity;
                    ptcl.velocity += posXZ;
                }

                EmitCommon(emitter, ptcl);
            }

            emitter.IsEmitted = true;
            return ptclFirst;
        }

        static PtclInstance CalcEmitCylinder(EmitterInstance emitter)
        {
            var data = emitter.data;
            var emitterSet = emitter.emitterSet;
            var mSys = EffectSystem.ActiveSystem;

            float emissionRate = emitter.AnimData[AnimGroupType.EmissionRatio] * emitter.controller.EmissionRatio;
            emitter.emitLostRate += emissionRate;

            int counter = (int)emitter.counter;
            if (counter == 0 && emitter.emitLostRate < 1.0f && emissionRate != 0.0f)
                emitter.emitLostRate = 1.0f;

            int numEmit = (int)MathF.Floor(emitter.emitLostRate);
            if (data.EquidistantParticleEmission) numEmit = 1;
            emitter.emitLostRate -= (int)numEmit;
            if (numEmit == 0)
                return null;

            float velocityMag = emitter.AnimData[AnimGroupType.AllDirectionVelocity] * emitter.emitterSet.allDirVel;

            float scaleX = data.VolumeScale.X * emitterSet.emitterVolumeScale.X * emitter.AnimData[(AnimGroupType)22];
            float scaleY = data.VolumeScale.Y * emitterSet.emitterVolumeScale.Y * emitter.AnimData[(AnimGroupType)23];
            float scaleZ = data.VolumeScale.Z * emitterSet.emitterVolumeScale.Z * emitter.AnimData[(AnimGroupType)24];

            float arcLength = data.ArcLength;
            float arcStartAngle = data.ArcStartAngle;
            if (data.UseArcUints)
            {
                arcLength = MathTriangular.Idx2Rad(data.ArcLengthUint);
                arcStartAngle = MathTriangular.Idx2Rad(data.ArcStartAngleUint);
            }

            if (data.RandomArcStartAngle)
                arcStartAngle = emitter.random.GetF32() * MathF.PI * 2.0f;

            PtclInstance ptclFirst = null;
            for (int i = 0; i < numEmit; i++)
            {
                PtclInstance ptcl = mSys.AllocPtcl(emitter.calc.GetPtclType());
                if (ptclFirst == null)
                    ptclFirst = ptcl;
                if (ptcl == null)
                    break;

                ptcl.data = data;
                ptcl.stripe = null;

                float sin_val = 0, cos_val = 0, angle = emitter.random.GetF32() * arcLength + arcStartAngle;
                MathTriangular.SinCosRad(ref sin_val, ref cos_val, angle);

                ptcl.pos.X = sin_val * scaleX;
                ptcl.pos.Y = emitter.random.GetF32Range(-1.0f, 1.0f) * scaleY;
                ptcl.pos.Z = cos_val * scaleZ;

                ptcl.velocity.X = sin_val * velocityMag;
                ptcl.velocity.Y = 0.0f;
                ptcl.velocity.Z = cos_val * velocityMag;

                if (data.ParticleAddXZVelocity != 0)
                {
                    var posXZ = new OpenTK.Vector3(ptcl.pos.X, 0.0f, ptcl.pos.Z);
                    if (posXZ.LengthSquared <= MathTriangular.FLT_MIN)
                    {
                        posXZ.X = emitter.random.GetF32Range(-1.0f, 1.0f);
                        posXZ.Z = emitter.random.GetF32Range(-1.0f, 1.0f);
                    }

                    if (posXZ.LengthSquared == 0.0f)
                        posXZ = new OpenTK.Vector3(0);
                    else
                        posXZ.Normalize();

                    posXZ *= data.ParticleAddXZVelocity;
                    ptcl.velocity += posXZ;
                }

                EmitCommon(emitter, ptcl);
            }

            emitter.IsEmitted = true;
            return ptclFirst;
        }

        static PtclInstance CalcEmitFillCylinder(EmitterInstance emitter)
        {
            var data = emitter.data;
            var emitterSet = emitter.emitterSet;
            var mSys = EffectSystem.ActiveSystem;

            float emissionRate = emitter.AnimData[AnimGroupType.EmissionRatio] * emitter.controller.EmissionRatio;
            emitter.emitLostRate += emissionRate;

            int counter = (int)emitter.counter;
            if (counter == 0 && emitter.emitLostRate < 1.0f && emissionRate != 0.0f)
                emitter.emitLostRate = 1.0f;

            int numEmit = (int)MathF.Floor(emitter.emitLostRate);
            if (data.EquidistantParticleEmission) numEmit = 1;
            emitter.emitLostRate -= (int)numEmit;
            if (numEmit == 0)
                return null;

            float velocityMag = emitter.AnimData[AnimGroupType.AllDirectionVelocity] * emitter.emitterSet.allDirVel;

            float scaleX = data.VolumeScale.X * emitterSet.emitterVolumeScale.X * emitter.AnimData[(AnimGroupType)22];
            float scaleY = data.VolumeScale.Y * emitterSet.emitterVolumeScale.Y * emitter.AnimData[(AnimGroupType)23];
            float scaleZ = data.VolumeScale.Z * emitterSet.emitterVolumeScale.Z * emitter.AnimData[(AnimGroupType)24];

            float arcLength = data.ArcLength;
            float arcStartAngle = data.ArcStartAngle;
            if (data.UseArcUints)
            {
                arcLength = MathTriangular.Idx2Rad(data.ArcLengthUint);
                arcStartAngle = MathTriangular.Idx2Rad(data.ArcStartAngleUint);
            }

            if (data.RandomArcStartAngle)
                arcStartAngle = emitter.random.GetF32() * MathF.PI * 2.0f;

            PtclInstance ptclFirst = null;
            for (int i = 0; i < numEmit; i++)
            {
                PtclInstance ptcl = mSys.AllocPtcl(emitter.calc.GetPtclType());
                if (ptclFirst == null)
                    ptclFirst = ptcl;
                if (ptcl == null)
                    break;

                ptcl.data = data;
                ptcl.stripe = null;

                float sin_val = 0, cos_val = 0, angle = emitter.random.GetF32() * arcLength + arcStartAngle;
                MathTriangular.SinCosRad(ref sin_val, ref cos_val, angle);

                float v0 = emitter.random.GetF32();
                float v1 = 1.0f - data.VolumeFillRatio;

                float a = v0 + v1 * v1 * (1.0f - v0);
                if (a <= 0.0f)
                    a = 0.0f;
                else
                    a = MathF.Sqrt(a);

                ptcl.pos.X = (sin_val * a) * scaleX;
                ptcl.pos.Y = emitter.random.GetF32Range(-1.0f, 1.0f) * scaleY;
                ptcl.pos.Z = (cos_val * a) * scaleZ;

                ptcl.velocity.X = sin_val * velocityMag;
                ptcl.velocity.Y = 0.0f;
                ptcl.velocity.Z = cos_val * velocityMag;

                if (data.ParticleAddXZVelocity != 0)
                {
                    var posXZ = new OpenTK.Vector3(ptcl.pos.X, 0.0f, ptcl.pos.Z);
                    if (posXZ.LengthSquared <= MathTriangular.FLT_MIN)
                    {
                        posXZ.X = emitter.random.GetF32Range(-1.0f, 1.0f);
                        posXZ.Z = emitter.random.GetF32Range(-1.0f, 1.0f);
                    }

                    if (posXZ.LengthSquared == 0.0f)
                        posXZ = new OpenTK.Vector3(0);
                    else
                        posXZ.Normalize();

                    posXZ *= data.ParticleAddXZVelocity;
                    ptcl.velocity += posXZ;
                }

                EmitCommon(emitter, ptcl);
            }

            emitter.IsEmitted = true;
            return ptclFirst;
        }

        static PtclInstance CalcEmitBox(EmitterInstance emitter)
        {
            var data = emitter.data;
            var emitterSet = emitter.emitterSet;
            var mSys = EffectSystem.ActiveSystem;

            float emissionRate = emitter.AnimData[AnimGroupType.EmissionRatio] * emitter.controller.EmissionRatio;
            emitter.emitLostRate += emissionRate;

            int counter = (int)emitter.counter;
            if (counter == 0 && emitter.emitLostRate < 1.0f && emissionRate != 0.0f)
                emitter.emitLostRate = 1.0f;

            int numEmit = (int)MathF.Floor(emitter.emitLostRate);
            if (data.EquidistantParticleEmission) numEmit = 1;
            emitter.emitLostRate -= (int)numEmit;
            if (numEmit == 0)
                return null;

            float velocityMag = emitter.AnimData[AnimGroupType.AllDirectionVelocity] * emitter.emitterSet.allDirVel;

            float scaleX = data.VolumeScale.X * emitterSet.emitterVolumeScale.X * emitter.AnimData[(AnimGroupType)22];
            float scaleY = data.VolumeScale.Y * emitterSet.emitterVolumeScale.Y * emitter.AnimData[(AnimGroupType)23];
            float scaleZ = data.VolumeScale.Z * emitterSet.emitterVolumeScale.Z * emitter.AnimData[(AnimGroupType)24];

            PtclInstance ptclFirst = null;
            for (int i = 0; i < numEmit; i++)
            {
                PtclInstance ptcl = mSys.AllocPtcl(emitter.calc.GetPtclType());
                if (ptclFirst == null)
                    ptclFirst = ptcl;
                if (ptcl == null)
                    break;

                ptcl.data = data;
                ptcl.stripe = null;

                uint v0 = emitter.random.GetU32();
                uint v1 = emitter.random.GetU32();

                OpenTK.Vector3 rndVec = new OpenTK.Vector3()
                {
                    X = emitter.random.GetF32Range(-1.0f, 1.0f),
                    Y = emitter.random.GetF32Range(-1.0f, 1.0f),
                    Z = emitter.random.GetF32Range(-1.0f, 1.0f),
                };

                if (v0 < 0x55555555)
                {
                    ptcl.pos.X = scaleX * rndVec.X;
                    ptcl.pos.Y = scaleY * rndVec.Y;
                    ptcl.pos.Z = (v1 < 0x7fffffff) ? scaleZ : -scaleZ;
                }
                else if (v0 < 0xAAAAAAAA)
                {
                    ptcl.pos.X = scaleX * rndVec.X;
                    ptcl.pos.Y = (v1 < 0x7fffffff) ? scaleY : -scaleY;
                    ptcl.pos.Z = scaleZ * rndVec.Z;
                }
                else // if (v0 < 0xFFFFFFFF)
                {
                    ptcl.pos.X = (v1 < 0x7fffffff) ? scaleX : -scaleX;
                    ptcl.pos.Y = scaleY * rndVec.Y;
                    ptcl.pos.Z = scaleZ * rndVec.Z;
                }

                OpenTK.Vector3 normalizedVel = new OpenTK.Vector3();
                if (ptcl.pos.X != 0.0f || ptcl.pos.Y != 0.0f || ptcl.pos.Z != 0.0f)
                {
                    normalizedVel = ptcl.pos;
                    normalizedVel.Normalize();
                }

                ptcl.velocity.X = normalizedVel.X * velocityMag;
                ptcl.velocity.Y = normalizedVel.Y * velocityMag;
                ptcl.velocity.Z = normalizedVel.Z * velocityMag;

                if (data.ParticleAddXZVelocity != 0)
                {
                    var posXZ = new OpenTK.Vector3(ptcl.pos.X, 0.0f, ptcl.pos.Z);
                    if (posXZ.LengthSquared <= MathTriangular.FLT_MIN)
                    {
                        posXZ.X = emitter.random.GetF32Range(-1.0f, 1.0f);
                        posXZ.Z = emitter.random.GetF32Range(-1.0f, 1.0f);
                    }

                    if (posXZ.LengthSquared == 0.0f)
                        posXZ = new OpenTK.Vector3(0);
                    else
                        posXZ.Normalize();

                    posXZ *= data.ParticleAddXZVelocity;
                    ptcl.velocity += posXZ;
                }

                EmitCommon(emitter, ptcl);
            }

            emitter.IsEmitted = true;
            return ptclFirst;
        }

        static PtclInstance CalcEmitFillBox(EmitterInstance emitter)
        {
            var data = emitter.data;
            var emitterSet = emitter.emitterSet;
            var mSys = EffectSystem.ActiveSystem;

            float emissionRate = emitter.AnimData[AnimGroupType.EmissionRatio] * emitter.controller.EmissionRatio;
            emitter.emitLostRate += emissionRate;

            int counter = (int)emitter.counter;
            if (counter == 0 && emitter.emitLostRate < 1.0f && emissionRate != 0.0f)
                emitter.emitLostRate = 1.0f;

            int numEmit = (int)MathF.Floor(emitter.emitLostRate);
            if (data.EquidistantParticleEmission) numEmit = 1;
            emitter.emitLostRate -= (int)numEmit;
            if (numEmit == 0)
                return null;

            float velocityMag = emitter.AnimData[AnimGroupType.AllDirectionVelocity] * emitter.emitterSet.allDirVel;

            float scaleX = data.VolumeScale.X * emitterSet.emitterVolumeScale.X * emitter.AnimData[(AnimGroupType)22];
            float scaleY = data.VolumeScale.Y * emitterSet.emitterVolumeScale.Y * emitter.AnimData[(AnimGroupType)23];
            float scaleZ = data.VolumeScale.Z * emitterSet.emitterVolumeScale.Z * emitter.AnimData[(AnimGroupType)24];

            PtclInstance ptclFirst = null;
            for (int i = 0; i < numEmit; i++)
            {
                PtclInstance ptcl = mSys.AllocPtcl(emitter.calc.GetPtclType());
                if (ptclFirst == null)
                    ptclFirst = ptcl;
                if (ptcl == null)
                    break;

                ptcl.data = data;
                ptcl.stripe = null;

                float v0 = 1.0f - data.VolumeFillRatio;
                if (v0 == 1.0f)
                    v0 = 0.999f;

                if (v0 == 0.0f)
                {
                    ptcl.pos.X = scaleX * emitter.random.GetF32Range(-1.0f, 1.0f);
                    ptcl.pos.Y = scaleY * emitter.random.GetF32Range(-1.0f, 1.0f);
                    ptcl.pos.Z = scaleZ * emitter.random.GetF32Range(-1.0f, 1.0f);
                }
                else
                {
                    float v1 = emitter.random.GetF32();
                    float v2 = 1.0f - v0;
                    float v3 = v2 * v0;
                    v1 *= 1.0f - v0 * v0 * v0;

                    if (v1 < v2)
                    {
                        ptcl.pos.X = emitter.random.GetF32();
                        ptcl.pos.Y = emitter.random.GetF32() * v2 + v0;
                        ptcl.pos.Z = emitter.random.GetF32();
                    }
                    else if (v1 < v2 + v3)
                    {
                        ptcl.pos.X = emitter.random.GetF32() * v2 + v0;
                        ptcl.pos.Y = emitter.random.GetF32() * v0;
                        ptcl.pos.Z = emitter.random.GetF32();
                    }
                    else
                    {
                        ptcl.pos.X = emitter.random.GetF32() * v0;
                        ptcl.pos.Y = emitter.random.GetF32() * v0;
                        ptcl.pos.Z = emitter.random.GetF32() * v2 + v0;
                    }

                    if (emitter.random.GetF32() < 0.5f)
                        ptcl.pos.X = -ptcl.pos.X;

                    if (emitter.random.GetF32() < 0.5f)
                        ptcl.pos.Y = -ptcl.pos.Y;

                    if (emitter.random.GetF32() < 0.5f)
                        ptcl.pos.Z = -ptcl.pos.Z;

                    ptcl.pos.X *= scaleX;
                    ptcl.pos.Y *= scaleY;
                    ptcl.pos.Z *= scaleZ;
                }


                OpenTK.Vector3 normalizedVel = new OpenTK.Vector3();
                if (ptcl.pos.X != 0.0f || ptcl.pos.Y != 0.0f || ptcl.pos.Z != 0.0f)
                {
                    normalizedVel = ptcl.pos;
                    normalizedVel.Normalize();
                }

                ptcl.velocity.X = normalizedVel.X * velocityMag;
                ptcl.velocity.Y = normalizedVel.Y * velocityMag;
                ptcl.velocity.Z = normalizedVel.Z * velocityMag;

                if (data.ParticleAddXZVelocity != 0)
                {
                    var posXZ = new OpenTK.Vector3(ptcl.pos.X, 0.0f, ptcl.pos.Z);
                    if (posXZ.LengthSquared <= MathTriangular.FLT_MIN)
                    {
                        posXZ.X = emitter.random.GetF32Range(-1.0f, 1.0f);
                        posXZ.Z = emitter.random.GetF32Range(-1.0f, 1.0f);
                    }

                    if (posXZ.LengthSquared == 0.0f)
                        posXZ = new OpenTK.Vector3(0);
                    else
                        posXZ.Normalize();

                    posXZ *= data.ParticleAddXZVelocity;
                    ptcl.velocity += posXZ;
                }

                EmitCommon(emitter, ptcl);
            }

            emitter.IsEmitted = true;
            return ptclFirst;
        }

        static PtclInstance CalcEmitLine(EmitterInstance emitter)
        {
            var data = emitter.data;
            var emitterSet = emitter.emitterSet;
            var mSys = EffectSystem.ActiveSystem;

            float emissionRate = emitter.AnimData[AnimGroupType.EmissionRatio] * emitter.controller.EmissionRatio;
            emitter.emitLostRate += emissionRate;

            int counter = (int)emitter.counter;
            if (counter == 0 && emitter.emitLostRate < 1.0f && emissionRate != 0.0f)
                emitter.emitLostRate = 1.0f;

            int numEmit = (int)MathF.Floor(emitter.emitLostRate);
            if (data.EquidistantParticleEmission) numEmit = 1;
            emitter.emitLostRate -= (int)numEmit;
            if (numEmit == 0)
                return null;

            float velocityMag = emitter.AnimData[AnimGroupType.AllDirectionVelocity] * emitter.emitterSet.allDirVel;

            float scaleZ = data.VolumeScale.Z * emitterSet.emitterVolumeScale.Z * emitter.AnimData[(AnimGroupType)22];
            float center = data.LineCenterPosition * scaleZ;

            PtclInstance ptclFirst = null;
            for (int i = 0; i < numEmit; i++)
            {
                PtclInstance ptcl = mSys.AllocPtcl(emitter.calc.GetPtclType());
                if (ptclFirst == null)
                    ptclFirst = ptcl;
                if (ptcl == null)
                    break;

                ptcl.data = data;
                ptcl.stripe = null;

                ptcl.pos.X = 0.0f;
                ptcl.pos.Y = 0.0f;
                ptcl.pos.Z = emitter.random.GetF32() * scaleZ - (scaleZ + center) / 2.0f;

                ptcl.velocity.X = 0.0f;
                ptcl.velocity.Y = 0.0f;
                ptcl.velocity.Z = velocityMag;

                if (data.ParticleAddXZVelocity != 0)
                {
                    var posXZ = new OpenTK.Vector3(ptcl.pos.X, 0.0f, ptcl.pos.Z);
                    if (posXZ.LengthSquared <= MathTriangular.FLT_MIN)
                    {
                        posXZ.X = emitter.random.GetF32Range(-1.0f, 1.0f);
                        posXZ.Z = emitter.random.GetF32Range(-1.0f, 1.0f);
                    }

                    if (posXZ.LengthSquared == 0.0f)
                        posXZ = new OpenTK.Vector3(0);
                    else
                        posXZ.Normalize();

                    posXZ *= data.ParticleAddXZVelocity;
                    ptcl.velocity += posXZ;
                }

                EmitCommon(emitter, ptcl);
            }

            emitter.IsEmitted = true;
            return ptclFirst;
        }

        static PtclInstance CalcEmitLineSameDivide(EmitterInstance emitter)
        {
            var data = emitter.data;
            var emitterSet = emitter.emitterSet;
            var mSys = EffectSystem.ActiveSystem;

            float emissionRate = emitter.AnimData[AnimGroupType.EmissionRatio] * emitter.controller.EmissionRatio;
            emitter.emitLostRate += emissionRate;

            int counter = (int)emitter.counter;
            if (counter == 0 && emitter.emitLostRate < 1.0f && emissionRate != 0.0f)
                emitter.emitLostRate = 1.0f;

            int numEmit = (int)MathF.Floor(emitter.emitLostRate);
            if (data.EquidistantParticleEmission) numEmit = 1;
            emitter.emitLostRate -= (int)numEmit;
            if (numEmit == 0)
                return null;

            float velocityMag = emitter.AnimData[AnimGroupType.AllDirectionVelocity] * emitter.emitterSet.allDirVel;

            float scaleZ = data.VolumeScale.Z * emitterSet.emitterVolumeScale.Z * emitter.AnimData[(AnimGroupType)22];
            float center = data.LineCenterPosition * scaleZ;

            float pos;
            float lengthUnit;

            if (numEmit == 1)
            {
                pos = 0.5f;
                lengthUnit = 0.0f;
            }
            else
            {
                pos = 0.0f;
                lengthUnit = 1.0f / (float)(numEmit - 1);
            }

            PtclInstance ptclFirst = null;
            for (int i = 0; i < numEmit; i++)
            {
                PtclInstance ptcl = mSys.AllocPtcl(emitter.calc.GetPtclType());
                if (ptclFirst == null)
                    ptclFirst = ptcl;
                if (ptcl == null)
                    break;

                ptcl.data = data;
                ptcl.stripe = null;

                ptcl.pos.X = 0.0f;
                ptcl.pos.Y = 0.0f;
                ptcl.pos.Z = pos * scaleZ - (scaleZ + center) / 2.0f;

                ptcl.velocity.X = 0.0f;
                ptcl.velocity.Y = 0.0f;
                ptcl.velocity.Z = velocityMag;

                pos += lengthUnit;

                if (data.ParticleAddXZVelocity != 0)
                {
                    var posXZ = new OpenTK.Vector3(ptcl.pos.X, 0.0f, ptcl.pos.Z);
                    if (posXZ.LengthSquared <= MathTriangular.FLT_MIN)
                    {
                        posXZ.X = emitter.random.GetF32Range(-1.0f, 1.0f);
                        posXZ.Z = emitter.random.GetF32Range(-1.0f, 1.0f);
                    }

                    if (posXZ.LengthSquared == 0.0f)
                        posXZ = new OpenTK.Vector3(0);
                    else
                        posXZ.Normalize();

                    posXZ *= data.ParticleAddXZVelocity;
                    ptcl.velocity += posXZ;
                }

                EmitCommon(emitter, ptcl);
            }

            emitter.IsEmitted = true;
            return ptclFirst;
        }

        static PtclInstance CalcEmitRectangle(EmitterInstance emitter)
        {
            var data = emitter.data;
            var emitterSet = emitter.emitterSet;
            var mSys = EffectSystem.ActiveSystem;

            float emissionRate = emitter.AnimData[AnimGroupType.EmissionRatio] * emitter.controller.EmissionRatio;
            emitter.emitLostRate += emissionRate;

            int counter = (int)emitter.counter;
            if (counter == 0 && emitter.emitLostRate < 1.0f && emissionRate != 0.0f)
                emitter.emitLostRate = 1.0f;

            int numEmit = (int)MathF.Floor(emitter.emitLostRate);
            if (data.EquidistantParticleEmission) numEmit = 1;
            emitter.emitLostRate -= (int)numEmit;
            if (numEmit == 0)
                return null;

            float velocityMag = emitter.AnimData[AnimGroupType.AllDirectionVelocity] * emitter.emitterSet.allDirVel;

            float scaleX = data.VolumeScale.X * emitterSet.emitterVolumeScale.X * emitter.AnimData[(AnimGroupType)22];
            float scaleZ = data.VolumeScale.Z * emitterSet.emitterVolumeScale.Z * emitter.AnimData[(AnimGroupType)23];

            PtclInstance ptclFirst = null;
            for (int i = 0; i < numEmit; i++)
            {
                PtclInstance ptcl = mSys.AllocPtcl(emitter.calc.GetPtclType());
                if (ptclFirst == null)
                    ptclFirst = ptcl;
                if (ptcl == null)
                    break;

                ptcl.data = data;
                ptcl.stripe = null;

                uint v0 = emitter.random.GetU32();
                uint v1 = emitter.random.GetU32();

                OpenTK.Vector3 rndVec = new OpenTK.Vector3()
                {
                    X = emitter.random.GetF32Range(-1.0f, 1.0f),
                    Y = 0.0f,
                    Z = emitter.random.GetF32Range(-1.0f, 1.0f),
                };

                if (v0 < 0x7fffffff)
                {
                    ptcl.pos.X = scaleX * rndVec.X;
                    ptcl.pos.Y = 0.0f;
                    ptcl.pos.Z = (v1 < 0x7fffffff) ? scaleZ : -scaleZ;
                }
                else if (v0 < 0xAAAAAAAA)
                {
                    ptcl.pos.X = scaleX * rndVec.X;
                    ptcl.pos.Y = 0.0f;
                    ptcl.pos.Z = (v1 < 0x7fffffff) ? scaleZ : -scaleZ;
                }
                else // if (v0 < 0xFFFFFFFF)
                {
                    ptcl.pos.X = (v1 < 0x7fffffff) ? scaleX : -scaleX;
                    ptcl.pos.Y = 0.0f;
                    ptcl.pos.Z = scaleZ * rndVec.Z;
                }

                OpenTK.Vector3 normalizedVel = new OpenTK.Vector3();
                if (ptcl.pos.X != 0.0f || ptcl.pos.Y != 0.0f || ptcl.pos.Z != 0.0f)
                {
                    normalizedVel = ptcl.pos;
                    normalizedVel.Normalize();
                }

                ptcl.velocity.X = normalizedVel.X * velocityMag;
                ptcl.velocity.Y = 0.0f;
                ptcl.velocity.Z = normalizedVel.Z * velocityMag;

                if (data.ParticleAddXZVelocity != 0)
                {
                    var posXZ = new OpenTK.Vector3(ptcl.pos.X, 0.0f, ptcl.pos.Z);
                    if (posXZ.LengthSquared <= MathTriangular.FLT_MIN)
                    {
                        posXZ.X = emitter.random.GetF32Range(-1.0f, 1.0f);
                        posXZ.Z = emitter.random.GetF32Range(-1.0f, 1.0f);
                    }

                    if (posXZ.LengthSquared == 0.0f)
                        posXZ = new OpenTK.Vector3(0);
                    else
                        posXZ.Normalize();

                    posXZ *= data.ParticleAddXZVelocity;
                    ptcl.velocity += posXZ;
                }

                EmitCommon(emitter, ptcl);
            }

            emitter.IsEmitted = true;
            return ptclFirst;
        }
    }
}

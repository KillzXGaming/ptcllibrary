﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;

namespace EffectLibrary
{
    public class EmitterSimpleCalc : EmitterCalc
    {
        public EmitterSimpleCalc(EffectSystem system) : base(system)
        {
       
        }

        public override PtclType GetPtclType() => PtclType.Simple;

        public override uint CalcParticle(EmitterInstance emitter, CpuCore core, bool noCalcBehavior, bool noMakePtclAttributeBuffer)
        {
            var mSys = EffectSystem.ActiveSystem;

            if (!noMakePtclAttributeBuffer)
            {
                MakeAttributeBuffer(emitter, (int)emitter.numParticles);
                UpdateDynamicBlock(emitter);
            }
            else
            {
                emitter.PtclAttributeBuffer = null;
                emitter.DynamicUniformBlock = null;
            }

            emitter.numDrawParticle = 0;

            var callback1 = mSys.GetCurrentCustomActionParticleCalcCallback(emitter);
            var callback2 = mSys.GetCurrentCustomActionParticleMakeAttributeCallback(emitter);

            var ptcl = emitter.particleHead; 
            bool reversed = false;

            if (emitter.data.ParticleSortReverse)
            {
                ptcl = emitter.particleTail;
                reversed = true;
            }

            for (; ptcl != null; ptcl = reversed ? ptcl.Prev : ptcl.Next)
            {
                if (ptcl.data == null)
                    continue;

                if (!noCalcBehavior)
                {
                    if (ptcl.lifespan <= (int)ptcl.counter || ptcl.lifespan == 1 && ptcl.counter != 0.0f)
                    {
                        RemoveParticle(emitter, ptcl, core);
                        continue;
                    }

                    CalcSimpleParticleBehavior(emitter, ptcl, core);
                }

                if (callback1 != null)
                {
                    ParticleCalcArg arg = new ParticleCalcArg()
                    {
                        emitter = emitter,
                        ptcl = ptcl,
                        core = core,
                        noCalcBehavior = noCalcBehavior,
                    };
                    callback1(arg);
                }

                if (!noMakePtclAttributeBuffer)
                {
                    MakeParticleAttributeBuffer(emitter.PtclAttributeBuffer[emitter.numDrawParticle], ptcl, emitter.shaderAvailableAttribFlg, emitter.data.CameraOffset);
                    ptcl.PtclAttributeBuffer = emitter.PtclAttributeBuffer[emitter.numDrawParticle++];

                    if (callback2 != null)
                    {
                        ParticleMakeAttrArg arg = new ParticleMakeAttrArg
                        {
                            emitter = emitter,
                            ptcl = ptcl,
                            core = core,
                            noCalcBehavior = noCalcBehavior,
                        };
                        callback2(arg);
                    }
                }
            }

            emitter.IsCalculated = true;
            return emitter.numDrawParticle;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EffectLibrary
{
    public partial class EmitterCalc
    {
        public void CalcParitcleBehavior(EmitterInstance emitter, PtclInstance ptcl, SimpleEmitterData data)
        {
            ApplyAnim(ptcl);

            float emissionSpeedInv = 1.0f - emitter.emissionSpeed;
            int counter = (int)ptcl.counter;    
            var matrixRT = ptcl.pMatrixRT;

            if (emitter.ptclFollowType == PtclFollowType.Translate)
            {
                ptcl.matrixSRT.M41 = emitter.matrixSRT.M41;
                ptcl.matrixSRT.M42 = emitter.matrixSRT.M42;
                ptcl.matrixSRT.M43 = emitter.matrixSRT.M43;

                ptcl.matrixRT.M41 = emitter.matrixRT.M41;
                ptcl.matrixRT.M42 = emitter.matrixRT.M42;
                ptcl.matrixRT.M43 = emitter.matrixRT.M43;
            }

            ptcl.pos += ptcl.velocity * ptcl.randomF32 * emitter.emissionSpeed;

            if (emitter.particleBehaviorFlg.HasFlag(ParticleBehavior.AirResist))
                ptcl.velocity *= data.AirResist + (1.0f - data.AirResist) * emissionSpeedInv;

            if (emitter.particleBehaviorFlg.HasFlag(ParticleBehavior.Gravity))
            {
                var gravity = data.Gravity * emitter.AnimData[AnimGroupType.Gravity] * emitter.emissionSpeed;

                if (data.TransformGravity)
                    ptcl.velocity += MatrixExtension.MultMTX(gravity, matrixRT);
                else
                    ptcl.velocity += gravity;
            }

            if (emitter.particleBehaviorFlg.HasFlag(ParticleBehavior.Color0Anim)) {
                ColorAnim_4k3v(emitter, ptcl, 0);
            }
            if (emitter.particleBehaviorFlg.HasFlag(ParticleBehavior.Color1Anim)) {
                ColorAnim_4k3v(emitter, ptcl, 1);
            }

            if (emitter.particleBehaviorFlg.HasFlag(ParticleBehavior.PatternAnim0)) CalculateTexAnim(0, ptcl, data);
            if (emitter.particleBehaviorFlg.HasFlag(ParticleBehavior.PatternAnim1)) CalculateTexAnim(1, ptcl, data);
            if (emitter.particleBehaviorFlg.HasFlag(ParticleBehavior.PatternAnim2)) CalculateTexAnim(2, ptcl, data);

            if (emitter.particleBehaviorFlg.HasFlag(ParticleBehavior.UVShiftAnim0)) TexUvShiftAnim(0, ptcl, data, emitter.emissionSpeed);
            if (emitter.particleBehaviorFlg.HasFlag(ParticleBehavior.UVShiftAnim1)) TexUvShiftAnim(1, ptcl, data, emitter.emissionSpeed);
            if (emitter.particleBehaviorFlg.HasFlag(ParticleBehavior.UVShiftAnim2)) TexUvShiftAnim(2, ptcl, data, emitter.emissionSpeed);

            if (emitter.particleBehaviorFlg.HasFlag(ParticleBehavior.Alpha0Anim))
            {
                if (counter <= ptcl.alphaAnim[0].time2)
                    ptcl.alpha[0] += ptcl.alphaAnim[0].startDiff * emitter.emissionSpeed;
                else if (counter > ptcl.alphaAnim[0].time3)
                    ptcl.alpha[0] += ptcl.alphaAnim[0].endDiff * emitter.emissionSpeed;
            }

            if (emitter.particleBehaviorFlg.HasFlag(ParticleBehavior.Alpha1Anim))
            {
                if (counter <= ptcl.alphaAnim[1].time2)
                    ptcl.alpha[1] += ptcl.alphaAnim[1].startDiff * emitter.emissionSpeed;

                else if (counter > ptcl.alphaAnim[1].time3)
                    ptcl.alpha[1] += ptcl.alphaAnim[1].endDiff * emitter.emissionSpeed;
            }

            if (emitter.particleBehaviorFlg.HasFlag(ParticleBehavior.ScaleAnim))
            {
                if (counter <= ptcl.scaleAnim.time2)
                {
                    ptcl.scale += ptcl.scaleAnim.startDiff * emitter.emissionSpeed;
                }
                else if (counter > ptcl.scaleAnim.time3)
                {
                    ptcl.scale += ptcl.scaleAnim.endDiff * emitter.emissionSpeed;
                }
            }

            if (emitter.particleBehaviorFlg.HasFlag(ParticleBehavior.Rotation))
                ptcl.rotation += ptcl.angularVelocity * emitter.emissionSpeed;

            if (emitter.particleBehaviorFlg.HasFlag(ParticleBehavior.RotationInertia))
                ptcl.angularVelocity *= data.RotationInertia + (1.0f - data.RotationInertia) * emissionSpeedInv;
        }

        private void ColorAnim_4k3v(EmitterInstance emitter, PtclInstance ptcl, int colorIdx)
        {
            var data = emitter.data;

            int period = ptcl.lifespan / (int)data.ColorRepeatCount[colorIdx];
            if (period == 0)
                period = ptcl.lifespan;

            int counter = (int)ptcl.counter - 1;
            if (data.ColorRandomCount[colorIdx] != 0)
                counter += (int)(ptcl.randomU32 >> 6);
            counter %= period;

            int time2 = (data.ColorKey4Time2[colorIdx] * period) / 100;
            if (counter < time2)
                ptcl.color[colorIdx] = data.ColorKeys[colorIdx, 0];
            else
            {
                int time3 = (data.ColorKey4Time3[colorIdx] * period) / 100;
                if (counter < time3)
                    ptcl.color[colorIdx] = data.ColorKeys[colorIdx, 0] + (data.ColorKeys[colorIdx, 1] - data.ColorKeys[colorIdx, 0]) * ((float)(counter - time2) / (float)(time3 - time2));

                else
                {
                    int time4 = (data.ColorKey4Time4[colorIdx] * period) / 100;
                    if (counter < time4)
                        ptcl.color[colorIdx] = data.ColorKeys[colorIdx, 1] + (data.ColorKeys[colorIdx, 2] - data.ColorKeys[colorIdx, 1]) * ((float)(counter - time3) / (float)(time4 - time3));

                    else
                        ptcl.color[colorIdx] = data.ColorKeys[colorIdx, 2];
                }
            }
        }

        private static void TexUvShiftAnim(int texIdx, PtclInstance ptcl, SimpleEmitterData data, float emissionSpeed)
        {
            if (data.TextureEmitters.Length <= texIdx)
                return;

            ptcl.texAnimParam[texIdx].scroll.X += data.TextureEmitters[texIdx].TranslateShift.X * emissionSpeed;
            ptcl.texAnimParam[texIdx].scroll.Y += data.TextureEmitters[texIdx].TranslateShift.Y * emissionSpeed;
            ptcl.texAnimParam[texIdx].scale.X += data.TextureEmitters[texIdx].ScaleShift.X * emissionSpeed;
            ptcl.texAnimParam[texIdx].scale.Y += data.TextureEmitters[texIdx].ScaleShift.Y * emissionSpeed;
            ptcl.texAnimParam[texIdx].rotate += data.TextureEmitters[texIdx].RotateShift * emissionSpeed;
        }

        private void CalculateTexAnim(int texIdx, PtclInstance ptcl, SimpleEmitterData data)
        {
            if (data.TextureEmitters.Length <= texIdx)
                return;

            int period = data.TextureEmitters[texIdx].TexPtnAnimPeriod;
            short size = data.TextureEmitters[texIdx].TexPtnAnimUsedSize;
            int counter = (int)ptcl.counter;
            uint idx;

            if (size == 0)
                return;

            // Four animation types:
            // 1. Fit to lifespan
            // 2. Clamp
            // 3. Loop
            // 4. Random (Not handled here; handled in Emit function)

            if (period == 0)
                idx = (uint)((counter * size) / ptcl.lifespan); // (time / lifespan) * size
            else
                idx = (uint)(counter / period);                  //  time / period


            if (data.TextureEmitters[texIdx].TexPtnAnimClamp && idx >= size)
                return;

            if (period != 0 && !data.TextureEmitters[texIdx].TexPtnAnimClamp
                             && data.TextureEmitters[texIdx].TexPtnAnimRandStart)
            {
                idx += ptcl.randomU32;
            }

            int texPtnAnimIdxDiv = data.TextureEmitters[texIdx].TexPtnAnimIdxDiv;
            int texPtnAnimIdx = data.TextureEmitters[texIdx].TexPtnAnimData[idx % size];
            int offsetX = texPtnAnimIdx % texPtnAnimIdxDiv;
            int offsetY = texPtnAnimIdx / texPtnAnimIdxDiv;

            ptcl.texAnimParam[texIdx].offset.X = data.TextureEmitters[texIdx].UvScaleInit.X * (float)offsetX;
            ptcl.texAnimParam[texIdx].offset.Y = data.TextureEmitters[texIdx].UvScaleInit.Y * (float)offsetY;
        }

        public void ChildTexPtnAnim(PtclInstance ptcl, uint childFlags, ChildData childData)
        {
            var textureEmitter = childData.TextureEmitter;

            int period = textureEmitter.TexPtnAnimPeriod;
            short size = textureEmitter.TexPtnAnimUsedSize;
            int counter = (int)ptcl.counter;
            uint idx;

            if (size == 0)
                return;

            // Four animation types:
            // 1. Fit to lifespan
            // 2. Clamp
            // 3. Loop
            // 4. Random (Not handled here; handled in Emit function)

            if (period == 0)
                idx = (uint)((counter * size) / ptcl.lifespan); // (time / lifespan) * size
            else
                idx = (uint)(counter / period);                  //  time / period


            if (textureEmitter.TexPtnAnimClamp && idx >= size)
                return;

            if (period != 0 && !textureEmitter.TexPtnAnimClamp && (childFlags & 0x4000) != 0)
            {
                idx += ptcl.randomU32;
            }

            int texPtnAnimIdxDiv = textureEmitter.TexPtnAnimIdxDiv;
            int texPtnAnimIdx = textureEmitter.TexPtnAnimData[idx % size];
            int offsetX = texPtnAnimIdx % texPtnAnimIdxDiv;
            int offsetY = texPtnAnimIdx / texPtnAnimIdxDiv;

            ptcl.texAnimParam[0].offset.X = textureEmitter.UvScaleInit.X * (float)offsetX;
            ptcl.texAnimParam[0].offset.Y = textureEmitter.UvScaleInit.Y * (float)offsetY;
        }

        public void CalcSimpleParticleBehavior(EmitterInstance emitter, PtclInstance ptcl, CpuCore core)
        {
            if (emitter.data.GPUCalc)
            {
                ptcl.counter += emitter.emissionSpeed;
                return;
            }

            var data = emitter.data;
            var posBefore = new OpenTK.Vector3(ptcl.pos);
            var matrixSRT = ptcl.pMatrixSRT;

            CalcParitcleBehavior(emitter, ptcl, data);

            ptcl.worldPos = MatrixExtension.MultVec(ptcl.pos, matrixSRT);
            if (emitter.particleBehaviorFlg.HasFlag(ParticleBehavior.WorldDiff))
            {
                var posDiff = ptcl.pos - posBefore;
                if (MathF.Abs(posDiff.X) > 0.0001 || MathF.Abs(posDiff.Y) > 0.0001 || MathF.Abs(posDiff.Z) > 0.0001)
                    ptcl.posDiff += posDiff - ptcl.posDiff;

                ptcl.worldPosDiff = MatrixExtension.MultVecSR(ptcl.posDiff, matrixSRT) * emitter.emissionSpeed;
            }
            ptcl.counter += emitter.emissionSpeed;
        }

        public void CalcComplexParticleBehavior(EmitterInstance emitter, PtclInstance ptcl, CpuCore core)
        {
            if (emitter.data.GPUCalc)
            {
                ptcl.counter += emitter.emissionSpeed;
                return;
            }

            var data = emitter.data as ComplexEmitterData;
            var posBefore = new OpenTK.Vector3(ptcl.pos);
            var matrixSRT = ptcl.pMatrixSRT;

            CalcParitcleBehavior(emitter, ptcl, data);

            if ((data.fluctuationFlags & 1) != 0)
                CalcFluctuation(emitter, ptcl);

            if (data.fieldFlags != 0)
            {
                foreach (var field in data.FieldData)
                {
                    if (field is FieldRandomData) this.FieldRandom(emitter, ptcl, field);
                    if (field is FieldMagnetData) this.FieldMagnet(emitter, ptcl, field);
                    if (field is FieldSpinData) this.FieldSpin(emitter, ptcl, field);
                    if (field is FieldCollisionData) this.FieldCollision(emitter, ptcl, field);
                    if (field is FieldConvergenceData) this.FieldConvergence(emitter, ptcl, field);
                    if (field is FieldPosAddData) this.FieldPosAdd(emitter, ptcl, field);
                    if (field is FieldCurlNoiseData) this.FieldCurlNoise(emitter, ptcl, field);
                }
            }

            var posDiff = ptcl.pos - posBefore;
            if (MathF.Abs(posDiff.X) > 0.0001 || MathF.Abs(posDiff.Y) > 0.0001 || MathF.Abs(posDiff.Z) > 0.0001)
                ptcl.posDiff += posDiff - ptcl.posDiff;

            ptcl.worldPos = MatrixExtension.MultVec(ptcl.pos, matrixSRT);
            if (emitter.particleBehaviorFlg.HasFlag(ParticleBehavior.WorldDiff))
                ptcl.worldPosDiff = MatrixExtension.MultVecSR(ptcl.posDiff, matrixSRT) * emitter.emissionSpeed;

            ptcl.counter += emitter.emissionSpeed;
        }

        public void CalcChildParticleBehavior(EmitterInstance emitter, PtclInstance ptcl)
        {
            ApplyAnim(ptcl);

            var data = (ComplexEmitterData)emitter.data;
            var childData = data.ChildData;

            var posBefore = new OpenTK.Vector3(ptcl.pos);

            float emissionSpeedInv = 1.0f - emitter.emissionSpeed;
            int counter = (int)ptcl.counter;
            var matrixRT = ptcl.pMatrixRT;
            var matrixSRT = ptcl.pMatrixSRT;

            ptcl.pos += ptcl.velocity * ptcl.randomF32 * emitter.emissionSpeed;

            ptcl.velocity *= childData.AirResist + (1.0f - data.AirResist) * emissionSpeedInv;

            {
                var gravity = childData.Gravity * emitter.emissionSpeed;

                if (data.TransformGravity)
                    ptcl.velocity += MatrixExtension.MultMTX(gravity, matrixRT);
                else
                    ptcl.velocity += gravity;
            }

            {
                if (counter <= childData.AlphaKey0.Time2)
                    ptcl.alpha[0] += ptcl.alphaAnim[0].startDiff * emitter.emissionSpeed;
                else if (counter >= childData.AlphaKey0.Time3)
                    ptcl.alpha[0] += ptcl.alphaAnim[0].endDiff * emitter.emissionSpeed;

                if (counter <= childData.AlphaKey1.Time2)
                    ptcl.alpha[1] += ptcl.alphaAnim[1].startDiff * emitter.emissionSpeed;
                else if (counter >= childData.AlphaKey1.Time3)
                    ptcl.alpha[1] += ptcl.alphaAnim[1].endDiff * emitter.emissionSpeed;
            }

            {
                if (counter >= childData.ScaleAnimTime1)
                {
                    ptcl.scale += ptcl.scaleAnim.startDiff * emitter.emissionSpeed;
                }
            }

            ptcl.rotation += ptcl.angularVelocity * emitter.emissionSpeed;

            ptcl.angularVelocity *= childData.RotInertia + (1.0f - childData.RotInertia) * emissionSpeedInv;

            if ((data.childFlags & 0x2000) != 0)
                ChildTexPtnAnim(ptcl, data.childFlags, childData);

            if ((data.childFlags & 0x800) != 0)
            {
                foreach (var field in data.FieldData)
                {
                    if (field is FieldRandomData) this.FieldRandom(emitter, ptcl, field);
                    if (field is FieldMagnetData) this.FieldMagnet(emitter, ptcl, field);
                    if (field is FieldSpinData) this.FieldSpin(emitter, ptcl, field);
                    if (field is FieldCollisionData) this.FieldCollision(emitter, ptcl, field);
                    if (field is FieldConvergenceData) this.FieldConvergence(emitter, ptcl, field);
                    if (field is FieldPosAddData) this.FieldPosAdd(emitter, ptcl, field);
                    if (field is FieldCurlNoiseData) this.FieldCurlNoise(emitter, ptcl, field);
                }
            }


            var posDiff = ptcl.pos - posBefore;
            if (MathF.Abs(posDiff.X) > 0.0001 || MathF.Abs(posDiff.Y) > 0.0001 || MathF.Abs(posDiff.Z) > 0.0001)
                ptcl.posDiff += posDiff - ptcl.posDiff;

            ptcl.worldPos = MatrixExtension.MultVec(ptcl.pos, ptcl.matrixSRT);
            ptcl.worldPosDiff = MatrixExtension.MultVecSR(ptcl.posDiff, matrixSRT) * emitter.emissionSpeed;

            ptcl.counter += emitter.emissionSpeed;
        }

        public void MakeParticleAttributeBuffer(PtclAttributeBuffer ptclAttributeBuffer, PtclInstance ptcl,
            ShaderAvailableAttrib shaderAvailableAttribFlg, float cameraOffset)
        {
            EmitterSet emitterSet = ptcl.emitter.emitterSet;
            ptclAttributeBuffer.WorldPosition = new OpenTK.Vector4(ptcl.worldPos, cameraOffset);
            //W for world position is the life span for particles for newer versions
            if (EffectSystem.ActiveSystem.HasResourceFlags(FileResourceFlags.HasGPUBehavior))
            {
                if (ptcl.data.GPUCalc)
                {
                    ptclAttributeBuffer.WorldPosition.Xyz = ptcl.pos;
                    ptclAttributeBuffer.WorldPosition.W = ptcl.spawnTime;
                }
                else
                    ptclAttributeBuffer.WorldPosition.W = ptcl.counter;
            }

            //XY for scale. ZW for rotation
            ptclAttributeBuffer.ScaleCenter.Xy = ptcl.scale * emitterSet.ptclEffectiveScale * ptcl.fluctuationScale;
            ptclAttributeBuffer.ScaleCenter.Z = ptcl.texAnimParam[0].rotate;
            if (ptcl.texAnimParam.Length > 1)
                ptclAttributeBuffer.ScaleCenter.W = ptcl.texAnimParam[1].rotate;

            //Color0 (used for animated colors that change during lifespan)
            ptclAttributeBuffer.Color0.Xyz = ptcl.color[0].Xyz;
            ptclAttributeBuffer.Color0.W = ptcl.alpha[0] * ptcl.fluctuationAlpha;

            //Older versions don't use alpha 1
            if (EffectSystem.ActiveSystem.HasResourceFlags(FileResourceFlags.HasAlpha1))
                ptcl.alpha[1] = ptcl.alpha[0];

            if (EffectSystem.ActiveSystem.HasResourceFlags(FileResourceFlags.HasGPUBehavior))
            {
                ptclAttributeBuffer.Vector.Xyz = new OpenTK.Vector3(ptcl.velocity.X, -ptcl.velocity.Y, ptcl.velocity.Z);
                ptclAttributeBuffer.Vector.W = ptcl.randomF32;
                ptclAttributeBuffer.Random.Xyz = ptcl.random;
                ptclAttributeBuffer.Random.W = ptcl.lifespan;
            }

            if (shaderAvailableAttribFlg.HasFlag(ShaderAvailableAttrib.Color1))
            {
                //Color1 (used for animated colors that change during lifespan)
                ptclAttributeBuffer.Color1.Xyz = ptcl.color[1].Xyz;
                ptclAttributeBuffer.Color1.W = ptcl.alpha[1] * ptcl.fluctuationAlpha;
            }

            ptclAttributeBuffer.TextureAnim.X = ptcl.texAnimParam[0].offset.X + ptcl.texAnimParam[0].scroll.X;
            ptclAttributeBuffer.TextureAnim.Y = ptcl.texAnimParam[0].offset.Y - ptcl.texAnimParam[0].scroll.Y;
            ptclAttributeBuffer.TextureAnim.Zw = ptcl.texAnimParam[0].scale;

            if (shaderAvailableAttribFlg.HasFlag(ShaderAvailableAttrib.WorldPosDif))
                ptclAttributeBuffer.WorldPositionDif = ptcl.worldPosDiff;

            if (shaderAvailableAttribFlg.HasFlag(ShaderAvailableAttrib.Rot))
            {
                ptclAttributeBuffer.Rotate.Xyz = ptcl.rotation;
                ptclAttributeBuffer.Rotate.W = 0.0f;
            }

            if (shaderAvailableAttribFlg.HasFlag(ShaderAvailableAttrib.SubTexAnim))
            {
                ptclAttributeBuffer.SubTextureAnim.X = ptcl.texAnimParam[1].offset.X + ptcl.texAnimParam[1].scroll.X;
                ptclAttributeBuffer.SubTextureAnim.Y = ptcl.texAnimParam[1].offset.Y - ptcl.texAnimParam[1].scroll.Y;
                ptclAttributeBuffer.SubTextureAnim.Zw = ptcl.texAnimParam[1].scale;
            }

            if (shaderAvailableAttribFlg.HasFlag(ShaderAvailableAttrib.EmMat))
            {
                if (ptcl.data.GPUCalc)
                {
                    ptclAttributeBuffer.EmissionMatrix[0] = ptcl.pMatrixSRT.Column0;
                    ptclAttributeBuffer.EmissionMatrix[1] = ptcl.pMatrixSRT.Column1;
                    ptclAttributeBuffer.EmissionMatrix[2] = ptcl.pMatrixSRT.Column2;
                }
                else
                {
                    ptclAttributeBuffer.EmissionMatrix[0] = ptcl.pMatrixRT.Column0;
                    ptclAttributeBuffer.EmissionMatrix[1] = ptcl.pMatrixRT.Column1;
                    ptclAttributeBuffer.EmissionMatrix[2] = ptcl.pMatrixRT.Column2;
                }
            }
        }
    }
}

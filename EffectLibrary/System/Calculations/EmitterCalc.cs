﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EffectLibrary
{
    public partial class EmitterCalc
    {
        public Func<EmitterInstance, PtclInstance>[] mEmitFunctions = new Func<EmitterInstance, PtclInstance>[]
        {
            CalcEmitPoint,
            CalcEmitCircle,
            CalcEmitCircleSameDivide,
            CalcEmitFillCircle,
            CalcEmitSphere,
            CalcEmitSphereSameDivide,
            CalcEmitSphereSameDivide64,
            CalcEmitFillSphere,
            CalcEmitCylinder,
            CalcEmitFillCylinder,
            CalcEmitBox,
            CalcEmitFillBox,
            CalcEmitLine,
            CalcEmitLineSameDivide,
            CalcEmitRectangle,
        };

        public EmitterCalc(EffectSystem system)
        {
        }

        public virtual PtclType GetPtclType() => PtclType.Simple;

        public virtual void CalcEmitter(EmitterInstance emitter)
        {
            var mSys = EffectSystem.ActiveSystem;
            EmitterSet emitterSet = emitter.emitterSet;
            var data = emitter.data;
            bool emit;

            if (emitterSet.doFade)
            {
                emit = !data.NoEmissionDuringFade;

                emitter.fadeAlpha -= data.AlphaFadeStep;
                if (emitter.fadeAlpha <= 0.0f)
                {
                    mSys.KillEmitter(emitter);
                    return;
                }
            }
            else
            {
                emit = true;

                emitter.fadeAlpha += data.AlphaFadeStep;
                if (emitter.fadeAlpha > 1.0f)
                    emitter.fadeAlpha = 1.0f;
            }

            ApplyAnim(emitter);
            UpdateEmitterInfoByEmit(emitter);

            int time = (int)emitter.counter - emitterSet.startFrame;
            if (time < data.EmissionEndFrame && emit)
            {
                if (time >= data.EmissionStartFrame)
                {
                    if (!data.EquidistantParticleEmission)
                    {
                        float interval = emitter.emissionInterval * emitter.controller.EmissionInterval;
                        float numEmit, numEmit2 = 0.0f;
                        if (!emitter.IsEmitted)
                            numEmit2 = 1.0f;

                        if ((int)interval != 0)
                        {
                            if (emitter.emissionSpeed >= 1.0f)
                            {
                                if ((int)emitter.emitCounter >= interval)
                                {
                                    numEmit = 1.0f;
                                    emitter.emitCounter = 0.0f;
                                }
                                else
                                {
                                    numEmit = numEmit2;
                                    emitter.emitCounter += emitter.emissionSpeed;
                                }
                            }
                            else
                            {
                                if (emitter.emitCounter >= interval && (int)emitter.counter != (int)emitter.counter2)
                                {
                                    numEmit = 1.0f;
                                    emitter.emitCounter = 0.0f;
                                    emitter.emitLostTime = 0.0f;
                                    emitter.preCalcCounter = emitter.counter;
                                }
                                else
                                {
                                    numEmit = numEmit2;
                                    emitter.emitCounter += emitter.emissionSpeed;
                                }
                            }
                        }
                        else
                        {
                            if (emitter.emissionSpeed >= 1.0f)
                                numEmit = 1.0f;
                            else
                            {
                                numEmit = (emitter.counter - emitter.preCalcCounter + emitter.emitLostTime);

                                if (emitter.emitLostTime >= interval)
                                    emitter.emitLostTime -= interval;

                                emitter.emitLostTime += emitter.counter - emitter.preCalcCounter - (int)numEmit;
                                emitter.emitCounter = 0.0f;
                                emitter.preCalcCounter = emitter.counter;

                                if (data.MaxLifespan == 1)
                                    numEmit = 1.0f;
                            }
                        }

                        for (int i = 0; i < (int)numEmit; i++)
                        {
                            if (emitterSet.numEmissionPoints > 0)
                            {
                                for (int j = 0; j < emitterSet.numEmissionPoints; j++)
                                {
                                    PtclInstance particleHeadBefore = emitter.particleHead;
                                    mEmitFunctions[(int)data.EmitterFunc].Invoke(emitter);
                                    for (PtclInstance ptcl = emitter.particleHead; ptcl != particleHeadBefore; ptcl = ptcl.Next)
                                        ptcl.pos += emitterSet.emissionPoints[j];
                                }
                            }
                            else
                            {
                                mEmitFunctions[(int)data.EmitterFunc].Invoke(emitter);
                            }
                        }
                        emitter.emissionInterval = data.EmissionInterval + emitter.random.GetS32(data.EmissionIntervalRandomness);
                    }
                    else
                    {
                        EmitSameDistance(data, emitter);
                    }
                }
            }
            else
            {
                if (emitter.particleHead == null && emitter.childParticleHead == null)
                {
                    mSys.KillEmitter(emitter);
                    return;
                }
            }

            emitter.counter2 = emitter.counter;
            emitter.counter += emitter.emissionSpeed;

            if (emitter.particleHead != null)
            {
                emitter.prevPos.X = emitter.matrixRT.M41;
                emitter.prevPos.Y = emitter.matrixRT.M42;
                emitter.prevPos.Z = emitter.matrixRT.M43;
                emitter.prevPosSet = true;
            }

            if (emitter.data.GPUCalc)
            {
                PtclInstance particleHeadBefore = emitter.particleHead;
                for (PtclInstance ptcl = emitter.particleHead; ptcl != particleHeadBefore; ptcl = ptcl.Next) { 
                    ptcl.worldPos = MatrixExtension.MultVec(ptcl.pos, ptcl.matrixSRT);
                }
            }
        }

        public void UpdateDynamicBlock(EmitterInstance emitter)
        {
            emitter.DynamicUniformBlock = new EmitterDynamicUniformBlock();
            emitter.DynamicUniformBlock.Init();

            var emitterSetColor = new OpenTK.Vector4(emitter.emitterSet.color) * emitter.data.ColorScale;

            emitter.DynamicUniformBlock.Color0.X = emitterSetColor.X * emitter.AnimData[AnimGroupType.Color0R];
            emitter.DynamicUniformBlock.Color0.Y = emitterSetColor.Y * emitter.AnimData[AnimGroupType.Color0G];
            emitter.DynamicUniformBlock.Color0.Z = emitterSetColor.Z * emitter.AnimData[AnimGroupType.Color0B];
            emitter.DynamicUniformBlock.Color0.W = emitter.emitterSet.color.W * emitter.AnimData[AnimGroupType.Alpha0] * emitter.fadeAlpha;

            emitter.DynamicUniformBlock.Color1.X = emitterSetColor.X * emitter.AnimData[AnimGroupType.Color1R];
            emitter.DynamicUniformBlock.Color1.Y = emitterSetColor.Y * emitter.AnimData[AnimGroupType.Color1G];
            emitter.DynamicUniformBlock.Color1.Z = emitterSetColor.Z * emitter.AnimData[AnimGroupType.Color1B];
            emitter.DynamicUniformBlock.Color1.W = emitter.emitterSet.color.W * emitter.AnimData[AnimGroupType.Alpha0] * emitter.fadeAlpha;

            emitter.DynamicUniformBlock.Param1 = new OpenTK.Vector4(emitter.counter, 1, 1, emitter.emissionSpeed);
            emitter.DynamicUniformBlock.EmissionTransform0 = emitter.matrixSRT;
            emitter.DynamicUniformBlock.EmissionTransform1 = emitter.matrixRT;
        }

        public virtual uint CalcParticle(EmitterInstance emitter, CpuCore core, bool noCalcBehavior, bool noMakePtclAttributeBuffer)
        {
            return 0;
        }

        public virtual uint CalcChildParticle(EmitterInstance emitter, CpuCore core, bool noCalcBehavior, bool noMakePtclAttributeBuffer)
        {
            return 0;
        }

        public void ApplyAnim(EmitterInstance emitter)
        {
            var data = emitter.data;
            var keyGroups = emitter.keyGroups;

            emitter.animMatrixRT = OpenTK.Matrix4.Identity;
            emitter.animMatrixSRT = OpenTK.Matrix4.Identity;

            emitter.animMatrixRT = data.AnimMatrixRT * emitter.animMatrixRT;
            emitter.animMatrixSRT = data.AnimMatrixSRT * emitter.animMatrixSRT;

            if (keyGroups == null)
            {
                var scale = new OpenTK.Vector3();
                var rotate = new OpenTK.Vector3();
                var translate = new OpenTK.Vector3();

                scale.X = data.BaseScale.X + emitter.scaleRandom.X;
                scale.Y = data.BaseScale.Y + emitter.scaleRandom.Y;
                scale.Z = data.BaseScale.Z + emitter.scaleRandom.Z;

                rotate.X = data.BaseRotation.X + emitter.rotateRandom.X;
                rotate.Y = data.BaseRotation.Y + emitter.rotateRandom.Y;
                rotate.Z = data.BaseRotation.Z + emitter.rotateRandom.Z;

                translate.X = data.BaseTranslation.X + emitter.translateRandom.X;
                translate.Y = data.BaseTranslation.Y + emitter.translateRandom.Y;
                translate.Z = data.BaseTranslation.Z + emitter.translateRandom.Z;

                emitter.animMatrixSRT = MatrixExtension.CreateSRT(scale, rotate, translate);
                emitter.animMatrixRT = MatrixExtension.CreateRT(rotate, translate);
            }
            else
            {
                for (int i = 0; i < keyGroups.Length; i++) {
                    emitter.AnimData[keyGroups[i].Type] = CalcAnimKeyFrame(keyGroups[i], emitter.counter);
                }

                var scale = new OpenTK.Vector3(
                    emitter.AnimData[(AnimGroupType)2], 
                    emitter.AnimData[(AnimGroupType)3],
                    emitter.AnimData[(AnimGroupType)4]);

                var rotate = new OpenTK.Vector3(
                    emitter.AnimData[(AnimGroupType)5],
                    emitter.AnimData[(AnimGroupType)6],
                    emitter.AnimData[(AnimGroupType)7]);

                var translate = new OpenTK.Vector3(
                    emitter.AnimData[(AnimGroupType)8],
                    emitter.AnimData[(AnimGroupType)9],
                    emitter.AnimData[(AnimGroupType)10]);

                emitter.animMatrixSRT = MatrixExtension.CreateSRT(scale, rotate, translate);
                emitter.animMatrixRT = MatrixExtension.CreateRT(rotate, translate);
            }
        }

        public void ApplyAnim(PtclInstance ptcl)
        {
            var keyGroups = ptcl.keyGroups;

            if (keyGroups != null)
            {
                int counter = (int)ptcl.counter;
                counter %= ptcl.lifespan;

                for (int i = 0; i < keyGroups.Length; i++) {
                    ptcl.AnimData[keyGroups[i].Type] = CalcAnimKeyFrame(keyGroups[i], counter);
                }

                if (ptcl.emitter.data.Color0Source == ColorSource.Key8)
                {
                    ptcl.color[0].X = ptcl.AnimData[AnimGroupType.EmitterColor0R];
                    ptcl.color[0].Y = ptcl.AnimData[AnimGroupType.EmitterColor0G];
                    ptcl.color[0].Z = ptcl.AnimData[AnimGroupType.EmitterColor0B];
                }
                if (ptcl.emitter.data.Color1Source == ColorSource.Key8)
                {
                    ptcl.color[1].X = ptcl.AnimData[AnimGroupType.EmitterColor1R];
                    ptcl.color[1].Y = ptcl.AnimData[AnimGroupType.EmitterColor1G];
                    ptcl.color[1].Z = ptcl.AnimData[AnimGroupType.EmitterColor1B];
                }
            }
        }

        public void UpdateEmitterInfoByEmit(EmitterInstance emitter)
        {
            emitter.matrixSRT = emitter.animMatrixSRT * emitter.emitterSet.matrixSRT;
            emitter.matrixRT = emitter.animMatrixRT * emitter.emitterSet.matrixRT;
        }

        public void EmitSameDistance(SimpleEmitterData data, EmitterInstance emitter)
        {
            if (!emitter.prevPosSet) {
                mEmitFunctions[(int)data.EmitterFunc].Invoke(emitter);
                return;
            }

            var prevPos = emitter.prevPos;
            var currPos = emitter.matrixRT.Row3.Xyz;

            var moved = prevPos - currPos;
            float movedDist = moved.Length;
            float lostDist = emitter.emitLostDistance;

            if (movedDist < data.PerFrameTravelDistanceFallback || movedDist == 0.0f)
                movedDist = data.PerFrameTravelDistanceMinClamp;
            else if (movedDist < data.PerFrameTravelDistanceMinClamp)
                movedDist = movedDist * data.PerFrameTravelDistanceMinClamp / movedDist;
            else if (data.PerFrameTravelDistanceMaxClamp < movedDist)
                movedDist = movedDist * data.PerFrameTravelDistanceMaxClamp / movedDist;

            float remainDist = lostDist + movedDist;
            int numEmit = (int)(remainDist / data.EquidistantParticleEmissionUnit); // No division-by-zero check

            for (int i = 0; i < numEmit; i++)
            {
                remainDist -= data.EquidistantParticleEmissionUnit;

                float prevCurrRatio = 0.0f;
                if (movedDist != 0.0f)
                    prevCurrRatio = remainDist / movedDist;

                var pos_0 = currPos * new OpenTK.Vector3(1 - prevCurrRatio);
                var pos_1 = prevPos * new OpenTK.Vector3(prevCurrRatio);
                var pos = pos_0 + pos_1;

                emitter.matrixRT.M41 = pos.X;
                emitter.matrixRT.M42 = pos.Y;
                emitter.matrixRT.M43 = pos.Z;
                emitter.matrixSRT.M41 = pos.X;
                emitter.matrixSRT.M42 = pos.Y;
                emitter.matrixSRT.M43 = pos.Z;

                mEmitFunctions[(int)data.EmitterFunc].Invoke(emitter);

                emitter.matrixRT.M41 = currPos.X;
                emitter.matrixRT.M42 = currPos.Y;
                emitter.matrixRT.M43 = currPos.Z;
                emitter.matrixSRT.M41 = currPos.X;
                emitter.matrixSRT.M42 = currPos.Y;
                emitter.matrixSRT.M43 = currPos.Z;
            }

            emitter.emitLostDistance = remainDist;
        }

        public void MakeAttributeBuffer(EmitterInstance emitter, int numInstances)
        {
            emitter.PtclAttributeBuffer = new  PtclAttributeBuffer[numInstances];
            for (int i = 0; i < numInstances; i++)
            {
                emitter.PtclAttributeBuffer[i] = new PtclAttributeBuffer();
                emitter.PtclAttributeBuffer[i].Init();
            }
        }

        public void MakeChildAttributeBuffer(EmitterInstance emitter, int numInstances)
        {
            emitter.ChildPtclAttributeBuffer = new PtclAttributeBuffer[numInstances];
            for (int i = 0; i < numInstances; i++)
            {
                emitter.ChildPtclAttributeBuffer[i] = new PtclAttributeBuffer();
                emitter.ChildPtclAttributeBuffer[i].Init();
            }
        }

        static void AddPtclToList(EmitterInstance emitter, PtclInstance ptcl)
        {
            if (emitter.particleHead == null)
            {
                emitter.particleHead = ptcl;
                ptcl.Next = null;
                ptcl.Prev = null;
            }
            else
            {
                emitter.particleHead.Prev = ptcl;
                ptcl.Next = emitter.particleHead;
                emitter.particleHead = ptcl;
                ptcl.Prev = null;
            }

            if (emitter.particleTail == null)
                emitter.particleTail = ptcl;

            emitter.numParticles++;
        }

        static void AddChildPtclToList(EmitterInstance emitter, PtclInstance childPtcl)
        {
            if (emitter.childParticleHead == null)
            {
                emitter.childParticleHead = childPtcl;
                childPtcl.Next = null;
                childPtcl.Prev = null;
            }
            else
            {
                emitter.childParticleHead.Prev = childPtcl;
                childPtcl.Next = emitter.childParticleHead;
                emitter.childParticleHead = childPtcl;
                childPtcl.Prev = null;
            }

            if (emitter.childParticleTail == null)
                emitter.childParticleTail = childPtcl;

            emitter.numChildParticles++;
        }

        public static void RemoveParticle(EmitterInstance emitter, PtclInstance ptcl, CpuCore core)
        {
            EffectSystem.ActiveSystem.AddPtclRemoveList(ptcl, (int)core);
        }
    }
}

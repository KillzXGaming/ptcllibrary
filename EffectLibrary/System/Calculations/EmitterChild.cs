﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EffectLibrary
{
    public partial class EmitterCalc
    {
        public static void EmitChildParticle(EmitterInstance emitter, PtclInstance ptcl)
        {
            var data = emitter.data as ComplexEmitterData;
            var childData = data.ChildData;
            for (int i = 0; i < childData.numChildParticles; i++)
            {
                PtclInstance childPtcl = EffectSystem.ActiveSystem.AllocPtcl(PtclType.Child);
                if (childPtcl == null)
                    continue;

                childPtcl.data = emitter.data;
                childPtcl.stripe = null;
                childPtcl.lifespan = childData.ptclMaxLifespan;
                childPtcl.emitter = emitter;

                if ((data.childFlags & (int)ChildFlags.InheritVelocity) != 0)
                    childPtcl.velocity = ptcl.posDiff * childData.VelocityInheritRatio;
                else
                    childPtcl.velocity = new OpenTK.Vector3(0);

                float alpha = childData.AlphaKey0.StartValue;
                if ((data.childFlags & (int)ChildFlags.InheritFlucAlpha) != 0)
                    alpha = ptcl.alpha[0] * ptcl.fluctuationAlpha;

                float scaleRandom = 1.0f - childData.PtclScaleRandom * emitter.random.GetF32();

                if ((data.childFlags & (int)ChildFlags.InheritPtclScale) != 0)
                {
                    var animScale = GetAnimationScale(emitter);
                    childPtcl.scale.X = ptcl.scale.X * childData.ScaleInheritRatio * ptcl.fluctuationScale * scaleRandom * animScale.X;
                    childPtcl.scale.Y = ptcl.scale.Y * childData.ScaleInheritRatio * ptcl.fluctuationScale * scaleRandom * animScale.Y;
                }
                else
                {
                    childPtcl.scale.X = scaleRandom * childData.PtclEmitScale.X;
                    childPtcl.scale.Y = scaleRandom * childData.PtclEmitScale.Y;
                }

                if ((data.childFlags & (int)ChildFlags.InheritRotation) != 0)
                {
                    switch (childData.rotationMode)
                    {
                        case VertexRotationMode.Rotate_X:
                            childPtcl.rotation.X = ptcl.rotation.X;
                            childPtcl.rotation.Y = 0.0f;
                            childPtcl.rotation.Z = 0.0f;
                            break;
                        case VertexRotationMode.Rotate_Y:
                            childPtcl.rotation.X = 0.0f;
                            childPtcl.rotation.Y = ptcl.rotation.Y;
                            childPtcl.rotation.Z = 0.0f;
                            break;
                        case VertexRotationMode.Rotate_Z:
                            childPtcl.rotation.X = 0.0f;
                            childPtcl.rotation.Y = 0.0f;
                            childPtcl.rotation.Z = ptcl.rotation.Z;
                            break;
                        default:
                            childPtcl.rotation = ptcl.rotation;
                            break;
                    }
                }
                else
                {
                    childPtcl.rotation.X = childData.PtclRotate.X + emitter.random.GetF32() * childData.PtclRotateRandom.X;
                    childPtcl.rotation.Y = childData.PtclRotate.Y + emitter.random.GetF32() * childData.PtclRotateRandom.Y;
                    childPtcl.rotation.Z = childData.PtclRotate.Z + emitter.random.GetF32() * childData.PtclRotateRandom.Z;
                }

                if ((data.childFlags & (int)ChildFlags.InheritColor0) != 0)
                {
                    childPtcl.color[0] = new OpenTK.Vector4(ptcl.color[0]);
                }
                else
                {
                    childPtcl.color[0].X = childData.PtclColor0.X;
                    childPtcl.color[0].Y = childData.PtclColor0.Y;
                    childPtcl.color[0].Z = childData.PtclColor0.Z;
                    childPtcl.color[0].W = 0.0f;
                }

                if ((data.childFlags & (int)ChildFlags.InheritColor1) != 0)
                {
                    childPtcl.color[1] = new OpenTK.Vector4(ptcl.color[1]);
                }
                else
                {
                    childPtcl.color[1].X = childData.PtclColor1.X;
                    childPtcl.color[1].Y = childData.PtclColor1.Y;
                    childPtcl.color[1].Z = childData.PtclColor1.Z;
                    childPtcl.color[1].W = 0.0f;
                }

                childPtcl.angularVelocity.X = childData.AngularVelocity.X + emitter.random.GetF32() * childData.AngularVelocityRandom.X;
                childPtcl.angularVelocity.Y = childData.AngularVelocity.Y + emitter.random.GetF32() * childData.AngularVelocityRandom.Y;
                childPtcl.angularVelocity.Z = childData.AngularVelocity.Z + emitter.random.GetF32() * childData.AngularVelocityRandom.Z;

                childPtcl.texAnimParam = new TexUVParam[1];
                childPtcl.texAnimParam[0] = new TexUVParam();
                childPtcl.texAnimParam[0].scroll = new OpenTK.Vector2(0);
                childPtcl.texAnimParam[0].rotate = 0.0f;
                childPtcl.texAnimParam[0].scale = new OpenTK.Vector2(1);

                if (childData.AlphaKey0.Time2 == 0)
                {
                    childPtcl.alphaAnim[0].startDiff = 0.0f;
                    childPtcl.alpha[0] = alpha;
                }
                else
                {
                    childPtcl.alphaAnim[0].startDiff = (alpha - childData.AlphaKey0.StartDifference) / (float)childData.AlphaKey0.Time2;
                    childPtcl.alpha[0] = childData.AlphaKey0.StartDifference;
                }

                childPtcl.alphaAnim[0].endDiff = (childData.AlphaKey0.EndDifference - alpha) / (float)(childPtcl.lifespan - childData.AlphaKey0.Time3);

                if (childData.AlphaKey1.Time2 == 0)
                {
                    childPtcl.alphaAnim[1].startDiff = 0.0f;
                    childPtcl.alpha[1] = alpha;
                }
                else
                {
                    childPtcl.alphaAnim[1].startDiff = (alpha - childData.AlphaKey1.StartDifference) / (float)childData.AlphaKey1.Time2;
                    childPtcl.alpha[1] = childData.AlphaKey1.StartDifference;
                }

                childPtcl.alphaAnim[1].endDiff = (childData.AlphaKey1.EndDifference - alpha) / (float)(childPtcl.lifespan - childData.AlphaKey1.Time3);

                float scaleAnimDurationInv = 1.0f / (childPtcl.lifespan - childData.ScaleAnimTime1);
                childPtcl.scaleAnim.startDiff.X = (childData.PtclScaleEnd.X - 1.0f) * scaleAnimDurationInv * childPtcl.scale.X;
                childPtcl.scaleAnim.startDiff.Y = (childData.PtclScaleEnd.Y - 1.0f) * scaleAnimDurationInv * childPtcl.scale.Y;

                childPtcl.fluctuationAlpha = 1.0f;
                childPtcl.fluctuationScale = 1.0f;

                if (childData.TextureEmitter.TexPtnAnimNum <= 1)
                    childPtcl.texAnimParam[0].offset = new OpenTK.Vector2(0.0f, 0.0f);
                else // TexPtnAnim Type Random
                {
                    int texPtnAnimIdx = (int)emitter.random.GetU32(childData.TextureEmitter.TexPtnAnimNum);
                    int texPtnAnimIdxDiv = childData.TextureEmitter.TexPtnAnimIdxDiv;
                    int offsetX = texPtnAnimIdx % texPtnAnimIdxDiv;
                    int offsetY = texPtnAnimIdx / texPtnAnimIdxDiv;

                    childPtcl.texAnimParam[0].offset.X = childData.TextureEmitter.UvScaleInit.X * (float)offsetX;
                    childPtcl.texAnimParam[0].offset.Y = childData.TextureEmitter.UvScaleInit.Y * (float)offsetY;
                }

                var randomNormVec3 = emitter.random.GetNormalizedVec3();
                var randomVec3 = emitter.random.GetVec3();

                childPtcl.velocity.X += randomNormVec3.X * childData.AllDirectionVelocity + randomVec3.X * childData.diffusionVel.X;
                childPtcl.velocity.Y += randomNormVec3.Y * childData.AllDirectionVelocity + randomVec3.Y * childData.diffusionVel.Y;
                childPtcl.velocity.Z += randomNormVec3.Z * childData.AllDirectionVelocity + randomVec3.Z * childData.diffusionVel.Z;

                childPtcl.pos.X = randomNormVec3.X * childData.PtclPosRandom + ptcl.pos.X;
                childPtcl.pos.Y = randomNormVec3.Y * childData.PtclPosRandom + ptcl.pos.Y;
                childPtcl.pos.Z = randomNormVec3.Z * childData.PtclPosRandom + ptcl.pos.Z;

                childPtcl.counter = 0.0f;
                childPtcl.randomU32 = emitter.random.GetU32();

                if ((data.childFlags & (int)ChildFlags.InheritSRT) == 0)
                {
                    if (emitter.ptclFollowType == PtclFollowType.SRT)
                    {
                        childPtcl.matrixRT = emitter.matrixRT;
                        childPtcl.matrixSRT = emitter.matrixSRT;
                        childPtcl.SetEmitterMatrixRef(emitter);
                    }
                    else
                    {
                        childPtcl.matrixRT = ptcl.matrixRT;
                        childPtcl.matrixSRT = ptcl.matrixSRT;
                        childPtcl.SetParticleMatrixRef(ptcl);
                    }
                }
                else
                {
                    childPtcl.SetEmitterMatrixRef(emitter);
                }

                childPtcl.randomU32 = emitter.random.GetU32(); // Again... ?
                childPtcl.randomF32 = 1.0f - childData.MomentumRandom * emitter.random.GetF32Range(-1.0f, 1.0f);
                childPtcl._140 = 0;

                var callback = EffectSystem.ActiveSystem.GetCurrentCustomActionParticleEmitCallback(emitter);
                if (callback != null)
                {
                    ParticleEmitArg arg = new ParticleEmitArg() { ptcl = childPtcl };
                    if (!callback.Invoke(arg))
                    {
                        RemoveParticle(emitter, childPtcl, CpuCore._1);
                        return;
                    }
                }

                AddChildPtclToList(emitter, childPtcl);
            }
        }

        private static OpenTK.Vector2 GetAnimationScale(EmitterInstance emitter)
        {
            if (emitter.keyGroups != null)
            {
                 foreach (var group in emitter.keyGroups)
                {
                    if (group.Type == AnimGroupType.PtclScaleX ||
                        group.Type == AnimGroupType.PtclScaleY)
                    {
                        return new OpenTK.Vector2(
                            emitter.AnimData[(AnimGroupType)17],
                            emitter.AnimData[(AnimGroupType)18]);
                    }
                }
            }
            return new OpenTK.Vector2(1.0f);
        }
    }
}

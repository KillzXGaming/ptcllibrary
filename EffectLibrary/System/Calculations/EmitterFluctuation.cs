﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EffectLibrary
{
    public partial class EmitterCalc
    {
        static float[] sFluctuationTbl;

        internal static void InitializeFluctuationTable()
        {
            sFluctuationTbl = new float[128];

            for (int i = 0; i < 128; i++)
                sFluctuationTbl[i] = MathTriangular.CosRad(i / 128.0f * 2.0f * 3.14159f) * 0.5f + 0.5f;
        }

        public void CalcFluctuation(EmitterInstance emitter, PtclInstance ptcl)
        {
            var data = emitter.data as ComplexEmitterData;
            var fluctuationData = data.FluctuationData;

            int idx = (int)(((int)ptcl.counter * fluctuationData.frequency) + ptcl.randomU32 * fluctuationData.enableRandom) & 127;
            float flux = 1.0f - sFluctuationTbl[idx] * fluctuationData.amplitude;

            if ((data.fluctuationFlags & 2) != 0) ptcl.fluctuationAlpha = flux;
            if ((data.fluctuationFlags & 4) != 0) ptcl.fluctuationScale = flux;
        }
    }
}

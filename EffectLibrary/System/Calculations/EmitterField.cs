﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;

namespace EffectLibrary
{
    public partial class EmitterCalc
    {
        public void FieldRandom(EmitterInstance emitter, PtclInstance ptcl, IFieldData fieldData)
        {
            FieldRandomData randomData = (FieldRandomData)fieldData;

            if ((int)ptcl.counter % randomData.period == 0)
            {
                var randomVec3 = emitter.random.GetVec3();
                ptcl.velocity.X += randomVec3.X * randomData.randomVelScale.X;
                ptcl.velocity.Y += randomVec3.Y * randomData.randomVelScale.Y;
                ptcl.velocity.Z += randomVec3.Z * randomData.randomVelScale.Z;
            }
        }

        public void FieldMagnet(EmitterInstance emitter, PtclInstance ptcl, IFieldData fieldData)
        {
            FieldMagnetData magnetData = (FieldMagnetData)fieldData;

            if ((magnetData.flags & 1) != 0)
                ptcl.velocity.X += (magnetData.pos.X - ptcl.pos.X - ptcl.velocity.X) * magnetData.strength;
            if ((magnetData.flags & 2) != 0)
                ptcl.velocity.Y += (magnetData.pos.Y - ptcl.pos.Y - ptcl.velocity.Y) * magnetData.strength;
            if ((magnetData.flags & 4) != 0)
                ptcl.velocity.Z += (magnetData.pos.Z - ptcl.pos.Z - ptcl.velocity.Z) * magnetData.strength;
        }

        public void FieldSpin(EmitterInstance emitter, PtclInstance ptcl, IFieldData fieldData)
        {
            FieldSpinData spinData = (FieldSpinData)fieldData;
            float sin_val = 0, cos_val = 0;
            uint angle = (uint)(spinData.angle * emitter.emissionSpeed * ptcl.randomF32);
            MathTriangular.SinCosIdx(ref sin_val, ref cos_val, angle);

            switch (spinData.axis)
            {
                case 0:
                    {
                        float y, z;
                        ptcl.pos.Y = (y = ptcl.pos.Y * cos_val + ptcl.pos.Z * sin_val);
                        ptcl.pos.Z = (z = ptcl.pos.Y * -sin_val + ptcl.pos.Z * cos_val);

                        if (spinData.diffusionVel == 0.0f)
                            break;

                        float magSq = y * y + z * z;
                        if (magSq <= 0.0f)
                            break;

                        float a = 1.0f / MathF.Sqrt(magSq) * spinData.diffusionVel * ptcl.randomF32 * emitter.emissionSpeed; // * emitter->emissionSpeed <-- mistake?
                        ptcl.pos.Y += y * a * emitter.emissionSpeed;
                        ptcl.pos.Z += z * a * emitter.emissionSpeed;
                        break;
                    }
                case 1:
                    {
                        float z, x;
                        ptcl.pos.Z = (z = ptcl.pos.Z * cos_val + ptcl.pos.X * sin_val);
                        ptcl.pos.X = (x = ptcl.pos.Z * -sin_val + ptcl.pos.X * cos_val);

                        if (spinData.diffusionVel == 0.0f)
                            break;

                        float magSq = z * z + x * x;
                        if (magSq <= 0.0f)
                            break;

                        float a = 1.0f / MathF.Sqrt(magSq) * spinData.diffusionVel * ptcl.randomF32;
                        ptcl.pos.Z += z * a * emitter.emissionSpeed;
                        ptcl.pos.X += x * a * emitter.emissionSpeed;
                        break;
                    }
                case 2:
                    {
                        float x, y;
                        ptcl.pos.X = (x = ptcl.pos.X * cos_val + ptcl.pos.Y * sin_val);
                        ptcl.pos.Y = (y = ptcl.pos.X * -sin_val + ptcl.pos.Y * cos_val);

                        if (spinData.diffusionVel == 0.0f)
                            break;

                        float magSq = x * x + y * y;
                        if (magSq <= 0.0f)
                            break;

                        float a = 1.0f / MathF.Sqrt(magSq) * spinData.diffusionVel * ptcl.randomF32;
                        ptcl.pos.X += x * a * emitter.emissionSpeed;
                        ptcl.pos.Y += y * a * emitter.emissionSpeed;
                    }
                    break;
            }
        }

        public void FieldCollision(EmitterInstance emitter, PtclInstance ptcl, IFieldData fieldData)
        {
            FieldCollisionData collisionData = (FieldCollisionData)fieldData;
            float y = collisionData.y;
            if (collisionData.coordSystem != 0)
            {
               Matrix4 matrixSRT;
                if (emitter.ptclFollowType == PtclFollowType.SRT)
                    matrixSRT = emitter.matrixSRT;
                else 
                    matrixSRT = ptcl.matrixSRT;

                Vector3 worldPos = MatrixExtension.MultVec(ptcl.pos, matrixSRT);
                switch (collisionData.collisionType)
                {
                    case 0:
                        if (worldPos.Y < y)
                        {
                            worldPos.Y = y;
                            ptcl.counter = (float)ptcl.lifespan - emitter.emissionSpeed;
                        }
                        break;
                    case 1:
                        if (worldPos.Y < y)
                        {
                            worldPos.Y = y + 0.0001f;

                            Vector3 worldVelocity = MatrixExtension.MultVecSR(ptcl.velocity, matrixSRT);
                            worldVelocity.Y *= -collisionData.friction;

                            var matrixSRTInv = matrixSRT.Inverted();

                            ptcl.pos = MatrixExtension.MultVec(worldPos, matrixSRTInv);
                            ptcl.velocity = MatrixExtension.MultVecSR(worldVelocity, matrixSRTInv);
                        }
                        break;
                }
            }
            else
            {

                switch (collisionData.collisionType)
                {
                    case 0:
                        if (ptcl.pos.Y < y)
                        {
                            ptcl.pos.Y = y;
                            ptcl.counter = (float)ptcl.lifespan - emitter.emissionSpeed;
                        }
                        break;
                    case 1:
                        if (ptcl.pos.Y < y)
                        {
                            ptcl.pos.Y = y;
                            ptcl.velocity.Y *= -collisionData.friction;
                        }
                        break;
                }
            }
        }

        public void FieldConvergence(EmitterInstance emitter, PtclInstance ptcl, IFieldData fieldData)
        {
            FieldConvergenceData convergenceData = (FieldConvergenceData)fieldData;
            ptcl.pos += (convergenceData.pos - ptcl.pos) * convergenceData.strength * ptcl.randomF32 * emitter.emissionSpeed;
        }

        public void FieldPosAdd(EmitterInstance emitter, PtclInstance ptcl, object fieldData)
        {
            FieldPosAddData posAddData = (FieldPosAddData)fieldData;
            ptcl.pos += posAddData.posAdd * ptcl.randomF32 * emitter.emissionSpeed;
        }

        public void FieldCurlNoise(EmitterInstance emitter, PtclInstance ptcl, object fieldData)
        {
            FieldCurlNoiseData curlNoiseData = (FieldCurlNoiseData)fieldData;
        }
    }
}

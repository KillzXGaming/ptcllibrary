﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EffectLibrary
{
    public partial class EmitterCalc
    {
        public float CalcAnimKeyFrame(WiiU.KeyGroup keyGroup, float frame)
        {
            var keys = keyGroup.KeyFrames;
            if (keys.Length == 1)
                return keys[0].Value;

            var leftKey = keys.First();
            var rightKey = keys.Last();
            float time = frame;

            if (keyGroup.LoopFlags != 0)
            {
                time = time % rightKey.Frame;

                if (time <= leftKey.Frame)
                    return leftKey.Value;
            }
            else
            {
                if (time <= leftKey.Frame)
                    return leftKey.Value;

                if (rightKey.Frame <= time)
                    return rightKey.Value;
            }

            int startIdx = -1;
            for (int i = 0; i < keys.Length; i++)
            {
                if (time < keys[i].Frame) break;
                startIdx++;
            }

            var secStartKey = keys[startIdx];
            var secEndKey = (startIdx + 1u != keys.Length) ? keys[startIdx + 1u] : keys[startIdx];

            float secDuration = secEndKey.Frame - secStartKey.Frame;
            if (secDuration == 0.0f)
                return secStartKey.Value;

            float rate = (time - secStartKey.Frame) / secDuration;

            if (keyGroup.Interpolation == 1)
                rate = rate * rate * (3.0f - 2.0f * rate);

            return secStartKey.Value * (1.0f - rate) + secEndKey.Value * rate;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EffectLibrary
{
    public partial class EmitterComplexCalc : EmitterSimpleCalc
    {
        public EmitterComplexCalc(EffectSystem system) : base(system)
        {

        }

        public override PtclType GetPtclType() => PtclType.Complex;

        public void CalcStripe(EmitterInstance emitter,
            PtclInstance ptcl, StripeData stripeData, ComplexEmitterData data, CpuCore core)
        {
            int counter = (int)ptcl.counter - 1;

            PtclStripe stripe = ptcl.stripe;
            if (stripe == null)
                return;

            PtclStripeQueue currentSlice = stripe.queue[stripe.queueRear];

            if ((data.stripeFlags & 1) != 0)
            {
                currentSlice.pos = ptcl.pos;
                currentSlice.emitterMatrixSRT = OpenTK.Matrix4.Identity;
            }
            else
            {
                float sliceInterpolation = stripeData.sliceInterpolation;
                if (counter > 2 && stripeData.numSliceHistory > 3 && sliceInterpolation < 1.0f && stripe.queueRear != stripe.queueFront)
                {
                    int prevIdx = (int)stripe.queueRear - 1;
                    if (prevIdx < 0)
                        prevIdx = (int)stripeData.numSliceHistory - 1;

                    int prev2Idx = prevIdx - 1;
                    if (prev2Idx < 0)
                        prev2Idx = (int)stripeData.numSliceHistory - 1;

                    var diff0 = ptcl.worldPos - stripe.pos0;
                    diff0 *= sliceInterpolation;
                    stripe.pos0 += diff0;

                    var diff1 = stripe.pos0 - stripe.pos1;
                    diff1 *= sliceInterpolation;
                    stripe.pos1 += diff1;

                    stripe.queue[prev2Idx].pos = stripe.pos1;

                    var diff2 = ptcl.worldPos - stripe.pos1;
                    diff2 *= 0.7f;
                    stripe.queue[prevIdx].pos = stripe.pos1 + diff2;

                    currentSlice.pos = ptcl.worldPos;
                }
                else
                {
                    stripe.pos0 = (stripe.pos1 = (currentSlice.pos = ptcl.worldPos));
                }
                currentSlice.emitterMatrixSRT = emitter.matrixSRT;
            }

            currentSlice.scale = ptcl.scale.X * emitter.emitterSet.ptclEffectiveScale.X;

            if (stripe.queueRear != stripe.queueFront)
            {
                int prevIdx = (int)stripe.queueRear - 1;
                if (prevIdx < 0)
                    prevIdx = (int)stripeData.numSliceHistory - 1;

                PtclStripeQueue prevSlice = stripe.queue[prevIdx];

                if (counter < 2)
                {
                    stripe.currentSliceDir = currentSlice.pos - prevSlice.pos;
                    if (stripe.currentSliceDir.Length > 0.0f)
                        stripe.currentSliceDir.Normalize();
                }
                else
                {
                    var posDiff = currentSlice.pos - prevSlice.pos;
                    if (posDiff.Length > 0.0f)
                        posDiff.Normalize();

                    var diff = posDiff - stripe.currentSliceDir;
                    diff *= stripeData.dirInterpolation;
                    stripe.currentSliceDir += diff;

                    if (stripe.currentSliceDir.Length > 0.0f)
                        stripe.currentSliceDir.Normalize();
                }

                currentSlice.dir = stripe.currentSliceDir;

                if (stripeData.type == 2)
                {
                    currentSlice.outer.X = currentSlice.emitterMatrixSRT[1, 0];
                    currentSlice.outer.Y = currentSlice.emitterMatrixSRT[1, 1];
                    currentSlice.outer.Z = currentSlice.emitterMatrixSRT[1, 2];
                }
                else
                {
                    var outer = new OpenTK.Vector3(
                        currentSlice.emitterMatrixSRT[1, 0],
                        currentSlice.emitterMatrixSRT[1, 1],
                        currentSlice.emitterMatrixSRT[1, 2]);

                    outer = OpenTK.Vector3.Cross(outer, stripe.currentSliceDir);
                    if (outer.Length > 0.0f)
                        outer.Normalize();

                    currentSlice.outer = outer;
                }
            }

            stripe.queue[stripe.queueRear] = currentSlice;

            if (++stripe.queueRear >= stripeData.numSliceHistory)
                stripe.queueRear = 0;

            if (stripe.queueRear == stripe.queueFront
                && ++stripe.queueFront >= stripeData.numSliceHistory)
            {
                stripe.queueFront = 0;
            }

            if (++stripe.queueCount >= stripeData.numSliceHistory)
                stripe.queueCount = stripeData.numSliceHistory;

            stripe.emitterMatrixSRT = emitter.matrixSRT;
            stripe.counter++;
        }

        public void EmitChildParticle(EmitterInstance emitter, PtclInstance ptcl, CpuCore core, ChildData childData)
        {
            int counter = (int)ptcl.counter - 1;
            if (counter < ((ptcl.lifespan - 1) * childData.startFramePercent / 100))
                return;

            if (ptcl.childEmitCounter >= childData.emissionInterval || childData.emissionInterval == 0 && childData.ptclMaxLifespan == 1)
            {
                if (ptcl.childPreCalcCounter > 0.0f)
                {
                    float time = emitter.counter - ptcl.childPreCalcCounter + ptcl.childEmitLostTime;
                    if (childData.emissionInterval != 0)
                        time /= childData.emissionInterval;

                    if (ptcl.childEmitLostTime >= childData.emissionInterval)
                        ptcl.childEmitLostTime -= childData.emissionInterval;

                    ptcl.childEmitLostTime += emitter.counter - ptcl.childPreCalcCounter - (int)time;
                }

                EffectSystem.ActiveSystem.AddPtclAdditionList(ptcl, core);

                ptcl.childEmitCounter = 0.0f;
                ptcl.childPreCalcCounter = emitter.counter;
            }
            else
            {
                ptcl.childEmitCounter += emitter.emissionSpeed;
            }
        }

        public void CalcComplexParticle(EmitterInstance emitter, PtclInstance ptcl, CpuCore core)
        {
            var data = emitter.data as ComplexEmitterData;

            if (data.VertexTransformMode == VertexTransformMode.Stripe)
            {
                var stripeData = data.StripeData;
                CalcStripe(emitter, ptcl, stripeData, data, core);
            }

            if ((data.childFlags & 1) != 0)
            {
                var childData = data.ChildData;
                EmitChildParticle(emitter, ptcl, core, childData);
            }
        }

        public override uint CalcParticle(EmitterInstance emitter, CpuCore core, bool noCalcBehavior, bool noMakePtclAttributeBuffer)
        {
            VertexTransformMode vertexTransformMode = emitter.data.VertexTransformMode;
            var data = emitter.data as ComplexEmitterData;
            var mSys = EffectSystem.ActiveSystem;

            if (!noMakePtclAttributeBuffer
                  && vertexTransformMode != VertexTransformMode.Stripe
                  && vertexTransformMode != VertexTransformMode.ComplexStripe)
            {
                MakeAttributeBuffer(emitter, (int)emitter.numParticles);
                UpdateDynamicBlock(emitter);
            }
            else
            {
                emitter.PtclAttributeBuffer = null;
                emitter.DynamicUniformBlock = null;
            }

            emitter.numDrawParticle = 0;

            var callback1 = mSys.GetCurrentCustomActionParticleCalcCallback(emitter);
            var callback2 = mSys.GetCurrentCustomActionParticleMakeAttributeCallback(emitter);

            var ptcl = emitter.particleHead;
            bool reversed = false;

            if (emitter.data.ParticleSortReverse)
            {
                ptcl = emitter.particleTail;
                reversed = true;
            }

            for (; ptcl != null; ptcl = reversed ? ptcl.Prev : ptcl.Next)
            {
                if (ptcl.data == null)
                    continue;

                if (!noCalcBehavior)
                {
                    if (ptcl.lifespan <= (int)ptcl.counter || ptcl.lifespan == 1 && ptcl.counter != 0.0f)
                    {
                        PtclStripe stripe = ptcl.stripe;
                        if (stripe != null)
                        {
                            StripeData stripeData = data.StripeData;

                            if (stripe.queueFront == stripe.queueRear)
                                RemoveParticle(emitter, ptcl, core);
                            else
                            {
                                if (++stripe.queueFront >= stripeData.numSliceHistory)
                                    stripe.queueFront = 0;

                                stripe.queueCount--;
                                stripe.emitterMatrixSRT = emitter.matrixSRT;

                                for (int i = 0; i < data.TextureEmitters.Length; i++)
                                {
                                    ptcl.texAnimParam[i].scroll.X += data.TextureEmitters[i].TranslateShift.X;
                                    ptcl.texAnimParam[i].scroll.Y += data.TextureEmitters[i].TranslateShift.Y;
                                    ptcl.texAnimParam[i].scale.X += data.TextureEmitters[i].ScaleShift.X;
                                    ptcl.texAnimParam[i].scale.Y += data.TextureEmitters[i].ScaleShift.Y;
                                    ptcl.texAnimParam[i].rotate += data.TextureEmitters[i].RotateShift;
                                }
                                emitter.numDrawParticle++;
                            }
                            stripe.counter++;
                        }
                        else
                        {
                            RemoveParticle(emitter, ptcl, core);
                        }
                        continue;
                    }
                    CalcComplexParticleBehavior(emitter, ptcl, core);
                    CalcComplexParticle(emitter, ptcl, core);
                }
                if (callback1 != null)
                {
                    ParticleCalcArg arg = new ParticleCalcArg()
                    {
                        emitter = emitter,
                        ptcl = ptcl,
                        core = core,
                        noCalcBehavior = noCalcBehavior,
                    };
                    callback1(arg);
                }
                if (!noMakePtclAttributeBuffer &&
                     vertexTransformMode != VertexTransformMode.Stripe &&
                     vertexTransformMode != VertexTransformMode.ComplexStripe)
                {
                    MakeParticleAttributeBuffer(emitter.PtclAttributeBuffer[emitter.numDrawParticle], ptcl, emitter.shaderAvailableAttribFlg, emitter.data.CameraOffset);
                    ptcl.PtclAttributeBuffer = emitter.PtclAttributeBuffer[emitter.numDrawParticle++];

                    if (callback2 != null)
                    {
                        ParticleMakeAttrArg arg = new ParticleMakeAttrArg
                        {
                            emitter = emitter,
                            ptcl = ptcl,
                            core = core,
                            noCalcBehavior = noCalcBehavior,
                        };
                        callback2(arg);
                    }
                }
            }

            if (data.VertexTransformMode == VertexTransformMode.Stripe)
            {
                mSys.renderers[(int)core].MakeStripeAttributeBlock(emitter);
            }

            emitter.IsCalculated = true;
            return emitter.numDrawParticle;
        }

        public override uint CalcChildParticle(EmitterInstance emitter, CpuCore core, bool noCalcBehavior, bool noMakePtclAttributeBuffer)
        {
            var mSys = EffectSystem.ActiveSystem;
            var data = emitter.data as ComplexEmitterData;

            if (!noMakePtclAttributeBuffer) {
                var renders = mSys.renderers;

                MakeChildAttributeBuffer(emitter, (int)emitter.numChildParticles);

                emitter.ChildDynamicUniformBlock = new EmitterDynamicUniformBlock();
                emitter.ChildDynamicUniformBlock.Init();

                var emitterSetColor = new OpenTK.Vector4(emitter.emitterSet.color) * emitter.data.ColorScale;

                emitter.ChildDynamicUniformBlock.Color0.X = emitterSetColor.X * emitter.AnimData[AnimGroupType.Color0R];
                emitter.ChildDynamicUniformBlock.Color0.Y = emitterSetColor.Y * emitter.AnimData[AnimGroupType.Color0G];
                emitter.ChildDynamicUniformBlock.Color0.Z = emitterSetColor.Z * emitter.AnimData[AnimGroupType.Color0B];
                emitter.ChildDynamicUniformBlock.Color0.W = emitter.emitterSet.color.W * emitter.AnimData[AnimGroupType.Alpha0] * emitter.fadeAlpha;

                emitter.ChildDynamicUniformBlock.Color1.X = emitterSetColor.X * emitter.AnimData[AnimGroupType.Color1R];
                emitter.ChildDynamicUniformBlock.Color1.Y = emitterSetColor.Y * emitter.AnimData[AnimGroupType.Color1G];
                emitter.ChildDynamicUniformBlock.Color1.Z = emitterSetColor.Z * emitter.AnimData[AnimGroupType.Color1B];
                emitter.ChildDynamicUniformBlock.Color1.W = emitter.emitterSet.color.W * emitter.AnimData[AnimGroupType.Alpha0] * emitter.fadeAlpha;
            }
            else
            {
                emitter.ChildPtclAttributeBuffer = null;
                emitter.ChildDynamicUniformBlock = null;
            }

            emitter.numDrawChildParticle = 0;

            var callback1 = mSys.GetCurrentCustomActionParticleCalcCallback(emitter);
            var callback2 = mSys.GetCurrentCustomActionParticleMakeAttributeCallback(emitter);

            for (PtclInstance ptcl = emitter.childParticleHead; ptcl != null; ptcl = ptcl.Next)
            {
                if (ptcl.data == null)
                    continue;

                if (!noCalcBehavior)
                {
                    if (ptcl.lifespan <= (int)ptcl.counter || ptcl.lifespan == 1 && ptcl.counter == 1.0f)
                    {
                        RemoveParticle(emitter, ptcl, core);
                        continue;
                    }

                    if ((data.childFlags & (int)ChildFlags.InheritSRT) != 0)
                    {
                        ptcl.matrixSRT = emitter.matrixSRT;
                        ptcl.matrixRT = emitter.matrixRT;
                    }

                    CalcChildParticleBehavior(emitter, ptcl);
                }
                if (callback1 != null)
                {
                    ParticleCalcArg arg = new ParticleCalcArg()
                    {
                        emitter = emitter,
                        ptcl = ptcl,
                        core = core,
                        noCalcBehavior = noCalcBehavior,
                    };
                    callback1(arg);
                }
                if (!noMakePtclAttributeBuffer)
                {
                    MakeParticleAttributeBuffer(emitter.ChildPtclAttributeBuffer[emitter.numDrawChildParticle], ptcl, emitter.childShaderAvailableAttribFlg, 0);
                    ptcl.PtclAttributeBuffer = emitter.ChildPtclAttributeBuffer[emitter.numDrawChildParticle++];

                    if (callback2 != null)
                    {
                        ParticleMakeAttrArg arg = new ParticleMakeAttrArg
                        {
                            emitter = emitter,
                            ptcl = ptcl,
                            core = core,
                            noCalcBehavior = noCalcBehavior,
                        };
                        callback2(arg);
                    }
                }
            }

            emitter.IsCalculated = true;
            return emitter.numDrawChildParticle;
        }
    }
}

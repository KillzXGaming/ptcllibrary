﻿using System;
using System.Collections.Generic;
using System.Text;
using Toolbox.Core;
using Toolbox.Core.Imaging;
using Toolbox.Core.WiiU;
using GLFrameworkEngine;

namespace EffectLibrary.WiiU
{
    public class OriginalTexureHandle : STGenericTexture
    {
        /// <summary>
        /// The texture section used in the effect data.
        /// </summary>
        public TextureRes Texture;

        public OriginalTexureHandle() { }

        public bool HasData => this.Width != 0;

        public OriginalTexureHandle(TextureRes texture) : base()
        {
            Texture = texture;
            ReloadImage();
        }

        private void ReloadImage()
        {
            if (!FormatList.ContainsKey(Texture.Format))
                return;

            Name = $"Texture";
            Width = Texture.Width;
            Height = Texture.Height;
            MipCount = 1;
            Depth = 1;
            ArrayCount = 1;

            Platform = new DefaultSwizzle(FormatList[Texture.OriginalFormat]);
            if (Texture.Format.ToString().Contains("SRGB"))
                IsSRGB = true;

            DisplayProperties = Texture;
        }

        static Dictionary<GX2TexResFormat, TexFormat> FormatList = new Dictionary<GX2TexResFormat, TexFormat>()
        {
            { GX2TexResFormat.TCS_R5_G6_B5_UNORM, TexFormat.RGB565_UNORM },
            { GX2TexResFormat.TCS_R8_G8_B8_A8, TexFormat.RGBA8_SRGB },
            { GX2TexResFormat.TCS_R8_G8_B8, TexFormat.RGB8_UNORM },
            { GX2TexResFormat.TCS_R8_G8_B8_A8_UNORM, TexFormat.RGBA8_UNORM },
            { GX2TexResFormat.TC_R8_G8_UNORM, TexFormat.RG8_UNORM },
            { GX2TexResFormat.TC_R8_UNORM, TexFormat.R8_UNORM },
            { GX2TexResFormat.T_BC1_SRGB, TexFormat.BC1_SRGB },
            { GX2TexResFormat.T_BC1_UNORM, TexFormat.BC1_UNORM },
            { GX2TexResFormat.T_BC2_SRGB, TexFormat.BC2_SRGB },
            { GX2TexResFormat.T_BC2_UNORM, TexFormat.BC2_UNORM },
            { GX2TexResFormat.T_BC3_SRGB, TexFormat.BC3_SRGB },
            { GX2TexResFormat.T_BC3_UNORM, TexFormat.BC3_UNORM },
            { GX2TexResFormat.T_BC4_UNORM, TexFormat.BC4_UNORM },
            { GX2TexResFormat.T_BC4_SNORM, TexFormat.BC4_SNORM },
            { GX2TexResFormat.T_BC5_UNORM, TexFormat.BC5_UNORM },
            { GX2TexResFormat.T_BC5_SNORM, TexFormat.BC5_SNORM },
        };

        public override byte[] GetImageData(int ArrayLevel = 0, int MipLevel = 0, int DepthLevel = 0)
        {
            return Texture.Data.ToArray();
        }

        public override void SetImageData(List<byte[]> imageData, uint width, uint height, int arrayLevel = 0)
        {
            throw new NotImplementedException();
        }
    }
}

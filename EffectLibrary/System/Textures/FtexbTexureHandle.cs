﻿using System;
using System.Collections.Generic;
using System.Text;
using Toolbox.Core;
using Toolbox.Core.Imaging;
using Toolbox.Core.WiiU;
using GLFrameworkEngine;

namespace EffectLibrary.WiiU
{
    public class FtexbTexureHandle : STGenericTexture
    {
        /// <summary>
        /// The texture section used in the effect data.
        /// </summary>
        public TextureRes Texture;

        public FtexbTexureHandle() { }

        public bool HasData => this.Width != 0;

        public FtexbTexureHandle(TextureRes texture) : base()
        {
            Texture = texture;
            ReloadImage();
           // this.LoadRenderableTexture();
        }

        private void ReloadImage()
        {
            if (!FormatList.ContainsKey(Texture.Format))
                return;

            Name = $"Texture";
            Width = Texture.Width;
            Height = Texture.Height;
            MipCount = Texture.MipCount;
            Depth = 1;
            ArrayCount = 1;
            RedChannel = SetChannel(Texture.CompSel[0]);
            GreenChannel = SetChannel(Texture.CompSel[1]);
            BlueChannel = SetChannel(Texture.CompSel[2]);
            AlphaChannel = SetChannel(Texture.CompSel[3]);

            if (Texture.Depth > 1)
                this.SurfaceType = STSurfaceType.Texture2D_Array;

            Platform = new WiiUSwizzle((GX2.GX2SurfaceFormat)FormatList[Texture.Format])
            {
                AAMode = GX2.GX2AAMode.GX2_AA_MODE_1X,
                TileMode = (GX2.GX2TileMode)Texture.TileMode,
                SurfaceDimension = GX2.GX2SurfaceDimension.DIM_2D,
                SurfaceUse = GX2.GX2SurfaceUse.USE_TEXTURE,
                Swizzle = Texture.Swizzle,
                Alignment = Texture.Alignment,
                Pitch = Texture.Pitch,
            };
            if (Texture.Format.ToString().Contains("SRGB"))
                IsSRGB = true;

            DisplayProperties = Texture;
        }

        static Dictionary<GX2TexResFormat, GX2.GX2SurfaceFormat> FormatList = new Dictionary<GX2TexResFormat, GX2.GX2SurfaceFormat>()
        {
            { GX2TexResFormat.TCS_R5_G6_B5_UNORM, GX2.GX2SurfaceFormat.TCS_R5_G6_B5_UNORM },
            { GX2TexResFormat.TCS_R8_G8_B8_A8, GX2.GX2SurfaceFormat.TCS_R8_G8_B8_A8_SRGB },
            { GX2TexResFormat.TCS_R8_G8_B8, GX2.GX2SurfaceFormat.TCS_R8_G8_B8_A8_UNORM },
            { GX2TexResFormat.TCS_R8_G8_B8_A8_UNORM, GX2.GX2SurfaceFormat.TCS_R8_G8_B8_A8_UNORM },
            { GX2TexResFormat.TC_R8_G8_UNORM, GX2.GX2SurfaceFormat.TC_R8_G8_UNORM },
            { GX2TexResFormat.TC_R8_UNORM, GX2.GX2SurfaceFormat.TC_R8_UNORM },
            { GX2TexResFormat.T_BC1_SRGB, GX2.GX2SurfaceFormat.T_BC1_SRGB },
            { GX2TexResFormat.T_BC1_UNORM, GX2.GX2SurfaceFormat.T_BC1_UNORM },
            { GX2TexResFormat.T_BC2_SRGB, GX2.GX2SurfaceFormat.T_BC2_SRGB },
            { GX2TexResFormat.T_BC2_UNORM, GX2.GX2SurfaceFormat.T_BC2_UNORM },
            { GX2TexResFormat.T_BC3_SRGB, GX2.GX2SurfaceFormat.T_BC3_SRGB },
            { GX2TexResFormat.T_BC3_UNORM, GX2.GX2SurfaceFormat.T_BC3_UNORM },
            { GX2TexResFormat.T_BC4_UNORM, GX2.GX2SurfaceFormat.T_BC4_UNORM },
            { GX2TexResFormat.T_BC4_SNORM, GX2.GX2SurfaceFormat.T_BC4_SNORM },
            { GX2TexResFormat.T_BC5_UNORM, GX2.GX2SurfaceFormat.T_BC5_UNORM },
            { GX2TexResFormat.T_BC5_SNORM, GX2.GX2SurfaceFormat.T_BC5_SNORM },
        };

        public override byte[] GetImageData(int ArrayLevel = 0, int MipLevel = 0, int DepthLevel = 0)
        {
            return Texture.Data.ToArray();
        }

        public override void SetImageData(List<byte[]> imageData, uint width, uint height, int arrayLevel = 0)
        {
            throw new NotImplementedException();
        }

        private STChannelType SetChannel(byte channelType)
        {
            if (channelType == 0) return STChannelType.Red;
            else if (channelType == 1) return STChannelType.Green;
            else if (channelType == 2) return STChannelType.Blue;
            else if (channelType == 3) return STChannelType.Alpha;
            else if (channelType == 4) return STChannelType.Zero;
            else return STChannelType.One;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK.Graphics.OpenGL;

namespace EffectLibrary
{
    public class TransformFeedbackBuffer
    {
        public int[] StreamOutIDs = new int[2] { -1, -1 };

        public void Init(int bufferSize)
        {
            for (int i = 0; i < StreamOutIDs.Length; i++)
            {
                //Init the steam out IDs
                StreamOutIDs[i] = GL.GenBuffer();

                //Create an empty fixed buffer to allocate the stream out data
                GL.BindBuffer(BufferTarget.ArrayBuffer, StreamOutIDs[i]);
                GL.BufferData(BufferTarget.ArrayBuffer, bufferSize, IntPtr.Zero, BufferUsageHint.StreamDraw);
            }
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
        }

        public void Bind(int attributeShaderLocation, int index, bool bindTransform)
        {
            if (attributeShaderLocation == -1)
                return;

            GL.EnableVertexAttribArray(attributeShaderLocation);
            //Bind buffer data
            GL.BindBuffer(BufferTarget.ArrayBuffer, StreamOutIDs[index]);
            //vec4 float buffer
            GL.VertexAttribPointer(attributeShaderLocation, 4, VertexAttribPointerType.Float, false, 0, 0);
            //Handle instancing
            GL.VertexAttribDivisor(attributeShaderLocation, 1);
            //The attribute is either binded while in transform feedback or being binded again when actually being drawn to the main shader.
            if (bindTransform)
            {
                //Bind above as transform feedback
                GL.BindBufferBase(BufferRangeTarget.TransformFeedbackBuffer, 0, StreamOutIDs[index]);
            }
        }

        //Must be called after GL.Flush()
        public void PrintFeedback(int id)
        {
            float[] buffer = new float[4 * 1024];

            GL.BindBuffer(BufferTarget.ArrayBuffer, StreamOutIDs[id]);
            GL.GetBufferSubData(BufferTarget.ArrayBuffer, IntPtr.Zero, 16, buffer);
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);

            int index = 0;
            for (int i = 0; i < 1; i++)
            {
                Console.WriteLine($"id {id} buffer {buffer[index]} {buffer[index + 1]} {buffer[index + 2]} {buffer[index + 3]}");
                index += 4;
            }
        }

        public void Dispose()
        {
            for (int i = 0; i < StreamOutIDs.Length; i++)
            {
                if (StreamOutIDs[i] != -1)
                    GL.DeleteBuffer(StreamOutIDs[i]);
            }
            StreamOutIDs = new int[2] { -1, -1 };
        }
    }
}

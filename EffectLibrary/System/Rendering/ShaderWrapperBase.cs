﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GLFrameworkEngine;

namespace EffectLibrary
{
    public class ShaderWrapperBase
    {
        public virtual void ReloadGLSLSource(ParticleShader shader)
        {

        }

        public virtual void UpdateVAO(ParticleShader shader, ParticleBaseMeshRender mesh)
        {

        }

        public virtual void InitAttributeLocations(ParticleShader shader)
        {

        }

        public virtual void SetSampler(ParticleShader shader, int location, int slot)
        {

        }

        public virtual void UpdateVertexUserData(UniformBlock block)
        {

        }

        public virtual void UpdatePixelUserData(UniformBlock block)
        {

        }
    }
}

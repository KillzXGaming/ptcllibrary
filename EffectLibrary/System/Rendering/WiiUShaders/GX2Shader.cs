﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Toolbox.Core.IO;
using System.Runtime.InteropServices;

namespace EffectLibrary.WiiU
{
    public class GX2Shader
    {
        public Stream Data { get; set; }

        public byte[] DataBytes
        {
            get
            {
                using (var binaryReader = new FileReader(Data, true)) {
                    return binaryReader.ReadBytes((int)binaryReader.Length);
                }
            }
        }

        public static string ReadName(FileReader reader, bool isGSH)
        {
            uint offset = reader.ReadUInt32();
            if (isGSH)
                offset = offset & ~0xCA700000;

            using (reader.TemporarySeek(offset, SeekOrigin.Begin)) {
                return reader.ReadZeroTerminatedString();
            }
        }

        public virtual void Write(FileWriter writer)
        {

        }

        internal static void WriteOffsetCount(FileWriter writer, int count)
        {
            writer.Write(count);
            writer.Write(0); //Data offset reserved
        }

        internal static long SatisfyOffset(FileWriter writer, long pos)
        {
            long currentPos = writer.Position;
            using (writer.TemporarySeek(pos, SeekOrigin.Begin))
            {
                writer.Write((uint)currentPos);
            }
            return currentPos;
        }

        public void UpdateUniforms(List<GX2UniformBlock> blocks, List<GX2UniformVar> uniformVars)
        {
            for (int i = 0; i < blocks.Count; i++)
            {
                foreach (var uniform in uniformVars) {
                    if (uniform.BlockIndex == i) {
                        blocks[i].Uniforms.Add(uniform);
                    }
                }
            }
        }
    }

    public class GX2VertexShader : GX2Shader
    {
        GX2VertexShaderStuct ShaderRegsHeader;

        public uint Mode { get; set; }

        public uint MaxRingItemSize { get; set; }

        public List<GX2UniformBlock> UniformBlocks = new List<GX2UniformBlock>();
        public List<GX2UniformVar> Uniforms = new List<GX2UniformVar>();
        public List<GX2AttributeVar> Attributes = new List<GX2AttributeVar>();
        public List<GX2SamplerVar> Samplers = new List<GX2SamplerVar>();
        public List<GX2LoopVar> Loops = new List<GX2LoopVar>();

        public uint HasStreamOut { get; set; }
        public uint[] StreamOut { get; set; }

        public GX2VertexShader(byte[] data, bool isGSHBinary = false) {
            Read(new FileReader(data), isGSHBinary);
        }

        public GX2VertexShader(FileReader reader, bool isGSHBinary = false) {
            Read(reader, isGSHBinary);
        }

        public void Save(Stream stream) {
            Write(new FileWriter(stream));
        }

        private void Read(FileReader reader, bool isGSHBinary = false) 
        {
            reader.SetByteOrder(isGSHBinary);

            long pos = reader.Position;
            ShaderRegsHeader = reader.ReadStruct<GX2VertexShaderStuct>();

            reader.SeekBegin(16);
            ShaderRegsHeader.spi_vs_out_id = reader.ReadUInt32s(10);
            reader.SeekBegin(68);
            ShaderRegsHeader.sq_vtx_semantic = reader.ReadUInt32s(32);
            reader.SeekBegin(208);

            uint size = reader.ReadUInt32();
            uint dataOffset = reader.ReadUInt32();

            if (!isGSHBinary)
                Data = new SubStream(reader.BaseStream, pos, dataOffset + size);

            Mode = reader.ReadUInt32();
            uint uniformBlockCount = reader.ReadUInt32();
            uint uniformBlocksOffset = reader.ReadUInt32();
            uint uniformVarCount = reader.ReadUInt32();
            uint uniformVarsOffset = reader.ReadUInt32();
            uint padding1 = reader.ReadUInt32();
            uint padding2 = reader.ReadUInt32();

            uint loopVarCount = reader.ReadUInt32();
            uint loopVarsOffset = reader.ReadUInt32();
            uint samplerVarCount = reader.ReadUInt32();
            uint samplerVarsOffset = reader.ReadUInt32();
            uint attribVarCount = reader.ReadUInt32();
            uint attribVarsOffset = reader.ReadUInt32();
            MaxRingItemSize = reader.ReadUInt32();
            HasStreamOut = reader.ReadUInt32();
            StreamOut = reader.ReadUInt32s(8);

            if (isGSHBinary)
            {
                uniformVarsOffset = uniformVarsOffset & ~0xD0600000;
                uniformBlocksOffset = uniformBlocksOffset & ~0xD0600000;
                samplerVarsOffset = samplerVarsOffset & ~0xD0600000;
                attribVarsOffset = attribVarsOffset & ~0xD0600000;
                loopVarsOffset = loopVarsOffset & ~0xD0600000;
            }

            reader.SeekBegin(uniformVarsOffset);
            for (int i = 0; i < uniformVarCount; i++)
                Uniforms.Add(new GX2UniformVar(reader, isGSHBinary));

            reader.SeekBegin(uniformBlocksOffset);
            for (int i = 0; i < uniformBlockCount; i++)
                UniformBlocks.Add(new GX2UniformBlock(reader, isGSHBinary));

            reader.SeekBegin(attribVarsOffset);
            for (int i = 0; i < attribVarCount; i++)
                Attributes.Add(new GX2AttributeVar(reader, isGSHBinary));

            reader.SeekBegin(samplerVarsOffset);
            for (int i = 0; i < samplerVarCount; i++)
                Samplers.Add(new GX2SamplerVar(reader, isGSHBinary));

            reader.SeekBegin(loopVarsOffset);
            for (int i = 0; i < loopVarCount; i++)
                Loops.Add(new GX2LoopVar(reader));

            UpdateUniforms(UniformBlocks, Uniforms);
        }

        private void Write(FileWriter writer, bool isGSHBinary = false)
        {
            writer.SetByteOrder(isGSHBinary);
            writer.WriteStruct(ShaderRegsHeader);

            writer.Write((uint)Data.Length);
            long _dataOfsPos = writer.Position;
            writer.Write(uint.MaxValue); //Data offset reserved
            writer.Write(Mode);

            long headerStart = writer.Position;
            WriteOffsetCount(writer, UniformBlocks.Count);
            WriteOffsetCount(writer, Uniforms.Count);
            WriteOffsetCount(writer, 0);
            WriteOffsetCount(writer, Samplers.Count);
            WriteOffsetCount(writer, Loops.Count);
            WriteOffsetCount(writer, Attributes.Count);
            writer.Write(MaxRingItemSize);
            writer.Write(HasStreamOut);
            writer.Write(StreamOut);

            long uniformBlocksPos = SatisfyOffset(writer, headerStart + 4);
            for (int i = 0; i < UniformBlocks.Count; i++)
            {
                writer.Write(uint.MaxValue); //Name offset reserved
                writer.Write(UniformBlocks[i].Offset);
                writer.Write(UniformBlocks[i].Size);
            }
            long uniformsPos = SatisfyOffset(writer, headerStart + 12);
            for (int i = 0; i < Uniforms.Count; i++)
            {
                writer.Write(uint.MaxValue); //Name offset reserved
                writer.Write((uint)Uniforms[i].Type);
                writer.Write(Uniforms[i].Count);
                writer.Write(Uniforms[i].Offset);
                writer.Write(Uniforms[i].BlockIndex);
            }
            long samplersPos = SatisfyOffset(writer, headerStart + 28);
            for (int i = 0; i < Samplers.Count; i++)
            {
                writer.Write(uint.MaxValue); //Name offset reserved
                writer.Write((uint)Samplers[i].Type);
                writer.Write(Samplers[i].Location);
            }
            SatisfyOffset(writer, headerStart + 36);
            for (int i = 0; i < Loops.Count; i++)
            {
                writer.Write(Loops[i].Offset);
                writer.Write(Loops[i].Value);
            }
            long attributesPos = SatisfyOffset(writer, headerStart + 44);
            for (int i = 0; i < Attributes.Count; i++)
            {
                writer.Write(uint.MaxValue); //Name offset reserved
                writer.Write((uint)Attributes[i].Type);
                writer.Write(Attributes[i].Count);
                writer.Write(Attributes[i].Location);
            }

            //Write string table
            for (int i = 0; i < UniformBlocks.Count; i++)
            {
                SatisfyOffset(writer, (uniformBlocksPos + i * 12));
                writer.WriteString(UniformBlocks[i].Name);
            }
            for (int i = 0; i < Uniforms.Count; i++)
            {
                SatisfyOffset(writer, (uniformsPos + i * 20));
                writer.WriteString(Uniforms[i].Name);
            }
            for (int i = 0; i < Samplers.Count; i++)
            {
                SatisfyOffset(writer, (samplersPos + i * 12));
                writer.WriteString(Samplers[i].Name);
            }
            for (int i = 0; i < Attributes.Count; i++)
            {
                SatisfyOffset(writer, (attributesPos + i * 16));
                writer.WriteString(Attributes[i].Name);
            }
            writer.Align(128);

            SatisfyOffset(writer, _dataOfsPos);
            writer.Write(Data.ToArray());
        }
    }

    public class GX2PixelShader : GX2Shader
    {
        GX2PixelShaderStuct ShaderRegsHeader;

        public List<GX2UniformBlock> UniformBlocks = new List<GX2UniformBlock>();
        public List<GX2UniformVar> Uniforms = new List<GX2UniformVar>();
        public List<GX2SamplerVar> Samplers = new List<GX2SamplerVar>();
        public List<GX2LoopVar> Loops = new List<GX2LoopVar>();

        public uint Mode { get; set; }

        public uint MaxRingItemSize { get; set; }

        public GX2PixelShader(byte[] data, bool isGSHBinary = false) {
            Read(new FileReader(data), isGSHBinary);
        }

        public GX2PixelShader(FileReader reader, bool isGSHBinary = false) {
            Read(reader, isGSHBinary);
        }

        public void Save(Stream stream) {
            Write(new FileWriter(stream));
        }

        private void Read(FileReader reader, bool isGSHBinary = false)
        {
            reader.SetByteOrder(isGSHBinary);

            long pos = reader.Position;
            ShaderRegsHeader = reader.ReadStruct<GX2PixelShaderStuct>();

            reader.SeekBegin(20);
            ShaderRegsHeader.spi_ps_input_cntls = reader.ReadUInt32s(32);
            reader.SeekBegin(164);

            uint size = reader.ReadUInt32();
            uint dataOffset = reader.ReadUInt32();
            Mode = reader.ReadUInt32();

            if (!isGSHBinary)
                Data = new SubStream(reader.BaseStream, pos, dataOffset + size);

            uint uniformBlockCount = reader.ReadUInt32();
            uint uniformBlocksOffset = reader.ReadUInt32();
            uint uniformVarCount = reader.ReadUInt32();
            uint uniformVarsOffset = reader.ReadUInt32();
            uint padding1 = reader.ReadUInt32();
            uint padding2 = reader.ReadUInt32();
            uint loopVarCount = reader.ReadUInt32();
            uint loopVarsOffset = reader.ReadUInt32();
            uint samplerVarCount = reader.ReadUInt32();
            uint samplerVarsOffset = reader.ReadUInt32();
            MaxRingItemSize = reader.ReadUInt32();

            if (isGSHBinary)
            {
                uniformVarsOffset = uniformVarsOffset & ~0xD0600000;
                uniformBlocksOffset = uniformBlocksOffset & ~0xD0600000;
                samplerVarsOffset = samplerVarsOffset & ~0xD0600000;
                loopVarsOffset = loopVarsOffset & ~0xD0600000;
            }

            reader.SeekBegin(uniformVarsOffset);
            for (int i = 0; i < uniformVarCount; i++)
                Uniforms.Add(new GX2UniformVar(reader, isGSHBinary));

            reader.SeekBegin(uniformBlocksOffset);
            for (int i = 0; i < uniformBlockCount; i++)
                UniformBlocks.Add(new GX2UniformBlock(reader, isGSHBinary));

            reader.SeekBegin(samplerVarsOffset);
            for (int i = 0; i < samplerVarCount; i++)
                Samplers.Add(new GX2SamplerVar(reader, isGSHBinary));

            reader.SeekBegin(loopVarsOffset);
            for (int i = 0; i < loopVarCount; i++)
                Loops.Add(new GX2LoopVar(reader));

            UpdateUniforms(UniformBlocks, Uniforms);
        }


        private void Write(FileWriter writer, bool isGSHBinary = false)
        {
            writer.SetByteOrder(isGSHBinary);
            writer.WriteStruct(ShaderRegsHeader);

            writer.Write((uint)Data.Length);
            long _dataOfsPos = writer.Position;
            writer.Write(uint.MaxValue); //Data offset reserved
            writer.Write(Mode);

            long headerStart = writer.Position;
            WriteOffsetCount(writer, UniformBlocks.Count);
            WriteOffsetCount(writer, Uniforms.Count);
            WriteOffsetCount(writer, 0);
            WriteOffsetCount(writer, Loops.Count);
            WriteOffsetCount(writer, Samplers.Count);
            writer.Write(MaxRingItemSize);
            writer.Write(0);
            writer.Write(new byte[28]);

            long uniformBlocksPos = SatisfyOffset(writer, headerStart + 4);
            for (int i = 0; i < UniformBlocks.Count; i++)
            {
                writer.Write(uint.MaxValue); //Name offset reserved
                writer.Write(UniformBlocks[i].Offset);
                writer.Write(UniformBlocks[i].Size);
            }
            long uniformsPos = SatisfyOffset(writer, headerStart + 12);
            for (int i = 0; i < Uniforms.Count; i++)
            {
                writer.Write(uint.MaxValue); //Name offset reserved
                writer.Write((uint)Uniforms[i].Type);
                writer.Write(Uniforms[i].Count);
                writer.Write(Uniforms[i].Offset);
                writer.Write(Uniforms[i].BlockIndex);
            }
            long samplersPos = SatisfyOffset(writer, headerStart + 36);
            for (int i = 0; i < Samplers.Count; i++)
            {
                writer.Write(uint.MaxValue); //Name offset reserved
                writer.Write((uint)Samplers[i].Type);
                writer.Write(Samplers[i].Location);
            }
            SatisfyOffset(writer, headerStart + 28);
            for (int i = 0; i < Loops.Count; i++)
            {
                writer.Write(Loops[i].Offset);
                writer.Write(Loops[i].Value);
            }
            //Write string table
            for (int i = 0; i < UniformBlocks.Count; i++)
            {
                SatisfyOffset(writer, (uniformBlocksPos + i * 12));
                writer.WriteString(UniformBlocks[i].Name);
            }
            for (int i = 0; i < Uniforms.Count; i++)
            {
                SatisfyOffset(writer, (uniformsPos + i * 20));
                writer.WriteString(Uniforms[i].Name);
            }
            for (int i = 0; i < Samplers.Count; i++)
            {
                SatisfyOffset(writer, (samplersPos + i * 12));
                writer.WriteString(Samplers[i].Name);
            }
            writer.Align(128);

            SatisfyOffset(writer, _dataOfsPos);
            writer.Write(Data.ToArray());
        }
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    class GX2VertexShaderStuct
    {
        public uint sq_pgm_resources_vs;
        public uint vgt_primitiveid_en;
        public uint spi_vs_out_config;
        public uint num_spi_vs_out_id;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
        public uint[] spi_vs_out_id;

        public uint pa_cl_vs_out_cntl;
        public uint sq_vtx_semantic_clear;
        public uint num_sq_vtx_semantic;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 32)]
        public uint[] sq_vtx_semantic;

        public uint vgt_strmout_buffer_en;
        public uint vgt_vertex_reuse_block_cntl;
        public uint vgt_hos_reuse_depth;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    class GX2PixelShaderStuct
    {
        public uint sq_pgm_resources_ps;
        public uint sq_pgm_exports_ps;
        public uint spi_ps_in_control_0;
        public uint spi_ps_in_control_1;
        public uint num_spi_ps_input_cntl;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 32)]
        public uint[] spi_ps_input_cntls;

        public uint cb_shader_mask;
        public uint cb_shader_control;
        public uint db_shader_control;
        public uint spi_input_z;
    }

    public class GX2UniformVar
    {
        public string Name { get; set; }
        public GX2ShaderVarType Type { get; set; }
        public uint Count { get; set; }
        public uint Offset { get; set; }
        public uint BlockIndex { get; set; }

        public GX2UniformVar() { }

        public GX2UniformVar(FileReader reader, bool isGSH)
        {
            Name = GX2Shader.ReadName(reader, isGSH);
            Type = reader.ReadEnum<GX2ShaderVarType>(false);
            Count = reader.ReadUInt32();
            Offset = reader.ReadUInt32();
            BlockIndex = reader.ReadUInt32();
        }
    }

    public class GX2SamplerVar
    {
        public string Name { get; set; }
        public GX2SamplerVarType Type { get; set; }
        public uint Location { get; set; }

        public GX2SamplerVar() { }

        public GX2SamplerVar(FileReader reader, bool isGSH)
        {
            Name = GX2Shader.ReadName(reader, isGSH);
            Type = reader.ReadEnum< GX2SamplerVarType>(false);
            Location = reader.ReadUInt32();
        }
    }

    public class GX2UniformBlock
    {
        public string Name { get; set; }
        public uint Offset { get; set; }
        public uint Size { get; set; }

        public List<GX2UniformVar> Uniforms = new List<GX2UniformVar>();

        public GX2UniformBlock() { }

        public GX2UniformBlock(FileReader reader, bool isGSH)
        {
            Name = GX2Shader.ReadName(reader, isGSH);
            Offset = reader.ReadUInt32();
            Size = reader.ReadUInt32();
        }
    }

    public class GX2AttributeVar
    {
        public string Name { get; set; }
        public GX2ShaderVarType Type { get; set; }
        public uint Count { get; set; }
        public int Location { get; set; }

        public GX2AttributeVar() { }

        public GX2AttributeVar(FileReader reader, bool isGSH)
        {
            Name = GX2Shader.ReadName(reader, isGSH);
            Type = reader.ReadEnum<GX2ShaderVarType>(true);
            Count = reader.ReadUInt32();
            Location = reader.ReadInt32();
        }
    }

    public class GX2LoopVar
    {
        public uint Offset { get; set; }
        public uint Value { get; set; }

        public GX2LoopVar() { }

        public GX2LoopVar(FileReader reader)
        {
            Offset = reader.ReadUInt32();
            Value = reader.ReadUInt32();
        }
    }

    public enum GX2SamplerVarType
    {
        SAMPLER_1D = 0,
        SAMPLER_2D = 1,
        SAMPLER_3D = 2,
        SAMPLER_CUBE = 4,
        SAMPLER_2D_SHADOW = 6,
        SAMPLER_2D_ARRAY = 10,
        SAMPLER_2D_ARRAY_SHADOW = 12,
        SAMPLER_CUBE_ARRAY = 13,
    }

    public enum GX2ShaderVarType
    {
        MATRIX4X4 = 1,
        INT = 2,
        UINT = 3,
        FLOAT = 4,
        FLOAT2 = 9,
        FLOAT3 = 10,
        FLOAT4 = 11,
        INT2 = 15,
        INT3 = 16,
        INT4 = 17,
    }
}

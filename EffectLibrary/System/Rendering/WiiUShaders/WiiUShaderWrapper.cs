﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EffectLibrary.WiiU;
using GLFrameworkEngine;
using OpenTK.Graphics.OpenGL;

namespace EffectLibrary
{
    public class WiiUShaderWrapper : ShaderWrapperBase
    {
        GFD GFDFile { get; set; }

        GX2UniformBlock VertexUserDataBlock { get; set; }
        GX2UniformBlock PixelUserDataBlock { get; set; }

        public GX2VertexShader VertexShader { get; set; }
        public GX2PixelShader PixelShader { get; set; }

        public WiiUShaderWrapper(GFD gfdFile) {
            GFDFile = gfdFile;
            VertexShader = gfdFile.TryGetVertexShader();
            PixelShader = gfdFile.TryGetPixelShader();
        }

        public override void ReloadGLSLSource(ParticleShader shader)
        {
            //Load shaders
            var gfd = GFDFile;

            string[] feedbackVaryings = GetStreamoutVaryings();

            shader.ShaderInfo = CafeShaderDecoder.LoadShaderProgram(
             gfd.TryGetVertexShaderRaw(),
             gfd.TryGetPixelShaderRaw(), feedbackVaryings);

            VertexUserDataBlock = VertexShader.UniformBlocks.FirstOrDefault(x => x.Name == "userUniformBlock");
            PixelUserDataBlock = PixelShader.UniformBlocks.FirstOrDefault(x => x.Name == "userUniformBlock");

            shader.ShaderInfo.Program.Enable();

            if (shader.VertexShaderKey.VertexTransformMode != VertexTransformMode.Stripe &&
                shader.VertexShaderKey.VertexTransformMode != VertexTransformMode.ComplexStripe)
            {
                InitializeVertexShaderLocation(shader);
                InitializeFragmentShaderLocation(shader);
                InitAttributeLocations(shader);
            }
            else
            {

                InitializeStripeVertexShaderLocation(shader);
                InitializeFragmentShaderLocation(shader);
                InitializeStripeAttribute(shader);
            }

            shader.ShaderInfo.Program.Disable();

            shader.Initialized = true;
        }

        private string[] GetStreamoutVaryings()
        {
            if (VertexShader.HasStreamOut != 0) {
                return new string[] { "STREAMOUT_0_0", "STREAMOUT_1_0" };

                return new string[] { "v_inStreamOutPos_0_0", "v_inStreamOutVec_0_0" };
            }

            
            return new string[0];
        }

        private void InitializeVertexShaderLocation(ParticleShader shader)
        {
            int viewIndex = GetVertexShaderBlockLocation(shader, "viewUniformBlock");
            int staticIndex = GetVertexShaderBlockLocation(shader, "emitterStaticUniformBlock");
            int dynamicIndex = GetVertexShaderBlockLocation(shader, "emitterDynamicUniformBlock");
            int userIndex = GetVertexShaderBlockLocation(shader, "userUniformBlock");

            shader.VertexViewUniformBlock = new UniformBlock(viewIndex);
            shader.VertexEmitterStaticUniformBlock = new UniformBlock(staticIndex);
            shader.VertexEmitterDynamicUniformBlock = new UniformBlock(dynamicIndex);

            shader.VertexUserBlock = new UniformBlock[2];
            shader.VertexUserBlock[0] = new UniformBlock(userIndex);
        }

        private void InitializeFragmentShaderLocation(ParticleShader shader)
        {
            int viewIndex = GetPixelShaderBlockLocation(shader, "viewUniformBlock");
            int staticIndex = GetPixelShaderBlockLocation(shader, "emitterStaticUniformBlock");
            int userIndex = GetPixelShaderBlockLocation(shader, "userUniformBlock");

            shader.FragmentViewUniformBlock = new UniformBlock(viewIndex);
            shader.FragmentEmitterStaticUniformBlock = new UniformBlock(staticIndex);

            shader.FragmentUserBlock = new UniformBlock[2];
            shader.FragmentUserBlock[0] = new UniformBlock(userIndex);

            InitSamplerLocations(shader);
        }

        private void InitializeStripeVertexShaderLocation(ParticleShader shader)
        {
            int stripeIndex = GetVertexShaderBlockLocation(shader, "stripeUniformBlock");
            int viewIndex = GetVertexShaderBlockLocation(shader, "viewUniformBlock");

            shader.VertexViewUniformBlock = new UniformBlock(viewIndex);
            shader.VertexStripeUniformBlock = new UniformBlock(stripeIndex);
        }

        public override void InitAttributeLocations(ParticleShader shader)
        {
            shader.attrPosBuffer = GetAttribute("v_inPos");
            if (!shader.VertexShaderKey.IsPrimitive)
                shader.attrIndexBuffer = GetAttribute("v_inIndex");
            shader.attrTexCoordBuffer = GetAttribute("v_inTexCoord");
            shader.attrNormalBuffer = GetAttribute("v_inNormal");
            shader.attrColorBuffer = GetAttribute("v_inColor");
            shader.attrWldPosBuffer = GetAttribute("v_inWldPos");
            shader.attrSclBuffer = GetAttribute("v_inScl");
            shader.attrColor0Buffer = GetAttribute("v_inColor0");
            shader.attrColor1Buffer = GetAttribute("v_inColor1");
            shader.attrTexAnimBuffer = GetAttribute("v_inTexAnim");
            shader.attrWldPosDfBuffer = GetAttribute("v_inWldPosDf");
            shader.attrRotBuffer = GetAttribute("v_inRot");
            shader.attrSubTexAnimBuffer = GetAttribute("v_inSubTexAnim");
            shader.attrEmMat0Buffer = GetAttribute("v_inEmtMat0");
            shader.attrEmMat1Buffer = GetAttribute("v_inEmtMat1");
            shader.attrEmMat2Buffer = GetAttribute("v_inEmtMat2");
            shader.attrStreamOutPos = GetAttribute("v_inStreamOutPos");
            shader.attrStreamOutVec = GetAttribute("v_inStreamOutVec");
        }

        private void InitializeStripeAttribute(ParticleShader shader)
        {
            shader.attrPosBuffer = GetAttribute("v_inPos");
            shader.attrOuterBuffer = GetAttribute("v_inOuter");
            shader.attrTexCoordBuffer = GetAttribute("v_inTexCoord");
            shader.attrDirBuffer = GetAttribute("v_inDir");
        }

        private int GetAttribute(string name)
        {
            foreach (var attribute in VertexShader.Attributes)
            {
                if (attribute.Name == name)
                    return attribute.Location;
            }
            return -1;
        }

        private int GetVertexShaderBlockLocation(ParticleShader shader, string name)
        {
            foreach (var block in VertexShader.UniformBlocks)
            {
                if (block.Name == name)
                    return GetBlockBinding(shader, $"vp_{block.Offset}");
            }
            return -1;
        }

        private int GetPixelShaderBlockLocation(ParticleShader shader, string name)
        {
            foreach (var block in PixelShader.UniformBlocks)
            {
                if (block.Name == name)
                    return GetBlockBinding(shader, $"fp_{block.Offset}");
            }
            return -1;
        }

        public override void SetSampler(ParticleShader shader, int location, int slot)
        {
            if (location != -1)
            {
                shader.ShaderInfo.Program.SetInt($"SPIRV_Cross_CombinedTEXTURE_{location}SAMPLER_{location}", slot + 1);
            }
        }

        private int GetBlockBinding(ParticleShader shader, string name)
        {
            return GL.GetUniformBlockIndex(shader.ShaderInfo.Program.program, name);
        }

        private void InitSamplerLocations(ParticleShader shader)
        {
            foreach (var sampler in PixelShader.Samplers)
            {
                switch (sampler.Name)
                {
                    case "textureSampler0":
                    case "s_firstTexture":
                    case "textureArraySampler0":
                        shader.FragmentSamplerLocations[0] = (int)sampler.Location;
                        break;
                    case "textureSampler1":
                    case "s_secondTexture":
                    case "textureArraySampler1":
                        shader.FragmentSamplerLocations[1] = (int)sampler.Location; break;
                    case "textureSampler2":
                        shader.FragmentSamplerLocations[2] = (int)sampler.Location; break;
                    case "s_depthBufferTexture":
                    case "depthBufferTexture":
                        shader.FragmentDBSamplerLocation = (int)sampler.Location; break;
                    case "s_frameBufferTexture":
                    case "frameBufferTexture":
                        shader.FragmentFBSamplerLocation = (int)sampler.Location; break;
                    case "texCubeLightMap":
                        shader.FragmentCubeLightMap = (int)sampler.Location; break;
                        break;
                   // default:
                    //    throw new Exception($"Undefined sampler {sampler.Name}!");
                }
            }

            foreach (var sampler in VertexShader.Samplers)
            {
                switch (sampler.Name)
                {
                    case "uExposureTexture": 
                        shader.VertexExposureTexture = (int)sampler.Location; break;
                    case "depthBufferTexture":
                        shader.VertexDBSamplerLocation = (int)sampler.Location; break;
                    case "texStaticDepthShadow":
                        shader.VertexShadowSamplerLocation = (int)sampler.Location; break;
                        /* default:
                             throw new Exception($"Undefined sampler {sampler.Name}!");*/
                }
            }
/*
            for (int i = 0; i < shader.FragmentSamplerLocations.Length; i++)
                SetSampler(shader, shader.FragmentSamplerLocations[i], i + 1);

            SetSampler(shader, shader.FragmentFBSamplerLocation, (int)TextureSlot.FrameBuffer + 1);
            SetSampler(shader, shader.FragmentDBSamplerLocation, (int)TextureSlot.DepthBuffer + 1);
            SetSampler(shader, shader.VertexDBSamplerLocation, (int)TextureSlot.DepthBuffer + 1);
            SetSampler(shader, shader.FragmentCubeLightMap, (int)TextureSlot.CubeLightMap + 1);

            SetSampler(shader, shader.VertexExposureTexture, 7 + 1);
            SetSampler(shader, shader.VertexShadowSamplerLocation, 8 + 1);*/

        }

        public override void UpdateVertexUserData(UniformBlock block)
        {
            if (VertexUserDataBlock == null)
                return;

            var uniformVars = VertexUserDataBlock.Uniforms;
            UpdateUserDataCommon(block, uniformVars);

        }

        public override void UpdatePixelUserData(UniformBlock block)
        {
            if (PixelUserDataBlock == null)
                return;

            var uniformVars = PixelUserDataBlock.Uniforms;
            UpdateUserDataCommon(block, uniformVars);
        }

        private void UpdateUserDataCommon(UniformBlock block, List<GX2UniformVar> uniformVars)
        {
            var mem = new System.IO.MemoryStream();
            using (var writer = new Toolbox.Core.IO.FileWriter(mem))
            {
                foreach (var uniform in uniformVars)
                {
                    writer.SeekBegin(uniform.Offset * 4);
                    switch (uniform.Name)
                    {
                        //Smash 4 user values
                        case "u_nwHdrRange":
                            writer.Write(new OpenTK.Vector4(0.5f));
                            break;
                        case "u_nwColorHdrRange":
                            writer.Write(new OpenTK.Vector4(1));
                            break;
                    }
                }
            }
            block.Add(mem.ToArray());
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using Syroot.BinaryData;

namespace EffectLibrary.WiiU
{
    public class GFD
    {
        public List<Block> Blocks = new List<Block>();

        public uint HeaderSize { get; set; }
        public uint VersionMajor { get; set; }
        public uint VersionMinor { get; set; }
        public uint GPUVersion { get; set; }
        public uint AlignMode { get; set; }

        public GFD(Stream stream)
        {
            using (var reader = new BinaryDataReader(stream)) {
                reader.ByteOrder = ByteOrder.BigEndian;
                Read(reader);
            }
        }

        public void Save(string fileName)
        {
            using (var fileStream = new System.IO.FileStream(fileName, FileMode.Create, FileAccess.Write, FileShare.Write))
            {
                Save(fileStream);
            }
        }

        public void Save(Stream stream)
        {
            using (var writer = new BinaryDataWriter(stream)) {
                writer.ByteOrder = ByteOrder.BigEndian;
                Write(writer);
            }
        }

        public byte[] TryGetVertexShaderRaw()
        {
            var shader = TryGetVertexShader();

            var mem = new MemoryStream();
            shader.Save(mem);
            return mem.ToArray();
        }

        public byte[] TryGetPixelShaderRaw()
        {
            var shader = TryGetPixelShader();

            var mem = new MemoryStream();
            shader.Save(mem);
            return mem.ToArray();
        }

        public GX2VertexShader TryGetVertexShader()
        {
            GX2VertexShader header = null;
            for (int i = 0; i < Blocks.Count; i++)
            {
                if (Blocks[i].Type == GFDType.VertexShaderHeader)
                    header = Blocks[i].DataObject as GX2VertexShader;
                if (Blocks[i].Type == GFDType.VertexShaderProgram)
                    header.Data = new MemoryStream(Blocks[i].Data);
            }
            return header;
        }

        public GX2PixelShader TryGetPixelShader()
        {
            GX2PixelShader header = null;
            for (int i = 0; i < Blocks.Count; i++)
            {
                if (Blocks[i].Type == GFDType.PixelShaderHeader)
                    header = Blocks[i].DataObject as GX2PixelShader;
                if (Blocks[i].Type == GFDType.PixelShaderProgram)
                    header.Data = new MemoryStream(Blocks[i].Data);
            }
            return header;
        }

        private void Read(BinaryDataReader reader)
        {
            reader.ReadUInt32(); //Gfx2
            HeaderSize = reader.ReadUInt32();
            VersionMajor = reader.ReadUInt32();
            VersionMinor = reader.ReadUInt32();
            GPUVersion = reader.ReadUInt32();
            AlignMode = reader.ReadUInt32();
            reader.ReadUInt32();
            reader.ReadUInt32();
            while (reader.BaseStream.Length > reader.Position)
            {
                Block block = new Block();
                block.Read(reader);
                Blocks.Add(block);

                if (block.Type == GFDType.EndOfFile)
                    break;
            }
        }

        public void Write(BinaryDataWriter writer)
        {
            writer.Write(System.Text.Encoding.ASCII.GetBytes("Gfx2"));
            writer.Write(HeaderSize);
            writer.Write(VersionMajor);
            writer.Write(VersionMinor);
            writer.Write(GPUVersion);
            writer.Write(AlignMode);
            writer.Write(0);
            writer.Write(0);
            foreach (var block in Blocks)
                block.Write(writer);
        }

        public class Block
        {
            public uint HeaderSize { get; set; }
            public uint VersionMajor { get; set; }
            public uint VersionMinor { get; set; }
            public GFDType Type { get; set; }

            public uint ID { get; set; }
            public uint TypeID { get; set; }

            public byte[] Data { get; set; }

            public object DataObject { get; set; }

            public void Read(BinaryDataReader reader)
            {
                reader.ReadUInt32(); //BLK{
                HeaderSize = reader.ReadUInt32();
                VersionMajor = reader.ReadUInt32();
                VersionMinor = reader.ReadUInt32();
                Type = (GFDType)reader.ReadUInt32();
                uint dataSize = reader.ReadUInt32();
                ID = reader.ReadUInt32();
                TypeID = reader.ReadUInt32();
                Data = reader.ReadBytes((int)dataSize);

                if (Type == GFDType.VertexShaderHeader)
                    DataObject = new GX2VertexShader(Data, true);
                if (Type == GFDType.PixelShaderHeader)
                    DataObject = new GX2PixelShader(Data, true);
            }

            public void Write(BinaryDataWriter writer)
            {
                writer.Write(System.Text.Encoding.ASCII.GetBytes("BLK{"));
                writer.Write(HeaderSize);
                writer.Write(VersionMajor);
                writer.Write(VersionMinor);
                writer.Write((uint)Type);
                writer.Write(Data.Length);
                writer.Write(ID);
                writer.Write(TypeID);
                writer.Write(Data);
            }
        }

        public enum GFDType
        {
            EndOfFile = 1,
            VertexShaderHeader = 3,
            VertexShaderProgram = 5,
            PixelShaderHeader = 6,
            PixelShaderProgram = 7,
        }
    }
}

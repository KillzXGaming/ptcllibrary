﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK.Graphics.OpenGL;
using GLFrameworkEngine;

namespace EffectLibrary
{
    public class RenderContext
    {
        public BlendType ActiveBlendType;
        public DepthType ActiveZBufATestType;

        public GLMaterialBlendState BlendState = new GLMaterialBlendState();

        public void SetupCommonState()
        {
            GL.Disable(EnableCap.CullFace);
            GL.CullFace(CullFaceMode.Back);

            GL.Enable(EnableCap.Blend);

            GL.Enable(EnableCap.DepthTest);
            GL.DepthFunc(DepthFunction.Greater);
            GL.DepthMask(false);

            ActiveBlendType = BlendType.DefaultAlphaBlend;
            ActiveZBufATestType = DepthType.DepthTestNoWriteLequal;
        }

        public void SetBlendType(BlendType blendType)
        {
            switch (blendType)
            {
                case BlendType.DefaultAlphaBlend:
                    BlendState.ColorSrc = BlendingFactorSrc.SrcAlpha;
                    BlendState.ColorDst = BlendingFactorDest.OneMinusSrcAlpha;
                    BlendState.ColorOp = BlendEquationMode.FuncAdd;
                    BlendState.AlphaSrc = BlendingFactorSrc.SrcAlpha;
                    BlendState.AlphaDst = BlendingFactorDest.OneMinusSrcAlpha;
                    BlendState.AlphaOp = BlendEquationMode.FuncAdd;
                    break;
                case BlendType.AddTranslucent:
                    BlendState.ColorSrc = BlendingFactorSrc.SrcAlpha;
                    BlendState.ColorDst = BlendingFactorDest.One;
                    BlendState.ColorOp = BlendEquationMode.FuncAdd;
                    BlendState.AlphaSrc = BlendingFactorSrc.SrcAlpha;
                    BlendState.AlphaDst = BlendingFactorDest.One;
                    BlendState.AlphaOp = BlendEquationMode.FuncAdd;
                    break;
                case BlendType.SubTranslucent:
                    BlendState.ColorSrc = BlendingFactorSrc.SrcAlpha;
                    BlendState.ColorDst = BlendingFactorDest.One;
                    BlendState.ColorOp = BlendEquationMode.FuncReverseSubtract;
                    BlendState.AlphaSrc = BlendingFactorSrc.SrcAlpha;
                    BlendState.AlphaDst = BlendingFactorDest.One;
                    BlendState.AlphaOp = BlendEquationMode.FuncReverseSubtract;
                    break;
                case BlendType.Screen:
                    BlendState.ColorSrc = BlendingFactorSrc.OneMinusDstColor;
                    BlendState.ColorDst = BlendingFactorDest.One;
                    BlendState.ColorOp = BlendEquationMode.FuncAdd;
                    BlendState.AlphaSrc = BlendingFactorSrc.OneMinusDstColor;
                    BlendState.AlphaDst = BlendingFactorDest.One;
                    BlendState.AlphaOp = BlendEquationMode.FuncAdd;
                    break;
                case BlendType.Multi:
                    BlendState.ColorSrc = BlendingFactorSrc.Zero;
                    BlendState.ColorDst = BlendingFactorDest.SrcColor;
                    BlendState.ColorOp = BlendEquationMode.FuncAdd;
                    BlendState.AlphaSrc = BlendingFactorSrc.Zero;
                    BlendState.AlphaDst = BlendingFactorDest.SrcColor;
                    BlendState.AlphaOp = BlendEquationMode.FuncAdd;
                    break;
            }
            this.ActiveBlendType = blendType;
        }

        public void SetZBufATest(DepthType zBufATestType)
        {
            BlendState.BlendColor = true;
            BlendState.AlphaTest = true;
            BlendState.AlphaValue = 0;
            BlendState.AlphaFunction = AlphaFunction.Greater;

            switch (zBufATestType)
            {
                case DepthType.DepthTestNoWriteLequal:
                    BlendState.DepthTest = true;
                    BlendState.DepthWrite = false;
                    BlendState.AlphaValue = 0.0f;
                    break;
                case DepthType.DepthTestWriteLequal:
                    BlendState.BlendColor = false;
                    BlendState.DepthTest = true;
                    BlendState.DepthWrite = true;
                    BlendState.AlphaValue = 0.5f;
                    break;
                case DepthType.NoDepthTest:
                    BlendState.DepthTest = false;
                    BlendState.DepthWrite = false;
                    BlendState.AlphaValue = 0.0f;
                    break;
            }

            this.ActiveZBufATestType = zBufATestType;
        }

        public void RenderBlendState()
        {
            BlendState.RenderDepthTest();
            BlendState.RenderBlendState();
            BlendState.RenderAlphaTest();
        }

        public void RenderPolygonState(DisplayFaceType displayFaceType)
        {
            GL.Enable(EnableCap.CullFace);
            switch (displayFaceType)
            {
                case DisplayFaceType.Both:
                    GL.Disable(EnableCap.CullFace);
                    break;
                case DisplayFaceType.Back:
                    GL.CullFace(CullFaceMode.Front);
                    break;
                case DisplayFaceType.Font:
                    GL.CullFace(CullFaceMode.Back);
                    break;
            }
        }

        public void SetupTexture(ParticleShader shader, ITextureResource texture, TextureSlot slot, int samplerID)
        {
            if (texture == null || texture.Handle == null || !texture.Initialized)
                return;

            if (texture.Handle.RenderableTex == null)
                texture.Handle.LoadRenderableTexture();

            var renderTexture = texture.Handle.RenderableTex as GLTexture;

            shader.ShaderWrapper.SetSampler(shader, samplerID, (int)slot);
            GL.ActiveTexture(TextureUnit.Texture0 + (int)slot + 1);
            renderTexture.Bind();
            SetupSampler(texture);

            int[] mask = new int[4]
            {
                OpenGLHelper.GetSwizzle(texture.Handle.RedChannel),
                OpenGLHelper.GetSwizzle(texture.Handle.GreenChannel),
                OpenGLHelper.GetSwizzle(texture.Handle.BlueChannel),
                OpenGLHelper.GetSwizzle(texture.Handle.AlphaChannel),
            };
            GL.TexParameter(renderTexture.Target, TextureParameterName.TextureSwizzleRgba, mask);
        }

        public void SetupTexture(ParticleShader shader, GLTexture texture, TextureSlot slot, int samplerID)
        {
            if (texture == null)
                return;

            shader.ShaderWrapper.SetSampler(shader, samplerID, (int)slot);
            GL.ActiveTexture(TextureUnit.Texture0 + ((int)slot) + 1);
            texture.Bind();
        }

        private void SetupSampler(ITextureResource texture)
        {
            var renderTexture = texture.Handle.RenderableTex as GLTexture;
            var target = renderTexture.Target;

            GL.BindTexture(target, renderTexture.ID);
            GL.TexParameter(target, TextureParameterName.TextureLodBias, texture.LODBias);
            GL.TexParameter(target, TextureParameterName.TextureMaxLod, texture.LODMax);
            GL.TexParameter(target, TextureParameterName.TextureMinLod, 0);
            if (texture.FilterMode == 0)
            {
                GL.TexParameter(target, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
                GL.TexParameter(target, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.LinearMipmapLinear);
            }
            else
            {
                GL.TexParameter(target, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Nearest);
                GL.TexParameter(target, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.NearestMipmapNearest);
            }
            GL.TexParameter(target, TextureParameterName.TextureWrapS, (int)ConvertWrapMode(texture.WrapX));
            GL.TexParameter(target, TextureParameterName.TextureWrapT, (int)ConvertWrapMode(texture.WrapY));
            GL.TexParameter(target, TextureParameterName.TextureWrapR, (int)TextureWrapMode.Repeat);

        }

        private TextureWrapMode ConvertWrapMode(WrapMode wrapMode)
        {
            if (wrapMode == WrapMode.Mirror)
                return TextureWrapMode.MirroredRepeat;
            if (wrapMode == WrapMode.Repeat)
                return TextureWrapMode.Repeat;

            return TextureWrapMode.Clamp;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using Toolbox.Core.IO;
using GLFrameworkEngine;

namespace EffectLibrary
{
    public interface IParticleUniformBlock
    {
        void Update(UniformBlock block, EffectSystem system)
        {

        }
    }

    public struct TextureTransform
    {
        public Vector2 TranslationShift { get; set; }
        public Vector2 Translation { get; set; }
        public Vector2 TranslationRnd { get; set; }

        public Vector2 ScaleShift { get; set; }
        public Vector2 Scale { get; set; }
        public Vector2 ScaleRnd { get; set; }

        public float RotationShift { get; set; }
        public float Rotation { get; set; }
        public float RotationRnd { get; set; }

        public Vector4 Param { get; set; }

        public void Init()
        {
            Translation = new Vector2();
            Scale = new Vector2(1, 1);
            Rotation = 0;
            Param = new Vector4(1);
        }
    }

    public class EmitterStaticUniformBlock : IParticleUniformBlock
    {
        public uint flag { get; set; }

        public TextureTransform[] Textures { get; set; }

        public int[] PtnAnim0_Tbl { get; set; }
        public int[] PtnAnim1_Tbl { get; set; }

        public Vector4 PtnAnim0_Prm { get; set; }
        public Vector4 PtnAnim1_Prm { get; set; }

        public Vector4 FlucX { get; set; }
        public Vector4 FlucY { get; set; }
        public Vector4 FresnelMinMax { get; set; }
        public Vector4 NearAlpha { get; set; }

        public Vector4 RotateVelocity { get; set; }
        public Vector4 RotateVelocityRnd { get; set; }

        public Vector4 AlphaAnim0;
        public Vector4 AlphaAnim1;
        public Vector4 AlphaAnim2;

        public Vector4 ScaleAnim0;
        public Vector4 ScaleAnim1;
        public Vector4 ScaleAnim2;

        //Color0 animated with scale
        public Vector4[] Color0 { get; set; }
        //Color0 animated with scale
        public Vector4[] Color1 { get; set; }

        public Vector4 ColorAnim0;
        public Vector4 ColorAnim1;

        public Vector4 VecParam { get; set; }
        public Vector4 GravityParam { get; set; }

        public Vector4 ShaderParamAnim0 { get; set; }
        public Vector4 ShaderParamAnim1 { get; set; }

        public Vector4 RotBasis { get; set; }
        public Vector4 ShaderParam { get; set; }

        //Old version
        public Vector4 UvScaleInit { get; set; }

        public void Init()
        {
            Textures = new TextureTransform[3];
            for (int i = 0; i < 3; i++)
            {
                Textures[i] = new TextureTransform();
                Textures[i].Init();
            }

            Color0 = new Vector4[8];
            Color1 = new Vector4[8];

            for (int i = 0; i < 8; i++)
            {
                Color0[i] = Vector4.One;
                Color1[i] = Vector4.One;
            }

            PtnAnim0_Tbl = new int[32];
            PtnAnim1_Tbl = new int[32];
            VecParam = new Vector4(1, 0, -1, 0);
            GravityParam = new Vector4(0);

            FresnelMinMax = new Vector4(1, 0, 0, 1);
            NearAlpha = new Vector4(10, 30, 100, 80);

            AlphaAnim0 = new Vector4(1, 1, 0.15f, 0);
            AlphaAnim1 = new Vector4(0, 0.85f, 5, 5);
            AlphaAnim2 = new Vector4(0, 0, 0.5f, 1.0f);

            ScaleAnim0 = new Vector4(5, 5, 1, 1);
            ScaleAnim1 = new Vector4(0, 1, 1, 0);
            ScaleAnim2 = new Vector4(1, 1, 1, 0);

            ColorAnim0 = new Vector4(0.2f, 0.6f, 0.8f, 20);
            ColorAnim1 = new Vector4(0.2f, 0.6f, 0.8f, 1);

            RotateVelocity = new Vector4(0, 0, 0, 1);

            ShaderParamAnim0 = new Vector4(1, 1, 0.25f, 1);
            ShaderParamAnim1 = new Vector4(1, 0, 0.75f, 0);

            UvScaleInit = new Vector4(1);
            RotBasis = new Vector4();
            ShaderParam = new Vector4();
        }

        public void Update(UniformBlock block, EffectSystem system)
        {
            var mem = new System.IO.MemoryStream();
            using (var writer = new Toolbox.Core.IO.FileWriter(mem))
            {
                if (!system.HasResourceFlags(FileResourceFlags.HasGPUBehavior))
                {
                    writer.Write(UvScaleInit);
                    writer.Write(RotBasis);
                    writer.Write(ShaderParam);
                }
                else
                {
                    writer.Write(flag);
                    writer.Write(0);
                    writer.Write(0);
                    writer.Write(0);

                    for (int i = 0; i < 3; i++)
                    {
                        //Data in the order of the texture emitter section
                        writer.Write(Textures[i].TranslationShift);
                        writer.Write(Textures[i].Translation);

                        writer.Write(Textures[i].TranslationRnd);
                        writer.Write(Textures[i].ScaleShift);

                        writer.Write(Textures[i].Scale);
                        writer.Write(Textures[i].ScaleRnd);

                        writer.Write(Textures[i].RotationShift);
                        writer.Write(Textures[i].Rotation);
                        writer.Write(Textures[i].RotationRnd);
                        writer.Write((float)i);

                        writer.Write(Textures[i].Param);
                    }
                    writer.Write(PtnAnim0_Tbl);
                    writer.Write(PtnAnim0_Prm);
                    writer.Write(PtnAnim1_Tbl);
                    writer.Write(PtnAnim1_Prm);
                    writer.Write(FlucX);
                    writer.Write(FlucY);
                    writer.Write(RotBasis);
                    writer.Write(FresnelMinMax);
                    writer.Write(NearAlpha);
                    writer.Write(RotateVelocity);
                    writer.Write(RotateVelocityRnd);
                    writer.Write(AlphaAnim0);
                    writer.Write(AlphaAnim1);
                    writer.Write(ScaleAnim0);
                    writer.Write(ScaleAnim1);
                    writer.Write(ScaleAnim2);
                    for (int i = 0; i < Color0.Length; i++)
                        writer.Write(Color0[i]);
                    for (int i = 0; i < Color1.Length; i++)
                        writer.Write(Color1[i]);
                    writer.Write(ColorAnim0);
                    writer.Write(ColorAnim1);
                    writer.Write(VecParam);
                    writer.Write(GravityParam);
                    writer.Write(AlphaAnim2);
                    for (int i = 0; i < 9; i++)
                        writer.Write(new Vector4());
                    writer.Write(ShaderParam);
                    writer.Write(ShaderParamAnim0);
                    writer.Write(ShaderParamAnim1);
                }
            }
            block.Add(mem.ToArray());
        }
    }

    public class EmitterDynamicUniformBlock : IParticleUniformBlock
    {
        //Color0 with scale
        public Vector4 Color0;
        //Color1 with scale
        public Vector4 Color1;

        public Vector4 Param1 { get; set; }
        public Vector4 Param2 { get; set; }

        public Matrix4 EmissionTransform0 { get; set; }
        public Matrix4 EmissionTransform1 { get; set; }

        public void Init()
        {
            Color0 = new Vector4(1);
            Color1 = new Vector4(1);
            //Todo figure out what params do what
            Param1 = new Vector4(0, 1, 1, 1.0f);
            Param2 = new Vector4(1, 1, 0, 0);
            //Todo figure out how the transform is created and updated
            EmissionTransform0 = Matrix4.Identity;
            EmissionTransform1 = Matrix4.Identity;

            EmissionTransform0 = Matrix4.CreateTranslation(-77, 100, 66);
            EmissionTransform1 = Matrix4.CreateTranslation(-77, 100, 66);
        }

        public void Update(UniformBlock block, EffectSystem system)
        {
            block.Add(Color0);
            block.Add(Color1);
            if (system.HasResourceFlags(FileResourceFlags.HasGPUBehavior))
            {
                block.Add(Param1);
                block.Add(Param2);
                block.Add(EmissionTransform0.Column0);
                block.Add(EmissionTransform0.Column1);
                block.Add(EmissionTransform0.Column2);
                block.Add(EmissionTransform0.Column3);
                block.Add(EmissionTransform1.Column0);
                block.Add(EmissionTransform1.Column1);
                block.Add(EmissionTransform1.Column2);
                block.Add(EmissionTransform1.Column3);
            }
        }
    }

    public struct ViewUniformBlock : IParticleUniformBlock
    {
        public Matrix4 ViewMatrix;
        public Matrix4 ViewProjectionMatrix;
        public Matrix4 ProjectionMatrix;
        public Matrix4 BillboardMatrix;
        public Vector4 EyeView;
        public Vector4 EyePosition;
        public Vector4 DepthBufferTextureMatrix;
        public Vector4 FrameBufferTextureMatrix;
        public Vector4 ViewParam;

        public void Update(UniformBlock block, EffectSystem system)
        {
            block.Add(this.ViewMatrix.Column0);
            block.Add(this.ViewMatrix.Column1);
            block.Add(this.ViewMatrix.Column2);
            block.Add(this.ViewMatrix.Column3);
            //Assume matrix proj is used for versions that support gpu calculation behavior
            if (system.HasResourceFlags(FileResourceFlags.HasGPUBehavior))
            {
                block.Add(this.ProjectionMatrix.Column0);
                block.Add(this.ProjectionMatrix.Column1);
                block.Add(this.ProjectionMatrix.Column2);
                block.Add(this.ProjectionMatrix.Column3);
            }
            block.Add(this.ViewProjectionMatrix.Column0);
            block.Add(this.ViewProjectionMatrix.Column1);
            block.Add(this.ViewProjectionMatrix.Column2);
            block.Add(this.ViewProjectionMatrix.Column3);
            block.Add(this.BillboardMatrix.Column0);
            block.Add(this.BillboardMatrix.Column1);
            block.Add(this.BillboardMatrix.Column2);
            block.Add(this.BillboardMatrix.Column3);
            block.Add(this.EyeView);
            block.Add(this.EyePosition);
            block.Add(this.FrameBufferTextureMatrix);
            block.Add(this.DepthBufferTextureMatrix);
            block.Add(this.ViewParam);
        }
    }

    public struct StripeUniformBlock : IParticleUniformBlock
    {
        public Vector4 stParam;
        public Vector4 uvScrollAnim;
        public Vector4 uvScrollAnim2;
        public Vector4 uvScaleRotateAnim0;
        public Vector4 uvScaleRotateAnim1;
        public Vector4 uvScaleRotateAnim2;
        public Vector4 vtxColor0;
        public Vector4 vtxColor1;
        public Matrix4 emitterMat;

        public void Update(UniformBlock block, EffectSystem system)
        {
            if (system.HasResourceFlags(FileResourceFlags.HasTexture3))
            {
                block.Add(this.stParam);
                block.Add(this.uvScrollAnim);
                block.Add(this.uvScrollAnim2);
                block.Add(this.uvScaleRotateAnim0);
                block.Add(this.uvScaleRotateAnim1);
                block.Add(this.uvScaleRotateAnim2);
                block.Add(this.vtxColor0);
                block.Add(this.vtxColor1);
                block.Add(this.emitterMat.Column0);
                block.Add(this.emitterMat.Column1);
                block.Add(this.emitterMat.Column2);
                block.Add(this.emitterMat.Column3);
            }
            else
            {
                block.Add(this.stParam);
                block.Add(this.uvScrollAnim);
                block.Add(this.uvScaleRotateAnim0);
                block.Add(this.uvScaleRotateAnim1);
                block.Add(this.vtxColor0);
                block.Add(this.vtxColor1);
                block.Add(this.emitterMat.Column0);
                block.Add(this.emitterMat.Column1);
                block.Add(this.emitterMat.Column2);
                block.Add(this.emitterMat.Column3);
            }
        }
    }

    public struct UserDataUniformBlock : IParticleUniformBlock
    {

    }

    public struct StripeVertexBuffer
    {
        public Vector4 pos;
        public Vector4 outer;
        public Vector4 texCoord;
        public Vector4 dir;
    };
}

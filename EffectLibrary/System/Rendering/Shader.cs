﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EffectLibrary.WiiU;
using System.IO;
using GLFrameworkEngine;

namespace EffectLibrary
{
    public class ParticleShader
    {
        public VertexShaderKey VertexShaderKey;
        public FragmentShaderKey FragmentShaderKey;

        public UniformBlock VertexViewUniformBlock;
        public UniformBlock FragmentViewUniformBlock;

        public UniformBlock VertexEmitterStaticUniformBlock;
        public UniformBlock VertexEmitterDynamicUniformBlock;

        public UniformBlock VertexStripeUniformBlock;

        public UniformBlock FragmentEmitterStaticUniformBlock;

        public UniformBlock[] VertexUserBlock = new UniformBlock[2];
        public UniformBlock[] FragmentUserBlock = new UniformBlock[2];

        public int attrPosBuffer = -1;
        public int attrIndexBuffer = -1;
        public int attrTexCoordBuffer = -1;
        public int attrNormalBuffer = -1;
        public int attrColorBuffer = -1;
        public int attrWldPosBuffer = -1;
        public int attrSclBuffer = -1;
        public int attrColor0Buffer = -1;
        public int attrColor1Buffer = -1;
        public int attrTexAnimBuffer = -1;
        public int attrWldPosDfBuffer = -1;
        public int attrRotBuffer = -1;
        public int attrSubTexAnimBuffer = -1;
        public int attrEmMat0Buffer = -1;
        public int attrEmMat1Buffer = -1;
        public int attrEmMat2Buffer = -1;

        //GPU
        public int attrVecBuffer = -1;
        public int attrRandomBuffer = -1;

        //Stream Out
        public int attrStreamOutPos = -1;
        public int attrStreamOutVec = -1;

        //Stripe
        public int attrOuterBuffer = -1;
        public int attrDirBuffer = -1;

        public int[] FragmentSamplerLocations = new int[3];
        public int[] FragmentSamplerLocations2 = new int[3];

        public int FragmentFBSamplerLocation = -1;
        public int FragmentDBSamplerLocation = -1;

        public int VertexFBSamplerLocation = -1;
        public int VertexDBSamplerLocation = -1;

        public int FragmentCubeLightMap = -1;

        public int VertexShadowSamplerLocation = -1;

        //SM3DW
        public int VertexExposureTexture = -1;

        public bool Initialized = false;

        public GLShaderInfo ShaderInfo;

        public ShaderWrapperBase ShaderWrapper;

        public ParticleShader()
        {
            for (int i = 0; i < FragmentSamplerLocations.Length; i++)
                FragmentSamplerLocations[i] = -1;
            for (int i = 0; i < FragmentSamplerLocations2.Length; i++)
                FragmentSamplerLocations2[i] = -1;
        }

        public void SetupShaderResource(Heap heap, IShaderResource shader) {
            ShaderWrapper = shader.CreatePlatformWrapper();
        }

        public void LoadShader() {
            if (!Initialized)
                ShaderWrapper.ReloadGLSLSource(this);
        }

        public void EnableInstanced()
        {

        }

        public void DisableInstanced()
        {

        }

        public void UpdateVAO(ParticleBaseMeshRender mesh) {
            ShaderWrapper.UpdateVAO(this, mesh);
        }

        public void BindViewUniformBlock(ViewUniformBlock block, EffectSystem system)
        {
            BindUniformBlock(VertexViewUniformBlock, block, system);
            BindUniformBlock(FragmentViewUniformBlock, block, system);
        }

        public void BindStaticUniformBlock(EmitterStaticUniformBlock block, EffectSystem system)
        {
            //Stripes don't use static buffers in vertex shader
            if (VertexEmitterStaticUniformBlock != null)
                BindUniformBlock(VertexEmitterStaticUniformBlock, block, system);
            BindUniformBlock(FragmentEmitterStaticUniformBlock, block, system);
        }

        public void BindDynamicUniformBlock(EmitterDynamicUniformBlock block, EffectSystem system)
        {
            BindUniformBlock(VertexEmitterDynamicUniformBlock, block, system);
        }

        public void BindStripeUniformBlock(StripeUniformBlock block, EffectSystem system)
        {
            BindUniformBlock(VertexStripeUniformBlock, block, system);
        }

        private void BindUniformBlock(UniformBlock glBlock, IParticleUniformBlock block, EffectSystem system)
        {
            glBlock.Buffer.Clear();
            block.Update(glBlock, system);

            glBlock.BindUniformBlock(ShaderInfo.Program.program);
            glBlock.UpdateBufferData();
        }

        public void BindVertexUserUniformBlock()
        {
            VertexUserBlock[0].Buffer.Clear();
            ShaderWrapper.UpdateVertexUserData(VertexUserBlock[0]);

            VertexUserBlock[0].BindUniformBlock(ShaderInfo.Program.program);
            VertexUserBlock[0].UpdateBufferData();
        }

        public void BindFragmentUserUniformBlock()
        {
            FragmentUserBlock[0].Buffer.Clear();
            ShaderWrapper.UpdatePixelUserData(FragmentUserBlock[0]);

            FragmentUserBlock[0].BindUniformBlock(ShaderInfo.Program.program);
            FragmentUserBlock[0].UpdateBufferData();
        }
    }
}

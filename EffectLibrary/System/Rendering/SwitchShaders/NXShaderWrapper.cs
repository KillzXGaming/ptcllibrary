﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EffectLibrary.Switch;
using GLFrameworkEngine;

namespace EffectLibrary
{
    public class NXShaderWrapper : ShaderWrapperBase
    {
        public BfshaLibrary.ShaderVariation ShaderVariation;

        public Dictionary<string, int> VertexSlots;
        public Dictionary<string, int> FragmentSlots;

        public UniformBlock VertexConstantsUniformBlock;
        public UniformBlock FragmentConstantsUniformBlock;

        public NXShaderWrapper(BfshaLibrary.ShaderVariation variation) {
            ShaderVariation = variation;

            VertexSlots = GetSlotData(ShaderVariation.BinaryProgram.ShaderReflection.VertexShaderCode);
            FragmentSlots = GetSlotData(ShaderVariation.BinaryProgram.ShaderReflection.PixelShaderCode);
        }

        public override void ReloadGLSLSource(ParticleShader shader)
        {
            shader.ShaderInfo = TegraShaderDecoder.LoadShaderProgram(ShaderVariation);

            shader.ShaderInfo.Program.Enable();

            InitalizeConstants(shader);

            if (shader.VertexShaderKey.VertexTransformMode != VertexTransformMode.Stripe &&
                shader.VertexShaderKey.VertexTransformMode != VertexTransformMode.ComplexStripe)
            {
                InitializeVertexShaderLocation(shader);
                InitializeFragmentShaderLocation(shader);
                InitAttributeLocations(shader);
            }
            else
            {
                InitializeStripeVertexShaderLocation(shader);
                InitializeFragmentShaderLocation(shader);
                InitializeStripeAttribute(shader);
            }

            shader.ShaderInfo.Program.Disable();

            shader.Initialized = true;
        }

        private void InitalizeConstants(ParticleShader shader)
        {
            var vertexConstants = shader.ShaderInfo.VertexConstants;
            var pixelConstants = shader.ShaderInfo.PixelConstants;

            if (vertexConstants != null) {
                var programID = shader.ShaderInfo.Program.program;
                var index = OpenTK.Graphics.OpenGL.GL.GetUniformBlockIndex(programID, $"vp_c1");

                VertexConstantsUniformBlock = new UniformBlock(index);
                VertexConstantsUniformBlock.Add(vertexConstants);

                VertexConstantsUniformBlock.BindUniformBlock(programID);
                VertexConstantsUniformBlock.UpdateBufferData();
            }
            if (pixelConstants != null) {
                var programID = shader.ShaderInfo.Program.program;
                var index = OpenTK.Graphics.OpenGL.GL.GetUniformBlockIndex(programID, $"fp_c1");

                FragmentConstantsUniformBlock = new UniformBlock(index);
                FragmentConstantsUniformBlock.Add(pixelConstants);

                FragmentConstantsUniformBlock.BindUniformBlock(programID);
                FragmentConstantsUniformBlock.UpdateBufferData();
            }
        }

        private void InitializeVertexShaderLocation(ParticleShader shader)
        {
            int viewIndex = GetVertexBlockLocation(shader, "sysViewUniformBlock");
            int staticIndex = GetVertexBlockLocation(shader, "sysEmitterStaticUniformBlock");
            int dynamicIndex = GetVertexBlockLocation(shader, "sysEmitterDynamicUniformBlock");

            shader.VertexViewUniformBlock = new UniformBlock(viewIndex);
            shader.VertexEmitterStaticUniformBlock = new UniformBlock(staticIndex);
            shader.VertexEmitterDynamicUniformBlock = new UniformBlock(dynamicIndex);
        }

        private void InitializeFragmentShaderLocation(ParticleShader shader)
        {
            int viewIndex = GetPixelBlockLocation(shader, "sysViewUniformBlock");
            int staticIndex = GetPixelBlockLocation(shader, "sysEmitterDynamicUniformBlock");

            shader.FragmentViewUniformBlock = new UniformBlock(viewIndex);
            shader.FragmentEmitterStaticUniformBlock = new UniformBlock(staticIndex);

            InitSamplerLocations(shader);
        }

        private void InitializeStripeVertexShaderLocation(ParticleShader shader)
        {
            int stripeIndex = GetVertexBlockLocation(shader, "sysStripeUniformBlock");
            int viewIndex = GetVertexBlockLocation(shader, "sysViewUniformBlock");

            shader.VertexViewUniformBlock = new UniformBlock(viewIndex);
            shader.VertexStripeUniformBlock = new UniformBlock(stripeIndex);
        }

        public override void UpdateVAO(ParticleShader shader, ParticleBaseMeshRender mesh)
        {
            Dictionary<string, int> attributes = new Dictionary<string, int>();
            if (shader.attrPosBuffer != -1) attributes.Add("v_inPos", shader.attrPosBuffer);
            if (shader.attrColorBuffer != -1) attributes.Add("v_inColor", shader.attrColorBuffer);
            if (shader.attrTexCoordBuffer != -1) attributes.Add("v_inTexCoord", shader.attrTexCoordBuffer);
            if (shader.attrNormalBuffer != -1) attributes.Add("v_inNormal", shader.attrNormalBuffer);

            if (shader.attrIndexBuffer != -1) attributes.Add("v_inIndex", shader.attrIndexBuffer);
            if (shader.attrColor0Buffer != -1) attributes.Add("v_inColor0", shader.attrColor0Buffer);
            if (shader.attrColor1Buffer != -1) attributes.Add("v_inColor1", shader.attrColor1Buffer);
            if (shader.attrSclBuffer != -1) attributes.Add("v_inScl", shader.attrSclBuffer);
            if (shader.attrWldPosBuffer != -1) attributes.Add("v_inWldPos", shader.attrWldPosBuffer);
            if (shader.attrWldPosDfBuffer != -1) attributes.Add("v_inWldPosDf", shader.attrWldPosDfBuffer);
            if (shader.attrRotBuffer != -1) attributes.Add("v_inRot", shader.attrRotBuffer);
            if (shader.attrVecBuffer != -1) attributes.Add("v_inVec", shader.attrVecBuffer);
            if (shader.attrRandomBuffer != -1) attributes.Add("v_inRandom", shader.attrRandomBuffer);

            mesh.UpdateVAO(attributes);
        }

        public override void InitAttributeLocations(ParticleShader shader)
        {
            if (VertexSlots == null)
                return;

            shader.attrPosBuffer = GetAttribute("sysPosAttr");
            if (!shader.VertexShaderKey.IsPrimitive)
                shader.attrIndexBuffer = GetAttribute("sysIndexAttr");
            shader.attrTexCoordBuffer = GetAttribute("sysTexCoordAttr");
            shader.attrNormalBuffer = GetAttribute("sysNormalAttr");
            shader.attrColorBuffer = GetAttribute("sysVertexColorAttr");
            shader.attrWldPosBuffer = GetAttribute("sysLocalPosAttr");
            shader.attrSclBuffer = GetAttribute("sysScaleAttr");
            shader.attrColor0Buffer = GetAttribute("sysVertexColor0Attr");
            shader.attrColor1Buffer = GetAttribute("sysVertexColor1Attr");
            shader.attrWldPosDfBuffer = GetAttribute("sysLocalDiffAttr");
            shader.attrRotBuffer = GetAttribute("sysInitRotateAttr");
            shader.attrVecBuffer = GetAttribute("sysLocalVecAttr");
            shader.attrRandomBuffer = GetAttribute("sysRandomAttr");


            shader.attrEmMat0Buffer = GetAttribute("v_inEmtMat0");
            shader.attrEmMat1Buffer = GetAttribute("v_inEmtMat1");
            shader.attrEmMat2Buffer = GetAttribute("v_inEmtMat2");

          //  shader.attrTexAnimBuffer = GetAttribute("v_inTexAnim");
            //shader.attrSubTexAnimBuffer = GetAttribute("v_inSubTexAnim");
        }

        private void InitializeStripeAttribute(ParticleShader shader)
        {
            shader.attrPosBuffer = GetAttribute("v_inPos");
            shader.attrOuterBuffer = GetAttribute("v_inOuter");
            shader.attrTexCoordBuffer = GetAttribute("v_inTexCoord");
            shader.attrDirBuffer = GetAttribute("v_inDir");
        }

        private int GetVertexShaderLocation(string name)
        {
            if (VertexSlots.ContainsKey(name))
                return VertexSlots[name];
            return -1;
        }

        private int GetPixelShaderLocation(string name)
        {
            if (FragmentSlots.ContainsKey(name))
                return FragmentSlots[name];
            return -1;
        }

        private int GetVertexBlockLocation(ParticleShader shader, string name)
        {
            var slot = GetVertexShaderLocation(name);
            var programID = shader.ShaderInfo.Program.program;
            //Get the slot to find the block index in shader.
            var index = OpenTK.Graphics.OpenGL.GL.GetUniformBlockIndex(programID, $"vp_c{slot + 3}");

            return index;
        }

        private int GetPixelBlockLocation(ParticleShader shader, string name)
        {
            var slot = GetPixelShaderLocation(name);
            var programID = shader.ShaderInfo.Program.program;
            //Get the slot to find the block index in shader.
            var index = OpenTK.Graphics.OpenGL.GL.GetUniformBlockIndex(programID, $"fp_c{slot + 3}");

            return index;
        }

        private int GetPixelSamplerLocation(ParticleShader shader, string name)
        {
            var slot = GetPixelShaderLocation(name);
            if (slot == -1)
                return -1;

            var programID = shader.ShaderInfo.Program.program;
            //Get the slot to find the block index in shader.
            string uniform = "fp_tex_tcb_" + ((slot * 2) + 8).ToString("X1");
            var index = OpenTK.Graphics.OpenGL.GL.GetUniformLocation(programID, uniform);
            Console.WriteLine($"GetPixelSamplerLocation {name} slot {slot} uniform {uniform} index {index}");
            return index;
        }

        private int GetAttribute(string name) {
            return GetVertexShaderLocation(name);
        }

        private void InitSamplerLocations(ParticleShader shader)
        {
            shader.FragmentSamplerLocations[0] = GetPixelSamplerLocation(shader, "sysTextureSampler0");
            shader.FragmentSamplerLocations[1] = GetPixelSamplerLocation(shader, "sysTextureSampler1");
            shader.FragmentSamplerLocations[2] = GetPixelSamplerLocation(shader, "sysTextureSampler2");

            shader.FragmentFBSamplerLocation = GetPixelSamplerLocation(shader, "sysFrameBufferTexture");
            shader.FragmentDBSamplerLocation = GetPixelSamplerLocation(shader, "sysDepthBufferTexture");
        }

        static Dictionary<string, int> GetSlotData(BfshaLibrary.ShaderReflectionData reflect)
        {
            Dictionary<string, int> slots = new Dictionary<string, int>();

            int index = 0;

            foreach (var att in reflect.ShaderInputDictionary.Keys)
                slots.Add(att, reflect.AttributeSlots[index++]);
            foreach (var att in reflect.ShaderOutputDictionary.Keys)
                slots.Add(att, reflect.AttributeSlots[index++]);
            foreach (var att in reflect.ShaderSamplerDictionary.Keys)
                slots.Add(att, reflect.AttributeSlots[index++]);
            foreach (var att in reflect.ShaderConstantBufferDictionary.Keys)
                slots.Add(att, reflect.AttributeSlots[index++]);
            return slots;
        }
    }
}

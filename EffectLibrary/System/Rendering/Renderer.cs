﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using GLFrameworkEngine;
using OpenTK.Graphics.OpenGL;

namespace EffectLibrary
{
    public partial class Renderer
    {
        public Vector2 depthBufferTextureOffset;
        public Vector2 depthBufferTextureScale;
        public Vector2 frameBufferTextureOffset;
        public Vector2 frameBufferTextureScale;

        public ShaderType ShaderType;

        private ParticleQuadRender QuadRender;
        private PrimitiveRenderer PrimitiveRenderer;
        private ParticleStripeRender StripeRender;

        public RenderContext RenderContext;

        PtclType currentParticleType;

        static GLTexture depthBufferTexture;
        static GLTexture frameBufferTexture;
        static GLTexture defaultTexture;
        static GLTexture cubeLightMapTexture;
        static GLTexture exposureTexture;
        static GLTexture shadowTexture;

        Vector3 eyeVec;

        Heap heap;

        SwapBuffer doubleBuffer;

        EffectSystem system;
        public ViewUniformBlock viewUniformBlock;

        Matrix4 view;

        int RenderVisibilityFlags;

        int stripeNumDrawVertex;

        GLContext Context;

        public Renderer(Heap argHeap, EffectSystem system, Config config) {
            this.system = system;

            depthBufferTextureOffset = new Vector2(0);
            depthBufferTextureScale  = new Vector2(1);
            frameBufferTextureOffset = new Vector2(0);
            frameBufferTextureScale  = new Vector2(1);

            heap = argHeap;

            depthBufferTexture = null;
            frameBufferTexture = null;
            defaultTexture = null;

            currentParticleType = PtclType.Max;
            ShaderType = ShaderType.Normal;
            RenderVisibilityFlags = 0x3F;

            //Inits a quad renderer
            QuadRender = new ParticleQuadRender();
            StripeRender = new ParticleStripeRender();

            uint doubleBufferSize = config.doubleBufferSize;
            doubleBufferSize += config.numParticleMax * 0xCC;
            doubleBufferSize += config.numEmitterMax * 0x114;

            doubleBuffer = new SwapBuffer();
            doubleBuffer.Initialize(heap, doubleBufferSize);

            RenderContext = new RenderContext();
            viewUniformBlock = new ViewUniformBlock();

            view = Matrix4.Identity;
            Context = null;
        }

        public void InitBufferTextures()
        {
            defaultTexture = GLTexture2D.CreateConstantColorTexture(4, 4, 255, 255, 255, 255);
            depthBufferTexture = GLTexture2D.CreateConstantColorTexture(4, 4, 255, 255, 255, 255);
            frameBufferTexture = GLTexture2D.CreateConstantColorTexture(4, 4, 255, 255, 255, 255);
            cubeLightMapTexture = GLTexture2DArray.CreateConstantColorTexture(4, 4, 6, 255, 255, 255, 255);
            exposureTexture = GLTexture2D.CreateConstantColorTexture(4, 4, 255, 255, 255, 255);
            shadowTexture = GLTexture2D.CreateConstantColorTexture(4, 4, 255, 255, 255, 255);

            cubeLightMapTexture.Bind();
            cubeLightMapTexture.GenerateMipmaps();
            cubeLightMapTexture.Unbind();
        }

        public void BeginRender(GLContext context, Matrix4 projection, Matrix4 view, 
            Vector3 cameraWorldPos, float zNear, float zFar)
        {
            this.view = view;

            if (defaultTexture == null)
                InitBufferTextures();

            if (ScreenBufferTexture.ScreenBuffer != null)
                frameBufferTexture = ScreenBufferTexture.ScreenBuffer;

            RenderContext.SetupCommonState();

            Context = context;

            Matrix4 billboard = Matrix4.Transpose(view);
            billboard[3, 0] = 0;
            billboard[3, 1] = 0;
            billboard[3, 2] = 0;

            billboard[0, 3] = 0;
            billboard[1, 3] = 0;
            billboard[2, 3] = 0;
            billboard[3, 3] = 1.0f;

            eyeVec = new Vector3(
                billboard[2, 0],
                billboard[2, 1],
                billboard[2, 2]);

            view[0, 3] = 0;
            view[1, 3] = 0;
            view[2, 3] = 0;
            view[3, 3] = 1.0f;

            Matrix4 viewProj = view * projection;

            viewUniformBlock.ViewMatrix = view;
            viewUniformBlock.ProjectionMatrix = projection;
            viewUniformBlock.ViewProjectionMatrix = viewProj;
            viewUniformBlock.BillboardMatrix = billboard;
            viewUniformBlock.EyeView = new Vector4(eyeVec, 0);
            viewUniformBlock.EyePosition = new Vector4(cameraWorldPos, 0);
            viewUniformBlock.DepthBufferTextureMatrix.Xy = depthBufferTextureScale;
            viewUniformBlock.DepthBufferTextureMatrix.Zw = depthBufferTextureOffset;
            viewUniformBlock.FrameBufferTextureMatrix.Xy = frameBufferTextureScale;
            viewUniformBlock.FrameBufferTextureMatrix.Zw = frameBufferTextureOffset;
            viewUniformBlock.ViewParam = new Vector4(zNear, zFar, zNear * zFar, zFar - zNear);
        }

        public bool SetupParticleShaderAndVertex(ParticleShader shader, MeshType type, PrimitiveRenderer primitive)
        {
            if (!shader.Initialized)
                shader.LoadShader();

            if (type == MeshType.Primitive && primitive == null | (primitive != null && !primitive.HasElements()))
                return false;

            this.PrimitiveRenderer = type == MeshType.Primitive ? primitive : null;

            if (shader.ShaderInfo == null)
                return false;

            Context.CurrentShader = shader.ShaderInfo.Program;

            //Bind the view block
            ViewUniformBlock viewUniformBlock = this.viewUniformBlock;
            shader.BindViewUniformBlock(viewUniformBlock, system);

            shader.BindVertexUserUniformBlock();
            shader.BindFragmentUserUniformBlock();

            return true;
        }

        public void RequestParticle(EmitterInstance emitter, ParticleShader shader, bool isChild, bool flushCache, object argData)
        {
            var data = emitter.data;

            uint numParticles = emitter.numParticles;
            var textures = new ITextureResource[3];

            CustomShaderCallBackID callbackID;
            DepthType zBufATestType;
            BlendType blendType;
            DisplayFaceType displaySideType;
            MeshType meshType;

            if (isChild)
            {
                var childData = emitter.GetChildData();

                numParticles = emitter.numChildParticles;
                textures[0] = childData.Texture;
                textures[1] = null;
                textures[2] = null;
                callbackID = (CustomShaderCallBackID)childData.ShaderUserSetting;
                zBufATestType = childData.ZbufferTestType;
                blendType = childData.BlendType;
                displaySideType = childData.DisplaySideType;
                meshType = childData.MeshType;
            }
            else
            {
                callbackID = (CustomShaderCallBackID)(data.ShaderUserSetting);
                zBufATestType = data.ZbufferTestType;
                blendType = data.BlendType;
                displaySideType = data.DisplaySideType;
                meshType = data.MeshType;
                textures[0] = data.Textures[0];
                textures[1] = data.Textures[1];
                if (data.Textures.Length > 2)
                    textures[2] = data.Textures[2];
            }

            if (numParticles == 0)
                return;

            RenderContext.SetZBufATest(zBufATestType);
            RenderContext.SetBlendType(blendType);
            RenderContext.RenderBlendState();
            RenderContext.RenderPolygonState(displaySideType);

            RenderContext.SetupTexture(shader, defaultTexture, TextureSlot._0, 0);
            for (int i = 0; i < shader.FragmentSamplerLocations.Length; i++)
            {
                if (shader.FragmentSamplerLocations[i] != -1)
                    RenderContext.SetupTexture(shader, textures[i], TextureSlot._0 + i, shader.FragmentSamplerLocations[i]);
            }

            if (shader.FragmentFBSamplerLocation != -1)
                RenderContext.SetupTexture(shader, frameBufferTexture, TextureSlot.FrameBuffer, shader.FragmentFBSamplerLocation);
            if (shader.FragmentDBSamplerLocation != -1)
                RenderContext.SetupTexture(shader, depthBufferTexture, TextureSlot.DepthBuffer, shader.FragmentDBSamplerLocation);
            if (shader.VertexDBSamplerLocation != -1)
                RenderContext.SetupTexture(shader, depthBufferTexture, TextureSlot.DepthBuffer, shader.VertexDBSamplerLocation);
            
            if (shader.FragmentCubeLightMap != -1)
                RenderContext.SetupTexture(shader, cubeLightMapTexture, TextureSlot.CubeLightMap, shader.FragmentCubeLightMap);

            if (shader.VertexExposureTexture != -1)
                RenderContext.SetupTexture(shader, exposureTexture, (TextureSlot)7, shader.VertexExposureTexture);
            if (shader.VertexShadowSamplerLocation != -1)
                RenderContext.SetupTexture(shader, shadowTexture, (TextureSlot)8, shader.VertexShadowSamplerLocation);


            var glShader = shader.ShaderInfo.Program;
            CafeShaderDecoder.SetShaderConstants(glShader, glShader.program, this.RenderContext.BlendState);

            if (system.GetCustomShaderRenderStateSetCallback((int)callbackID) != null)
            {
                RenderStateSetArg arg = new RenderStateSetArg()
                {
                    emitter = emitter,
                    renderer = this,
                    flushCache = flushCache,
                    argData = argData,
                };
                system.GetCustomShaderRenderStateSetCallback((int)callbackID).Invoke(arg);
            }

            if (!isChild && data.ParticleZSort)
            {
                int i = 0;

                PtclViewZ[] sortedPtcls = new PtclViewZ[numParticles];

                PtclInstance ptcl = emitter.particleHead;
                while (ptcl != null)
                {
                    sortedPtcls[i].ptcl = ptcl;
                    sortedPtcls[i].ZOrder =
                        (uint)(view[0, 2] * ptcl.worldPos.X +
                               view[1, 2] * ptcl.worldPos.Y +
                               view[2, 2] * ptcl.worldPos.Z + view[3, 2]);

                    sortedPtcls[i].idx = i;
                    ptcl = ptcl.Next; i++;
                }

                Array.Sort(sortedPtcls, new SortByZOrder());

                for (int j = 0; j < i; j++)
                {
                    BindParticleAttributeBlock(new PtclAttributeBuffer[1] { emitter.PtclAttributeBuffer[sortedPtcls[j].idx] }, shader, 1);

                    BindVAO(meshType);
                    emitter.PositionStreamOut.Bind(shader.attrStreamOutPos, 0, false);
                    emitter.VectorStreamOut.Bind(shader.attrStreamOutVec, 1, false);

                    if (meshType == MeshType.Primitive && PrimitiveRenderer != null)
                        PrimitiveRenderer.Draw(Context);
                    else
                        QuadRender.Draw(Context);
                }
            }
            else
            {
                uint numDrawParticle;

                if (!isChild)
                {
                    numDrawParticle = emitter.numDrawParticle;
                    BindParticleAttributeBlock(emitter.PtclAttributeBuffer, shader, numDrawParticle);
                }
                else
                {
                    numDrawParticle = emitter.numDrawChildParticle;
                    BindParticleAttributeBlock(emitter.ChildPtclAttributeBuffer, shader, numDrawParticle);
                }

                BindVAO(meshType);
                emitter.PositionStreamOut.Bind(shader.attrStreamOutPos, 0, false);
                emitter.VectorStreamOut.Bind(shader.attrStreamOutVec, 1, false);

                if (meshType == MeshType.Primitive && PrimitiveRenderer != null)
                    PrimitiveRenderer.Draw(Context, (int)numDrawParticle);
                else
                    QuadRender.Draw(Context, (int)numDrawParticle);
            }
        }

        public void DrawStreamOut(EmitterInstance emitter)
        {
            if (emitter.PositionStreamOut.StreamOutIDs[0] == -1)
                return;

            var data = emitter.data;

            ParticleShader shader = emitter.shader[(int)this.ShaderType];
            if (shader == null
                || emitter.PtclAttributeBuffer == null
                || emitter.DynamicUniformBlock == null
                || !SetupParticleShaderAndVertex(shader, emitter.data.MeshType, emitter.primitive))
            {
                return;
            }

            EmitterStaticUniformBlock emitterStaticUniformBlock = emitter.StaticUniformBlock;
            shader.BindStaticUniformBlock(emitterStaticUniformBlock, system);

            EmitterDynamicUniformBlock emitterDynamicUniformBlock = emitter.DynamicUniformBlock;
            shader.BindDynamicUniformBlock(emitterDynamicUniformBlock, system);

            shader.EnableInstanced();

            var glShader = shader.ShaderInfo.Program;
            CafeShaderDecoder.SetShaderConstants(glShader, glShader.program, this.RenderContext.BlendState);

            SetupStreamout(emitter, shader, data.MeshType);

            shader.DisableInstanced();

            GL.Flush();
            emitter.PositionStreamOut.PrintFeedback(0);
            emitter.VectorStreamOut.PrintFeedback(1);
        }

        public void EntryChildParticleSub(EmitterInstance emitter, bool flushCache, object argData)
        {
            ParticleShader shader = emitter.childShader[(int)this.ShaderType];
            if (shader == null
                || emitter.ChildPtclAttributeBuffer == null
                || emitter.ChildDynamicUniformBlock == null
                || !SetupParticleShaderAndVertex(shader, emitter.GetChildData().MeshType, emitter.childPrimitive))
            {
                return;
            }

            EmitterStaticUniformBlock emitterStaticUniformBlock = emitter.ChildStaticUniformBlock;
            shader.BindStaticUniformBlock(emitterStaticUniformBlock, system);

            EmitterDynamicUniformBlock emitterDynamicUniformBlock = emitter.ChildDynamicUniformBlock;
            shader.BindDynamicUniformBlock(emitterDynamicUniformBlock, system);

            shader.EnableInstanced();
            RequestParticle(emitter, shader, true, flushCache, argData);
            shader.DisableInstanced();
        }

        public void BindParticleAttributeBlock(PtclAttributeBuffer[] buffer, ParticleShader shader, uint numInstances)
        {
            if (numInstances == 0)
                return;

            if (PrimitiveRenderer != null)
            {
                shader.UpdateVAO(PrimitiveRenderer);
                PrimitiveRenderer.UpdateInstancedVertexData(buffer);
            }
            else
            {
                shader.UpdateVAO(QuadRender);
                QuadRender.UpdateInstancedVertexData(buffer);
            }
        }

        public void EntryParticleSub(EmitterInstance emitter, bool flushCache, object argData)
        {
            ParticleShader shader = emitter.shader[(int)this.ShaderType];
            if (shader == null
                || emitter.PtclAttributeBuffer == null
                || emitter.DynamicUniformBlock == null
                || !SetupParticleShaderAndVertex(shader, emitter.data.MeshType, emitter.primitive))
            {
                return;
            }

            EmitterStaticUniformBlock emitterStaticUniformBlock = emitter.StaticUniformBlock;
            shader.BindStaticUniformBlock(emitterStaticUniformBlock, system);

            EmitterDynamicUniformBlock emitterDynamicUniformBlock = emitter.DynamicUniformBlock;
            shader.BindDynamicUniformBlock(emitterDynamicUniformBlock, system);

            shader.EnableInstanced();
            RequestParticle(emitter, shader, false, flushCache, argData);
            shader.DisableInstanced();
        }

        public void EntryParticle(EmitterInstance emitter, bool flushCache, object argData)
        {
            var callback = system.GetCustomShaderDrawOverrideCallback(emitter.data.ShaderUserSetting);
            ShaderDrawOverrideArg arg = new ShaderDrawOverrideArg
            {
                emitter = emitter,
                renderer = this,
                flushCache = flushCache,
                argData = argData,
            };

            if ((emitter.controller.VisibilityFlags & RenderVisibilityFlags) == 0)
                return;

            bool stripe = false;
            if (emitter.data.VertexTransformMode == VertexTransformMode.Stripe ||
                emitter.data.VertexTransformMode == VertexTransformMode.ComplexStripe)
            {
                EntryStripe(emitter, flushCache, argData);
                stripe = true;
            }

            if (emitter.data.Type == EmitterType.Complex && emitter.HasChild())
            {
                var cdata = emitter.data as ComplexEmitterData;
                var childCallback = system.GetCustomShaderDrawOverrideCallback((int)emitter.GetChildData().ShaderUserSetting);

                if ((cdata.childFlags & (int)ChildFlags.DrawBeforeParent) != 0 && emitter.numDrawChildParticle != 0 && emitter.ChildPtclAttributeBuffer != null)
                {
                    currentParticleType = PtclType.Child;

                    if (childCallback != null)
                        childCallback(arg);
                    else
                        EntryChildParticleSub(emitter, flushCache, argData);
                }

                if (cdata.DisplayParent && !stripe && emitter.numDrawParticle != 0 && emitter.PtclAttributeBuffer != null)
                {
                    currentParticleType = PtclType.Complex;

                    if (callback != null)
                        callback(arg);
                    else
                        EntryParticleSub(emitter, flushCache, argData);
                }

                if ((cdata.childFlags & (int)ChildFlags.DrawBeforeParent) == 0 && emitter.numDrawChildParticle != 0 && emitter.ChildPtclAttributeBuffer != null)
                {
                    currentParticleType = PtclType.Child;

                    if (childCallback != null)
                        childCallback(arg);
                    else
                        EntryChildParticleSub(emitter, flushCache, argData);
                }
            }
            else
            {
                if (emitter.data.DisplayParent && !stripe && emitter.numDrawParticle != 0 && emitter.PtclAttributeBuffer != null)
                {
                    currentParticleType = PtclType.Simple;

                    if (callback != null)
                        callback(arg);
                    else
                        EntryParticleSub(emitter, flushCache, argData);
                }
            }
        }

        public void SetupStreamout(EmitterInstance emitter, ParticleShader shader, MeshType meshType)
        {
            int numInstances = 1024;

            var numDrawParticle = emitter.numParticles;
            BindParticleAttributeBlock(emitter.PtclAttributeBuffer, shader, numDrawParticle);

            BindVAO(meshType);

            //Disable as drawing is not intended
            GL.Enable(EnableCap.RasterizerDiscard);

            emitter.PositionStreamOut.Bind(shader.attrStreamOutPos, 0, true);
            emitter.VectorStreamOut.Bind(shader.attrStreamOutVec, 1, true);

            //Enter transform mode
            //Points per individual value
            GL.BeginTransformFeedback(TransformFeedbackPrimitiveType.Points);
            GL.DrawArraysInstanced(PrimitiveType.Points, 0, 1, numInstances);
            GL.EndTransformFeedback();

            GL.BindVertexArray(0);

            GL.Disable(EnableCap.RasterizerDiscard);
        }

        private void BindVAO(MeshType meshType)
        {
            if (meshType == MeshType.Primitive && PrimitiveRenderer != null)
                PrimitiveRenderer.Bind(Context);
            else
                QuadRender.Bind(Context);
        }

        public void EndRender()
        {
        }

        public void SwapDoubleBuffer()
        {
            doubleBuffer.Swap();
            stripeNumDrawVertex = 0;
        }

        public void ResetStripe()
        {
            stripeNumDrawVertex = 0;
        }

        public void FlushCache()
        {

        }

        class SortByZOrder : IComparer<PtclViewZ>
        {
            public int Compare(PtclViewZ a, PtclViewZ b)
            {
                if (a.ZOrder < 0.0f && b.ZOrder < 0.0f)
                {
                    if (a.ZOrder < b.ZOrder)
                        return -1;
                }
                else
                {
                    if (a.ZOrder > b.ZOrder)
                        return -1;
                }
                return 1;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EffectLibrary
{
    public class SwapBuffer
    {
        uint currentBufferIdx;
        uint bufferSize;
        uint bufferUsedSize;
        uint bufferFlushedSize;
        byte[][] buffer = new byte[2][];

        public void Initialize(Heap heap, uint size)
        {
            currentBufferIdx = 0;
            bufferSize = size;
            bufferUsedSize = 0;
            bufferFlushedSize = 0;
            buffer[0] = new byte[0x100];
            buffer[1] = new byte[0x100];
        }

        public void Swap()
        {
            currentBufferIdx = (uint)((currentBufferIdx == 0) ? 1 : 0);
            bufferUsedSize = 0;
            bufferFlushedSize = 0;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;

namespace EffectLibrary
{
    public partial class Renderer
    {
        public uint MakeStripeAttributeBlockCore(ref PtclStripe stripe, ref StripeVertexBuffer[] stripeVertexBuffer, uint firstVertex)
        {
            if (stripe == null || stripe.data == null)
                return 0;

            stripe.numDraw = 0;

            ComplexEmitterData cdata = stripe.data;
            StripeData stripeData  = cdata.StripeData;

            int histQueueCount = (int)stripe.queueCount;
            int sliceHistInterval = (int)stripeData.sliceHistInterval;

            int numSliceHistory = Math.Min(sliceHistInterval, histQueueCount);
            if (numSliceHistory < 3)
                return 0;

            uint numDrawStripe = 0;

            float invRatio = 1.0f / (float)(numSliceHistory - 2);
            float invTexRatio;

            if (stripeData.textureType == 1 && stripe.counter < sliceHistInterval)
                invTexRatio = 1.0f / (float)(sliceHistInterval - 2);
            else
                invTexRatio = invRatio;

            float alphaRange = stripeData.alphaEnd - stripeData.alphaStart;
            stripe.drawFirstVertex = firstVertex;

            for (int i = 0; i < numSliceHistory - 1; i++)
            {
                uint idx = firstVertex + numDrawStripe;
                StripeVertexBuffer buffer0 = stripeVertexBuffer[idx + 0];
                StripeVertexBuffer buffer1 = stripeVertexBuffer[idx + 1];

                float ratio = (float)i * invRatio;
                float texRatio = (float)i * invTexRatio;

                float v0 = ratio * (float)(histQueueCount - 2) + 0.5f;
                int v1 = (int)v0;

                int sliceHistIdx = (int)stripe.queueRear - v1 - 1;
                if (sliceHistIdx < 0)
                    sliceHistIdx += (int)stripeData.numSliceHistory;

                float alpha = (stripeData.alphaStart + alphaRange * ratio) * stripe.particle.alpha[0] * stripe.particle.emitter.emitterSet.color.W * stripe.particle.emitter.fadeAlpha;

                buffer0.pos.Xyz = stripe.queue[sliceHistIdx].pos;
                buffer0.pos.W = alpha * stripe.particle.emitter.AnimData[(AnimGroupType)14];
                buffer1.pos.Xyz = buffer0.pos.Xyz;
                buffer1.pos.W = buffer0.pos.W;

                buffer0.dir.Xyz = stripe.queue[sliceHistIdx].dir;
                buffer1.dir.Xyz = buffer0.dir.Xyz;

                buffer0.outer.Xyz = stripe.queue[sliceHistIdx].outer;
                buffer1.outer.Xyz = buffer0.outer.Xyz;

                buffer0.outer.W = stripe.queue[sliceHistIdx].scale;
                buffer1.outer.W = -stripe.queue[sliceHistIdx].scale;

                buffer0.texCoord.X = stripe.particle.texAnimParam[0].offset.X;
                buffer0.texCoord.Y = stripe.particle.texAnimParam[0].offset.Y + texRatio * stripe.data.TextureEmitters[0].UvScaleInit.Y;
                buffer1.texCoord.X = stripe.particle.texAnimParam[0].offset.X + stripe.data.TextureEmitters[0].UvScaleInit.X;
                buffer1.texCoord.Y = buffer0.texCoord.Y;

                buffer0.texCoord.Z = stripe.particle.texAnimParam[1].offset.X;
                buffer0.texCoord.W = stripe.particle.texAnimParam[1].offset.Y + texRatio * stripe.data.TextureEmitters[1].UvScaleInit.Y;
                buffer1.texCoord.Z = stripe.particle.texAnimParam[1].offset.X + stripe.data.TextureEmitters[1].UvScaleInit.X;
                buffer1.texCoord.W = buffer0.texCoord.W;

                stripeVertexBuffer[idx + 0] = buffer0;
                stripeVertexBuffer[idx + 1] = buffer1;

                numDrawStripe += 2;
            }

            stripe.numDraw = numDrawStripe;
            stripeNumDrawVertex += (int)numDrawStripe;

            return numDrawStripe;
        }

        public void GetPositionOnCubic(ref Vector3 result, Vector3 startPos, Vector3 startVel, Vector3 endPos, Vector3 endVel, float time)
        {
            Matrix4 hermite = new Matrix4(
                2.0f, -2.0f, 1.0f, 1.0f,
               -3.0f, 3.0f, -2.0f, -1.0f,
                0.0f, 0.0f, 1.0f, 0.0f,
                1.0f, 0.0f, 0.0f, 0.0f);

            Matrix4 mtx = new Matrix4(
                startPos.X, startPos.Y, startPos.Z, 0.0f,
                endPos.X, endPos.Y, endPos.Z, 0.0f,
                startVel.X, startVel.Y, startVel.Z, 0.0f,
                endVel.X, endVel.Y, endVel.Z, 1.0f);

            mtx = hermite * mtx;
            Vector3 timeVector = new Vector3(time * time * time, time * time, time);
            result = MatrixExtension.MultVec(timeVector, mtx);
        }

        public uint MakeStripeAttributeBlockCoreDivide(ref PtclStripe stripe, ref StripeVertexBuffer[] stripeVertexBuffer, uint firstVertex, int numDivisions)
        {
            if (stripe == null || stripe.data == null)
                return 0;

            stripe.numDraw = 0;

            ComplexEmitterData cdata = stripe.data;
            StripeData stripeData = cdata.StripeData;

            int histQueueCount = (int)stripe.queueCount;
            if (histQueueCount < 3)
                return 0;

            int sliceHistInterval = (int)stripeData.sliceHistInterval;

            int numSliceHistory = Math.Min(sliceHistInterval + (sliceHistInterval - 1) * numDivisions,
                                histQueueCount += (histQueueCount - 1) * numDivisions);

            if (numSliceHistory < 3)
                return 0;

            uint numDrawStripe = 0;

            float invRatio = 1.0f / (float)(numSliceHistory - 2);
            float invDivRatio = 1.0f / (float)(numDivisions + 1);
            float invTexRatio;

            if (stripeData.textureType == 1 && stripe.counter < sliceHistInterval)
                invTexRatio = 1.0f / (float)((sliceHistInterval - 2) + (sliceHistInterval - 1) * numDivisions);
            else
                invTexRatio = invRatio;

            float alphaRange = stripeData.alphaEnd - stripeData.alphaStart;
            stripe.drawFirstVertex = firstVertex;

            for (int i = 0; i < numSliceHistory - 1; i++)
            {
                uint idx = firstVertex + numDrawStripe;
                StripeVertexBuffer buffer0 = stripeVertexBuffer[idx + 0];
                StripeVertexBuffer buffer1 = stripeVertexBuffer[idx + 1];

                float ratio = (float)i * invRatio;
                float texRatio = (float)i * invTexRatio;
                float divRatio = ratio * invDivRatio;

                float v0 = divRatio * (float)(histQueueCount - 2);
                int v1 = (int)v0;

                int sliceHistIdx = (int)stripe.queueRear - v1 - 1;
                int nextSliceHistIdx = sliceHistIdx - 1;

                if (sliceHistIdx < 0)
                    sliceHistIdx += (int)stripeData.numSliceHistory;

                if (nextSliceHistIdx < 0)
                    nextSliceHistIdx += (int)stripeData.numSliceHistory;

                int prevSliceHistIdx = sliceHistIdx + 1;
                if (prevSliceHistIdx >= stripeData.numSliceHistory)
                    prevSliceHistIdx -= (int)stripeData.numSliceHistory;

                int nextSliceHist2Idx = nextSliceHistIdx - 1;
                if (nextSliceHist2Idx < 0)
                    nextSliceHist2Idx += (int)stripeData.numSliceHistory;

                float delta = v0 - (float)v1;

                uint idx0 = (uint)prevSliceHistIdx;
                uint idx1 = (uint)sliceHistIdx;
                uint idx2 = (uint)nextSliceHistIdx;
                uint idx3 = (uint)nextSliceHist2Idx;

                if (v1 == 0)
                {
                    idx0 = (uint)sliceHistIdx;
                    idx1 = (uint)nextSliceHistIdx;
                }

                if (v1 >= stripe.queueCount - 2)
                {
                    idx2 = (uint)sliceHistIdx;
                    idx3 = (uint)nextSliceHistIdx;
                }

                Vector3 diff0 = stripe.queue[idx0].pos - stripe.queue[idx1].pos;
                Vector3 diff1 = stripe.queue[idx2].pos - stripe.queue[idx1].pos;
                Vector3 diff2 = stripe.queue[idx1].pos - stripe.queue[idx2].pos;
                Vector3 diff3 = stripe.queue[idx3].pos - stripe.queue[idx2].pos;

                Vector3 startVel = (diff1 - diff0) * 0.5f;
                Vector3 endVel = (diff3 - diff2) * 0.5f;

                Vector3 pos = Vector3.Zero;
                GetPositionOnCubic(ref pos, stripe.queue[sliceHistIdx].pos, startVel, stripe.queue[nextSliceHistIdx].pos, endVel, delta);

                float alpha = (stripeData.alphaStart + alphaRange * ratio) * stripe.particle.alpha[0] * stripe.particle.emitter.emitterSet.color.W * stripe.particle.emitter.fadeAlpha;

                buffer0.pos.Xyz = pos;
                buffer0.pos.W = alpha * stripe.particle.emitter.AnimData[(AnimGroupType)14];
                buffer1.pos.Xyz = pos;
                buffer1.pos.W = buffer0.pos.W;

                float invDelta = 1.0f - delta;

                buffer0.outer.Xyz = stripe.queue[sliceHistIdx].outer * invDelta + stripe.queue[nextSliceHistIdx].outer * delta;
                buffer0.outer.W = stripe.queue[sliceHistIdx].scale;
                buffer1.outer.Xyz = buffer0.outer.Xyz;
                buffer1.outer.W = -stripe.queue[sliceHistIdx].scale;

                buffer0.dir.Xyz = stripe.queue[sliceHistIdx].dir * invDelta + stripe.queue[nextSliceHistIdx].dir * delta;
                buffer1.dir.Xyz = buffer0.dir.Xyz;

                buffer0.texCoord.X = stripe.particle.texAnimParam[0].offset.X;
                buffer0.texCoord.Y = stripe.particle.texAnimParam[0].offset.Y + texRatio * stripe.data.TextureEmitters[0].UvScaleInit.Y;
                buffer1.texCoord.X = stripe.particle.texAnimParam[0].offset.X + stripe.data.TextureEmitters[0].UvScaleInit.X;
                buffer1.texCoord.Y = buffer0.texCoord.Y;

                buffer0.texCoord.Z = stripe.particle.texAnimParam[1].offset.X;
                buffer0.texCoord.W = stripe.particle.texAnimParam[1].offset.Y + texRatio * stripe.data.TextureEmitters[1].UvScaleInit.Y;
                buffer1.texCoord.Z = stripe.particle.texAnimParam[1].offset.X + stripe.data.TextureEmitters[1].UvScaleInit.X;
                buffer1.texCoord.W = buffer0.texCoord.W;

                stripeVertexBuffer[idx + 0] = buffer0;
                stripeVertexBuffer[idx + 1] = buffer1;

                numDrawStripe += 2;
            }

            stripe.numDraw = numDrawStripe;
            stripeNumDrawVertex += (int)numDrawStripe;

            return numDrawStripe;
        }

        public bool MakeStripeAttributeBlock(EmitterInstance emitter)
        {
            PtclInstance ptclFirst = emitter.particleHead;

            if (ptclFirst == null || ptclFirst.stripe == null || ptclFirst.stripe.data == null)
                return false;

            ComplexEmitterData cdata = ptclFirst.stripe.data;
            StripeData stripeData = cdata.StripeData;
            uint numDivisions = stripeData.numDivisions;

            uint numVertex = 0;
            uint numDrawStripe = 0;

            for (PtclInstance ptcl = ptclFirst; ptcl != null; ptcl = ptcl.Next)
            {
                if (ptcl.lifespan == 0)
                    continue;

                PtclStripe stripe = ptcl.stripe;
                if (stripe == null || stripe.data == null)
                    continue;

                uint numSliceHistory = Math.Min(stripeData.sliceHistInterval + (stripeData.sliceHistInterval - 1) * numDivisions,
                                                stripe.queueCount + (stripe.queueCount - 1) * numDivisions);
                if (numSliceHistory <= 1)
                    continue;

                numVertex += (numSliceHistory - 1) * 2;
            }

            if (numVertex == 0)
                return false;

            emitter.StripeVertexBuffer = new StripeVertexBuffer[numVertex];
            if (numDivisions == 0)
            {
                for (PtclInstance ptcl = ptclFirst; ptcl != null; ptcl = ptcl.Next)
                {
                    if (ptcl.lifespan == 0)
                        continue;

                    numDrawStripe += MakeStripeAttributeBlockCore(ref ptcl.stripe, ref emitter.StripeVertexBuffer, numDrawStripe);
                }
            }
            else
            {
                for (PtclInstance ptcl = ptclFirst; ptcl != null; ptcl = ptcl.Next)
                {
                    if (ptcl.lifespan == 0)
                        continue;

                    numDrawStripe += MakeStripeAttributeBlockCoreDivide(ref ptcl.stripe, ref emitter.StripeVertexBuffer, numDrawStripe, (int)numDivisions);
                }
            }

            emitter.numDrawStripe = numDrawStripe;
            return true;
        }

        public bool ConnectionStripeUvScaleCalc(ref float invTexRatio, ref float texRatioSub, EmitterInstance emitter, int numParticles, float invRatio, int connectionType)
        {
            ComplexEmitterData cdata = (ComplexEmitterData)emitter.particleHead.data;
            StripeData stripeData = cdata.StripeData;

            texRatioSub = 0.0f;

            if (!(stripeData.textureType == 1 && emitter.counter < emitter.data.MaxLifespan))
            {
                invTexRatio = invRatio;
                return false;
            }

            int emissionDuration, numEmit;
            if (emitter.data.EmissionEndFrame != 0x7FFFFFFF && (emitter.data.EmissionEndFrame - emitter.data.EmissionStartFrame) < emitter.data.MaxLifespan)
            {
                emissionDuration = (int)(emitter.data.EmissionEndFrame - emitter.data.EmissionStartFrame + emitter.data.EmissionInterval);
                numEmit = (int)(emissionDuration / (emitter.data.EmissionInterval + 1)) * (int)emitter.data.EmissionRatio;
            }
            else
            {
                emissionDuration = (int)emitter.data.MaxLifespan;
                numEmit = (int)(emissionDuration / (emitter.data.EmissionInterval + 1) + 1) * (int)emitter.data.EmissionRatio;
            }

            if (connectionType != 0)
            {
                numEmit++;
                numParticles++;
            }

            invTexRatio = 1.0f / (float)(numEmit - 1);
            texRatioSub = 1.0f - invTexRatio * (numParticles - 1);

            return true;
        }

        public uint MakeConnectionStripeAttributeBlockCore(EmitterInstance emitter, int numParticles,
            PtclInstance ptclLast, PtclInstance ptclBeforeLast, int connectionType,
            StripeVertexBuffer[] stripeVertexBuffer, uint firstVertex)
        {
            ComplexEmitterData cdata = (ComplexEmitterData)emitter.particleHead.data;
            StripeData stripeData = cdata.StripeData;

            bool edgeConnected = false;
            int numSliceHistory = numParticles;

            if (connectionType == 1 || connectionType == 2)
            {
                edgeConnected = true;
                numSliceHistory++;
            }

            uint numDrawStripe = 0;

            float invRatio = 1.0f / (float)(numSliceHistory - 1);
            float invTexRatio = 0, texRatioSub = 0;
            ConnectionStripeUvScaleCalc(ref invTexRatio, ref texRatioSub, emitter, numParticles, invRatio, connectionType);

            float alphaRange = stripeData.alphaEnd - stripeData.alphaStart;

            PtclInstance ptclFirst = emitter.particleHead;
            PtclInstance ptcl = ptclFirst;
            PtclInstance ptcl_next = ptclFirst.Next;

            Vector3 currentSliceDir = Vector3.Zero;
            Vector3 currPos = Vector3.Zero, nextPos = Vector3.Zero;

            for (int i = 0; i < numSliceHistory; i++)
            {
                uint idx = firstVertex + numDrawStripe;
                StripeVertexBuffer buffer0 = stripeVertexBuffer[idx + 0];
                StripeVertexBuffer buffer1 = stripeVertexBuffer[idx + 1];

                float ratio = (float)i * invRatio;
                float texRatio = 1.0f - ((float)i * invTexRatio) - texRatioSub;

                float v0 = ratio * (float)(numSliceHistory - 1);
                int v1 = (int)(v0 + 0.5f);

                float delta = v0 - (float)v1;
                if (delta < 0.0f)
                    delta = 0.0f;

                if (connectionType == 1 && i == 0)
                {
                    currPos = ptclLast.worldPos;
                    nextPos = ptcl.worldPos;
                }
                else if (connectionType == 1 && ptcl == ptclLast)
                {
                    currPos = ptclLast.worldPos;
                    nextPos = ptclFirst.worldPos;
                }
                else if (connectionType == 2 && i == 0)
                {
                    currPos.X = emitter.matrixSRT[3, 0];
                    currPos.Y = emitter.matrixSRT[3, 1];
                    currPos.Z = emitter.matrixSRT[3, 2];
                    nextPos = ptcl.worldPos;
                }
                else if (ptcl_next != null)
                {
                    nextPos = ptcl_next.worldPos;
                    currPos = ptcl.worldPos;
                }
                else
                {
                   Vector3 posDiff = nextPos - currPos;
                    nextPos = nextPos + posDiff;
                    currPos = ptcl.worldPos;
                }

                Vector3 pos = nextPos - currPos;
                pos *= delta;
                pos = currPos + pos;

                Vector3 dir = nextPos - currPos;

                float alpha = (stripeData.alphaStart + alphaRange * ratio) * ptcl.alpha[0];

                if (stripeData.dirInterpolation != 1.0f)
                {
                    if (i == 0)
                        currentSliceDir = dir;

                    else
                    {
                        Vector3 diff = dir - currentSliceDir;
                        diff *= stripeData.dirInterpolation;
                        currentSliceDir += diff;

                        if (currentSliceDir.Length > 0.0f)
                            currentSliceDir.Normalize();

                        dir = currentSliceDir;
                    }
                }

                buffer0.pos.Xyz = pos;
                buffer0.pos.W = alpha * ptcl.emitter.AnimData[(AnimGroupType)14] * ptcl.emitter.emitterSet.color.W;
                buffer1.pos.Xyz = pos;
                buffer1.pos.W = buffer0.pos.W;

                buffer0.dir = new Vector4(dir, 0.0f);
                buffer1.dir = new Vector4(dir, 0.0f);

                if (stripeData.type == 0)
                {
                    buffer0.outer.Xyz = eyeVec;
                    buffer1.outer.Xyz = eyeVec;

                    buffer0.outer.W = ptcl.scale.X * emitter.emitterSet.ptclEffectiveScale.X;
                    buffer1.outer.W = -buffer0.outer.W;
                }
                else if (stripeData.type == 2)
                {
                    Vector3 outer;
                    if (emitter.ptclFollowType == PtclFollowType.SRT)
                    {
                        outer.X = emitter.matrixSRT[1, 0];
                        outer.Y = emitter.matrixSRT[1, 1];
                        outer.Z = emitter.matrixSRT[1, 2];
                    }
                    else
                    {
                        outer.X = ptcl.matrixSRT[1, 0];
                        outer.Y = ptcl.matrixSRT[1, 1];
                        outer.Z = ptcl.matrixSRT[1, 2];
                    }

                    buffer0.outer.Xyz = outer * ptcl.scale.X;
                    buffer1.outer.Xyz = outer * ptcl.scale.X;

                    buffer0.outer.W = ptcl.scale.X * emitter.emitterSet.ptclEffectiveScale.X;
                    buffer1.outer.W = -buffer0.outer.W;
                }
                else if (stripeData.type == 1)
                {
                    Vector3 outer;
                    if (emitter.ptclFollowType == PtclFollowType.SRT)
                    {
                        outer.X = emitter.matrixSRT[1, 0];
                        outer.Y = emitter.matrixSRT[1, 1];
                        outer.Z = emitter.matrixSRT[1, 2];
                    }
                    else
                    {
                        outer.X = ptcl.matrixSRT[1, 0];
                        outer.Y = ptcl.matrixSRT[1, 1];
                        outer.Z = ptcl.matrixSRT[1, 2];
                    }

                    outer = Vector3.Cross(outer, dir);
                    if (outer.Length > 0.0f)
                        outer.Normalize();

                    buffer0.outer.Xyz = outer;
                    buffer1.outer.Xyz = outer;

                    buffer0.outer.W = ptcl.scale.X * emitter.emitterSet.ptclEffectiveScale.X;
                    buffer1.outer.W = -buffer0.outer.W;
                }

                buffer0.texCoord.X = emitter.data.TextureEmitters[0].UvScaleInit.X;
                buffer0.texCoord.Y = 1.0f - texRatio * emitter.data.TextureEmitters[0].UvScaleInit.Y;
                buffer1.texCoord.X = 0.0f;
                buffer1.texCoord.Y = 1.0f - texRatio * emitter.data.TextureEmitters[0].UvScaleInit.Y;

                buffer0.texCoord.Z = emitter.data.TextureEmitters[1].UvScaleInit.X;
                buffer0.texCoord.W = 1.0f - texRatio * emitter.data.TextureEmitters[1].UvScaleInit.Y;
                buffer1.texCoord.Z = 0.0f;
                buffer1.texCoord.W = 1.0f - texRatio * emitter.data.TextureEmitters[1].UvScaleInit.Y;

                stripeVertexBuffer[idx + 0] = buffer0;
                stripeVertexBuffer[idx + 1] = buffer1;

                numDrawStripe += 2;

                if (!(edgeConnected && i == 0) && ptcl_next != null)
                {
                    ptcl = ptcl_next;
                    ptcl_next = ptcl.Next;
                }
            }

            stripeNumDrawVertex += (int)numDrawStripe;
            return numDrawStripe;
        }

        public uint MakeConnectionStripeAttributeBlockCoreDivide(EmitterInstance emitter, int numParticles,
            PtclInstance ptclLast, PtclInstance ptclBeforeLast, int connectionType,
            StripeVertexBuffer[] stripeVertexBuffer, uint firstVertex)
        {
            ComplexEmitterData cdata = (ComplexEmitterData)emitter.particleHead.data;
            StripeData stripeData = cdata.StripeData;

            int numDivisions = (int)stripeData.numDivisions;
            int numVertex = numParticles + (numParticles - 1) * numDivisions;

            if (numParticles < 2)
                return 0;

            int numSliceHistory = numParticles;
            if (connectionType == 1 || connectionType == 2)
            {
                numSliceHistory++;
                numVertex += numDivisions;
            }

            uint numDrawStripe = 0;

            float invRatio = 1.0f / (float)(numVertex - 1);
            float invTexRatio = 0, texRatioSub = 0;
            if (ConnectionStripeUvScaleCalc(ref invTexRatio, ref texRatioSub, emitter, numParticles, invRatio, connectionType))
                invTexRatio /= numDivisions + 1;

            float alphaRange = stripeData.alphaEnd - stripeData.alphaStart;

            PtclInstance ptclFirst = emitter.particleHead;
            PtclInstance ptcl_prev = ptclFirst.Prev;
            PtclInstance ptcl = ptclFirst;
            PtclInstance ptcl_next = ptclFirst.Next;
            PtclInstance ptcl_next2 = null;
            if (ptcl_next != null)
                ptcl_next2 = ptcl_next.Next;

            float invDivRatioStep = 1.0f / (float)(numDivisions + 1);
            int currentSliceIdx = 0;

            Vector3 dir = Vector3.Zero, dirInit = Vector3.Zero, currentSliceDir = Vector3.Zero;
            Vector3 currPos = Vector3.Zero, nextPos = Vector3.Zero, prevPos = Vector3.Zero, next2Pos = Vector3.Zero;
            Vector3 vtxPos = Vector3.Zero, prevVtxPos = Vector3.Zero;

            for (int i = 0; i < numSliceHistory; i++)
            {
                if (connectionType == 1 && i == 0)
                {
                    currPos = ptclLast.worldPos;
                    nextPos = ptcl.worldPos;

                    ptcl_prev = ptclBeforeLast;
                    ptcl = ptclLast;
                    ptcl_next = ptclFirst;
                    ptcl_next2 = ptclFirst.Next;
                }
                else if (connectionType == 1 && ptcl == ptclLast)
                {
                    currPos = ptclLast.worldPos;
                    nextPos = ptclFirst.worldPos;
                }
                else if (connectionType == 2 && i == 0)
                {
                    currPos.X = emitter.matrixSRT[3, 0];
                    currPos.Y = emitter.matrixSRT[3, 1];
                    currPos.Z = emitter.matrixSRT[3, 2];
                    nextPos = ptcl.worldPos;

                    ptcl_prev = null;
                    ptcl = ptclFirst;
                    ptcl_next = ptclFirst;
                    ptcl_next2 = ptclFirst.Next;
                }
                else if (ptcl_next != null)
                {
                    nextPos = ptcl_next.worldPos;
                    currPos = ptcl.worldPos;
                }
                else
                {
                    Vector3 posDiff = nextPos - currPos;
                    nextPos += posDiff;
                    currPos = ptcl.worldPos;
                }

                if (ptcl_prev != null)
                    prevPos = ptcl_prev.worldPos;

                else
                {
                    Vector3 posDiff = currPos - nextPos;
                    prevPos = currPos + posDiff;
                }

                if (ptcl_next2 != null)
                    next2Pos = ptcl_next2.worldPos;

                else if (connectionType == 1 && i == numSliceHistory - 2)
                    next2Pos = ptclFirst.worldPos;

                else
                {
                    Vector3 posDiff = nextPos - currPos;
                    next2Pos += posDiff;
                }

                Vector3 diff0 = prevPos - currPos;
                Vector3 diff1 = nextPos - currPos;

                Vector3 startVel = diff1 - diff0;
                startVel *= 0.5f;

                Vector3 diff2 = currPos - nextPos;
                Vector3 diff3 = next2Pos - nextPos;

                Vector3 endVel = diff3 - diff2;
                endVel *= 0.5f;

                bool notCubic = diff2.Length < ptcl.scale.X * 0.25f;

                float invDivRatio = 0.0f;

                for (int j = 0; j < numDivisions + 1; j++, invDivRatio += invDivRatioStep)
                {
                    float divRatio = 1.0f - invDivRatio;
                    if (i == numSliceHistory - 1 && j != 0)
                        break;

                    uint idx = firstVertex + numDrawStripe;
                    StripeVertexBuffer buffer0 = stripeVertexBuffer[idx + 0];
                    StripeVertexBuffer buffer1 = stripeVertexBuffer[idx + 1];

                    prevVtxPos = vtxPos;

                    if (notCubic)
                    {
                        Vector3 pos0 = currPos * divRatio;
                        Vector3 pos1 = nextPos * invDivRatio;
                        vtxPos = pos0 + pos1;
                    }
                    else
                    {
                        GetPositionOnCubic(ref vtxPos, currPos, startVel, nextPos, endVel, invDivRatio);
                    }

                    if (i == 0 && j == 0)
                        dirInit = (dir = startVel);
                    else if (connectionType == 1 && i == numSliceHistory - 1)
                        dir = dirInit;
                    else
                        dir = vtxPos - prevVtxPos;

                    if (dir.Length > 0.0f)
                        dir.Normalize();

                    if (stripeData.dirInterpolation != 1.0f)
                    {
                        if (i == 0)
                            currentSliceDir = dir;

                        else
                        {
                            Vector3 diff = dir - currentSliceDir;
                            diff *= stripeData.dirInterpolation;
                            currentSliceDir += diff;

                            if (currentSliceDir.Length > 0.0f)
                                currentSliceDir.Normalize();

                            dir = currentSliceDir;
                        }
                    }

                    float ratio = (float)currentSliceIdx * invRatio;
                    float texRatio = 1.0f - ((float)currentSliceIdx * invTexRatio) - texRatioSub;

                    float alpha = (stripeData.alphaStart + alphaRange * ratio) * ptcl.alpha[0];

                    buffer0.pos.Xyz = vtxPos;
                    buffer0.pos.W = alpha * ptcl.emitter.AnimData[(AnimGroupType)14] * ptcl.emitter.emitterSet.color.W;
                    buffer1.pos.Xyz = vtxPos;
                    buffer1.pos.W = buffer0.pos.W;

                    buffer0.dir = new Vector4(dir, 0.0f);
                    buffer1.dir = new Vector4(dir, 0.0f);

                    PtclInstance _ptcl = ptcl_next;
                    if (_ptcl == null)
                        _ptcl = ptcl;

                    if (stripeData.type == 0)
                    {
                        buffer0.outer.Xyz = eyeVec;
                        buffer1.outer.Xyz = eyeVec;

                        buffer0.outer.W = ptcl.scale.X * emitter.emitterSet.ptclEffectiveScale.X * divRatio + _ptcl.scale.X * emitter.emitterSet.ptclEffectiveScale.X * invDivRatio;
                        buffer1.outer.W = -buffer0.outer.W;
                    }
                    else if (stripeData.type == 2)
                    {
                        Vector3 outer;
                        if (emitter.ptclFollowType == PtclFollowType.SRT)
                        {
                            outer.X = emitter.matrixSRT[1, 0];
                            outer.Y = emitter.matrixSRT[1, 1];
                            outer.Z = emitter.matrixSRT[1, 2];
                        }
                        else
                        {
                            outer.X = ptcl.matrixSRT[1, 0];
                            outer.Y = ptcl.matrixSRT[1, 1];
                            outer.Z = ptcl.matrixSRT[1, 2];
                        }

                        buffer0.outer.Xyz = outer * ptcl.scale.X;
                        buffer1.outer.Xyz = outer * ptcl.scale.X;

                        buffer0.outer.W = ptcl.scale.X * emitter.emitterSet.ptclEffectiveScale.X * divRatio + _ptcl.scale.X * emitter.emitterSet.ptclEffectiveScale.X * invDivRatio;
                        buffer1.outer.W = -buffer0.outer.W;
                    }
                    else if (stripeData.type == 1)
                    {
                       Vector3 outer;
                        if (emitter.ptclFollowType == PtclFollowType.SRT)
                        {
                            outer.X = emitter.matrixSRT[1, 0];
                            outer.Y = emitter.matrixSRT[1, 1];
                            outer.Z = emitter.matrixSRT[1, 2];
                        }
                        else
                        {
                            outer.X = ptcl.matrixSRT[1, 0];
                            outer.Y = ptcl.matrixSRT[1, 1];
                            outer.Z = ptcl.matrixSRT[1, 2];
                        }

                        outer = Vector3.Cross(outer, dir);
                        if (outer.Length > 0.0f)
                            outer.Normalize();

                        buffer0.outer.Xyz = outer;
                        buffer1.outer.Xyz = outer;

                        buffer0.outer.W = ptcl.scale.X * emitter.emitterSet.ptclEffectiveScale.X * divRatio + _ptcl.scale.X * emitter.emitterSet.ptclEffectiveScale.X * invDivRatio;
                        buffer1.outer.W = -buffer0.outer.W;
                    }

                    buffer0.texCoord.X = 0.0f;
                    buffer0.texCoord.Y = 1.0f - texRatio * emitter.data.TextureEmitters[0].UvScaleInit.Y;
                    buffer1.texCoord.X = emitter.data.TextureEmitters[0].UvScaleInit.X;
                    buffer1.texCoord.Y = 1.0f - texRatio * emitter.data.TextureEmitters[0].UvScaleInit.Y;

                    buffer1.texCoord.Z = 0.0f;
                    buffer0.texCoord.W = 1.0f - texRatio * emitter.data.TextureEmitters[1].UvScaleInit.Y;
                    buffer0.texCoord.Z = emitter.data.TextureEmitters[1].UvScaleInit.X;
                    buffer1.texCoord.W = 1.0f - texRatio * emitter.data.TextureEmitters[1].UvScaleInit.Y;

                    numDrawStripe += 2;
                    currentSliceIdx++;
                }

                ptcl_prev = ptcl;
                ptcl = ptcl_next;
                ptcl_next = ptcl_next2;

                if (ptcl_next != null)
                    ptcl_next2 = ptcl_next.Next;

                if (connectionType == 1 && ptcl_next2 == null)
                    ptcl_next2 = ptclFirst;
            }

            stripeNumDrawVertex += (int)numDrawStripe;
            return numDrawStripe;
        }

        public StripeVertexBuffer[] MakeConnectionStripeAttributeBlock(EmitterInstance emitter, bool flushCache)
        {
            uint numParticles = emitter.numParticles;
            if (numParticles == 0)
                return null;

            PtclInstance ptclFirst = emitter.particleHead;
            if (ptclFirst == null)
                return null;

            ComplexEmitterData cdata = (ComplexEmitterData)ptclFirst.data;
            StripeData stripeData = cdata.StripeData;

            uint numDivisions = stripeData.numDivisions;
            uint connectionType = stripeData.connectionType;

            PtclInstance ptclLast = emitter.particleTail;
            PtclInstance ptclBeforeLast = ptclLast.Prev;

            uint numVertex = numParticles * 2 + ((numParticles - 1) * 2) * numDivisions;
            if (connectionType == 1 || connectionType == 2)
                numVertex += 2 + numDivisions * 2;

            if (numVertex == 0)
                return null;

            StripeVertexBuffer[] stripeVertexBuffer =new StripeVertexBuffer[numVertex];

            if (numDivisions == 0)
                emitter.numDrawStripe = MakeConnectionStripeAttributeBlockCore(emitter, (int)numParticles, ptclLast, ptclBeforeLast, (int)connectionType, stripeVertexBuffer, 0);
            else
                emitter.numDrawStripe = MakeConnectionStripeAttributeBlockCoreDivide(emitter, (int)numParticles, ptclLast, ptclBeforeLast, (int)connectionType, stripeVertexBuffer, 0);

            return stripeVertexBuffer;
        }



        public bool SetupStripeDrawSetting(EmitterInstance emitter, bool flushCache, object argData)
        {
            if (emitter.shader == null)
                return false;

            SimpleEmitterData data = emitter.data;

            ParticleShader shader = emitter.shader[(int)ShaderType];
            if (shader == null)
                return false;

            if (!shader.Initialized)
                shader.LoadShader();

            if (shader.ShaderInfo == null)
                return false;

            Context.CurrentShader = shader.ShaderInfo.Program;

            //Bind the view block
            ViewUniformBlock viewUniformBlock = this.viewUniformBlock;
            shader.BindViewUniformBlock(viewUniformBlock, system);

            //Bind the static block
            EmitterStaticUniformBlock emitterStaticUniformBlock = emitter.StaticUniformBlock;
            shader.BindStaticUniformBlock(emitterStaticUniformBlock, system);

            RenderContext.SetZBufATest(data.ZbufferTestType);
            RenderContext.SetBlendType(data.BlendType);
            RenderContext.RenderBlendState();
            RenderContext.RenderPolygonState(data.DisplaySideType);

            var glShader = shader.ShaderInfo.Program;
            CafeShaderDecoder.SetShaderConstants(glShader, glShader.program, this.RenderContext.BlendState);

            for (int i = 0; i < data.Textures.Length; i++)
            {
                if (shader.FragmentSamplerLocations[i] != -1)
                    RenderContext.SetupTexture(shader, data.Textures[i], TextureSlot._0 + i, shader.FragmentSamplerLocations[i]);
            }

            if (shader.FragmentShaderKey.FragmentSoftEdge != 0 && data.Textures[0] != null)
                RenderContext.SetupTexture(shader, data.Textures[0], TextureSlot._3, shader.FragmentSamplerLocations2[0]);
            if (shader.FragmentShaderKey.FragmentShaderMode == 1 && data.Textures[1] != null)
                RenderContext.SetupTexture(shader, data.Textures[1], TextureSlot._3, shader.FragmentSamplerLocations2[1]);

            var callback = system.GetCustomShaderRenderStateSetCallback(data.ShaderUserSetting);
            if (callback != null)
            {
                RenderStateSetArg arg = new RenderStateSetArg()
                {
                    emitter = emitter,
                    renderer = this,
                    flushCache = flushCache,
                    argData = argData,
                };
                callback(arg);
            }

            return true;
        }

        public void EntryConnectionStripe(EmitterInstance emitter, bool flushCache, object argData)
        {
            ParticleShader shader = emitter.shader[(int)ShaderType];
            if (shader == null)
                return;

            PtclInstance ptcl = emitter.particleHead;
            if (ptcl == null)
                return;

            currentParticleType = PtclType.Complex;

            StripeVertexBuffer[] stripeVertexBuffer = MakeConnectionStripeAttributeBlock(emitter, flushCache);
            if (stripeVertexBuffer == null || emitter.numDrawStripe < 4)
                return;

            currentParticleType = PtclType.Complex;

            if (!SetupStripeDrawSetting(emitter, flushCache, argData))
                return;

            StripeRender.UpdateStripeVertexData(stripeVertexBuffer);

            Vector3 emitterSetColor = emitter.emitterSet.color.Xyz * emitter.data.ColorScale;
            Vector3 emitterColor0 = emitterSetColor * new Vector3(
                emitter.AnimData[AnimGroupType.Color0R],
                emitter.AnimData[AnimGroupType.Color0G],
                emitter.AnimData[AnimGroupType.Color0B]);
            Vector3 emitterColor1 = emitterSetColor * new Vector3(
                emitter.AnimData[AnimGroupType.Color1R],
                emitter.AnimData[AnimGroupType.Color1G],
                emitter.AnimData[AnimGroupType.Color1B]);

            ComplexEmitterData cdata = (ComplexEmitterData)emitter.data;
            StripeData stripeData = cdata.StripeData;
            uint numDrawStripe = emitter.numDrawStripe;

            Vector3 ptclColor0 = ptcl.color[0].Xyz * emitterColor0;
            Vector3 ptclColor1 = ptcl.color[1].Xyz * emitterColor1;

            {
                StripeUniformBlock uniformBlock = new StripeUniformBlock();
                uniformBlock.stParam.X = 1.0f;
                uniformBlock.stParam.Y = 0;
                uniformBlock.stParam.Z = emitter.data.CameraOffset;
                uniformBlock.stParam.W = 1.0f;

                uniformBlock.uvScrollAnim.X = emitter.data.TextureEmitters[0].Translate.X + emitter.counter * + emitter.data.TextureEmitters[0].TranslateShift.X;
                uniformBlock.uvScrollAnim.Y = emitter.data.TextureEmitters[0].Translate.Y - emitter.counter * emitter.data.TextureEmitters[0].TranslateShift.Y;
                uniformBlock.uvScrollAnim.Z = emitter.data.TextureEmitters[1].Translate.X + emitter.counter * emitter.data.TextureEmitters[1].TranslateShift.X;
                uniformBlock.uvScrollAnim.W = emitter.data.TextureEmitters[1].Translate.Y - emitter.counter * emitter.data.TextureEmitters[1].TranslateShift.Y;

                if (emitter.data.TextureEmitters.Length > 2)
                {
                    uniformBlock.uvScrollAnim2.X = emitter.data.TextureEmitters[2].Translate.X + emitter.data.TextureEmitters[2].TranslateShift.X;
                    uniformBlock.uvScrollAnim2.Y = emitter.data.TextureEmitters[2].Translate.Y - emitter.data.TextureEmitters[2].TranslateShift.Y;
                }

                uniformBlock.uvScaleRotateAnim0.X = emitter.data.TextureEmitters[0].Scale.X + emitter.counter * emitter.data.TextureEmitters[0].ScaleShift.X;
                uniformBlock.uvScaleRotateAnim0.Y = emitter.data.TextureEmitters[0].Scale.Y + emitter.counter * emitter.data.TextureEmitters[0].ScaleShift.Y;
                uniformBlock.uvScaleRotateAnim0.Z = emitter.counter * emitter.data.TextureEmitters[0].RotateShift;
                uniformBlock.uvScaleRotateAnim0.W = 0.0f;

                uniformBlock.uvScaleRotateAnim1.X = emitter.data.TextureEmitters[1].Scale.X + emitter.counter * emitter.data.TextureEmitters[1].ScaleShift.X;
                uniformBlock.uvScaleRotateAnim1.Y = emitter.data.TextureEmitters[1].Scale.Y + emitter.counter * emitter.data.TextureEmitters[1].ScaleShift.Y;
                uniformBlock.uvScaleRotateAnim1.Z = emitter.counter * emitter.data.TextureEmitters[1].RotateShift;
                uniformBlock.uvScaleRotateAnim1.W = 0.0f;

                if (emitter.data.TextureEmitters.Length > 2)
                {
                    uniformBlock.uvScaleRotateAnim2.X = emitter.data.TextureEmitters[2].Scale.X + emitter.counter * emitter.data.TextureEmitters[2].ScaleShift.X;
                    uniformBlock.uvScaleRotateAnim2.Y = emitter.data.TextureEmitters[2].Scale.Y + emitter.counter * emitter.data.TextureEmitters[2].ScaleShift.Y;
                    uniformBlock.uvScaleRotateAnim2.Z = emitter.counter * emitter.data.TextureEmitters[2].RotateShift;
                    uniformBlock.uvScaleRotateAnim2.W = 0.0f;
                }

                uniformBlock.vtxColor0 = new Vector4(ptclColor0, ptcl.emitter.fadeAlpha);
                uniformBlock.vtxColor1 = new Vector4(ptclColor1, ptcl.emitter.fadeAlpha);

                uniformBlock.emitterMat.Column0 = ptcl.emitter.matrixSRT.Column0;
                uniformBlock.emitterMat.Column1 = ptcl.emitter.matrixSRT.Column1;
                uniformBlock.emitterMat.Column2 = ptcl.emitter.matrixSRT.Column2;
                uniformBlock.emitterMat.Column3 = ptcl.emitter.matrixSRT.Column3;

                shader.BindStripeUniformBlock(uniformBlock, system);

                StripeRender.Draw(Context, (int)numDrawStripe);
            }

            if (stripeData.crossType == 1)
            {
                StripeUniformBlock uniformBlock = new StripeUniformBlock();
                uniformBlock.stParam.X = 0;
                uniformBlock.stParam.Y = 1.0f;
                uniformBlock.stParam.Z = emitter.data.CameraOffset;
                uniformBlock.stParam.W = 1.0f;

                uniformBlock.uvScrollAnim.X = emitter.data.TextureEmitters[0].Translate.X + emitter.counter * +emitter.data.TextureEmitters[0].TranslateShift.X;
                uniformBlock.uvScrollAnim.Y = emitter.data.TextureEmitters[0].Translate.Y - emitter.counter * emitter.data.TextureEmitters[0].TranslateShift.Y;
                uniformBlock.uvScrollAnim.Z = emitter.data.TextureEmitters[1].Translate.X + emitter.counter * emitter.data.TextureEmitters[1].TranslateShift.X;
                uniformBlock.uvScrollAnim.W = emitter.data.TextureEmitters[1].Translate.Y - emitter.counter * emitter.data.TextureEmitters[1].TranslateShift.Y;

                if (emitter.data.TextureEmitters.Length > 2)
                {
                    uniformBlock.uvScrollAnim2.X = emitter.data.TextureEmitters[2].Translate.X + emitter.data.TextureEmitters[2].TranslateShift.X;
                    uniformBlock.uvScrollAnim2.Y = emitter.data.TextureEmitters[2].Translate.Y - emitter.data.TextureEmitters[2].TranslateShift.Y;
                }

                uniformBlock.uvScaleRotateAnim0.X = emitter.data.TextureEmitters[0].Scale.X + emitter.counter * emitter.data.TextureEmitters[0].ScaleShift.X;
                uniformBlock.uvScaleRotateAnim0.Y = emitter.data.TextureEmitters[0].Scale.Y + emitter.counter * emitter.data.TextureEmitters[0].ScaleShift.Y;
                uniformBlock.uvScaleRotateAnim0.Z = emitter.counter * emitter.data.TextureEmitters[0].RotateShift;
                uniformBlock.uvScaleRotateAnim0.W = 0.0f;

                uniformBlock.uvScaleRotateAnim1.X = emitter.data.TextureEmitters[1].Scale.X + emitter.counter * emitter.data.TextureEmitters[1].ScaleShift.X;
                uniformBlock.uvScaleRotateAnim1.Y = emitter.data.TextureEmitters[1].Scale.Y + emitter.counter * emitter.data.TextureEmitters[1].ScaleShift.Y;
                uniformBlock.uvScaleRotateAnim1.Z = emitter.counter * emitter.data.TextureEmitters[1].RotateShift;
                uniformBlock.uvScaleRotateAnim1.W = 0.0f;

                if (emitter.data.TextureEmitters.Length > 2)
                {
                    uniformBlock.uvScaleRotateAnim2.X = emitter.data.TextureEmitters[2].Scale.X + emitter.counter * emitter.data.TextureEmitters[2].ScaleShift.X;
                    uniformBlock.uvScaleRotateAnim2.Y = emitter.data.TextureEmitters[2].Scale.Y + emitter.counter * emitter.data.TextureEmitters[2].ScaleShift.Y;
                    uniformBlock.uvScaleRotateAnim2.Z = emitter.counter * emitter.data.TextureEmitters[2].RotateShift;
                    uniformBlock.uvScaleRotateAnim2.W = 0.0f;
                }

                uniformBlock.vtxColor0 = new Vector4(ptclColor0, ptcl.emitter.fadeAlpha);
                uniformBlock.vtxColor1 = new Vector4(ptclColor1, ptcl.emitter.fadeAlpha);

                uniformBlock.emitterMat.Column0 = ptcl.emitter.matrixSRT.Column0;
                uniformBlock.emitterMat.Column1 = ptcl.emitter.matrixSRT.Column1;
                uniformBlock.emitterMat.Column2 = ptcl.emitter.matrixSRT.Column2;
                uniformBlock.emitterMat.Column3 = ptcl.emitter.matrixSRT.Column3;

                shader.BindStripeUniformBlock(uniformBlock, system);

                StripeRender.Draw(Context, (int)numDrawStripe);
            }
        }

        public void EntryStripe(EmitterInstance emitter, bool flushCache, object argData)
        {
            ParticleShader shader = emitter.shader[(int)ShaderType];
            if (shader == null)
                return;

            if (emitter.data.VertexTransformMode == VertexTransformMode.ComplexStripe)
            {
                EntryConnectionStripe(emitter, flushCache, argData);
                return;
            }

            StripeVertexBuffer[] stripeVertexBuffer = emitter.StripeVertexBuffer;
            if (stripeVertexBuffer == null)
                return;

            currentParticleType = PtclType.Complex;

            if (!SetupStripeDrawSetting(emitter, flushCache, argData))
                return;

            StripeRender.UpdateStripeVertexData(stripeVertexBuffer);

            Vector3 emitterSetColor = emitter.emitterSet.color.Xyz * emitter.data.ColorScale;
            Vector3 emitterColor0 = emitterSetColor * new Vector3(
                emitter.AnimData[AnimGroupType.Color0R],
                emitter.AnimData[AnimGroupType.Color0G],
                emitter.AnimData[AnimGroupType.Color0B]);
            Vector3 emitterColor1 = emitterSetColor * new Vector3(
                emitter.AnimData[AnimGroupType.Color1R],
                emitter.AnimData[AnimGroupType.Color1G],
                emitter.AnimData[AnimGroupType.Color1B]);

            for (PtclInstance ptcl = emitter.particleHead; ptcl != null; ptcl = ptcl.Next)
            {
                PtclStripe stripe = ptcl.stripe;
                ComplexEmitterData cdata = (ComplexEmitterData)ptcl.data;
                StripeData stripeData = cdata.StripeData;

                if (stripe == null || cdata == null)
                    continue;

                if (stripe.numDraw < 4)
                    continue;

                Vector3 ptclColor0 = ptcl.color[0].Xyz * emitterColor0;
                Vector3 ptclColor1 = ptcl.color[1].Xyz * emitterColor1;

                {
                    StripeUniformBlock uniformBlock = new StripeUniformBlock();
                    uniformBlock.stParam.X = 1.0f;
                    uniformBlock.stParam.Y = 0;
                    uniformBlock.stParam.Z = emitter.data.CameraOffset;
                    uniformBlock.stParam.W = ptcl.scale.X;

                    uniformBlock.uvScrollAnim.X = ptcl.texAnimParam[0].offset.X + ptcl.texAnimParam[0].scroll.X;
                    uniformBlock.uvScrollAnim.Y = ptcl.texAnimParam[0].offset.Y - ptcl.texAnimParam[0].scroll.Y;
                    uniformBlock.uvScrollAnim.Z = ptcl.texAnimParam[1].offset.X + ptcl.texAnimParam[1].scroll.X;
                    uniformBlock.uvScrollAnim.W = ptcl.texAnimParam[1].offset.Y - ptcl.texAnimParam[1].scroll.Y;
                    if (ptcl.texAnimParam.Length > 2)
                    {
                        uniformBlock.uvScrollAnim2.X = ptcl.texAnimParam[2].offset.X + ptcl.texAnimParam[2].scroll.X;
                        uniformBlock.uvScrollAnim2.Y = ptcl.texAnimParam[2].offset.Y - ptcl.texAnimParam[2].scroll.Y;
                    }

                    uniformBlock.uvScaleRotateAnim0.X = ptcl.texAnimParam[0].scale.X;
                    uniformBlock.uvScaleRotateAnim0.Y = ptcl.texAnimParam[0].scale.Y;
                    uniformBlock.uvScaleRotateAnim0.Z = ptcl.texAnimParam[0].rotate;
                    uniformBlock.uvScaleRotateAnim0.W = 0.0f;

                    uniformBlock.uvScaleRotateAnim1.X = ptcl.texAnimParam[1].scale.X;
                    uniformBlock.uvScaleRotateAnim1.Y = ptcl.texAnimParam[1].scale.Y;
                    uniformBlock.uvScaleRotateAnim1.Z = ptcl.texAnimParam[1].rotate;
                    uniformBlock.uvScaleRotateAnim1.W = 0.0f;

                    if (ptcl.texAnimParam.Length > 2)
                    {
                        uniformBlock.uvScaleRotateAnim2.X = ptcl.texAnimParam[2].scale.X;
                        uniformBlock.uvScaleRotateAnim2.Y = ptcl.texAnimParam[2].scale.Y;
                        uniformBlock.uvScaleRotateAnim2.Z = ptcl.texAnimParam[2].rotate;
                        uniformBlock.uvScaleRotateAnim2.W = 0.0f;
                    }

                    uniformBlock.vtxColor0 = new Vector4(ptclColor0, ptcl.emitter.fadeAlpha);
                    uniformBlock.vtxColor1 = new Vector4(ptclColor1, ptcl.emitter.fadeAlpha);

                    uniformBlock.emitterMat.Column0 = ptcl.emitter.matrixSRT.Column0;
                    uniformBlock.emitterMat.Column1 = ptcl.emitter.matrixSRT.Column1;
                    uniformBlock.emitterMat.Column2 = ptcl.emitter.matrixSRT.Column2;
                    uniformBlock.emitterMat.Column3 = ptcl.emitter.matrixSRT.Column3;

                    shader.BindStripeUniformBlock(uniformBlock, system);

                    StripeRender.Draw(Context, (int)ptcl.stripe.numDraw, (int)ptcl.stripe.drawFirstVertex, 1);
                }

                if (stripeData.crossType == 1)
                {
                    StripeUniformBlock uniformBlock = new StripeUniformBlock();
                    uniformBlock.stParam.X = 0;
                    uniformBlock.stParam.Y = 1;
                    uniformBlock.stParam.Z = emitter.data.CameraOffset;
                    uniformBlock.stParam.W = ptcl.scale.X;

                    uniformBlock.uvScrollAnim.X = ptcl.texAnimParam[0].offset.X + ptcl.texAnimParam[0].scroll.X;
                    uniformBlock.uvScrollAnim.Y = ptcl.texAnimParam[0].offset.Y - ptcl.texAnimParam[0].scroll.Y;
                    uniformBlock.uvScrollAnim.Z = ptcl.texAnimParam[1].offset.X + ptcl.texAnimParam[1].scroll.X;
                    uniformBlock.uvScrollAnim.W = ptcl.texAnimParam[1].offset.Y - ptcl.texAnimParam[1].scroll.Y;

                    uniformBlock.uvScaleRotateAnim0.X = ptcl.texAnimParam[0].scale.X;
                    uniformBlock.uvScaleRotateAnim0.Y = ptcl.texAnimParam[0].scale.Y;
                    uniformBlock.uvScaleRotateAnim0.Z = ptcl.texAnimParam[0].rotate;
                    uniformBlock.uvScaleRotateAnim0.W = 0.0f;

                    uniformBlock.uvScaleRotateAnim1.X = ptcl.texAnimParam[1].scale.X;
                    uniformBlock.uvScaleRotateAnim1.Y = ptcl.texAnimParam[1].scale.Y;
                    uniformBlock.uvScaleRotateAnim1.Z = ptcl.texAnimParam[1].rotate;
                    uniformBlock.uvScaleRotateAnim1.W = 0.0f;

                    uniformBlock.vtxColor0 = new Vector4(ptclColor0, ptcl.emitter.fadeAlpha);
                    uniformBlock.vtxColor1 = new Vector4(ptclColor1, ptcl.emitter.fadeAlpha);

                    uniformBlock.emitterMat.Column0 = ptcl.emitter.matrixSRT.Column0;
                    uniformBlock.emitterMat.Column1 = ptcl.emitter.matrixSRT.Column1;
                    uniformBlock.emitterMat.Column2 = ptcl.emitter.matrixSRT.Column2;
                    uniformBlock.emitterMat.Column3 = ptcl.emitter.matrixSRT.Column3;

                    shader.BindStripeUniformBlock(uniformBlock, system);

                    StripeRender.Draw(Context, (int)ptcl.stripe.numDraw, (int)ptcl.stripe.drawFirstVertex, 1);
                }
            }
        }
    }
}

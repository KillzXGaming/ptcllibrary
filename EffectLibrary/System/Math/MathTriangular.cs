﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EffectLibrary
{
    public partial class MathTriangular
    {
        const uint I_HALF_ROUND_IDX = 0x80000000;
        public const float FLT_MIN = 1.1754943508222875E-38f;
        public const float F_ULP = float.Epsilon * 2.0f; // 2.384185791015625E-07f

        public static void SinCosIdx(ref float sin_val, ref float cos_val, uint angle)
        {
            uint idx = (angle >> 24) & 0xff;
            float del = (float)(angle & 0xffffff) / 0x1000000;
            var sample = GetSample(idx);

            sin_val = sample.sin_val + sample.sin_delta * del;
            cos_val = sample.cos_val + sample.cos_delta * del;
        }

        public static float SinIdx(uint angle)
        {
            uint idx = (angle >> 24) & 0xff;
            float del = (float)(angle & 0xffffff) / 0x1000000;
            var sample = GetSample(idx);
            return sample.sin_val + sample.sin_delta * del;
        }

        public static float CosIdx(uint angle)
        {
            uint idx = (angle >> 24) & 0xff;
            float del = (float)(angle & 0xffffff) / 0x1000000;
           var sample = GetSample(idx);
            return sample.cos_val + sample.cos_delta * del;
        }

        public static float Idx2Rad(float angle)
        {
            return angle * (MathF.PI / I_HALF_ROUND_IDX);
        }

        public static uint Rad2Idx(float rad)
        {
            return (uint)(rad * (I_HALF_ROUND_IDX / MathF.PI));
        }

        public static void SinCosRad(ref float sin_val, ref float cos_val, float rad)
        {
            SinCosIdx(ref sin_val, ref cos_val, Rad2Idx(rad));
        }

        public static float SinRad(float rad)
        {
            return SinIdx(Rad2Idx(rad));
        }

        public static float CosRad(float rad)
        {
            return CosIdx(Rad2Idx(rad));
        }
    }
}

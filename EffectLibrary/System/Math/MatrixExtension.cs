﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;

namespace EffectLibrary
{
    public static class MatrixExtension
    {
        public static Matrix4 CreateSRT(Vector3 scale, Vector3 rotate, Vector3 translate)
        {
          return Matrix4.CreateScale(scale) *
                (Matrix4.CreateRotationX(rotate.X) *
                 Matrix4.CreateRotationY(rotate.Y) *
                 Matrix4.CreateRotationZ(rotate.Z)) *
                 Matrix4.CreateTranslation(translate);
        }

        public static Matrix4 CreateRT(Vector3 rotate, Vector3 translate)
        {
            return (Matrix4.CreateRotationX(rotate.X) *
                   Matrix4.CreateRotationY(rotate.Y) *
                   Matrix4.CreateRotationZ(rotate.Z)) *
                   Matrix4.CreateTranslation(translate);
        }

        public static Matrix4 Copy(Matrix4 dst, Matrix4 src)
        {
            if (src != dst)
                dst = src;

            return dst;
        }

        public static Matrix4 MakeVectorRotation(Vector3 a, Vector3 b)
        {
            Vector3 cross = Vector3.Cross(a, b);
            float dot = Vector3.Dot(a, b) + 1.0f;

            Vector4 q = Vector4.Zero;
            if (dot <= MathTriangular.F_ULP)
                q = new Vector4(1, 0, 0, 0);
            else
            {
                float v1 = MathF.Sqrt(2.0f * dot);
                float v2 = 1.0f / v1;
                q.Xyz = new Vector3(cross * v2);
                q.W = v1 * 0.5f;
            }
            return MakeQ(q);
        }

        public static Matrix4 MakeQ(Vector4 q)
        {
            float v = 2.0f / (q.X * q.X + q.Y * q.Y + q.Z * q.Z + q.W * q.W);

            float v1 = q.X * v;
            float v2 = q.Y * v;
            float v3 = q.Z * v;
            float v4 = q.W * v1;
            float v5 = q.W * v2;
            float v6 = q.W * v3;
            float v7 = q.X * v1;
            float v8 = q.X * v2;
            float v9 = q.X * v3;
            float v10 = q.Y * v2;
            float v11 = q.Y * v3;
            float v12 = q.Z * v3;

            Matrix4 dst = Matrix4.Identity;

            dst[0, 0] = 1.0f - (v10 + v12);
            dst[1, 0] = v8 - v6;
            dst[2, 0] = v9 + v5;
            dst[3, 0] = 0.0f;

            dst[0, 1] = v8 + v6;
            dst[1, 1] = 1.0f - (v7 + v12);
            dst[2, 1] = v11 - v4;
            dst[3, 1] = 0.0f;

            dst[0, 2] = v9 - v5;
            dst[1, 2] = v11 + v4;
            dst[2, 2] = 1.0f - (v7 + v10);
            dst[3, 2] = 0.0f;

            return dst;
        }

        public static Vector3 MultVec(Vector3 v, Matrix4 m)
        {
            Vector3 dst = new Vector3();
            dst.X = m[0, 0] * v.X + m[1, 0] * v.Y + m[2, 0] * v.Z + m[3, 0];
            dst.Y = m[0, 1] * v.X + m[1, 1] * v.Y + m[2, 1] * v.Z + m[3, 1];
            dst.Z = m[0, 2] * v.X + m[1, 2] * v.Y + m[2, 2] * v.Z + m[3, 2];
            return dst;
        }

        public static Vector3 MultMTX(Vector3 v, Matrix4 m)
        {
            Vector3 dst = new Vector3();
            dst.X = v.X * m[0, 0] + v.Y * m[0, 1] + v.Z * m[0, 2];
            dst.Y = v.X * m[1, 0] + v.Y * m[1, 1] + v.Z * m[1, 2];
            dst.Z = v.X * m[2, 0] + v.Y * m[2, 1] + v.Z * m[2, 2];
            return dst;
        }

        public static Vector3 MultVecSR(Vector3 v, Matrix4 m)
        {
            Vector3 dst = new Vector3();
            dst.X = m[0, 0] * v.X + m[1, 0] * v.Y + m[2, 0] * v.Z;
            dst.Y = m[0, 1] * v.X + m[1, 1] * v.Y + m[2, 1] * v.Z;
            dst.Z = m[0, 2] * v.X + m[1, 2] * v.Y + m[2, 2] * v.Z;
            return dst;
        }
    }
}

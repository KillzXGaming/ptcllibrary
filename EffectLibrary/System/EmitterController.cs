﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EffectLibrary
{
    public struct EmitterController
    {
        public float EmissionRatio;
        public float EmissionInterval;
        public float Life;
        public uint VisibilityFlags;

        public EmitterInstance emitter;

        public void SetFollowType(PtclFollowType followType)
        {
            emitter.ptclFollowType = followType;
        }
    }
}

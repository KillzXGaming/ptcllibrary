﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EffectLibrary
{
    // https://github.com/open-ead/sead/blob/master/include/random/seadRandom.h
    // https://github.com/open-ead/sead/blob/master/modules/src/random/seadRandom.cpp
    //https://github.com/open-ead/NW4F-Eft/blob/nsmbu/include/eft_Random.h

    public class Random
    {
        public static Random RND = new Random();

        private uint mX;
        private uint mY;
        private uint mZ;
        private uint mW;

        public Random() => Init();

        public Random(uint seed) => Init(seed);

        public Random(uint seed_x, uint seed_y, uint seed_z, uint seed_w) {
            Init(seed_x, seed_y, seed_z, seed_w);
        }

        public void Init() => Init(0);

        public virtual void Init(uint seed) {
            uint a = 0x6C078965;
            mX = a * (seed ^ (seed >> 30)) + 1;
            mY = a * (mX ^ (mX >> 30)) + 2;
            mZ = a * (mY ^ (mY >> 30)) + 3;
            mW = a * (mZ ^ (mZ >> 30)) + 4;
        }

        public void Init(uint seed_x, uint seed_y, uint seed_z, uint seed_w) {
            if ((seed_x | seed_y | seed_z | seed_w) == 0) // seeds must not be all zero.
            {
                 Init(0);
                return;
            }

            mX = seed_x;
            mY = seed_y;
            mZ = seed_z;
            mW = seed_w;
        }

        public virtual uint GetU32() {
            uint x = mX ^ (mX << 11);
            mX = mY;
            mY = mZ;
            mZ = mW;
            mW = mW ^ (mW >> 19) ^ x ^ (x >> 8);
            return mW;
        }

        public uint GetU32(uint max) {
            return (uint)(GetU32() * (ulong)(max) >> 32);
        }

        public int GetS32Range(int a, int b) {
            return (int)GetU32((uint)(b - a)) + a;
        }

        public float GetF32() {
            return GetU32() * (1.0f / 4294967296.0f);
        }

        public float GetF32(float max) {
            return GetF32() * max;
        }

        public float GetF32Range(float a, float b) {
            return GetF32(b - a) + a;
        }
    }
}

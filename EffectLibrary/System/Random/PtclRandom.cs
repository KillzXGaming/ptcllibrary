﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace EffectLibrary
{
    public class PtclRandom
    {
        public static Random gRandom = new Random();

        public static Random GetGlobalRandom() {
            return gRandom;
        }

        static Vector3[] mVec3Tbl;
        static Vector3[] mNormalizedVec3Tbl;

        private ushort randomVec3Idx;
        private ushort randomNormVec3Idx;
        private uint val;

        public PtclRandom() { }

        public static void Initialize(Heap heap)
        {
            Random random = new Random(12345679);

            mVec3Tbl = new Vector3[0x200];
            mNormalizedVec3Tbl = new Vector3[0x200];

            for (int i = 0; i < 0x200; i++)
            {
                mVec3Tbl[i].X = random.GetF32Range(-1.0f, 1.0f);
                mVec3Tbl[i].Y = random.GetF32Range(-1.0f, 1.0f);
                mVec3Tbl[i].Z = random.GetF32Range(-1.0f, 1.0f);
                mNormalizedVec3Tbl[i].X = random.GetF32Range(-1.0f, 1.0f);
                mNormalizedVec3Tbl[i].Y = random.GetF32Range(-1.0f, 1.0f);
                mNormalizedVec3Tbl[i].Z = random.GetF32Range(-1.0f, 1.0f);
                Vector3.Normalize(mNormalizedVec3Tbl[i]);
            }
        }

        public void Init(uint seed)
        {
            randomVec3Idx = (ushort)seed;
            randomNormVec3Idx = (ushort)(seed >> 16);
            val = seed;
        }

        public uint GetU32()
        {
            uint x = val;
            val = val * 0x41C64E6D + 12345;
            return x;
        }

        public int GetS32(int max)
        {
            return (int)(GetU32() * (ulong)max >> 32);
        }

        public Vector3 GetVec3()
        {
            return mVec3Tbl[randomVec3Idx++ & 0x1FF];
        }

        public Vector3 GetNormalizedVec3()
        {
            return mNormalizedVec3Tbl[randomVec3Idx++ & 0x1FF];
        }

        public uint GetU32(uint max)
        {
            return (uint)(GetU32() * (ulong)max >> 32);
        }

        public int GetS32Range(int a, int b)
        {
            return (int)GetU32((uint)(b - a)) + a;
        }

        public float GetF32()
        {
            return GetU32() * (1.0f / 4294967296.0f);
        }

        public float GetF32(float max)
        {
            return GetF32() * max;
        }

        public float GetF32Range(float a, float b)
        {
            return GetF32(b - a) + a;
        }
    }
}

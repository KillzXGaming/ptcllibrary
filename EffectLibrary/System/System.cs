﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;

namespace EffectLibrary
{
    public struct PtclViewZ
    {
        public EmitterSet EmitterSet;
        public uint ZOrder;
        public PtclInstance ptcl;
        public int idx;
    }

    public partial class EffectSystem
    {
        public static EffectSystem ActiveSystem = new EffectSystem();

        const int MAX_GROUP_COUNT = 64; // Maximum of 64 groups

        public Resource[] Resources;

        public int GetMaxEmitterSetCount() => (int)this.numEmitterSetMax;

        public void Initialize(Heap heap, Config config)
        {
            if (Initialized)
                return;

            this.heap = heap;
            //Setup max configuration data
            numResourceMax = config.numResourceMax;
            numEmitterMax = config.numEmitterMax;
            numParticleMax = config.numParticleMax;
            numEmitterSetMax = config.numEmitterSetMax;
            numStripeMax = config.numStripeMax;
            numEmitterSetMaxMask = numEmitterSetMax - 1;
            numEmitterMaxMask = numEmitterMax - 1;
            numUnusedEmitters = numEmitterMax;
            numStripeMaxMask = numStripeMax - 1;
            numParticleMaxMask = numParticleMax - 1;

            //Calculation parameter defaults
            numCalcEmitterSet = 0;
            numCalcEmitter = 0;
            numCalcParticle = 0;
            numCalcStripe = 0;

            //Indices for current values
            currentEmitterSetIdx = 0;
            currentEmitterIdx = 0;
            currentStripeIdx = 0;
            currentEmitterSetCreateID = 0;
            currentCallbackID = (uint)CustomActionCallBackID.Invalid;

            _530 = 0;
            _8A8 = -1;
            _538 = 0;

            doubleBufferSwapped = 0;
            currentParticleIdx = 0;

            //Setup groups
            emitterGroups         = new EmitterInstance[MAX_GROUP_COUNT];
            emitterSetGroupHead   = new EmitterSet[MAX_GROUP_COUNT];
            emitterSetGroupTail   = new EmitterSet[MAX_GROUP_COUNT];
            stripeGroups          = new PtclStripe[MAX_GROUP_COUNT];
            _570[(int)CpuCore._0] = new uint[MAX_GROUP_COUNT];
            _570[(int)CpuCore._1] = new uint[MAX_GROUP_COUNT];
            _570[(int)CpuCore._2] = new uint[MAX_GROUP_COUNT];

            PtclRandom.Initialize(heap);

            Resources = new Resource[numResourceMax];
            for (int i = 0; i < (int)CpuCore.Max; i++)
            {
                renderers[i] = new Renderer(heap, this, config);
            }

            emitterSets = new EmitterSet[numEmitterSetMax];
            for (int i = 0; i < numEmitterSetMax; i++)
            {
                emitterSets[i] = new EmitterSet();
                emitterSets[i].System = this;
            }

            emitters = new EmitterInstance[numEmitterMax];
            emitterStaticUniformBlocks = new EmitterStaticUniformBlock[numEmitterMax * 2];

            for (int i = 0; i < numEmitterMax; i++)
            {
                emitters[i] = new EmitterInstance();
                emitters[i].calc = null;
                emitters[i].StaticUniformBlock = emitterStaticUniformBlocks[i];
                emitters[i].ChildStaticUniformBlock = emitterStaticUniformBlocks[(int)numEmitterMax + i];
            }

            particles = new PtclInstance[numParticleMax];
            alphaAnim = new AlphaAnim[numParticleMax];
            scaleAnim = new ScaleAnim[numParticleMax];

            EmitterCalc.InitializeFluctuationTable();

            emitterCalc[(int)EmitterType.Simple] = new EmitterSimpleCalc(this);
            emitterCalc[(int)EmitterType.Complex] = new EmitterComplexCalc(this);

            stripes = new PtclStripe[numStripeMax];

            for (int i = 0; i < numParticleMax; i++) {
                particles[i] = new PtclInstance();
            }

            for (int i = 0; i < (int)CpuCore.Max; i++)
            {
                particlesToRemove[i] = new PtclInstance[numParticleMax];
                childParticles[i] = new PtclInstance[numParticleMax];
                numParticleToRemove[i] = 0;
                numChildParticle[i] = 0;

                for (int j = 0; j < numParticleMax; j++)
                {
                    particlesToRemove[i][j] = null;
                    childParticles[i][j] = null;
                }
            }

            for (int i = 0; i < numStripeMax; i++)
            {
                stripes[i] = new PtclStripe();
                stripes[i].queue = new PtclStripeQueue[256];

                stripes[i].data = null;
                stripes[i].queueFront = 0;
                stripes[i].queueRear = 0;
            }

            for (int i = 0; i < (int)CustomActionCallBackID.Max; i++)
            {
                customActionEmitterPreCalcCallback[i] = null;
                customActionParticleEmitCallback[i] = null;
                customActionParticleRemoveCallback[i] = null;
                customActionParticleCalcCallback[i] = null;
                customActionParticleMakeAttrCallback[i] = null;
                customActionEmitterPostCalcCallback[i] = null;
                customActionEmitterDrawOverrideCallback[i] = null;
            }

            for (int i = 0; i < (int)CustomShaderCallBackID.Max; i++)
            {
                customShaderEmitterPostCalcCallback[i] = null;
                customShaderDrawOverrideCallback[i] = null;
                customShaderRenderStateSetCallback[i] = null;
            }

            for (int i = 0; i < (int)CpuCore.Max; i++)
            {
                sortedEmitterSets[i] = new PtclViewZ[(int)numEmitterSetMax];
                numSortedEmitterSets[i] = 0;
                _A14[i] = 0;
            }

            Initialized = true;
        }

        public bool HasResourceFlags(FileResourceFlags flag) {
            return Resources[0].ResourceFile.Flags.HasFlag(flag);
        }

        public void RemovePtcl_()
        {
            for (int i = 0; i < (int)CpuCore.Max; i++)
            {
                for (int j = 0; j < numParticleToRemove[i]; j++)
                {
                    PtclInstance ptcl = particlesToRemove[i][j];
                    {
                        EmitterInstance emitter = ptcl.emitter;

                        if (ptcl.type == PtclType.Child) 
                            emitter.numChildParticles--;
                        else 
                            emitter.numParticles--;

                        if (emitter.particleHead == ptcl)
                        {
                            emitter.particleHead = ptcl.Next;

                            if (emitter.particleHead != null)
                                emitter.particleHead.Prev = null;

                            if (emitter.particleTail == ptcl)
                                emitter.particleTail = null;
                        }
                        else if (emitter.childParticleHead == ptcl)
                        {
                            emitter.childParticleHead = ptcl.Next;

                            if (emitter.childParticleHead != null)
                                emitter.childParticleHead.Prev = null;

                            if (emitter.childParticleTail == ptcl)
                                emitter.childParticleTail = null;
                        }
                        else if (emitter.particleTail == ptcl)
                        {
                            emitter.particleTail = ptcl.Prev;

                            if (emitter.particleTail != null)
                                emitter.particleTail.Next = null;
                        }
                        else if (emitter.childParticleTail == ptcl)
                        {
                            emitter.childParticleTail = ptcl.Prev;

                            if (emitter.childParticleTail != null)
                                emitter.childParticleTail.Next = null;
                        }
                        else
                        {
                            if (ptcl.Next != null)
                                ptcl.Next.Prev = ptcl.Prev;

                            if (ptcl.Prev != null)
                                ptcl.Prev.Next = ptcl.Next;
                        }

                        var callback = GetCurrentCustomActionParticleRemoveCallback(ptcl.emitter);
                        if (callback != null)
                        {
                            ParticleRemoveArg arg = new ParticleRemoveArg { ptcl = ptcl };
                            callback(arg);
                        }

                        ptcl.data = null;
                    }

                    if (ptcl.stripe != null)
                    {
                        RemoveStripe(ptcl.stripe);
                        ptcl.stripe = null;
                    }

                    particlesToRemove[i][j] = null;
                }
                numParticleToRemove[i] = 0;
            }
        }

        public void RemoveStripe(PtclStripe stripe)
        {
            stripe.data = null;

            if (stripeGroups[stripe.groupID] == stripe)
            {
                stripeGroups[stripe.groupID] = stripe.Next;

                if (stripeGroups[stripe.groupID] != null)
                    stripeGroups[stripe.groupID].Prev = null;
            }
            else
            {
                if (stripe.Next != null)
                    stripe.Next.Prev = stripe.Prev;

                if (stripe.Prev != null)
                    stripe.Prev.Next = stripe.Next;
            }
        }

        public EmitterSet RemoveEmitterSetFromDrawList(EmitterSet emitterSet)
        {
            if (emitterSet == null)
                return null;

            EmitterSet next = emitterSet.Next;

            if (emitterSet == emitterSetGroupHead[emitterSet.groupID])
            {
                emitterSetGroupHead[emitterSet.groupID] = emitterSet.Next;

                if (emitterSetGroupHead[emitterSet.groupID] != null)
                    emitterSetGroupHead[emitterSet.groupID].Prev = null;

                if (emitterSet == emitterSetGroupTail[emitterSet.groupID])
                    emitterSetGroupTail[emitterSet.groupID] = null;
            }
            else
            {
                if (emitterSet == emitterSetGroupTail[emitterSet.groupID])
                    emitterSetGroupTail[emitterSet.groupID] = emitterSet.Prev;

                if (emitterSet.Next != null)
                    emitterSet.Next.Prev = emitterSet.Prev;

                if (emitterSet.Prev != null)
                    emitterSet.Prev.Next = emitterSet.Next;
            }

            emitterSet.Next = null;
            emitterSet.Prev = null;
            return next;
        }

        public void RemovePtcl()
        {
            RemovePtcl_();

            for (int i = 0; i < 64u; i++)
            {
                for (EmitterSet emitterSet = emitterSetGroupHead[i]; emitterSet != null;)
                {
                    emitterSet = (emitterSet.numEmitter == 0) ? RemoveEmitterSetFromDrawList(emitterSet)
                                                               : emitterSet.Next;
                }
            }
        }

        public void AddPtclRemoveList(PtclInstance ptcl, int core)
        {
            particlesToRemove[core][numParticleToRemove[core]++] = ptcl;
            ptcl.lifespan = 0;
        }

        public void EmitChildParticle()
        {
            for (int i = 0; i < (int)CpuCore.Max; i++)
            {
                for (int j = 0; j < numChildParticle[i]; j++)
                {
                    PtclInstance ptcl = childParticles[i][j];
                    EmitterComplexCalc.EmitChildParticle(ptcl.emitter, ptcl);
                    childParticles[i][j] = null;
                }
                numChildParticle[i] = 0;
            }
        }

        public void AddPtclAdditionList(PtclInstance ptcl, CpuCore core)
        {
            if (numChildParticle[(int)core] > numParticleMax)
                return;

            childParticles[(int)core][numChildParticle[(int)core]] = ptcl;
            numChildParticle[(int)core]++;
        }

        public PtclStripe AllocAndConnectStripe(EmitterInstance emitter, PtclInstance ptcl)
        {
            for (int i = 0; i < numStripeMax; i++)
            {
                currentStripeIdx++;
                currentStripeIdx &= (int)numStripeMaxMask;

                if (stripes[currentStripeIdx].data == null)
                {
                    PtclStripe stripe = stripes[currentStripeIdx];
                    var groupID = emitter.groupID;

                    if (stripeGroups[groupID] == null)
                    {
                        stripeGroups[groupID] = stripe;
                        stripe.Next = null;
                        stripe.Prev = null;
                    }
                    else
                    {
                        stripeGroups[groupID].Prev = stripe;
                        stripe.Next = stripeGroups[groupID];
                        stripeGroups[groupID] = stripe;
                        stripe.Prev = null;
                    }

                    stripe.particle = ptcl;
                    stripe.queueFront = 0;
                    stripe.queueRear = 0;
                    stripe.queueCount = 0;
                    stripe.groupID = (uint)emitter.groupID;
                    stripe.data = (ComplexEmitterData)emitter.data;
                    stripe.counter = 0;
                    stripe.queue[0].outer = new Vector3();

                    return stripe;
                }
            }
            return null;
        }

        public PtclInstance AllocPtcl(PtclType type)
        {
            for (int i = 0; i < numParticleMax; i++)
            {
                currentParticleIdx++;
                currentParticleIdx &= (int)numParticleMaxMask;

                if (particles[currentParticleIdx].data == null)
                {
                    numEmittedParticle++;
                    PtclInstance ptcl = particles[currentParticleIdx];
                    ptcl.type = type;
                    return ptcl;
                }
            }
            return null;
        }

        public EmitterSet AllocEmitterSet(Handle handle)
        {
            EmitterSet emitterSet = null;
            for (int i = 0; i < numEmitterSetMax; i++)
            {
                currentEmitterSetIdx++;
                currentEmitterSetIdx &= (int)numEmitterSetMaxMask;

                if (emitterSets[currentEmitterSetIdx].numEmitter == 0) {
                    emitterSet = emitterSets[currentEmitterSetIdx];
                    break;
                }
            }

            handle.emitterSet = emitterSet;
            return emitterSet;
        }

        public EmitterInstance AllocEmitter(byte groupID)
        {
            EmitterInstance emitter = null;
            for (int i = 0; i < numEmitterMax; i++)
            {
                currentEmitterIdx++;
                currentEmitterIdx &= (int)numEmitterMaxMask;

                if (emitters[currentEmitterIdx].calc == null) {
                    emitter = emitters[currentEmitterIdx];
                    break;
                }
            }

            if (emitter == null)
                return null;

            if (emitterGroups[groupID] == null)
            {
                emitterGroups[groupID] = emitter;
                emitter.Next = null;
                emitter.Prev = null;
            }
            else
            {
                emitterGroups[groupID].Prev = emitter;
                emitter.Next = emitterGroups[groupID];
                emitterGroups[groupID] = emitter;
                emitter.Prev = null;
            }

            numUnusedEmitters--;
            return emitter;
        }

        public void AddEmitterSetToDrawList(EmitterSet emitterSet, byte groupID)
        {
            if (emitterSetGroupHead[groupID] == null)
            {
                emitterSetGroupHead[groupID] = emitterSet;
                emitterSet.Prev = null;
                emitterSet.Next = null;
            }
            else
            {
                emitterSetGroupTail[groupID].Next = emitterSet;
                emitterSet.Prev = emitterSetGroupTail[groupID];
                emitterSet.Next = null;
            }
            emitterSetGroupTail[groupID] = emitterSet;
        }

        public bool Initialized;
        Heap heap;

        uint numResourceMax;
        uint numUnusedEmitters; 
        uint numEmitterMax;
        uint numParticleMax;
        uint numStripeMax;
        uint numEmitterMaxMask;
        uint numParticleMaxMask;
        uint numStripeMaxMask;
        uint numEmitterSetMax;
        uint numEmitterSetMaxMask;
        public uint numCalcEmitter;
        public uint numCalcParticle;
        public uint numCalcEmitterSet;
        public uint numEmittedParticle;
        public uint numCalcStripe;

        int currentEmitterIdx;
        int currentParticleIdx;
        int currentEmitterSetIdx;
        int currentStripeIdx;
        public uint currentEmitterSetCreateID;
        public uint currentCallbackID;

        public EmitterSet[] emitterSets;

        public Matrix4[] view = new Matrix4[3];

        public EmitterInstance[] emitterGroups  = new EmitterInstance[MAX_GROUP_COUNT];
        EmitterSet[] emitterSetGroupHead = new EmitterSet[MAX_GROUP_COUNT];
        EmitterSet[] emitterSetGroupTail = new EmitterSet[MAX_GROUP_COUNT];
        PtclStripe[] stripeGroups        = new PtclStripe[MAX_GROUP_COUNT];
        PtclStripe[] stripes;
        EmitterInstance[] emitters;
        EmitterStaticUniformBlock[] emitterStaticUniformBlocks;

        PtclInstance[] particles;
        PtclInstance[][] childParticles    = new PtclInstance[(int)CpuCore.Max][];
        PtclInstance[][] particlesToRemove = new PtclInstance[(int)CpuCore.Max][];
        PtclViewZ[][] sortedEmitterSets = new PtclViewZ[(int)CpuCore.Max][];

        int[] numChildParticle           = new int[(int)CpuCore.Max];
        int[] numParticleToRemove        = new int[(int)CpuCore.Max];
        int[] numSortedEmitterSets      = new int[(int)CpuCore.Max];

        uint[][] _570         = new uint[(int)CpuCore.Max][];
        uint[] _A14           = new uint[(int)CpuCore.Max];

        public Renderer[] renderers = new Renderer[(int)CpuCore.Max];
        public EmitterCalc[] emitterCalc = new EmitterCalc[(int)EmitterType.Max];


        AlphaAnim[] alphaAnim;
        ScaleAnim[] scaleAnim;

        ulong activeGroupsFlg;

        uint _530;
        uint _538;

        public int _8A8;


        public int doubleBufferSwapped;
    }
}

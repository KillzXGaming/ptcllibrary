﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EffectLibrary
{
    public partial class EffectSystem
    {
        public int GetEmitterSetIndex(string name)
        {
            for (int i = 0; i < emitterSets.Length; i++)
            {
                if (emitterSets[i] != null && emitterSets[i].name == name)
                    return i;
            }
            return -1;
        }

        public void ClearResource(Heap heap, uint resourceID)
        {
            if (heap != null)
            {
                Resources[resourceID].Finalize(heap);
                heap.Free(Resources[resourceID]);
            }
            else
            {
                Heap resHeap = Resources[resourceID].heap;
                Resources[resourceID].Finalize(resHeap);
                resHeap.Free(Resources[resourceID]);
            }
            Resources[resourceID] = null;
        }

        public void EntryResource(Heap heap,Resource resource, uint resourceID)
        {
            if (Resources[resourceID] != null)
                ClearResource(null, resourceID);

            Resources[resourceID] = resource;
        }

        public void KillEmitter(EmitterInstance emitter)
        {
            PtclInstance ptcl;

            Console.WriteLine($"KillEmitter {emitter.data.Name}");

            for (ptcl = emitter.particleHead; ptcl != null; ptcl = ptcl.Next)
                AddPtclRemoveList(ptcl, (int)CpuCore._1);

            for (ptcl = emitter.childParticleHead; ptcl != null; ptcl = ptcl.Next)
                AddPtclRemoveList(ptcl, (int)CpuCore._1);

            emitter.emitterSet.numEmitter--;
            emitter.emitterSetCreateID = 0xFFFFFFFF;
            emitter.calc = null;

            numUnusedEmitters++;
            if (emitter.emitterSet.numEmitter == 0)
                numCalcEmitterSet--;

            if (emitter == emitterGroups[emitter.groupID])
            {
                emitterGroups[emitter.groupID] = emitter.Next;

                if (emitterGroups[emitter.groupID] != null)
                    emitterGroups[emitter.groupID].Prev = null;
            }
            else
            {
                if (emitter.Next != null)
                    emitter.Next.Prev = emitter.Prev;

                //if (emitter->prev != NULL) <-- No check, because... Nintendo
                emitter.Prev.Next = emitter.Next;
            }

            RemovePtcl_();
        }

        public void KillEmitterGroup(byte groupID)
        {
            for (var emitter = emitterGroups[groupID]; emitter != null; emitter = emitter.Next)
                KillEmitter(emitter);

            emitterSetGroupHead[groupID] = null;
            emitterSetGroupTail[groupID] = null;
        }

        public void KillEmitterSet(EmitterSet emitterSet)
        {
            if (!(emitterSet.numEmitter > 0))
                return;

            for (int i = 0; i < emitterSet.numEmitterAtCreate; i++)
            {
                if (emitterSet.emitters[i].emitterSetCreateID == emitterSet.createID
             && emitterSet.emitters[i].calc != null)
                {
                    KillEmitter(emitterSet.emitters[i]);
                }
            }

            RemoveEmitterSetFromDrawList(emitterSet);
        }
    }
}

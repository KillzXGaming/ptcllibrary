﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;

namespace EffectLibrary
{
    public class EmitterSet
    {
        public EffectSystem System;
        public int numEmitter = 0;
        public int numEmitterAtCreate = 0;
        public uint numPrimitive;
        public uint createID = 0;
        public uint userData = 0;

        public bool Initialized = false;

        public string name;

        public EmitterInstance[] emitters = new EmitterInstance[16];
        public EmitterController[] controllers = new EmitterController[16];
        public EmitterData[] emitterRefs;

        public ParticleShader[] shaders;
        public PrimitiveRenderer[] primitives;

        public uint resourceID;
        public uint emitterSetID;
        public uint groupID;
        public uint _unusedFlags;

        public Matrix4 matrixSRT;
        public Matrix4 matrixRT;

        public Vector3 scaleForMatrix;
        public Vector2 ptclScale;
        public Vector2 ptclEmitScale;
        public Vector2 ptclEffectiveScale;
        public Vector3 emitterVolumeScale;

        public Vector4 color { get; set; }

        public float allDirVel;
        public float dirVel;
        public float dirVelRandom;

        public Vector3 addVelocity;
        public Vector3 dir;
        public Vector3 ptclRotate;
        public uint numEmissionPoints;
        public Vector3[] emissionPoints;

        public bool doFade;
        public byte dirSet;
        public byte noCalc;
        public byte noDraw;

        public uint numShader;
        public ParticleShader[] Shaders;

        public EmitterSet Next;
        public EmitterSet Prev;

        public byte infiniteLifespan;
        public byte _unused;
        public byte[] _unusedPad = new byte[6];

        public int startFrame;

        public IEmitterSet data;

        public EmitterSet()
        {
            color = new Vector4(1, 1, 1, 1);
            ptclRotate = new Vector3(0, 0, 0);
        }

        public void SetMatrix(Matrix4 matrixSRT)
        {
            this.matrixSRT = matrixSRT;

            scaleForMatrix.X = new Vector3(matrixSRT[0, 0], matrixSRT[0, 1], matrixSRT[0, 2]).Length;
            scaleForMatrix.Y = new Vector3(matrixSRT[1, 0], matrixSRT[1, 1], matrixSRT[1, 2]).Length;
            scaleForMatrix.Z = new Vector3(matrixSRT[2, 0], matrixSRT[2, 1], matrixSRT[2, 2]).Length;

            if (scaleForMatrix.X > 0.0f)
            {
                float xInv = 1.0f / scaleForMatrix.X;
                matrixRT[0, 0] = matrixSRT[0, 0] * xInv;
                matrixRT[0, 1] = matrixSRT[0, 1] * xInv;
                matrixRT[0, 2] = matrixSRT[0, 2] * xInv;
            }
            else
            {
                matrixRT[0, 0] = 0;
                matrixRT[0, 1] = 0;
                matrixRT[0, 2] = 0;
            }

            if (scaleForMatrix.Y > 0.0f)
            {
                float yInv = 1.0f / scaleForMatrix.Y;
                matrixRT[1, 0] = matrixSRT[1, 0] * yInv;
                matrixRT[1, 1] = matrixSRT[1, 1] * yInv;
                matrixRT[1, 2] = matrixSRT[1, 2] * yInv;
            }
            else
            {
                matrixRT[1, 0] = 0;
                matrixRT[1, 1] = 0;
                matrixRT[1, 2] = 0;
            }

            if (scaleForMatrix.Z > 0.0f)
            {
                float zInv = 1.0f / scaleForMatrix.Z;
                matrixRT[2, 0] = matrixSRT[2, 0] * zInv;
                matrixRT[2, 1] = matrixSRT[2, 1] * zInv;
                matrixRT[2, 2] = matrixSRT[2, 2] * zInv;
            }
            else
            {
                matrixRT[2, 0] = 0;
                matrixRT[2, 1] = 0;
                matrixRT[2, 2] = 0;
            }

            matrixRT[3, 0] = matrixSRT[3, 0];
            matrixRT[3, 1] = matrixSRT[3, 1];
            matrixRT[3, 2] = matrixSRT[3, 2];

            ptclEffectiveScale.X = ptclScale.X * scaleForMatrix.X;
            ptclEffectiveScale.Y = ptclScale.Y * scaleForMatrix.Y;
        }

        public void Kill()
        {
            EffectSystem.ActiveSystem.KillEmitterSet(this);
        }
    }
}

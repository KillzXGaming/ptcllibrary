﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EffectLibrary.WiiU;
using OpenTK;

namespace EffectLibrary
{
    public partial class EffectSystem 
    {
        public void InitializeEmitter(EmitterInstance emitter, SimpleEmitterData data, uint resourceID, uint emitterSetID, uint seed, bool keepCreateID)
        {
            Random gRandom = PtclRandom.GetGlobalRandom();

            uint data_seed = data.RandomSeed;
            if (data_seed == 0xFFFFFFFF) //Use random emitter set seed
                emitter.random.Init(seed);
            else if (data_seed == 0) //Use random seed per emitter
                emitter.random.Init(gRandom.GetU32());
            else
                emitter.random.Init(data_seed); //Use emitter seed

            if (!keepCreateID)
                emitter.emitterSetCreateID = currentEmitterSetCreateID;

            emitter.Init(data);
            emitter.calc = emitterCalc[(int)emitter.data.Type];
            emitter.controller.SetFollowType(emitter.data.PtclFollowType);

            emitter.shader = new ParticleShader[(int)ShaderType.Max];
            emitter.childShader = new ParticleShader[(int)ShaderType.Max];

            for (int i = 0; i < (int)ShaderType.Max; i++)
            {
                VertexShaderKey vertexShaderKey = new VertexShaderKey();
                vertexShaderKey.Initialize(emitter.data);

                FragmentShaderKey fragmentShaderKey = new FragmentShaderKey();
                fragmentShaderKey.Initialize(emitter.data);

                emitter.shader[i] = Resources[resourceID].GetShader(emitterSetID, vertexShaderKey, fragmentShaderKey, data.ShaderIndex1);
              //  if (emitter.shader[i] == null && i == 0)
                 //   throw new Exception($"Failed to load shader binary!");
            }

            emitter.primitive = null;
            if (data.MeshType == MeshType.Primitive && data.PrimitiveIndex != -1)
                emitter.primitive = Resources[resourceID].Primitives[data.PrimitiveIndex];

            emitter.childPrimitive = null;
            if (emitter.HasChild())
            {
                var childData = emitter.GetChildData();
                if (childData.MeshType == MeshType.Primitive)
                    emitter.childPrimitive = Resources[resourceID].Primitives[childData.PrimitiveIdx];

                for (int i = 0; i < (int)ShaderType.Max; i++)
                {
                    VertexShaderKey vertexShaderKey = new VertexShaderKey();
                    vertexShaderKey.Initialize(childData);

                    FragmentShaderKey fragmentShaderKey = new FragmentShaderKey();
                    fragmentShaderKey.Initialize(childData, ((ComplexEmitterData)emitter.data).childFlags);

                    emitter.childShader[i] = Resources[resourceID].GetShader(emitterSetID, vertexShaderKey, fragmentShaderKey, childData.ShaderIndex1);
                    if (emitter.shader[i] == null && i == 0)
                        throw new Exception($"Failed to load shader binary!");
                }
            }

            emitter.UpdateResInfo();
        }

        public bool CreateEmitterSetID(Handle handle, Matrix4 matrixSRT, int emitterSetID, uint resourceID, byte groupID, uint emitterEnableMask)
        {
            Random gRandom = PtclRandom.GetGlobalRandom();
            int numEmitter = Resources[resourceID].EmitterSets[emitterSetID].numEmitter;
            if (numEmitter > numUnusedEmitters)
                return false;

            EmitterSet emitterSet = AllocEmitterSet(handle);
            if (emitterSet == null)
                return false;

            emitterSet.matrixSRT = matrixSRT;
            emitterSet.matrixRT = matrixSRT;

            emitterSet._unusedFlags = 0;
            emitterSet.allDirVel = 1.0f;
            emitterSet.dirVel = 1.0f;
            emitterSet.dirVelRandom = 1.0f;
            emitterSet.startFrame = 0;
            emitterSet.numEmissionPoints = 0;
            emitterSet.doFade = false;
            emitterSet.dirSet = 0;
            emitterSet.noCalc = 0;
            emitterSet.noDraw = 0;
            emitterSet.infiniteLifespan = 0;
            emitterSet._unused = 0x80;

            emitterSet.scaleForMatrix = new Vector3(1.0f, 1.0f, 1.0f);
            emitterSet.ptclScale = new Vector2(1.0f, 1.0f);
            emitterSet.ptclEmitScale = new Vector2(1.0f, 1.0f);
            emitterSet.ptclEffectiveScale = new Vector2(1.0f, 1.0f);
            emitterSet.emitterVolumeScale = new Vector3(1.0f, 1.0f, 1.0f);
            emitterSet.color = new Vector4(1.0f, 1.0f, 1.0f, 1.0f);
            emitterSet.addVelocity = new Vector3(0.0f, 0.0f, 0.0f);
            emitterSet.ptclRotate = new Vector3(0.0f, 0.0f, 0.0f);

            for (int i = 0; i < 16; i++)
                emitterSet.emitters[i] = null;

            emitterSet.createID = currentEmitterSetCreateID;
            emitterSet.resourceID = resourceID;
            emitterSet.emitterSetID = (uint)emitterSetID;
            emitterSet.Next = null;
            emitterSet.Prev = null;
            emitterSet.groupID = groupID;
            emitterSet.userData = 0;
            emitterSet.numEmitter = 0;

            AddEmitterSetToDrawList(emitterSet, groupID);

            uint seed = gRandom.GetU32();

            handle.createID = currentEmitterSetCreateID;
            for (int i = numEmitter - 1; i >= 0; i--)
            {
                var visible = (emitterEnableMask & 1 << i);
                if (visible == 0)
                    continue;

                 EmitterInstance emitter = AllocEmitter(groupID);
                 var data = (SimpleEmitterData)Resources[resourceID].EmitterSets[emitterSetID].emitterRefs[i];

                emitterSet.emitters[emitterSet.numEmitter++] = emitter;
                emitter.emitterSet = emitterSet;

                emitter.controller = emitterSet.controllers[i];
                emitter.controller.emitter = emitter;
                emitter.controller.EmissionRatio = 1.0f;
                emitter.controller.EmissionInterval = 1.0f;
                emitter.controller.Life = 1.0f;
                emitter.controller.VisibilityFlags = 0x3F;

                emitter.groupID = groupID;

                emitterSet._unusedFlags |= (uint)(1 << data._29C);

                InitializeEmitter(emitter, data, resourceID, (uint)emitterSetID, seed, false);

                if (emitter.data.MaxLifespan == 0x7FFFFFFF)
                    emitterSet.infiniteLifespan = 1;
            }

            emitterSet.numEmitterAtCreate = emitterSet.numEmitter;
            emitterSet.Initialized = true;

            numCalcEmitterSet++;
            currentEmitterSetCreateID++;

            return true;
        }
    }
}

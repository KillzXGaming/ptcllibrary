﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Threading.Tasks;
using Syroot.BinaryData;

namespace EffectLibrary
{
    public class FragmentShaderKey : IResData
    {
        public byte FragmentShaderMode { get; set; }
        public byte FragmentSoftEdge { get; set; }
        public byte FragmentTextureMode { get; set; }
        public byte FragmentColorMode { get; set; }
        public byte FragmentAlphaMode { get; set; }
        public byte UserSetting { get; set; }
        public bool IsPrimitive { get; set; }

        public byte FragmentTextureColorBlendType { get; set; }
        public byte FragmentTextureAlphaBlendType { get; set; }
        public byte FragmentPrimitiveColorBlendType { get; set; }
        public byte FragmentPrimitiveAlphaBlendType { get; set; }

        public byte FragmentTexture1ColorSrc { get; set; }
        public byte FragmentTexture2ColorSrc { get; set; }
        public byte FragmentPrimitiveColorSrc { get; set; }
        public byte FragmentTexture1AlphaSrc { get; set; }
        public byte FragmentTexture2AlphaSrc { get; set; }
        public byte FragmentPrimitiveAlphaSrc { get; set; }

        public byte FragmentReflectionAlpha { get; set; }

        public uint UserFlag { get; set; }
        public uint UserSwitchFlag { get; set; }
        public Memory<byte> UserMacro { get; set; }
        public byte[] Unknown { get; set; }

        public void Read(BinaryFileReader reader)
        {
            FragmentShaderMode = reader.ReadByte();
            FragmentSoftEdge = reader.ReadByte();
            FragmentTextureMode = reader.ReadByte();
            FragmentColorMode = reader.ReadByte();
            FragmentAlphaMode = reader.ReadByte();
            UserSetting = reader.ReadByte();
            IsPrimitive = reader.ReadBoolean();
            FragmentTextureColorBlendType = reader.ReadByte();
            FragmentTextureAlphaBlendType = reader.ReadByte();
            FragmentPrimitiveColorBlendType = reader.ReadByte();
            FragmentPrimitiveAlphaBlendType = reader.ReadByte();

            FragmentTexture1ColorSrc = reader.ReadByte();
            FragmentTexture2ColorSrc = reader.ReadByte();
            FragmentPrimitiveColorSrc = reader.ReadByte();
            FragmentTexture1AlphaSrc = reader.ReadByte();
            FragmentTexture2AlphaSrc = reader.ReadByte();
            FragmentPrimitiveAlphaSrc = reader.ReadByte();

            FragmentReflectionAlpha = reader.ReadByte();
            reader.ReadUInt16();
            UserFlag = reader.ReadUInt32();
            UserSwitchFlag = reader.ReadUInt32();
            UserMacro = reader.ReadBytes(16);
            Unknown = reader.ReadBytes(4);
        }

        public void Write(BinaryFileWriter writer)
        {
        }

        void InitializeSimple(SimpleEmitterData data)
        {
            FragmentShaderMode = (byte)data.FragmentShaderMode;
            FragmentSoftEdge = data.FragmentSoftEdge;
            FragmentTextureMode = (byte)(data.Textures[1] != null && data.Textures[1].HasData ? 1 : 0);
            FragmentColorMode = (byte)data.ColorMode;
            FragmentAlphaMode = (byte)data.AlphaMode;
            UserSetting = data.ShaderUserSetting;
            UserFlag = data.ShaderUserFlag;
            UserSwitchFlag = data.ShaderUserSwitchFlag;
            FragmentReflectionAlpha = data.FragmentReflectionAlpha;
            IsPrimitive = data.MeshType == MeshType.Primitive;
            FragmentTextureColorBlendType = (byte)data.FragmentTextureColorBlendType;
            FragmentTextureAlphaBlendType = (byte)data.FragmentTextureAlphaBlendType;
            FragmentPrimitiveColorBlendType = (byte)data.FragmentPrimitiveColorBlendType;
            FragmentPrimitiveAlphaBlendType = (byte)data.FragmentPrimitiveAlphaBlendType;

            FragmentTexture1ColorSrc =  (byte)(data.Flags >> 11 & 1);
            FragmentTexture2ColorSrc =  (byte)(data.Flags >> 12 & 1);
            FragmentPrimitiveColorSrc = (byte)(data.Flags >> 13 & 1);
            FragmentTexture1AlphaSrc =  (byte)(data.Flags >> 14 & 1);
            FragmentTexture2AlphaSrc =  (byte)(data.Flags >> 15 & 1);
            FragmentPrimitiveAlphaSrc = (byte)(data.Flags >> 16 & 1);
            UserMacro = new byte[16];
        }

        public void Initialize(SimpleEmitterData data)
        {
            InitializeSimple(data);
            UserMacro.Span[0] = 0;
        }

        public void Initialize(ChildData data, uint childFlags)
        {
            FragmentShaderMode = (byte)data.FragmentShaderMode;
            FragmentSoftEdge = data.FragmentSoftEdge;
            FragmentTextureMode = 0;
            FragmentColorMode = (byte)data.FragmentColorMode;
            FragmentAlphaMode = (byte)data.FragmentAlphaMode;
            UserSetting = (byte)data.ShaderUserSetting;
            UserFlag = data.ShaderUserFlag;
            UserSwitchFlag = data.ShaderUserSwitchFlag;
            FragmentReflectionAlpha = data.FragmentReflectionAlpha;
            FragmentTextureColorBlendType = 0;
            FragmentTextureAlphaBlendType = 0;
            IsPrimitive = data.MeshType == MeshType.Primitive;

            FragmentPrimitiveColorBlendType = (byte)data.primitiveColorBlend;
            FragmentPrimitiveAlphaBlendType = (byte)data.primitiveAlphaBlend;

            FragmentTexture1ColorSrc = (byte)(childFlags >> 17 & 1);
            FragmentTexture2ColorSrc = 0;
            FragmentPrimitiveColorSrc = (byte)(childFlags >> 18 & 1);
            FragmentTexture1AlphaSrc = (byte)(childFlags >> 19 & 1);
            FragmentTexture2AlphaSrc = 0;
            FragmentPrimitiveAlphaSrc = (byte)(childFlags >> 20 & 1);
        }

        public static bool operator ==(FragmentShaderKey c1, FragmentShaderKey c2)
        {
            return c1.Equals(c2);
        }

        public static bool operator !=(FragmentShaderKey c1, FragmentShaderKey c2)
        {
            return !c1.Equals(c2);
        }

        public bool Equals(FragmentShaderKey other)
        {
            return (FragmentShaderMode == other.FragmentShaderMode
               && FragmentSoftEdge == other.FragmentSoftEdge
               && FragmentTextureMode == other.FragmentTextureMode
               && FragmentColorMode == other.FragmentColorMode
               && FragmentAlphaMode == other.FragmentAlphaMode
               && UserSetting == other.UserSetting
               && IsPrimitive == other.IsPrimitive
               && FragmentTextureColorBlendType == other.FragmentTextureColorBlendType
               && FragmentTextureAlphaBlendType == other.FragmentTextureAlphaBlendType
               && FragmentPrimitiveColorBlendType == other.FragmentPrimitiveColorBlendType
               && FragmentPrimitiveAlphaBlendType == other.FragmentPrimitiveAlphaBlendType
               && FragmentTexture1ColorSrc == other.FragmentTexture1ColorSrc
               && FragmentTexture2ColorSrc == other.FragmentTexture2ColorSrc
               && FragmentPrimitiveColorSrc == other.FragmentPrimitiveColorSrc
               && FragmentTexture1AlphaSrc == other.FragmentTexture1AlphaSrc
               && FragmentTexture2AlphaSrc == other.FragmentTexture2AlphaSrc
               && FragmentPrimitiveAlphaSrc == other.FragmentPrimitiveAlphaSrc
               && FragmentReflectionAlpha == other.FragmentReflectionAlpha
               && UserFlag == other.UserFlag
               && UserSwitchFlag == other.UserSwitchFlag
               /*&& UserFlag.Equals(other.UserFlag)*/); //Todo comparing macros doesn't work?
        }
    }
}

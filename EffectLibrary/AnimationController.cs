﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Toolbox.Core.Animations;

namespace EffectLibrary
{
    /// <summary>
    /// Represents an animation controller to globally handle particle playback.
    /// </summary>
    public class AnimationController : STAnimation
    {
        public ParticleRenderer ParticleRenderer;

        public AnimationController()
        {
            //Set a default framecount
            FrameCount = 120;
            //Play at 60fps
            FrameRate = 60;
            Loop = false;
        }

        public void LoadRender(ParticleRenderer renderer) {
            ParticleRenderer = renderer;
        }

        public void UpdateFramecount(List<IEmitterSet> emitterSets)
        {
            if (ParticleSettings.UseFixedFrameCount) {
                FrameCount = ParticleSettings.FixedFrameCount;
                return;
            }

            FrameCount = 0;
            foreach (var emitterSet in emitterSets) {
                foreach (SimpleEmitterData emitter in emitterSet.EmitterList) {
                    FrameCount = MathF.Max(FrameCount, emitter.MaxLifespan);
                    if (emitter.MaxLifespan == 2147483647)
                    {
                        FrameCount = 100000;
                        return;
                    }
                }
            }
        }

        public override void NextFrame()
        {
            base.NextFrame();

            ParticleRenderer.OnNextFrame(this.Frame);
        }
    }
}

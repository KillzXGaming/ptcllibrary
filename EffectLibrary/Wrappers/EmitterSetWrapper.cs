﻿using System;
using System.Collections.Generic;
using System.Text;
using Toolbox.Core;
using Toolbox.Core.Animations;
using CafeStudio.UI;
using EffectLibrary.UI;
using OpenTK;

namespace EffectLibrary
{
    public class EmitterSetWrapper : IPropertyUI, IAnimationContainer
    {
        public AnimationController AnimationController = new AnimationController();
        public IEnumerable<STAnimation> AnimationList => new List<STAnimation>() { AnimationController };

        public Type GetTypeUI() => typeof(EmitterSetGUI);

        public IEmitterSet EmitterSetData { get; set; }

        public Vector4 Color { get; set; } = Vector4.One;

        public Vector3 AddVelocity { get; set; }
        public float AllDirectionVelocity { get; set; }
        public float DirectionalVelocity { get; set; }
        public float DirectionalVelocityRandom { get; set; }

        public STGenericModel ModelAttachment;
        public STBone BoneAttachment;
        public Matrix4 ParentMatrix = Matrix4.Identity;

        //SRT position preview
        public Vector3 Position { get; set; }
        public Vector3 Rotation { get; set; }
        public Vector3 Scale { get; set; } = new Vector3(1);

        public Matrix4 Matrix4 { get; set; }

        public Vector3 RotationDegrees
        {
            get { return Rotation * Toolbox.Core.STMath.Rad2Deg; }
            set { Rotation = value * Toolbox.Core.STMath.Deg2Rad; }
        }

        public void UpdateHandle()
        {
            var handle = EmitterSetData.EmitterSetHandle;
            handle.color = this.Color;
            handle.addVelocity = this.AddVelocity;
            handle.allDirVel = this.AllDirectionVelocity;
            handle.dirVel = this.DirectionalVelocity;
            handle.dirVelRandom = this.DirectionalVelocityRandom;
        }

        public void UpdateMatrix()
        {
            var handle = EmitterSetData.EmitterSetHandle;

            var positionMat = Matrix4.CreateTranslation(Position);
            var rotationMat = Matrix4.CreateRotationX(Rotation.X) *
                              Matrix4.CreateRotationY(Rotation.Y) *
                              Matrix4.CreateRotationZ(Rotation.Z);
            var scaleMat = Matrix4.CreateScale(Scale);
            EmitterSetData.MatrixSRT = ParentMatrix * (scaleMat * rotationMat * positionMat);
            if (BoneAttachment != null)
                EmitterSetData.MatrixSRT = BoneAttachment.Transform * EmitterSetData.MatrixSRT;

            handle.SetMatrix(EmitterSetData.MatrixSRT);
        }

        public void OnLoadUI(object uiInstance)
        {
            if (EmitterSetData.EmitterSetHandle == null)
                return;

            var editor = (EmitterSetGUI)uiInstance;
            editor.OnLoad(this);
        }

        public void OnRenderUI(object uiInstance)
        {
            if (EmitterSetData.EmitterSetHandle == null)
                return;

            var editor = (EmitterSetGUI)uiInstance;
            editor.LoadEditor(this);
        }

        public EmitterSetWrapper(IEmitterSet emitterSet) {
            EmitterSetData = emitterSet;
        }
    }
}

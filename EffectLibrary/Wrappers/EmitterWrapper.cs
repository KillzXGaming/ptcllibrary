﻿using System;
using System.Collections.Generic;
using System.Text;
using Toolbox.Core;
using Toolbox.Core.ViewModels;
using CafeStudio.UI;
using EffectLibrary.UI;

namespace EffectLibrary
{
    public class EmitterWrapper : IPropertyUI
    {
        public Type GetTypeUI() => typeof(EmitterGUI);

        public IEmitter EmitterData { get; set; }

        public void OnLoadUI(object uiInstance)
        {
            var editor = (EmitterGUI)uiInstance;
            editor.OnLoad(this);
        }

        public void OnRenderUI(object uiInstance)
        {
            var editor = (EmitterGUI)uiInstance;
            editor.LoadEditor(this);
        }

        public EmitterWrapper(IEmitter emitter) {
            EmitterData = emitter;
        }
    }
}

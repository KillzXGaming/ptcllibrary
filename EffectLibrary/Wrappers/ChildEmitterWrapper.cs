﻿using System;
using System.Collections.Generic;
using System.Text;
using Toolbox.Core;
using Toolbox.Core.ViewModels;
using CafeStudio.UI;
using EffectLibrary.UI;

namespace EffectLibrary
{
    public class ChildEmitterWrapper : IPropertyUI
    {
        public Type GetTypeUI() => typeof(EmitterChildGUI);

        public ChildData ChildData { get; set; }
        public ComplexEmitterData ComplexEmitterData { get; set; }

        public void OnLoadUI(object uiInstance)
        {
            var editor = (EmitterChildGUI)uiInstance;
            editor.OnLoad(ChildData);
        }

        public void OnRenderUI(object uiInstance)
        {
            var editor = (EmitterChildGUI)uiInstance;
            editor.LoadEditor(ComplexEmitterData, ChildData);
        }

        public ChildEmitterWrapper(ComplexEmitterData emitter, ChildData data) {
            ComplexEmitterData = emitter;
            ChildData = data;
        }
    }
}

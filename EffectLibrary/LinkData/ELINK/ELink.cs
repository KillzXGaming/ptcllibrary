﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Toolbox.Core.IO;
using Newtonsoft.Json;

namespace EffectLibrary.WiiU
{
    public class ELink
    {
        public Dictionary<string, EffectList> Effects = new Dictionary<string, EffectList>();

        public ELink(string fileName)
        {
            using (var reader = new FileReader(fileName)) {
                Read(reader);
            }
        }

        public ELink(Stream stream)
        {
            using (var reader = new FileReader(stream))
            {
                Read(reader);
            }
        }

        void Read(FileReader reader)
        {
            reader.ByteOrder = Syroot.BinaryData.ByteOrder.BigEndian;
            reader.ReadSignature(4, "eflk");
            uint Version = reader.ReadUInt32();
            uint EmitterCount = reader.ReadUInt32();
            uint StringTableOffset = reader.ReadUInt32();

            for (int i = 0; i < EmitterCount; i++)
            {
                uint dataOffset = reader.ReadUInt32();
                uint nameOffset = reader.ReadUInt32();

                EffectList effectList = new EffectList();

                using (reader.TemporarySeek(StringTableOffset + nameOffset, SeekOrigin.Begin)) {
                    effectList.Name = reader.ReadZeroTerminatedString(Encoding.UTF8);
                }
                using (reader.TemporarySeek(dataOffset, SeekOrigin.Begin)) {
                    effectList.Read(reader);
                }

                Effects.Add(effectList.Name, effectList);
            }
        }

        public void Save(string fileName)
        {
            string json = JsonConvert.SerializeObject(this, Formatting.Indented);
            File.WriteAllText(fileName, json);
        }
    }

    public class EffectList
    {
        public string Name { get; set; }

        [JsonProperty(ItemConverterType = typeof(NoFormattingConverter))]
        public Dictionary<string, EmitterSetParam> EmitterSetParams = new Dictionary<string, EmitterSetParam>();
        [JsonProperty(ItemConverterType = typeof(NoFormattingConverter))]
        public Dictionary<string, EffectResource> EffectResources = new Dictionary<string, EffectResource>();
        [JsonProperty(ItemConverterType = typeof(NoFormattingConverter))]
        public Dictionary<string, EffectInstance> EffectInstances = new Dictionary<string, EffectInstance>();
        [JsonProperty(ItemConverterType = typeof(NoFormattingConverter))]
        public Dictionary<string, EmitterActionLink> EmitterActions = new Dictionary<string, EmitterActionLink>();

        [JsonIgnore]
        public List<EmitterIndexLink> EmitterIndexLinks = new List<EmitterIndexLink>();

        public void Read(FileReader reader)
        {
            long pos = reader.Position;

            uint unk = reader.ReadUInt32(); //9
            uint numEmitterParam = reader.ReadUInt32(); 
            uint numInstances = reader.ReadUInt32(); 
            uint numEmitterSet = reader.ReadUInt32(); 
            uint numAction = reader.ReadUInt32(); 
            uint stringTablePos = (uint)pos + reader.ReadUInt32();
            uint unk2 = reader.ReadUInt32(); //4

            for (int i = 0; i < numEmitterParam; i++)
            {
                var emitterSet = new EmitterSetParam(reader, stringTablePos);
                EmitterSetParams.Add(emitterSet.Name + $"_set{i}", emitterSet);
            }
            for (int i = 0; i < numEmitterParam; i++)
                EmitterIndexLinks.Add(new EmitterIndexLink(reader));
            for (int i = 0; i < numEmitterSet; i++)
            {
                var emitterRes = new EffectResource(reader, EmitterSetParams.Values.ToList(), stringTablePos);
                EffectResources.Add(emitterRes.Name + $"_res{i}", emitterRes);
            }
            for (int i = 0; i < numInstances; i++)
            {
                var effectInstance = new EffectInstance(reader, stringTablePos);
                EffectInstances.Add(effectInstance.Name + $"_instance{i}", effectInstance);
            }
            for (int i = 0; i < numAction; i++)
            {
                var effectAction = new EmitterActionLink(reader, EffectInstances.Values.ToList(), stringTablePos);
                EmitterActions.Add(effectAction.Name + $"_action{i}", effectAction);
            }
        }

        public class EmitterSetParam
        {
            public string Name { get; set; }

            public int Index { get; set; }

            public float Scale { get; set; }

            public float[] Position { get; set; }

            public float[] Rotate { get; set; }

            public float[] RGBA { get; set; }

            [JsonIgnore]
            public float Unknown { get; set; }
            [JsonIgnore]
            public uint[] Unknowns2 { get; set; }
            [JsonIgnore]
            public uint[] Unknowns3 { get; set; }
            [JsonIgnore]
            public float Unknowns4 { get; set; }

            public EmitterSetParam(FileReader reader, uint stringTableOffset)
            {
                Index = reader.ReadInt32();
                Unknowns2 = reader.ReadUInt32s(7);
                Name = ReadName(reader, stringTableOffset);
                Unknowns3 = reader.ReadUInt32s(5);
                Unknowns4 = reader.ReadSingle();
                Scale = reader.ReadSingle();
                Position = reader.ReadSingles(3);
                Rotate = reader.ReadSingles(3);
                RGBA = reader.ReadSingles(4);
            }
        }

        public class EmitterIndexLink
        {
            public int Index1 { get; set; }
            public int Index2 { get; set; }

            public uint Value1 { get; set; }
            public uint Value2 { get; set; }

            public EmitterIndexLink(FileReader reader)
            {
                Index1 = reader.ReadInt32();
                Value1 = reader.ReadUInt32();
                Index2 = reader.ReadInt32();
                Value2 = reader.ReadUInt32();
            }
        }

        public class EffectResource
        {
            public string Name { get; set; }

            ushort StartEmitterSetIndex { get; set; }
            ushort EndEmitterSetIndex { get; set; }

            public List<string> EmitterSets { get; set; } = new List<string>();

            public EffectResource(FileReader reader, List<EmitterSetParam> emitterSets, uint stringTableOffset)
            {
                Name = ReadName(reader, stringTableOffset);
                reader.ReadUInt32s(3); //always 0
                //Indices for which emitter sets to use (start and end index)
                StartEmitterSetIndex = reader.ReadUInt16();
                EndEmitterSetIndex = reader.ReadUInt16();

                EmitterSets.Clear();
                for (int i= StartEmitterSetIndex; i < emitterSets.Count; i++)
                {
                    EmitterSets.Add(emitterSets[i].Name);

                    if (i == EndEmitterSetIndex)
                        break;
                }
            }
        }

        public class EffectInstance
        {
            public string Name { get; set; }
            public string BoneName { get; set; }

            public uint Duration { get; set; }

            public uint Unknown { get; set; }
            public uint Unknown2 { get; set; }
            public uint Unknown3 { get; set; }
            public uint Unknown4 { get; set; }

            public uint[] Unknowns5 { get; set; }

            public EffectInstance(FileReader reader, uint stringTableOffset)
            {
                uint index = reader.ReadUInt32();
                Unknown = reader.ReadUInt32();
                Name = ReadName(reader, stringTableOffset);
                Unknown2 = reader.ReadUInt32();
                Unknown3 = reader.ReadUInt32();
                Duration = reader.ReadUInt32();
                Unknown4 = reader.ReadUInt32();
                BoneName = ReadName(reader, stringTableOffset);
                Unknowns5 = reader.ReadUInt32s(7);
            }
        }

        public class EmitterActionLink
        {
            public string Name { get; set; }

            ushort InstanceStartIndex { get; set; }
            ushort InstanceEndIndex { get; set; }

            public List<string> Instances { get; set; } = new List<string>();

            public EmitterActionLink(FileReader reader, List<EffectInstance> instances, uint stringTableOffset)
            {
                Name = ReadName(reader, stringTableOffset);
                reader.ReadUInt32(); //always 0
                InstanceStartIndex = reader.ReadUInt16();
                InstanceEndIndex = reader.ReadUInt16();

                Instances.Clear();
                for (int i = InstanceStartIndex; i < instances.Count; i++)
                {
                    Instances.Add(instances[i].Name);

                    if (i == InstanceEndIndex)
                        break;
                }
            }
        }

        static string ReadName(FileReader reader, uint stringTableOffset)
        {
            uint offset = reader.ReadUInt32();
            using (reader.TemporarySeek(stringTableOffset + offset, SeekOrigin.Begin)) {
                return reader.ReadZeroTerminatedString();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Toolbox.Core.IO;

namespace EffectLibrary.Switch
{
    public class UserDataTable
    {
        public uint[] CRC32Hashes;
        public uint[] ExRegionOffsets;

        public void Read(FileReader reader, int EntryCount)
        {
            CRC32Hashes = reader.ReadUInt32s(EntryCount);
            ExRegionOffsets = reader.ReadUInt32s(EntryCount);
        }
    }
}
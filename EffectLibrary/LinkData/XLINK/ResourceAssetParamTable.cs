﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Toolbox.Core.IO;

namespace EffectLibrary.Switch
{
    public class ResourceAssetParamTable    
    {
        public uint Mask;

        public uint FirstReference;
        public uint SecondReference;
        public uint ThirdReference;

        public void Read(FileReader reader)
        {
            Mask = reader.ReadUInt32();
            if ((Mask & 1) != 0)
                FirstReference = reader.ReadUInt32();
            if ((Mask & 2) != 0)
                SecondReference = reader.ReadUInt32();
            if ((Mask & 4) != 0)
                ThirdReference = reader.ReadUInt32();
        }
    }
}

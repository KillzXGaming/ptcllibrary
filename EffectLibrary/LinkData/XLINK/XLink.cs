﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Toolbox.Core.IO;

namespace EffectLibrary.Switch
{
    public class XLink
    {
        public ushort ByteOrderMark;

        public UserDataTable UserDataTable;
        public ParamDefineTable ParamDefineTable;

        public List<ResourceAssetParamTable> ResourceAssetParamTables = new List<ResourceAssetParamTable>();
        public List<TriggerOverwriteParamTable> TriggerOverwriteParamTables = new List<TriggerOverwriteParamTable>();

        public List<LocalNameProperty> LocalNameProperties = new List<LocalNameProperty>();
        public List<LocalNameProperty> LocalNameEnumProperties = new List<LocalNameProperty>();

        public List<ConditionTable> ConditionTables = new List<ConditionTable>();

        public uint[] DirectValues { get; set; }

        internal uint nameTablePos;

        public XLink(string fileName) {
            using (var reader = new FileReader(fileName)) {
                Read(reader);
            }
        }

        public XLink(Stream stream)
        {
            using (var reader = new FileReader(stream)) {
                Read(reader);
            }
        }

        void Read(FileReader reader)
        {
            reader.ByteOrder = Syroot.BinaryData.ByteOrder.LittleEndian;
            reader.ReadSignature(4, "XLNK");
            uint FileSize = reader.ReadUInt32();
            uint Version = reader.ReadUInt32();
            uint numResParam = reader.ReadUInt32();
            uint numResAssetParam = reader.ReadUInt32();
            uint numResTriggerOverwriteParam = reader.ReadUInt32();
            uint triggerOverwriteParamTablePos = reader.ReadUInt32();
            uint localPropertyNameRefTablePos = reader.ReadUInt32();
            uint numLocalPropertyNameRefTable = reader.ReadUInt32();
            uint numLocalPropertyEnumNameRefTable = reader.ReadUInt32();
            uint numDirectValueTable = reader.ReadUInt32();
            uint numRandomTable = reader.ReadUInt32();
            uint numCurveTable = reader.ReadUInt32();
            uint numCurvePointTable = reader.ReadUInt32();
            uint exRegionPos = reader.ReadUInt32();
            uint numUser = reader.ReadUInt32();
            uint conditionTablePos = reader.ReadUInt32();
            nameTablePos = reader.ReadUInt32();

            UserDataTable = new UserDataTable();
            UserDataTable.Read(reader, (int)numUser);

            ParamDefineTable = new ParamDefineTable();
            ParamDefineTable.Read(reader);

            for (int i = 0; i < numResAssetParam; i++)
            {
                var resAssetsParam = new ResourceAssetParamTable();
                resAssetsParam.Read(reader);
                ResourceAssetParamTables.Add(resAssetsParam);
            }

            reader.SeekBegin(triggerOverwriteParamTablePos);
            for (int i = 0; i < numResTriggerOverwriteParam; i++)
            {
                var triggerOverwriteParamTbl = new TriggerOverwriteParamTable();
                triggerOverwriteParamTbl.Read(reader);
                TriggerOverwriteParamTables.Add(triggerOverwriteParamTbl);
            }

            reader.SeekBegin(localPropertyNameRefTablePos);
            for (int i = 0; i < numLocalPropertyNameRefTable; i++)
            {
                var localNameProp = new LocalNameProperty();
                localNameProp.Read(reader, nameTablePos);
                LocalNameProperties.Add(localNameProp);

                Console.WriteLine($"Prob {localNameProp.Name}");
            }

            for (int i = 0; i < numLocalPropertyEnumNameRefTable; i++)
            {
                var localNameProp = new LocalNameProperty();
                localNameProp.Read(reader, nameTablePos);
                LocalNameEnumProperties.Add(localNameProp);

                Console.WriteLine($"Enum {localNameProp.Name}");
            }

            DirectValues = reader.ReadUInt32s((int)numDirectValueTable);

            reader.SeekBegin(conditionTablePos);
            while (reader.Position < nameTablePos)
            {
                var condTable = new ConditionTable();
                condTable.Read(reader, nameTablePos);
                ConditionTables.Add(condTable);
            }

            foreach (var condTable in ConditionTables.OrderBy(x => x.Value))
            {
                if (condTable.PropertyType == 0)
                    Console.WriteLine($"condTable {condTable.ParentContainerType} {condTable.Value} {LocalNameEnumProperties[condTable.LocalEnumIndex]}");
                else
                    Console.WriteLine($"condTable {condTable.ParentContainerType} {condTable.Value}");


            }
        }
    }
}

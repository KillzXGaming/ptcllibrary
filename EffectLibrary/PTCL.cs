﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Threading.Tasks;
using Syroot.BinaryData;
using Toolbox.Core;
using Toolbox.Core.IO;
using Toolbox.Core.ViewModels;
using Toolbox.Core.Animations;
using GLFrameworkEngine;

namespace EffectLibrary
{
    public class PTCL : NodeBase, IFileFormat, IRenderableFile
    {
        public bool CanSave { get; set; } = true;

        public string[] Description { get; set; } = new string[] { "PTCL" };
        public string[] Extension { get; set; } = new string[] { "*.ptcl" };

        public File_Info FileInfo { get; set; }

        public bool CanAddFiles { get; set; } = false;
        public bool CanRenameFiles { get; set; } = false;
        public bool CanReplaceFiles { get; set; } = false;
        public bool CanDeleteFiles { get; set; } = false;

        public List<ArchiveFileInfo> files = new List<ArchiveFileInfo>();
        public IEnumerable<ArchiveFileInfo> Files => files;

        public bool Identify(File_Info fileInfo, System.IO.Stream stream)
        {
            using (var reader = new FileReader(stream, true)) {
                return reader.CheckSignature(4, "EFTF") ||
                       reader.CheckSignature(4, "SPBD") ||
                       reader.CheckSignature(4, "VFXB");
            }
        }

        public GenericRenderer Renderer { get; set; }

        public IFileHeader FileHeader { get; set; }

        public bool IsSwitch { get; private set; }

        public PTCL() { }

        public PTCL(string fileName)
        {
            FileInfo = new File_Info();
            using (var stream = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.Read)) {
                Load(stream);
            }
        }

        public PTCL(Stream stream) {
            Load(stream);
        }

        public void Load(System.IO.Stream stream) {
            IsSwitch = IsSwitchBinary(stream);
            FileInfo.KeepOpen = true;
            Tag = this;

            using (var fileReader = new BinaryFileReader(this, stream, IsSwitch)) {
                fileReader.Execute();
            }

            if (Runtime.OpenTKInitialized)
            {
                Renderer = new ParticleRenderer(FileHeader);
            }
            ReloadWrappers();
        }

        public void ReloadWrappers()
        {
            if (FileInfo == null)
                return;

            Header = this.FileInfo.FileName;

            var emitterFolder = new NodeBase("Emitter Sets");
            this.AddChild(emitterFolder);

            foreach (var em in FileHeader.EmitterSetList)
                em.IsVisible = false;

            foreach (var emitterSet in FileHeader.EmitterSetList)
            {
                var emitterSetNode = new NodeBase(emitterSet.Name);
                var emitterSetWrapper = new EmitterSetWrapper(emitterSet);
                emitterSetWrapper.AnimationController.LoadRender(this.Renderer as ParticleRenderer);

                emitterSetNode.Tag = emitterSetWrapper;
                emitterFolder.AddChild(emitterSetNode);

                emitterSetNode.OnSelected += delegate
                {
                    if (emitterSet.IsVisible)
                        return;

                    var emitterSets = FileHeader.EmitterSetList.ToList();
                    int index = emitterSets.IndexOf(emitterSet);
                    byte groupID = 0;

                    var renderer = (ParticleRenderer)Renderer;
                    renderer.KillEmitterSet(groupID);
                    renderer.SpawnEmitterSet(index, groupID);

                    emitterSetWrapper.AnimationController.UpdateFramecount(new List<IEmitterSet>() { emitterSet });

                    foreach (var em in FileHeader.EmitterSetList)
                        em.IsVisible = false;

                    emitterSet.IsVisible = true;
                    foreach (var em in emitterSet.EmitterList)
                        em.IsVisible = true;
                };

                foreach (var emitter in emitterSet.EmitterList) {
                    var emitterNode = new NodeBase(emitter.Name);
                    emitterNode.Tag = new EmitterWrapper(emitter);
                    emitterSetNode.AddChild(emitterNode);

                    emitterNode.OnSelected += delegate
                    {
                        foreach (var em in emitterSet.EmitterList)
                            em.IsVisible = false;

                        emitter.IsVisible = true;
                    };
                    if (emitter is ComplexEmitterData && (((ComplexEmitterData)emitter).childFlags & 1) == 1)
                    {
                        var child = ((ComplexEmitterData)emitter).ChildData;

                        var cNode = new NodeBase(child.Name);
                        cNode.Tag = new ChildEmitterWrapper((ComplexEmitterData)emitter, child);
                        emitterNode.AddChild(cNode);
                    }
                }
            }
        }

        public void Save(System.IO.Stream stream)
        {
            using (var fileWriter = new BinaryFileWriter(this, stream, IsSwitch)) {
                fileWriter.Execute();
            }
        }

        public void Save(string fileName)
        {
            using (var stream = new FileStream(fileName, FileMode.Create, FileAccess.Write, FileShare.Write)) {
                Save(stream);
            }
        }

        internal void Read(BinaryFileReader reader)
        {
            if (reader.IsSwitch)
                FileHeader = reader.ReadSection<Switch.VFXB_Header>();
            else
                FileHeader = reader.ReadSection<WiiU.PTCL_Header>();
        }

        internal void Write(BinaryFileWriter writer) {
            writer.WriteSection((EffectLibrary.IResData)FileHeader);
        }

        public static bool IsSwitchBinary(Stream stream)
        {
            using (var reader = new BinaryDataReader(stream, true))
            {
                reader.ByteOrder = ByteOrder.LittleEndian;

                reader.Seek(4, SeekOrigin.Begin);
                uint paddingCheck = reader.ReadUInt32();
                reader.Position = 0;

                return paddingCheck == 0x20202020;
            }
        }
    }
}

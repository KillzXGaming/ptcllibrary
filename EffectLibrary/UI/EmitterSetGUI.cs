﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ImGuiNET;
using CafeStudio.UI;

namespace EffectLibrary.UI
{
    class EmitterSetGUI
    {
        public bool updateEmitterInit;

        public void OnLoad(EmitterSetWrapper wrapper)
        {
        }

        public void LoadEditor(EmitterSetWrapper wrapper)
        {
            var data = wrapper.EmitterSetData.EmitterSetHandle;

            ImGui.BeginTabBar("Menu1");

            if (ImguiCustomWidgets.BeginTab("Menu1", "Emitter Set Data (Preview Only!!)"))
            {
                DrawEmitterSetPropertyUI(wrapper, data);
                ImGui.EndTabItem();
            }

            ImGui.EndTabBar();

            if (updateEmitterInit) {
                ParticleRenderer.RespawnEmitterSet = true;
            }
        }

        private void DrawEmitterSetPropertyUI(EmitterSetWrapper wrapper, EmitterSet data)
        {
            if (ImGui.Button("Start Fade")) {
                data.doFade = true;
            }
     
            bool transform = false;
            bool updateHandle = false;

            if (ImGui.CollapsingHeader("Emitter Set Color", ImGuiTreeNodeFlags.DefaultOpen))
            {
                updateHandle |= ImGuiHelper.InputTKVector4Color4("Color", wrapper, "Color", ImGuiColorEditFlags.HDR | ImGuiColorEditFlags.Float);
            }

            if (ImGui.CollapsingHeader("Emitter Set Velocity", ImGuiTreeNodeFlags.DefaultOpen))
            {
                updateHandle |= ImGuiHelper.InputTKVector3("AddVelocity", wrapper, "AddVelocity");
                updateHandle |= ImGuiHelper.InputFromFloat("All Direction Velocity", wrapper, "AllDirectionVelocity");
                updateHandle |= ImGuiHelper.InputFromFloat("Directional Velocity", wrapper, "DirectionalVelocity");
                updateHandle |= ImGuiHelper.InputFromFloat("Directional Velocity Random", wrapper, "DirectionalVelocityRandom");
            }
            if (ImGui.CollapsingHeader("Emitter Set SRT", ImGuiTreeNodeFlags.DefaultOpen))
            {
                transform |= ImGuiHelper.InputTKVector3("Position", wrapper, "Position");
                transform |= ImGuiHelper.InputTKVector3("Rotation", wrapper, "RotationDegrees");
                transform |= ImGuiHelper.InputTKVector3("Scale", wrapper, "Scale");
            }

            if (ImGui.CollapsingHeader("Attach Model", ImGuiTreeNodeFlags.DefaultOpen))
            {
                string modelName = wrapper.ModelAttachment != null ? wrapper.ModelAttachment.Name : "";

                if (ImGui.BeginCombo("Model Attachment", modelName))
                {
                    foreach (var render in GLFrameworkEngine.DataCache.ModelCache.Values)
                    {
                        foreach (var model in render.Models)
                        {
                            bool selected = model.ModelData == wrapper.ModelAttachment;

                            if (ImGui.Selectable(model.Name, selected)) {
                                wrapper.ModelAttachment = model.ModelData;
                                render.Transform.TransformUpdated += delegate
                                {
                                    wrapper.ParentMatrix = render.Transform.TransformMatrix;
                                    wrapper.UpdateMatrix();
                                };
                            }

                            if (selected)
                                ImGui.SetItemDefaultFocus();
                        }
                    }
                    ImGui.EndCombo();
                }

                if (wrapper.ModelAttachment != null)
                {
                    string boneName = wrapper.BoneAttachment != null ? wrapper.BoneAttachment.Name : "";

                    if (ImGui.BeginCombo("Bone Attachment", boneName))
                    {
                        foreach (var bone in wrapper.ModelAttachment.Skeleton.Bones)
                        {
                            bool selected = bone == wrapper.BoneAttachment;

                            if (ImGui.Selectable(bone.Name, selected)) {
                                wrapper.BoneAttachment = bone;
                                bone.TransformUpdated += delegate
                                {
                                    wrapper.UpdateMatrix();
                                };
                            }

                            if (selected)
                                ImGui.SetItemDefaultFocus();
                        }
                        ImGui.EndCombo();
                    }
                }
            }

            if (transform) {
                wrapper.UpdateMatrix();
            }
            if (updateHandle) {
                wrapper.UpdateHandle();
            }
        }
    }
}

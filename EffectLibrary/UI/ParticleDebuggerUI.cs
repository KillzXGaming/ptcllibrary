﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ImGuiNET;
using EffectLibrary.WiiU;

namespace EffectLibrary
{
    public class ParticleDebuggerUI
    {
        public ParticleRenderer ParticleRenderer;
        bool open = true;

        int selectedParticleIndex = -1;
        int selectedEmitterIndex = -1;
        string selectedEmitterName = "";
        PtclInstance selectedPtcl;
        PtclInstance selectedChildPtcl;

        public ParticleDebuggerUI(ParticleRenderer render) {
            ParticleRenderer = render;
        }

        public void Render()
        {
            if (!ImGui.Begin("Particle Debugger", ref open, ImGuiWindowFlags.NoDocking))
                return;

            ImGui.InputFloat("Emission Speed", ref ParticleSettings.EmissionSpeed);
            ImGui.Checkbox("Use Fixed Frame Count", ref ParticleSettings.UseFixedFrameCount);
            if (ParticleSettings.UseFixedFrameCount) {
                ImGui.SameLine();
                ImGui.PushItemWidth(100);
                ImGui.InputFloat("Frame Count", ref ParticleSettings.FixedFrameCount);
                ImGui.PopItemWidth();
            }
            ImGui.Checkbox("Respawn At Start", ref ParticleSettings.RespawnAtStartFrame);

            var system = EffectSystem.ActiveSystem;
            for (int i = 0; i < ParticleRenderer.EmitterSetHandles.Length; i++)
            {
                if (ParticleRenderer.EmitterSetHandles[i] == null) //Skip uncreated handles
                    continue;

                var emitterSet = ParticleRenderer.EmitterSetHandles[i].emitterSet;
                if (emitterSet == null) //Set could not be spawned (possibly over the max set limit)
                    continue;

                if (ImGui.BeginCombo("Emitters", selectedEmitterName))
                {
                    for (int j = emitterSet.emitters.Length - 1; j >= 0; j--)
                    {
                        var emitter = emitterSet.emitters[j];
                        if (emitter == null) //Not all slots have active emitters so skip
                            continue;

                        bool isSelected = selectedEmitterIndex == j;
                        if (ImGui.Selectable(emitter.data.Name, isSelected))
                        {
                            selectedEmitterIndex = j;
                            selectedParticleIndex = -1;
                            selectedEmitterName = emitter.data.Name;
                        }

                        if (isSelected)
                            ImGui.SetItemDefaultFocus();
                    }
                    ImGui.EndCombo();
                }
        
                if (selectedEmitterIndex != -1) {
                    DrawDebugEmitterUI(emitterSet.emitters[selectedEmitterIndex]);
                }
                break;
            }

            ImGui.End();
        }

        private void DrawDebugEmitterUI(EmitterInstance emitter)
        {
            if (emitter == null)
                return;

            int particleIndex = 0;

            ImGui.Columns(2);

            if (ImGui.BeginChild("PARTICLE_LIST"))
            {
                for (PtclInstance ptcl = emitter.particleHead; ptcl != null; ptcl = ptcl.Next)
                {
                    if (ImGui.Selectable($"Particle {particleIndex}", selectedParticleIndex == particleIndex))
                    {
                        selectedParticleIndex = particleIndex;
                    }

                    if (selectedParticleIndex == particleIndex)
                    {
                        selectedPtcl = ptcl;
                        selectedChildPtcl = null;
                    }

                    particleIndex++;
                }
                for (PtclInstance ptcl = emitter.childParticleHead; ptcl != null; ptcl = ptcl.Next)
                {
                    if (ImGui.Selectable($"Child Particle {particleIndex}", selectedParticleIndex == particleIndex))
                    {
                        selectedParticleIndex = particleIndex;
                    }

                    if (selectedParticleIndex == particleIndex)
                    {
                        selectedChildPtcl = ptcl;
                        selectedPtcl = null;
                    }
                    particleIndex++;
                }
            }
            ImGui.EndChild();
            ImGui.NextColumn();

            if (ImGui.BeginChild("PARTICLE_DATA"))
            {
                if (selectedPtcl != null)
                {
                    if (selectedPtcl.stripe != null)
                        DrawStripePtclUI(selectedPtcl, selectedPtcl.emitter.shaderAvailableAttribFlg,
                            selectedPtcl.emitter.StaticUniformBlock, selectedPtcl.emitter.DynamicUniformBlock);
                    else
                        DrawPtclUI(selectedPtcl, selectedPtcl.emitter.shaderAvailableAttribFlg,
                            selectedPtcl.emitter.StaticUniformBlock, selectedPtcl.emitter.DynamicUniformBlock);
                }
                if (selectedChildPtcl != null)
                {
                    DrawPtclUI(selectedChildPtcl, selectedChildPtcl.emitter.childShaderAvailableAttribFlg,
                        selectedChildPtcl.emitter.ChildStaticUniformBlock, selectedChildPtcl.emitter.ChildDynamicUniformBlock);
                }
            }
            ImGui.EndChild();

            ImGui.NextColumn();
            ImGui.Columns(1);
        }

        private void DrawStripePtclUI(PtclInstance ptcl, ShaderAvailableAttrib shaderAvailableAttribFlg,
    EmitterStaticUniformBlock staticUniformBlock, EmitterDynamicUniformBlock dynamicUniformBlock)
        {
            var emitter = ptcl.emitter;

            if (ImGui.CollapsingHeader("Particle Info", ImGuiTreeNodeFlags.DefaultOpen))
            {
                ImGui.Text($"Counter {ptcl.counter} / {ptcl.lifespan}");
                ImGui.Text($"posDiff {ptcl.posDiff}");

                ImGui.Text($"velocity {ptcl.velocity}");
                ImGui.Text($"angularVelocity {ptcl.angularVelocity}");
                ImGui.Text($"fluctuationScale {ptcl.fluctuationScale}");
                ImGui.Text($"fluctuationAlpha {ptcl.fluctuationAlpha}");
            }
            if (ImGui.CollapsingHeader("Stripe Info", ImGuiTreeNodeFlags.DefaultOpen))
            {
                ImGui.Text($"Counter {ptcl.stripe.counter}");
                ImGui.Text($"queueCount {ptcl.stripe.queueCount}");
                ImGui.Text($"drawFirstVertex {ptcl.stripe.drawFirstVertex}");
                ImGui.Text($"numDraw {ptcl.stripe.numDraw}");
                ImGui.Text($"currentSliceDir {ptcl.stripe.currentSliceDir}");
            }

            if (emitter.StripeVertexBuffer == null)
                return;

            int index = 0;
            foreach (var vtx in emitter.StripeVertexBuffer)
            {
                if (ImGui.CollapsingHeader($"Attribute Buffer {index++}", ImGuiTreeNodeFlags.DefaultOpen))
                {
                    ImGui.Text($"pos {vtx.pos}");
                    ImGui.Text($"dir {vtx.dir}");
                    ImGui.Text($"outer {vtx.outer}");
                    ImGui.Text($"texCoord {vtx.texCoord}");
                }
            }
        }

        private void DrawPtclUI(PtclInstance ptcl, ShaderAvailableAttrib shaderAvailableAttribFlg,
            EmitterStaticUniformBlock staticUniformBlock, EmitterDynamicUniformBlock dynamicUniformBlock)
        {
            var buffer = ptcl.PtclAttributeBuffer;
            if (buffer != null && dynamicUniformBlock != null)
            {
                if (ImGui.CollapsingHeader("Particle Info", ImGuiTreeNodeFlags.DefaultOpen))
                {
                    ImGui.Text($"Counter {ptcl.counter} / {ptcl.lifespan}");
                    ImGui.Text($"posDiff {ptcl.posDiff}");

                    ImGui.Text($"velocity {ptcl.velocity}");
                    ImGui.Text($"angularVelocity {ptcl.angularVelocity}");
                    ImGui.Text($"fluctuationScale {ptcl.fluctuationScale}");
                    ImGui.Text($"fluctuationAlpha {ptcl.fluctuationAlpha}");
                }
                if (ImGui.CollapsingHeader("Attribute Buffer", ImGuiTreeNodeFlags.DefaultOpen))
                {
                    if (shaderAvailableAttribFlg.HasFlag(ShaderAvailableAttrib.WorldPos))
                        ImGui.Text($"WorldPosition {buffer.WorldPosition.Xyz}");
                    if (shaderAvailableAttribFlg.HasFlag(ShaderAvailableAttrib.WorldPosDif))
                        ImGui.Text($"WorldPositionDif {buffer.WorldPositionDif}");
                    if (shaderAvailableAttribFlg.HasFlag(ShaderAvailableAttrib.Color0))
                    {
                        ImGui.Text($"Color0 {buffer.Color0.Xyz}");
                        ImGui.Text($"Alpha0 {buffer.Color0.W}");
                    }
                    if (shaderAvailableAttribFlg.HasFlag(ShaderAvailableAttrib.Color1))
                    {
                        ImGui.Text($"Color1 {buffer.Color1.Xyz}");
                        ImGui.Text($"Alpha1 {buffer.Color1.W}");
                    }
                    if (shaderAvailableAttribFlg.HasFlag(ShaderAvailableAttrib.Scale))
                        ImGui.Text($"ScaleCenter {buffer.ScaleCenter}");
                    if (shaderAvailableAttribFlg.HasFlag(ShaderAvailableAttrib.Rot))
                        ImGui.Text($"Rotate {buffer.Rotate}");
                    if (shaderAvailableAttribFlg.HasFlag(ShaderAvailableAttrib.TexAnim))
                        ImGui.Text($"TextureAnim {buffer.TextureAnim}");
                    if (shaderAvailableAttribFlg.HasFlag(ShaderAvailableAttrib.SubTexAnim))
                        ImGui.Text($"SubTextureAnim {buffer.SubTextureAnim}");
                    if (shaderAvailableAttribFlg.HasFlag(ShaderAvailableAttrib.EmMat))
                    {
                        ImGui.Text($"EmissionMatrix[0] {buffer.EmissionMatrix[0]}");
                        ImGui.Text($"EmissionMatrix[1] {buffer.EmissionMatrix[1]}");
                        ImGui.Text($"EmissionMatrix[2] {buffer.EmissionMatrix[2]}");
                    }
                }
                if (ImGui.CollapsingHeader("Static Block", ImGuiTreeNodeFlags.DefaultOpen))
                {
                    var block = staticUniformBlock;
                    ImGui.Text($"UvScaleInit {block.UvScaleInit}");
                    ImGui.Text($"RotBasis {block.RotBasis}");
                    ImGui.Text($"ShaderParam {block.ShaderParam}");
                }
                if (ImGui.CollapsingHeader("Dynamic Block", ImGuiTreeNodeFlags.DefaultOpen))
                {
                    var block = dynamicUniformBlock;
                    ImGui.Text($"Color0 {block.Color0}");
                    ImGui.Text($"Color1 {block.Color1}");
                    ImGui.Text($"Param1 {block.Param1}");
                }
                if (ImGui.CollapsingHeader("View Block", ImGuiTreeNodeFlags.DefaultOpen))
                {
                    var block = EffectSystem.ActiveSystem.renderers[(int)CpuCore._1].viewUniformBlock;
                    ImGui.Text($"EyePosition {block.EyePosition}");
                    ImGui.Text($"EyeView {block.EyeView}");
                    ImGui.Text($"BillboardMatrix {block.BillboardMatrix}");
                    ImGui.Text($"ViewMatrix {block.ViewMatrix}");
                    ImGui.Text($"ProjectionMatrix {block.ProjectionMatrix}");
                    ImGui.Text($"ViewParam {block.ViewParam}");
                }
            }
        }
    }
}

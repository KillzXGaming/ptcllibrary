﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Threading.Tasks;

namespace EffectLibrary
{
    public class ColorKeyFrame
    {
        public OpenTK.Vector4 Color { get; set; }

        public float Ratio { get; set; }

        public int Time => (int)(Ratio * 100);

        public bool IsSelected { get; set; }

        public ColorKeyFrame(float ratio, OpenTK.Vector4 color)
        {
            Ratio = ratio;
            Color = color;
        }
    }
}

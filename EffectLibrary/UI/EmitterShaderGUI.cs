﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ImGuiNET;
using CafeStudio.UI;
using GLFrameworkEngine;
using Toolbox.Core;

namespace EffectLibrary.UI
{
    public class EmitterShaderGUI
    {
        EmitterInstance ActiveInstance = null;

        public void LoadEditor(EmitterWrapper emitter)
        {
            if (ActiveInstance == null)
            {
                foreach (var emitterSet in EffectSystem.ActiveSystem.emitterSets)
                {
                    if (emitterSet == null)
                        continue;

                    foreach (var emit in emitterSet.emitters)
                    {
                        if (emit != null && emit.data.Name == emitter.EmitterData.Name)
                        {
                            ActiveInstance = emit;
                            break;
                        }
                    }
                }
            }

            if (ActiveInstance != null)
                ShaderProgramViewer.Render(ActiveInstance.shader[0]);
        }
    }
}

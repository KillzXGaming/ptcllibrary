﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Numerics;
using ImGuiNET;
using CafeStudio.UI;
using Toolbox.Core;

namespace EffectLibrary.UI
{
    public class EmitterGUI
    {
        EmitterTextureGUI TextureGUI = new EmitterTextureGUI();
        EmitterColorUI ColorUI = new EmitterColorUI();
        EmitterShaderGUI ShaderGUI = new EmitterShaderGUI();
        EmitterAnimationEditor AnimationGUI = new EmitterAnimationEditor();

        bool updateEmitterInit;

        public void OnLoad(EmitterWrapper wrapper)
        {
            ColorUI.OnLoad(wrapper);
        }

        public void LoadEditor(EmitterWrapper emitter)
        {
            updateEmitterInit = false;

            ImGui.BeginTabBar("Menu1");

            if (ImguiCustomWidgets.BeginTab("Menu1", "Emitter Data"))
            {
                DrawEmitterPropertyUI(emitter);
                ImGui.EndTabItem();
            }
            if (ImguiCustomWidgets.BeginTab("Menu1", "Emission Data"))
            {
                DrawEmissionUI(emitter);
                ImGui.EndTabItem();
            }
            if (ImguiCustomWidgets.BeginTab("Menu1", "Particle Data"))
            {
                DrawParticlePropertyUI(emitter);
                ImGui.EndTabItem();
            }
            if (ImguiCustomWidgets.BeginTab("Menu1", "Texture Maps"))
            {
                TextureGUI.LoadEditor(emitter, ref updateEmitterInit);
                ImGui.EndTabItem();
            }
            if (ImguiCustomWidgets.BeginTab("Menu1", "Blending"))
            {
                DrawBlendUI(emitter);
                ImGui.EndTabItem();
            }
            if (ImguiCustomWidgets.BeginTab("Menu1", "Shader Data"))
            {
                ShaderGUI.LoadEditor(emitter);
                ImGui.EndTabItem();
            }
            if (ImguiCustomWidgets.BeginTab("Menu1", "Animation Data"))
            {
                AnimationGUI.LoadEditor((SimpleEmitterData)emitter.EmitterData);
                ImGui.EndTabItem();
            }

            
            ImGui.EndTabBar();

            if (updateEmitterInit) {
                ParticleRenderer.RespawnEmitterSet = true;
            }
        }

        public void DrawColorUI(EmitterWrapper emitter)
        {
            var data = emitter.EmitterData as SimpleEmitterData;

            ColorUI.DrawColorUI(data, ref updateEmitterInit);
        }

        public void DrawBlendUI(EmitterWrapper emitter)
        {
            var data = emitter.EmitterData as SimpleEmitterData;

            ImGuiHelper.ComboFromEnum<BlendType>($"Blend Type", data, "BlendType");
            ImGuiHelper.ComboFromEnum<DisplayFaceType>($"DisplayType", data, "DisplaySideType");

            if (EffectSystem.ActiveSystem.HasResourceFlags(FileResourceFlags.HasNearFarAlpha))
            {
                updateEmitterInit |= ImGui.InputFloat("Near Alpha Min", ref data.NearAlphaParameters.X);
                updateEmitterInit |= ImGui.InputFloat("Near Alpha Max", ref data.NearAlphaParameters.Y);

                updateEmitterInit |= ImGui.InputFloat("Far Alpha Min", ref data.NearAlphaParameters.Z);
                updateEmitterInit |= ImGui.InputFloat("Far Alpha Max", ref data.NearAlphaParameters.W);

                updateEmitterInit |= ImGui.InputFloat("Fresnel Alpha Min", ref data.FresnelAlphaMin);
                updateEmitterInit |= ImGui.InputFloat("Fresnel Alpha Max", ref data.FresnelAlphaMax);
            }
            bool hasSoftEdge = data.FragmentSoftEdge != 0;
            if (ImGui.Checkbox("Soft Edge", ref hasSoftEdge)) {
                data.FragmentSoftEdge = (byte)(hasSoftEdge ? 1 : 0);
            }

            if (data.FragmentSoftEdge != 0)
            {
                updateEmitterInit |= ImGuiHelper.InputFromFloat($"SoftEdgeFadeDistance", data, "FragmentSoftEdgeFadeDistance");
                updateEmitterInit |= ImGuiHelper.InputFromFloat($"SoftEdgeVolume", data, "FragmentSoftEdgeVolume");
            }
        }

        public void DrawParticlePropertyUI(EmitterWrapper emitter)
        {
            var data = emitter.EmitterData as SimpleEmitterData;
            if (ImGui.CollapsingHeader("Particle", ImGuiTreeNodeFlags.DefaultOpen))
            {
                ImGuiHelper.ComboFromEnum<PtclFollowType>($"Follow Type", data, "PtclFollowType");
                ImGuiHelper.ComboFromEnum<MeshType>($"Mesh Type", data, "MeshType");
                ImGuiHelper.ComboFromEnum<VertexTransformMode>($"Particle Type", data, "VertexTransformMode");
                ImGuiHelper.InputFromFloat($"Camera Offset", data, "CameraOffset");
                ImGuiHelper.InputTKVector2($"Rotation Pivot ", data, "RotationPivot");
            }
            if (ImGui.CollapsingHeader("Rotation", ImGuiTreeNodeFlags.DefaultOpen))
            {
                ImGuiHelper.ComboFromEnum<VertexRotationMode>($"Rotation Mode", data, "ParticleRotationMode");
                updateEmitterInit |= ImGuiHelper.InputTKVector3($"Rotate ", data, "ptclRotateDegrees");
                updateEmitterInit |= ImGuiHelper.InputTKVector3($"Rotate Random ", data, "ptclRotateDegreesRandom");
            }

            if (ImGui.CollapsingHeader("Scale", ImGuiTreeNodeFlags.DefaultOpen))
            {
                if (data.HasAnimationGroup(AnimGroupType.PtclScaleX))
                {
                    KeyDataEditor.LoadKeys(data.GetKeyGroups(AnimGroupType.PtclScaleX, AnimGroupType.PtclScaleY),
                        new string[] { "Scale X","Scale Y"}, "scaleTable");
                }
                else
                    updateEmitterInit |= ImGuiHelper.InputTKVector2($"Scale ", data, "PtclScale");
                updateEmitterInit |= ImGuiHelper.InputTKVector2($"Scale Random ", data, "PtclScaleRandom");
            }
            if (ImGui.CollapsingHeader("Velocity", ImGuiTreeNodeFlags.DefaultOpen))
            {
                ImGuiHelper.InputFromFloat($"Additive Position XZ", data, "ParticleAddXZVelocity");
                ImGuiHelper.InputTKVector3($"Angle ", data, "AngularVelocity");
                ImGuiHelper.InputTKVector3($"Angle Random", data, "AngularVelocityRandom");
                ImGuiHelper.InputTKVector3($"Direction", data, "Direction");
                ImGuiHelper.InputFromFloat($"All Directions Velocity", data, "AllDirectionVelocity");
                ImGuiHelper.InputFromFloat($"Directional Velocity", data, "DirectionVelocity");
                ImGuiHelper.InputFromFloat($"Directional Velocity Rnd", data, "DirectionVelocityRandom");

                ImGuiHelper.InputFromFloat($"PositionRandomizer", data, "PositionRandomizer");
            }
            if (ImGui.CollapsingHeader("Gravity", ImGuiTreeNodeFlags.DefaultOpen))
            {
                ImGuiHelper.InputFromFloat($"Air Resist", data, "AirResist");
                ImGuiHelper.InputTKVector3($"Gravity", data, "Gravity");
            }
        }

        public void DrawEmitterPropertyUI(EmitterWrapper emitter)
        {
            var data = emitter.EmitterData as SimpleEmitterData;

            if (ImGui.CollapsingHeader("Info", ImGuiTreeNodeFlags.DefaultOpen))
            {
                ImGuiHelper.InputFromText($"Name", data, "Name", 64);
                ImGuiHelper.InputFromBoolean($"Is Visible", data, "isVisible");
                ImGuiHelper.InputFromBoolean($"Render Particles", data, "DisplayParent");
                
                if (EffectSystem.ActiveSystem.HasResourceFlags(FileResourceFlags.HasGPUBehavior))
                    ImGui.Checkbox($"Calculate In Shader", ref data.GPUCalc);

                ImGuiHelper.ComboFromEnum<EmitterType>($"Type", data, "Type");
                updateEmitterInit |= ImGuiHelper.InputFromUint($"Random Seed", data, "RandomSeed");

                if (ImGui.RadioButton("Defalt Sort", !data.ParticleSortReverse && !data.ParticleZSort))
                {
                    data.ParticleSortReverse = false;
                    data.ParticleZSort = false;
                }
                ImGui.SameLine();
                if (ImGui.RadioButton("Reverse Sort", data.ParticleSortReverse))
                {
                    data.ParticleSortReverse = true;
                    data.ParticleZSort = false;
                }
                ImGui.SameLine();
                if (ImGui.RadioButton("Z Sort", data.ParticleZSort))
                {
                    data.ParticleZSort = true;
                    data.ParticleSortReverse = false;
                }
            }
            if (ImGui.CollapsingHeader("Emitter", ImGuiTreeNodeFlags.DefaultOpen))
            {
                updateEmitterInit |= ImGuiHelper.InputFromUint("Life Span", data, "MaxLifespan");
                updateEmitterInit |= ImGuiHelper.InputFromUint("Life Span Randomness", data, "LifespanRandomizer");

                updateEmitterInit |= ImGuiHelper.InputTKVector4Color4("Color0", data, "Color0", ImGuiColorEditFlags.HDR | ImGuiColorEditFlags.Float);
                updateEmitterInit |= ImGuiHelper.InputTKVector4Color4("Color1", data, "Color1", ImGuiColorEditFlags.HDR | ImGuiColorEditFlags.Float);

                var alpha = data.EmitterAlpha;
                if (ImGui.InputFloat("Alpha", ref alpha)) {
                    data.EmitterAlpha = alpha;
                    updateEmitterInit = true;
                }
            }

            ColorUI.DrawColorUI(data, ref updateEmitterInit);

            if (ImGui.CollapsingHeader("Emitter SRT", ImGuiTreeNodeFlags.DefaultOpen))
            {
                ImGuiHelper.InputTKVector3("Scale", data, "BaseScale");
                ImGuiHelper.InputTKVector3("Translation", data, "BaseTranslation");
                ImGuiHelper.InputTKVector3("Rotation", data, "BaseRotation");

                ImGuiHelper.InputTKVector3("Translation Random", data, "TranslationRandom");
                ImGuiHelper.InputTKVector3("Rotation Random", data, "RotationRandom");
            }
        }

        public void DrawEmissionUI(EmitterWrapper emitter)
        {
            var data = emitter.EmitterData as SimpleEmitterData;

            if (ImGui.CollapsingHeader("Emission", ImGuiTreeNodeFlags.DefaultOpen))
            {
                if (data.EmitterFunc == EmitterFunctions.SphereDiv64)
                    ImGui.Text($"Spawn Rate : {SphereVolumeDivTables.GetDivSphere64Count(data.SphereDivTblIdx+2)} (Fixed to sphere div count))");
                else if (data.EmitterFunc == EmitterFunctions.SphereDiv)
                    ImGui.Text($"Spawn Rate : {SphereVolumeDivTables.GetDivSphereCount(data.SphereDivTblIdx)} (Fixed to sphere div count))");
                else
                    updateEmitterInit |= ImGuiHelper.InputFromFloat("Spawn Rate", data, "EmissionRatio");


                updateEmitterInit |= ImGuiHelper.InputFromUint("Spawn Rate Randomness", data, "EmissionRatioRandomness");
                ImGuiHelper.InputFromUint("Spawn Tick", data, "EmissionInterval");
                ImGuiHelper.InputFromInt("Spawn Tick Randomness", data, "EmissionIntervalRandomness");
                ImGuiHelper.InputFromBoolean("Emission Distance Interval", data, "EquidistantParticleEmission");

                updateEmitterInit |= ImGuiHelper.InputFromUint("Start Frame", data, "EmissionStartFrame");
                updateEmitterInit |= ImGuiHelper.InputFromUint("End Frame", data, "EmissionEndFrame");
            }
            if (ImGui.CollapsingHeader("Emitter Function", ImGuiTreeNodeFlags.DefaultOpen))
            {
                DrawEmitterFunctionUI(data);
            }
        }

        public void DrawEmitterFunctionUI(SimpleEmitterData data)
        {
            bool edited = ImGuiHelper.ComboFromEnum<EmitterFunctions>("Emit Type", data, "EmitterFunc");
            updateEmitterInit |= edited;

            if (edited && data.EmitterFunc == EmitterFunctions.SphereDiv ||
                edited && data.EmitterFunc == EmitterFunctions.SphereDiv64)
            {
                if (data.EmitterFunc == EmitterFunctions.SphereDiv)
                {
                    data.SphereDivTblIdx = Math.Min(data.SphereDivTblIdx, (byte)7);
                    var divCount = SphereVolumeDivTables.GetDivSphereCount(data.SphereDivTblIdx);
                    data.EmissionRatio = divCount;
                }
                else
                {
                    data.SphereDivTblIdx = Math.Min(data.SphereDivTblIdx, (byte)60);
                    var divCount = SphereVolumeDivTables.GetDivSphere64Count(data.SphereDivTblIdx);
                    data.EmissionRatio = divCount;
                }
            }

            switch (data.EmitterFunc)
            {
                case EmitterFunctions.Sphere:
                case EmitterFunctions.SphereDiv:
                case EmitterFunctions.SphereDiv64:
                case EmitterFunctions.SphereFill:
                case EmitterFunctions.Circle:
                case EmitterFunctions.CircleDiv:
                case EmitterFunctions.CircleFill:
                case EmitterFunctions.Cylinder:
                case EmitterFunctions.CylinderFill:
                    DrawLongitudeInfo(data);
                    break;
                case EmitterFunctions.Box:
                    ImGuiHelper.InputTKVector3($"Length ", data, "VolumeScale");
                    ImGuiHelper.InputTKVector3($"Scale ", data, "EmissionShapeScale");
                    break;
                case EmitterFunctions.BoxFill:
                    ImGuiHelper.InputTKVector3($"Length ", data, "VolumeScale");
                    ImGuiHelper.InputFromFloat($"Hollowness ", data, "VolumeFillRatio");
                    ImGuiHelper.InputTKVector3($"Scale ", data, "EmissionShapeScale");
                    break;
                case EmitterFunctions.Line:
                case EmitterFunctions.LineDiv:
                    ImGuiHelper.InputFromFloat($"Position ", data, "LineCenterPosition");
                    ImGuiHelper.InputTKVector3($"Length ", data, "VolumeScale");
                    ImGuiHelper.InputTKVector3($"Scale ", data, "EmissionShapeScale");
                    break;
            }
        }

        public void DrawLongitudeInfo(SimpleEmitterData data)
        {
            float arcLength = data.ArcLength * STMath.Rad2Deg;
            ImGuiHelper.InputTKVector3($"Radius ", data, "VolumeScale");
            ImGuiHelper.InputTKVector3($"Scale ", data, "EmissionShapeScale");

            if (data.EmitterFunc == EmitterFunctions.SphereDiv64)
            {
                int index = data.SphereDivTblIdx;
                if (ImGui.InputInt("Div Index", ref index, 1, 1))
                {
                    data.SphereDivTblIdx = (byte)Math.Max(0, Math.Min(index, 60));
                    var divCount = SphereVolumeDivTables.GetDivSphere64Count(data.SphereDivTblIdx);
                    data.EmissionRatio = divCount;
                    updateEmitterInit = true;
                }
            }
            if (data.EmitterFunc == EmitterFunctions.SphereDiv)
            {
                int index = data.SphereDivTblIdx;
                if (ImGui.InputInt("Div Index", ref index))
                {
                    data.SphereDivTblIdx = (byte)Math.Max(0, Math.Min(index, 7));
                    var divCount = SphereVolumeDivTables.GetDivSphereCount(data.SphereDivTblIdx);
                    data.EmissionRatio = divCount;
                    updateEmitterInit = true;

                }
            }

            if (data.EmitterFunc == EmitterFunctions.Sphere)
            {
                if (data.SphereUseLatitude) {
                    ImGuiHelper.InputTKVector3($"Direction ", data, "SphereLatitudeDir");
                }
            }

            if (ImGui.InputFloat("Arc Length", ref arcLength))
            {
                data.ArcLength = arcLength * STMath.Deg2Rad;
            }
            float arcStartAngle = data.ArcStartAngle * STMath.Rad2Deg;
            if (ImGui.InputFloat("Arc Start Angle", ref arcStartAngle))
            {
                data.ArcStartAngle = arcStartAngle * STMath.Deg2Rad;
            }
            ImGuiHelper.InputFromBoolean($"RandomArcStartAngle ", data, "RandomArcStartAngle");

            switch (data.EmitterFunc)
            {
                case EmitterFunctions.CircleFill:
                case EmitterFunctions.SphereFill:
                case EmitterFunctions.CylinderFill:
                    ImGuiHelper.InputFromFloat($"Hollowness ", data, "VolumeFillRatio");
                    break;
            }
        }
    }
}

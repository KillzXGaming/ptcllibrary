﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using ImGuiNET;
using CafeStudio.UI;

namespace EffectLibrary.UI
{
    public class EmitterColorUI
    {
        public ColorKeyEditor Color0Editor = new ColorKeyEditor();
        public ColorKeyEditor Color1Editor = new ColorKeyEditor();
        public ColorKeyEditor Alpha0Editor = new ColorKeyEditor();
        public ColorKeyEditor Alpha1Editor = new ColorKeyEditor();

        public void OnLoad(EmitterWrapper wrapper)
        {
            var data = wrapper.EmitterData as SimpleEmitterData;

            Color0Editor.ReloadKeyframes(data.GetAnimatedKeys(0));
            Color1Editor.ReloadKeyframes(data.GetAnimatedKeys(1));
            Alpha0Editor.ReloadKeyframes(data.GetAnimatedKeys(2));
            Alpha1Editor.ReloadKeyframes(data.GetAnimatedKeys(3));
        }

        public void DrawColorUI(SimpleEmitterData data, ref bool updateEmitterInit)
        {
            string colorFunc = ColorModes.ContainsKey(data.ColorMode) ? $"({ColorModes[data.ColorMode]})" : "";
            string alphaFunc = AlphaModes.ContainsKey(data.AlphaMode) ? $"({AlphaModes[data.AlphaMode]})" : "";

            if (ImGui.CollapsingHeader($"Color {colorFunc}", ImGuiTreeNodeFlags.DefaultOpen))
            {
                ImGuiHelper.InputFromFloat("Color Scale", data, "ColorScale", true);

                updateEmitterInit |= ImGuiHelper.ComboFromEnum<AnimationFunctions>("Color 0 Type", data, "AnimColor0Func");
                DrawColorFunc(data, data.AnimColor0Func, 0);
                updateEmitterInit |= ImGuiHelper.ComboFromEnum<AnimationFunctions>("Color 1 Type", data, "AnimColor1Func");
                DrawColorFunc(data, data.AnimColor1Func, 1);
            }

            if (ImGui.CollapsingHeader($"Alpha {alphaFunc}", ImGuiTreeNodeFlags.DefaultOpen))
            {
                updateEmitterInit |= ImGuiHelper.ComboFromEnum<AnimationFunctions>("Alpha 0 Type", data, "AnimAlpha0Func");
                DrawAlphaFunc(data, data.AnimAlpha0Func, 0);

                if (EffectSystem.ActiveSystem.HasResourceFlags(FileResourceFlags.HasAlpha1))
                {
                    updateEmitterInit |= ImGuiHelper.ComboFromEnum<AnimationFunctions>("Alpha 1 Type", data, "AnimAlpha1Func");
                    DrawAlphaFunc(data, data.AnimAlpha1Func, 1);
                }
            }
        }

        private Dictionary<ColorMode, string> ColorModes = new Dictionary<ColorMode, string>()
        {
            { ColorMode.Color0, "Color0" },
            { ColorMode.Color0MulTexture, "Color0 * Texture" },
            { ColorMode.Color0MulTextureAddColor1MulInvTexture, "Color0 * Texture + Color1 * (1 - Texture)" },
            { ColorMode.Color0MulTextureAddColor1, "Color0 * Texture + Color1" },
        };

        private Dictionary<AlphaMode, string> AlphaModes = new Dictionary<AlphaMode, string>()
        {
            { AlphaMode.TextureAlphaMulAlpha0, "TextureA * Alpha0" },
            { AlphaMode.TextureAlphaMinusOneMinusAlpha0Mul2, "(TextureA  - (1 - Alpha)) * 2" },
            { AlphaMode.TextureRedMinusOneMinusAlpha0Mul2, "TextureR * Alpha0" },
            { AlphaMode.TextureRedMulAlpha0, "(TextureR  - (1 - Alpha)) * 2" },
            { AlphaMode.TextureAlphaMulAlpha0MulAlpha1, "TextureA * Alpha0 * Alpha1" },
        };

        private void DrawColorFunc(SimpleEmitterData data, AnimationFunctions func, int index)
        {
            var flags = ImGuiColorEditFlags.HDR | ImGuiColorEditFlags.NoInputs | ImGuiColorEditFlags.Float;

            switch (func)
            {
                case AnimationFunctions.Constant:
                    {
                        var colors = data.ColorKeys[index, 0];
                        var vec = new Vector4(colors.X, colors.Y, colors.Z, colors.W);
                        var size = new Vector2(ImGui.CalcItemWidth(), ImGui.GetFrameHeight());

                        if (ImGui.ColorButton($"Color {index}", vec, flags, size))
                        {
                            ImGui.OpenPopup($"#color{index}Picker");
                        }

                        if (ImGui.BeginPopup($"#color{index}Picker"))
                        {
                            if (ImGui.ColorPicker4("##picker", ref vec, flags))
                            {
                                data.ColorKeys[index, 0] = new OpenTK.Vector4(vec.X, vec.Y, vec.Z, vec.W);
                            }
                            ImGui.EndPopup();
                        }
                    }
                    break;
                case AnimationFunctions.Key4Value3:
                    if (index == 0)
                        Color0Editor.Draw($"#color{index}Slider4v3");
                    else
                        Color1Editor.Draw($"#color{index}Slider4v3");
                    break;
                case AnimationFunctions.Key8:
                    if (index == 0)
                        Color0Editor.Draw($"#color{index}Slider8");
                    else
                        Color1Editor.Draw($"#color{index}Slider8");
                    break;
                case AnimationFunctions.Random:
                    {
                        var numRandom = data.ColorRandomCount[index];
                        numRandom = 3;

                        var totalSize = new Vector2(ImGui.CalcItemWidth(), ImGui.GetFrameHeight());
                        var width = totalSize.X / numRandom;

                        for (int i = 0; i < numRandom; i++)
                        {
                            if (i > 0)
                                ImGui.SameLine();

                            var colorData = data.ColorKeys[index, i];
                            var color = new Vector4(colorData.X, colorData.Y, colorData.Z, colorData.W);
                            if (ImGui.ColorButton($"Color {index} rnd{i}", color, flags, new Vector2(width, totalSize.Y)))
                            {
                                ImGui.OpenPopup($"#color{index}Picker");
                            }
                        }
                    }
                    break;
            }
        }

        private void DrawAlphaFunc(SimpleEmitterData data, AnimationFunctions func, int index)
        {
            switch (func)
            {
                case AnimationFunctions.Constant:
                    var input = data.GetType().GetProperty($"AlphaKey{index}");
                    var alphaSection = (AlphaSection)input.GetValue(data);
                    var vec = new Vector4(alphaSection.StartValue);
                    var size = new Vector2(ImGui.CalcItemWidth(), ImGui.GetFrameHeight());
                    var flags = ImGuiColorEditFlags.HDR | ImGuiColorEditFlags.NoInputs | ImGuiColorEditFlags.Float;

                    if (ImGui.ColorButton($"Alpha {index}", vec, flags, size))
                    {
                        ImGui.OpenPopup($"#alpha{index}Picker");
                    }

                    if (ImGui.BeginPopup($"#alpha{index}Picker"))
                    {
                        if (ImGui.ColorPicker4("##picker", ref vec, flags))
                        {
                            alphaSection.StartValue = vec.X;
                            input.SetValue(data, alphaSection);
                        }
                        ImGui.EndPopup();
                    }
                    break;
                case AnimationFunctions.Key4Value3:
                    Alpha0Editor.Draw($"#alpha{index}Slider4v3");
                    break;
                case AnimationFunctions.Key8:
                    Alpha1Editor.Draw($"#alpha{index}Slider8");
                    break;
            }
        }
    }
}

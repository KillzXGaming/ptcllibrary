﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EffectLibrary.WiiU;
using ImGuiNET;

namespace EffectLibrary.UI
{
    public class KeyDataEditor
    {
        public static void LoadKeys(KeyGroup[] keyGroups, string[] groupNames, string key)
        {
            ImGui.Button("Add Key");
            ImGui.SameLine();
            ImGui.Button("Remove Key");

            ImGui.PushStyleVar(ImGuiStyleVar.FrameBorderSize, 2);
            ImGui.PushStyleColor(ImGuiCol.Border, new System.Numerics.Vector4(0, 0, 0, 1));

            if (ImGui.BeginTable($"##{key}FrameTable", 3)) {
                ImGui.TableSetupColumn("Frame");

                for (int i = 0; i < groupNames.Length; i++)
                    ImGui.TableSetupColumn(groupNames[i]);

                ImGui.TableHeadersRow();

                for (int j = 0; j < keyGroups[0].KeyFrames.Length; j++)
                {
                    ImGui.TableNextRow();
                    ImGui.TableSetColumnIndex(0);

                    var frame = keyGroups[0].KeyFrames[j].Frame;
                    ImGui.InputFloat($"##{key}FRAME{frame}", ref frame);

                    for (int i = 0; i < keyGroups.Length; i++)
                    {
                        ImGui.TableSetColumnIndex(i + 1);

                        var value = keyGroups[i].KeyFrames[j].Value;
                        ImGui.InputFloat($"##{key}KEY{i}{j}", ref value);
                    }
                }

                ImGui.EndTable();
            }

            ImGui.PopStyleVar();
            ImGui.PopStyleColor();
        }
    }
}

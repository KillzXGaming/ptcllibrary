﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Threading.Tasks;
using ImGuiNET;

namespace EffectLibrary.UI
{
    public class ColorKeyEditor
    {
        List<ColorKeyFrame> KeyFrames = new List<ColorKeyFrame>();

        public void ReloadKeyframes(List<ColorKeyFrame> keyFrames) {
            KeyFrames = keyFrames;
        }

        private Vector2 selectionPos = new Vector2();
        private int SelectedIndex = -1;
        private int HoveredIndex = -1;

        public void Draw(string id)
        {
            ImGui.SetCursorPosX(5);

            var drawList = ImGui.GetWindowDrawList();
            var gradient_size = new Vector2(ImGui.CalcItemWidth(), ImGui.GetFrameHeight());
            var cursor = ImGui.GetCursorScreenPos();
            var p = ImGui.GetCursorPos();

            //Draw empty
            if (KeyFrames.Count == 0)
            {
                Vector2 end = new Vector2(cursor.X + gradient_size.X, cursor.Y + gradient_size.Y);
                drawList.AddRectFilled(cursor, end, ImGui.GetColorU32(new Vector4(0,0,0,1)));
            }

            ColorKeyFrame previousKey = null;
            for (int i = 0; i < KeyFrames.Count; i++)
            {
                //Key ratio (0.0 - 1.0)
                var ratio = KeyFrames[i].Ratio;
                var posX = cursor.X + gradient_size.X * ratio;

                if (previousKey != null)
                {
                    var prevX = cursor.X + gradient_size.X * previousKey.Ratio;
                    //Second key clamp to start
                    if (i == 1)
                        prevX = cursor.X;

                    //Last key clamp to end
                    if (i == KeyFrames.Count - 1)
                        posX = cursor.X + gradient_size.X;

                    //Get the previous position and current positions (bottom left, top right)
                    var pos1 = new Vector2(prevX, cursor.Y);
                    var pos2 = new Vector2(posX, cursor.Y + gradient_size.Y);

                    //Previous and current colors
                    uint colora = ImGui.GetColorU32(new Vector4(
                        previousKey.Color.X, previousKey.Color.Y,
                        previousKey.Color.Z, previousKey.Color.W));
                    uint colorb = ImGui.GetColorU32(new Vector4(
                        KeyFrames[i].Color.X, KeyFrames[i].Color.Y,
                        KeyFrames[i].Color.Z, KeyFrames[i].Color.W));

                    //Add colors to gradient
                    drawList.AddRectFilledMultiColor(pos1, pos2, colora, colorb, colorb, colora);
                }
                previousKey = KeyFrames[i];
            }

            //Draw key frames
            for (int i = 0; i < KeyFrames.Count; i++)
            {
                var ratio = KeyFrames[i].Ratio;
                var keyPos = cursor.X + gradient_size.X * ratio;

                float keyWidth = 3;
                float borderSize = 1;

                Vector4 color = new Vector4(1);
                if (SelectedIndex == i)
                    color = new Vector4(1, 1, 0, 1);
                if (HoveredIndex == i)
                    color = new Vector4(1, 1, 0, 1);

                //Draw key item
                drawList.AddRect(
                     new Vector2(keyPos - keyWidth, cursor.Y + borderSize),
                    new Vector2(keyPos + keyWidth, cursor.Y + gradient_size.Y - borderSize),
                    ImGui.GetColorU32(color));

                //Draw key border
                drawList.AddRect(
                 new Vector2(keyPos - keyWidth - borderSize, cursor.Y),
                new Vector2(keyPos + keyWidth + borderSize, cursor.Y + gradient_size.Y),
                ImGui.GetColorU32(new Vector4(0,0,0,1)));


                var pos = ImGui.GetCursorPos();

                ImGui.SetCursorPos(new Vector2(p.X - 4 + gradient_size.X * ratio, p.Y));
                bool isSelected = ImGui.InvisibleButton(id + $"#key{i}", new Vector2(8, gradient_size.Y));
                if (isSelected)
                {
                    selectionPos = ImGui.GetMousePos();
                    SelectedIndex = i;
                }
                if (ImGui.IsItemHovered())
                {
                    HoveredIndex = i;
                }
                if (SelectedIndex == i && ImGui.IsItemHovered() && ImGui.IsMouseDown(ImGuiMouseButton.Left))
                {
                    var currentPos = ImGui.GetMousePos();
                    var diff = selectionPos - currentPos;
                    var movement = diff.X;
                    Console.WriteLine($"movement {movement}");

                    float newRatio = movement / gradient_size.X;
                    KeyFrames[i].Ratio = newRatio;
                }

                ImGui.SetCursorPos(pos);
            }

            ImGui.InvisibleButton(id, gradient_size);
        }
    }
}

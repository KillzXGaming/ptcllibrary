﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Numerics;
using ImGuiNET;
using CafeStudio.UI;

namespace EffectLibrary.UI
{
    public class EmitterAnimationEditor
    {
        string[] groups = new string[]
        {
            "Emission Ratio",
            "Emitter Scale",
            "Emitter Rotate",
            "Emitter Translate",
            "All Direction Velocity",
            "Direction Velocity",
            "Emission Shape Size",
            "Particle Scale",
            "Alpha",
            "Gravity",
        };

        private int selectedIndex = -1;

        public void LoadEditor(SimpleEmitterData data)
        {
            ImGui.Columns(2);

            for (int i = 0; i < groups.Length; i++)
            {
                var groupData = GetAnimGroups(groups[i]);
                if (data.HasAnimationGroup(groupData[0]))
                {
                    if (ImGui.Selectable(groups[i], selectedIndex == i))
                        selectedIndex = i;
                }
                else
                {
                    ImGui.PushStyleColor(ImGuiCol.Text, new Vector4(0.7f, 0.7f, 0.7f, 1.0f));
                    if (ImGui.Selectable(groups[i], selectedIndex == i))
                        selectedIndex = i;
                    ImGui.PopStyleColor();
                }
            }

            ImGui.NextColumn();

            if (selectedIndex != -1)
            {
                if (ImGui.BeginChild("keyWindow")) {
                    AnimationKeyWindow(data, groups[selectedIndex]);
                }
                ImGui.EndChild();
            }

            ImGui.NextColumn();
        }

        private int selectedKeyRow = -1;

        private void AnimationKeyWindow(SimpleEmitterData data, string group)
        {
            var groupData = GetAnimGroups(group);
            var keyGroups = data.GetKeyGroups(groupData);

            bool addKey = ImGui.Button("Add Key");
            ImGui.SameLine();
            bool removeKey = ImGui.Button("Remove Key");

            if (addKey)
            {
                foreach (var keyGroup in keyGroups) {
                    var lastFrame = keyGroup.KeyFrames.Last().Frame;
                    var insertFrame = keyGroup.KeyFrames.Length > 0 ? lastFrame : 0;

                    float defaultValue = 0.0f;

                    if (keyGroup.Type.ToString().Contains("Scale"))
                        defaultValue = 1.0f;

                    keyGroup.InsertKey(insertFrame, defaultValue);
                }
            }

            if (ImGui.BeginTable($"##FrameTable", 3))
            {
                ImGui.TableSetupColumn("Frame");

                for (int i = 0; i < groupData.Length; i++)
                    ImGui.TableSetupColumn(groupData[i].ToString());

                ImGui.TableHeadersRow();

                if (keyGroups.Length > 0)
                {
                    for (int j = 0; j < keyGroups[0].KeyFrames.Length; j++)
                    {
                        ImGui.TableNextRow();
                        ImGui.TableSetColumnIndex(0);

                        var keyFrame = keyGroups[0].KeyFrames[j];

                        ImGui.PushItemWidth(-1);
                        ImGuiHelper.InputFromFloat($"##FRAME{j}", keyFrame, "Frame");
                        ImGui.PopItemWidth();

                        for (int i = 0; i < keyGroups.Length; i++)
                        {
                            ImGui.TableSetColumnIndex(i + 1);

                            keyFrame = keyGroups[i].KeyFrames[j];

                            ImGui.PushItemWidth(-1);
                            ImGuiHelper.InputFromFloat($"##KEY{i}{j}", keyFrame, "Value");
                            ImGui.PopItemWidth();
                        }
                    }
                }
                ImGui.EndTable();
            }
        }

        private AnimGroupType[] GetAnimGroups(string group)
        {
            switch (group)
            {
                case "Emission Ratio":
                    return new AnimGroupType[] { AnimGroupType.EmissionRatio };
                case "Emitter Scale":
                    return new AnimGroupType[] { 
                        AnimGroupType.ScaleX, 
                        AnimGroupType.ScaleY, 
                        AnimGroupType.ScaleZ };
                case "Emitter Rotate":
                    return new AnimGroupType[] {
                        AnimGroupType.RotationX,
                        AnimGroupType.RotationY,
                        AnimGroupType.RotationZ };
                case "Emitter Translate":
                    return new AnimGroupType[] {
                        AnimGroupType.PositionX,
                        AnimGroupType.PositionY,
                        AnimGroupType.PositionZ };
                case "All Direction Velocity":
                    return new AnimGroupType[] { AnimGroupType.AllDirectionVelocity };
                case "Direction Velocity":
                    return new AnimGroupType[] { AnimGroupType.DirectionVelocity };
                case "Emission Shape Size":
                    return new AnimGroupType[] {
                        AnimGroupType.EmissionShapeScaleX,
                        AnimGroupType.EmissionShapeScaleY,
                        AnimGroupType.EmissionShapeScaleZ};
                case "Particle Scale":
                    return new AnimGroupType[] { AnimGroupType.PtclScaleX, AnimGroupType.PtclScaleY };
                case "Alpha":
                    return new AnimGroupType[] { AnimGroupType.Alpha0 };
                case "Gravity":
                    return new AnimGroupType[] { AnimGroupType.Gravity };
                default:
                    throw new Exception();
            }
        }

        private void SetAnimationEntry()
        {

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ImGuiNET;
using CafeStudio.UI;

namespace EffectLibrary.UI
{
    class EmitterChildGUI
    {
        EmitterTextureGUI TextureGUI = new EmitterTextureGUI();
        EmitterShaderGUI ShaderGUI = new EmitterShaderGUI();

        public bool updateEmitterInit;

        public void OnLoad(ChildData data)
        {
        }

        public void LoadEditor(ComplexEmitterData emitter, ChildData data)
        {
            ImGui.BeginTabBar("Menu1");

            bool followEmitter = (emitter.childFlags & (int)ChildFlags.InheritSRT) != 0;

            if (ImguiCustomWidgets.BeginTab("Menu1", "Child Data"))
            {
                ImGui.Checkbox("Follow Emitter", ref followEmitter);
                ImGui.EndTabItem();
            }
            if (ImguiCustomWidgets.BeginTab("Menu1", "Texture Data"))
            {
                TextureGUI.LoadEditor(data, ref updateEmitterInit);
                ImGui.EndTabItem();
            }

            ImGui.EndTabBar();

            if (updateEmitterInit) {
                ParticleRenderer.RespawnEmitterSet = true;
            }
        }
    }
}

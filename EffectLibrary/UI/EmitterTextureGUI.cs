﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ImGuiNET;
using CafeStudio.UI;
using GLFrameworkEngine;
using Toolbox.Core;

namespace EffectLibrary.UI
{
    public class EmitterTextureGUI
    {
        private int SelectedIndex = 0;

        private Dictionary<int, int> Icons = new Dictionary<int, int>();

        public void LoadEditor(EmitterWrapper emitter, ref bool updateEmitterInit)
        {
            var data = emitter.EmitterData as SimpleEmitterData;

            DrawTextureButton(data.TextureEmitters, emitter.EmitterData.GetTextureWrapper(0), 0);
            DrawTextureButton(data.TextureEmitters, emitter.EmitterData.GetTextureWrapper(1), 1);
            DrawTextureButton(data.TextureEmitters, emitter.EmitterData.GetTextureWrapper(2), 2);

            if (ImGui.CollapsingHeader("Parameters", ImGuiTreeNodeFlags.DefaultOpen)) {
                DrawTextureParams(emitter.EmitterData, SelectedIndex, ref updateEmitterInit);
            }
        }

        public void LoadEditor(ChildData chilData, ref bool updateEmitterInit)
        {
            DrawTextureButton(new TextureEmitter[] { chilData.TextureEmitter },
                chilData.Texture.Handle, 0);

            if (ImGui.CollapsingHeader("Parameters", ImGuiTreeNodeFlags.DefaultOpen))
            {
                DrawTextureParams(chilData, SelectedIndex, ref updateEmitterInit);
            }
        }

        public void DrawTextureParams(ChildData childData, int index, ref bool updateEmitterInit)
        {

        }

        public void DrawTextureParams(IEmitter emitterData, int index, ref bool updateEmitterInit) {
            var texture = emitterData.GetTextureWrapper(index);
            var emitter = emitterData as SimpleEmitterData;


            var textureRes = emitter.Textures[index];
            var sampler = emitter.TextureEmitters[index];

            ImGuiHelper.ComboFromEnum<WrapMode>("Wrap X", textureRes, "WrapX");
            ImGuiHelper.ComboFromEnum<WrapMode>("Wrap Y", textureRes, "WrapY");
            ImGuiHelper.ComboFromEnum<FilterMode>("Filter Mode", textureRes, "FilterMode");

            updateEmitterInit |= ImGuiHelper.ComboFromEnum<TextureRepeat>("Repeat Mode", sampler, "RepeatMode");
            
            if (ImGui.CollapsingHeader("Texture SRT", ImGuiTreeNodeFlags.DefaultOpen))
            {
                updateEmitterInit |= ImGuiHelper.InputFromVector2("Scale", sampler, "Scale");
                updateEmitterInit |= ImGuiHelper.InputFromFloat("Rotate", sampler, "Rotate");
                updateEmitterInit |= ImGuiHelper.InputFromVector2("Translate", sampler, "Translate");
            }
            if (ImGui.CollapsingHeader("Texture SRT Shift", ImGuiTreeNodeFlags.DefaultOpen))
            {
                updateEmitterInit |= ImGuiHelper.InputFromVector2("Scale ##add", sampler, "ScaleShift");
                updateEmitterInit |= ImGuiHelper.InputFromFloat("Rotate ##add", sampler, "RotateShift");
                updateEmitterInit |= ImGuiHelper.InputFromVector2("Translate ##add", sampler, "TranslateShift");
            }
            if (ImGui.CollapsingHeader("Texture SRT Random", ImGuiTreeNodeFlags.DefaultOpen))
            {
                updateEmitterInit |= ImGuiHelper.InputFromVector2("Scale ##rnd", sampler, "ScaleRnd");
                updateEmitterInit |= ImGuiHelper.InputFromFloat("Rotate ##rnd", sampler, "RotateRnd");
                updateEmitterInit |= ImGuiHelper.InputFromVector2("Translate ##rnd", sampler, "TranslateRnd");
            }
            if (ImGui.CollapsingHeader("Texture Pattern", ImGuiTreeNodeFlags.DefaultOpen))
            {
                updateEmitterInit |= ImGuiHelper.ComboFromEnum<TexturePatternType>("Pattern Type", sampler, "PatternType");
            }
        }

        public void DrawTextureButton(TextureEmitter[] textureParams, STGenericTexture texture, int index)
        {
            if (texture == null)
                return;

            var textureParam = textureParams[index];

            var selectionColor = ImGui.GetStyle().Colors[(int)ImGuiCol.ButtonActive];
            var outlineColor = ImGui.GetStyle().Colors[(int)ImGuiCol.TabUnfocused];

            ImGui.PushStyleVar(ImGuiStyleVar.FrameBorderSize, 2);
            ImGui.PushStyleColor(ImGuiCol.Border,
                SelectedIndex == index ? selectionColor : outlineColor);
            ImGui.PushStyleColor(ImGuiCol.ButtonHovered, 0);

            if (texture.RenderableTex == null)
                texture.LoadRenderableTexture();

            if (texture.RenderableTex != null) {
                if (!Icons.ContainsKey(texture.RenderableTex.ID))
                {
                    var iconID = IconRender.CreateTextureRender(texture, 100, 100, true);
                    Icons.Add(texture.RenderableTex.ID, iconID);
                }

                int id = Icons[texture.RenderableTex.ID];

                if (index > 0) ImGui.SameLine();

                bool clicked = ImGui.ImageButton((IntPtr)id, new System.Numerics.Vector2(100, 100),
                    new System.Numerics.Vector2(1, 0),
                    new System.Numerics.Vector2(0, 1));

                if (clicked) {
                    SelectedIndex = index;
                }
            }

            ImGui.PopStyleVar();
            ImGui.PopStyleColor(2);
        }
    }
}

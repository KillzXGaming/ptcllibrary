﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK.Graphics.OpenGL;
using ImGuiNET;
using System.IO;
using CafeStudio.UI;

namespace EffectLibrary
{
    public class ShaderProgramViewer
    {
        static string VertexShaderSource;
        static string FragShaderSource;

        static string vertexShaderPath;
        static string fragmentShaderPath;

        static string selectedStage = "Vertex";

        public static void Render(ParticleShader shader)
        {
            if (ImGui.BeginCombo("Stage", selectedStage))
            {
                if (ImGui.Selectable("Vertex"))
                {
                    selectedStage = "Vertex";
                }
                if (ImGui.Selectable("Pixel"))
                {
                    selectedStage = "Pixel";
                }
                ImGui.EndCombo();
            }

            ImGui.BeginTabBar("menu_shader1");
            if (ImguiCustomWidgets.BeginTab("menu_shader1", $"Shader Code"))
            {
                if (shader.ShaderInfo != null)
                    LoadShaderStageCode(shader.ShaderInfo);
                ImGui.EndTabItem();
            }
            if (ImguiCustomWidgets.BeginTab("menu_shader1", $"Shader Info"))
            {
                if (shader.ShaderWrapper is WiiUShaderWrapper)
                    LoadShaderInfo((WiiUShaderWrapper)shader.ShaderWrapper);
                ImGui.EndTabItem();
            }
            ImGui.EndTabBar();
        }

        static void LoadShaderInfo(WiiUShaderWrapper shaderInfo)
        {
            if (selectedStage == "Vertex")
            {
                var gx2Shader = shaderInfo.VertexShader;

                if (ImGui.CollapsingHeader("Attributes", ImGuiTreeNodeFlags.DefaultOpen))
                {
                    for (int i = 0; i < gx2Shader.Attributes.Count; i++)
                        ImGui.Text($"In {gx2Shader.Attributes[i].Name} Location {gx2Shader.Attributes[i].Location} Location {gx2Shader.Attributes[i].Type}");
                }

                if (ImGui.CollapsingHeader("Uniform Blocks", ImGuiTreeNodeFlags.DefaultOpen))
                {
                    for (int i = 0; i < gx2Shader.UniformBlocks.Count; i++)
                    {
                        if (ImGui.CollapsingHeader($"Uniforms##{i}"))
                        {
                            ImGui.Text($"{gx2Shader.UniformBlocks[i].Name} Location {gx2Shader.UniformBlocks[i].Offset}");

                            var uniforms = gx2Shader.Uniforms.OrderBy(x => x.Offset).ToList();
                            for (int j = 0; j < uniforms.Count; j++)
                                if (uniforms[j].BlockIndex == i)
                                    ImGui.Text($"{uniforms[j].Name} Type {uniforms[j].Type} offset {uniforms[j].Offset}");
                        }
                    }
                }
                if (ImGui.CollapsingHeader("Samplers", ImGuiTreeNodeFlags.DefaultOpen))
                {
                    for (int i = 0; i < gx2Shader.Samplers.Count; i++)
                        ImGui.Text($"{gx2Shader.Samplers[i].Name} Location {gx2Shader.Samplers[i].Location} Type {gx2Shader.Samplers[i].Type}");
                }
            }
            if (selectedStage == "Pixel")
            {
                var gx2Shader = shaderInfo.PixelShader;

                if (ImGui.CollapsingHeader("Uniform Blocks", ImGuiTreeNodeFlags.DefaultOpen))
                {
                    for (int i = 0; i < gx2Shader.UniformBlocks.Count; i++)
                    {
                        ImGui.Text($"{gx2Shader.UniformBlocks[i].Name} Location {gx2Shader.UniformBlocks[i].Offset}");

                        if (ImGui.CollapsingHeader($"Uniforms##{i}"))
                        {
                            var uniforms = gx2Shader.Uniforms.OrderBy(x => x.Offset).ToList();
                            for (int j = 0; j < uniforms.Count; j++)
                                if (uniforms[j].BlockIndex == i)
                                    ImGui.Text($"{uniforms[j].Name} Type {uniforms[j].Type} offset {uniforms[j].Offset}");
                        }
                    }
                }

                if (ImGui.CollapsingHeader("Samplers", ImGuiTreeNodeFlags.DefaultOpen))
                {
                    for (int i = 0; i < gx2Shader.Samplers.Count; i++)
                        ImGui.Text($"{gx2Shader.Samplers[i].Name} Location {gx2Shader.Samplers[i].Location} Type {gx2Shader.Samplers[i].Type}");
                }
            }
        }

        static void LoadShaderStageCode(GLShaderInfo shaderInfo)
        {
            if (vertexShaderPath != shaderInfo.VertPath)
            {
                vertexShaderPath = shaderInfo.VertPath;
                VertexShaderSource = System.IO.File.ReadAllText(vertexShaderPath);
            }
            if (fragmentShaderPath != shaderInfo.FragPath)
            {
                fragmentShaderPath = shaderInfo.FragPath;
                FragShaderSource = System.IO.File.ReadAllText(fragmentShaderPath);
            }

            if (ImGui.BeginChild("stage_window"))
            {
                var size = ImGui.GetWindowSize();
                if (selectedStage == "Vertex")
                {
                    ImGui.InputTextMultiline("Vertex", ref VertexShaderSource, 4000, size);
                }
                if (selectedStage == "Pixel")
                {
                    ImGui.InputTextMultiline("Pixel", ref FragShaderSource, 4000, size);
                }
            }
            ImGui.EndChild();
        }
    }
}

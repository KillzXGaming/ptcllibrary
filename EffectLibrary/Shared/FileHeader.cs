﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EffectLibrary
{
    public class FileHeader : IResData
    {
        public uint Version { get; set; }

        public virtual IEnumerable<IEmitterSet> EmitterSetList { get; }

        public virtual void Read(BinaryFileReader reader)
        {

        }

        public virtual void Write(BinaryFileWriter reader)
        {

        }

        public virtual WiiU.KeyTable ReadKeyTable(BinaryFileReader reader, uint offset)
        {
            return null;
        }

        public virtual float[] ReadParameters(BinaryFileReader reader, uint offset, uint count)
        {
            return new float[0];
        }

        public virtual string ReadStringTable(BinaryFileReader reader)
        {
            return "";
        }
    }
}

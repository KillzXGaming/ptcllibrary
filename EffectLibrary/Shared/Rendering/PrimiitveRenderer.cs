﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK.Graphics.OpenGL;
using OpenTK;
using GLFrameworkEngine;

namespace EffectLibrary
{
    public class PrimitiveRenderer : ParticleBaseMeshRender
    {
        private int Ibo = -1;

        private int IndexCount;
        private PrimitiveType PrimitiveType = PrimitiveType.Triangles;

        private uint[] IndexBuffer;

        private IPrimitive Primitive;

        public bool HasElements() => IndexCount > 0;

        public PrimitiveRenderer(IPrimitive primitive) {
            Primitive = primitive;
            InitPrimitive();
        }

        public override void Init() { }

        public void InitPrimitive()
        {
            //Load vaos
            int[] buffers = new int[3];
            GL.GenBuffers(3, buffers);

            Vbo = buffers[0];
            Ibo = buffers[1];
            InstancedVbo = buffers[2];

            vao = new VertexBufferObject(Vbo, null, InstancedVbo);

            InitAttributes();
            UpdateVAO();
            UpdateVertexData();
        }

        public override void Draw(GLContext context)
        {
            vao.Enable(context.CurrentShader);
            vao.BindVertexArray();
            GL.DrawElements(PrimitiveType, IndexCount, DrawElementsType.UnsignedInt, IndexBuffer);
            GL.BindVertexArray(0);
        }

        public override void Draw(GLContext context, int numInstances)
        {
            vao.Enable(context.CurrentShader);
            vao.BindVertexArray();
            GL.DrawElementsInstanced(PrimitiveType, IndexCount,
                DrawElementsType.UnsignedInt, IndexBuffer, numInstances);
            GL.BindVertexArray(0);
        }


        public override void UpdateInstances(PtclAttributeBuffer[] buffer)
        {
            UpdateVAO();
            UpdateInstancedVertexData(buffer);
        }

        public override void InitAttributes()
        {
            Attributes.Add(new Attribute("v_inPos", 4));
            if (Primitive.HasNormals) Attributes.Add(new Attribute("v_inNormal", 4));
            if (Primitive.HasTexCoords) Attributes.Add(new Attribute("v_inTexCoord", 4));
            Attributes.Add(new Attribute("v_inColor", 4));

            InitInstancedAttributes();
        }

        public override void UpdateVertexData()
        {
            IndexCount = (int)Primitive.GetElementCount();

            float[] vertexBuffer = GetVertexBuffer();
            IndexBuffer = Primitive.GetIndexBuffer();

            GL.BindBuffer(BufferTarget.ElementArrayBuffer, Ibo);
            GL.BufferData(BufferTarget.ElementArrayBuffer, IndexBuffer.Length * sizeof(uint), IndexBuffer, BufferUsageHint.StaticDraw);
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, 0);

            GL.BindBuffer(BufferTarget.ArrayBuffer, Vbo);
            GL.BufferData(BufferTarget.ArrayBuffer, vertexBuffer.Length * sizeof(float), vertexBuffer, BufferUsageHint.StaticDraw);
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
        }

        public override float[] GetVertexBuffer()
        {
            List<float> vertexBuffer = new List<float>();

            var positions = Primitive.GetPositionData();
            var normals = Primitive.GetNormalsData();
            var texCoords = Primitive.GetTexCoordData();
            var colors = Primitive.GetColorData();

            uint vertexCount = Primitive.GetVertexCount();
            for (int i = 0; i < vertexCount; i++)
            {
                foreach (var att in Attributes)
                {
                    switch (att.Name)
                    {
                        case "v_inPos":
                            var position = positions[i];
                            vertexBuffer.Add(position.X);
                            vertexBuffer.Add(position.Y);
                            vertexBuffer.Add(position.Z);
                            vertexBuffer.Add(0);
                            break;
                        case "v_inNormal":
                            var normal = normals[i];
                            vertexBuffer.Add(normal.X);
                            vertexBuffer.Add(normal.Y);
                            vertexBuffer.Add(normal.Z);
                            vertexBuffer.Add(0);
                            break;
                        case "v_inColor":
                            var color = new System.Numerics.Vector4(1);
                            if (Primitive.HasColors)
                                color = colors[i];
                            vertexBuffer.Add(color.X);
                            vertexBuffer.Add(color.Y);
                            vertexBuffer.Add(color.Z);
                            vertexBuffer.Add(color.W);
                            break;
                        case "v_inTexCoord":
                            var texCoord = texCoords[i];
                            vertexBuffer.Add(texCoord.X);
                            vertexBuffer.Add(texCoord.Y);
                            vertexBuffer.Add(texCoord.X);
                            vertexBuffer.Add(texCoord.Y);
                            break;
                    }
                }
            }
            return vertexBuffer.ToArray();
        }
    }
}

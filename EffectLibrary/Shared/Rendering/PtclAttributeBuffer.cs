﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;

namespace EffectLibrary
{
    public class PtclAttributeBuffer
    {
        public Vector4 WorldPosition;
        public Vector3 WorldPositionDif;

        public Vector4[] EmissionMatrix;

        public Vector4 Color0;
        public Vector4 Color1;

        public Vector4 Random;
        public Vector4 ScaleCenter;
        public Vector4 Rotate;

        public Vector4 Vector;

        //Used in older versions
        public Vector4 TextureAnim;
        public Vector4 SubTextureAnim;

        public void Init()
        {
            EmissionMatrix = new Vector4[3] 
            {
                new Vector4(1,0,0,0), new Vector4(0,1,0,0), new Vector4(0,0,1,0)
            };
            WorldPosition = new Vector4(0);
            WorldPositionDif = new Vector3(0, 0.0001f, 0);
            Color0 = new Vector4(1);
            Color1 = new Vector4(1);
            Random = new OpenTK.Vector4(0.8090375f, 0.127444f, 0.1851955f, 0.9987962f);
            ScaleCenter = new Vector4(20,20,0,0);
            Rotate = new Vector4(0);
            TextureAnim = new Vector4(0, 0, 1, 1);
            SubTextureAnim = new Vector4(0, 0, 1, 1);
            Vector = new Vector4(0);
        }
    }
}

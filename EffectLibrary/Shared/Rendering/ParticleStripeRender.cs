﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK.Graphics.OpenGL;
using OpenTK;
using GLFrameworkEngine;

namespace EffectLibrary
{
    public class ParticleStripeRender : ParticleBaseMeshRender
    {
        public override void Init()
        {
            //Load vaos
            Vbo = GL.GenBuffer();
            vao = new VertexBufferObject(Vbo, null);

            InitAttributes();
            UpdateVAO();
            UpdateVertexData();
        }

        public void UpdateStripeVertexData(StripeVertexBuffer[] buffer)
        {
            float[] vertexBuffer = GetStripeVertexBuffer(buffer);

            GL.BindBuffer(BufferTarget.ArrayBuffer, Vbo);
            GL.BufferData(BufferTarget.ArrayBuffer, vertexBuffer.Length * sizeof(float), vertexBuffer, BufferUsageHint.StaticDraw);
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
        }

        public override void InitAttributes()
        {
            Attributes.Add(new Attribute("v_inPos", 4));
            Attributes.Add(new Attribute("v_inOuter", 4));
            Attributes.Add(new Attribute("v_inTexCoord", 4));
            Attributes.Add(new Attribute("v_inDir", 4));
        }

        public float[] GetStripeVertexBuffer(StripeVertexBuffer[] buffer)
        {
            List<float> vertexBuffer = new List<float>();
            for (int i = 0; i < buffer.Length; i++)
            {
                foreach (var att in Attributes)
                {
                    switch (att.Name)
                    {
                        case "v_inPos":
                            vertexBuffer.Add(buffer[i].pos.X);
                            vertexBuffer.Add(buffer[i].pos.Y);
                            vertexBuffer.Add(buffer[i].pos.Z);
                            vertexBuffer.Add(buffer[i].pos.W);
                            break;
                        case "v_inOuter":
                            vertexBuffer.Add(buffer[i].outer.X);
                            vertexBuffer.Add(buffer[i].outer.Y);
                            vertexBuffer.Add(buffer[i].outer.Z);
                            vertexBuffer.Add(buffer[i].outer.W);
                            break;
                        case "v_inTexCoord":
                            vertexBuffer.Add(buffer[i].texCoord.X);
                            vertexBuffer.Add(buffer[i].texCoord.Y);
                            vertexBuffer.Add(buffer[i].texCoord.Z);
                            vertexBuffer.Add(buffer[i].texCoord.W);
                            break;
                        case "v_inDir":
                            vertexBuffer.Add(buffer[i].dir.X);
                            vertexBuffer.Add(buffer[i].dir.Y);
                            vertexBuffer.Add(buffer[i].dir.Z);
                            vertexBuffer.Add(buffer[i].dir.W);
                            break;
                    }
                }
            }
            return vertexBuffer.ToArray();
        }

        public override void Draw(GLContext context, int numVertices)
        {
            if (numVertices == 0)
                return;

            vao.Enable(context.CurrentShader);
            vao.BindVertexArray();
            GL.DrawArrays(PrimitiveType.TriangleStrip, 0, numVertices);
            GL.BindVertexArray(0);
        }

        public void Draw(GLContext context, int numVertices, int startVertex, int numInstances)
        {
            if (numVertices == 0)
                return;

            vao.Enable(context.CurrentShader);
            vao.BindVertexArray();
            GL.DrawArrays(PrimitiveType.TriangleStrip, startVertex, numVertices);
            GL.BindVertexArray(0);
        }
    }
}

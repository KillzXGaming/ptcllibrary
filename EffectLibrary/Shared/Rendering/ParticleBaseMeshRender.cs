﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK.Graphics.OpenGL;
using OpenTK;
using GLFrameworkEngine;

namespace EffectLibrary
{
    public class ParticleBaseMeshRender 
    {
        internal int Vbo = -1;
        internal int InstancedVbo = -1;
        internal int FeedbackID;

        internal List<Attribute> Attributes = new List<Attribute>();
        internal List<Attribute> AttributeInstances = new List<Attribute>();
        internal List<Attribute> AttributeStreamOut = new List<Attribute>();

        internal VertexBufferObject vao;

        public ParticleBaseMeshRender()
        {
            Init();
        }

        public virtual void Init()
        {
            //Load vaos
            Vbo = GL.GenBuffer();
            InstancedVbo = GL.GenBuffer();
            FeedbackID = GL.GenBuffer();
            vao = new VertexBufferObject(Vbo, null, InstancedVbo);

            InitAttributes();
            UpdateVAO();
            UpdateVertexData();
        }

        public virtual void UpdateInstances(PtclAttributeBuffer[] buffer)
        {
            UpdateVAO();
            UpdateInstancedVertexData(buffer);
        }

        public void Bind(GLContext context)
        {
            vao.BindVertexArray();
        }

        public virtual void InitAttributes()
        {
            Attributes.Clear();

            Attributes.Add(new Attribute("v_inPos", 4));
            Attributes.Add(new Attribute("v_inColor", 4));
            Attributes.Add(new Attribute("v_inIndex", 4));

            InitInstancedAttributes();
        }

        public virtual void InitInstancedAttributes()
        {
            AttributeInstances.Clear();

            AttributeInstances.Add(new Attribute("v_inColor0", 4));
            AttributeInstances.Add(new Attribute("v_inColor1", 4));
            AttributeInstances.Add(new Attribute("v_inRandom", 4));
            AttributeInstances.Add(new Attribute("v_inScl", 4));
            AttributeInstances.Add(new Attribute("v_inWldPos", 4));
            AttributeInstances.Add(new Attribute("v_inRot", 4));
            AttributeInstances.Add(new Attribute("v_inTexAnim", 4));
            AttributeInstances.Add(new Attribute("v_inSubTexAnim", 4));
            AttributeInstances.Add(new Attribute("v_inWldPosDf", 4));
            AttributeInstances.Add(new Attribute("v_inVec", 4));

            AttributeInstances.Add(new Attribute("v_inEmtMat0", 4));
            AttributeInstances.Add(new Attribute("v_inEmtMat1", 4));
            AttributeInstances.Add(new Attribute("v_inEmtMat2", 4));
        }

        public virtual void UpdateVertexData()
        {
            float[] vertexBuffer = GetVertexBuffer();

            GL.BindBuffer(BufferTarget.ArrayBuffer, Vbo);
            GL.BufferData(BufferTarget.ArrayBuffer, vertexBuffer.Length * sizeof(float), vertexBuffer, BufferUsageHint.StaticDraw);
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
        }

        public virtual float[] GetVertexBuffer()
        {
            List<float> vertexBuffer = new List<float>();
            return vertexBuffer.ToArray();
        }

        internal void UpdateVAO()
        {
            int stride = Attributes.Sum(x => x.Stride);
            int offset = 0;

            vao.Clear();
            foreach (var att in Attributes)
            {
                vao.AddAttribute(att.Name + "_0_0", att.ElementCount, att.Type, false, stride, offset);
                offset += att.Stride;
            }

            stride = AttributeInstances.Sum(x => x.Stride);
            offset = 0;
            foreach (var att in AttributeInstances)
            {
                vao.AddInstancedAttribute(att.Name + "_0_0", att.ElementCount, att.Type, false, stride, offset);
                offset += att.Stride;
            }
            vao.Initialize();
        }

        internal void UpdateVAO(Dictionary<string, int> locations)
        {
            int stride = Attributes.Sum(x => x.Stride);
            int offset = 0;

            vao.Clear();
            foreach (var att in Attributes)
            {
                if (!locations.ContainsKey(att.Name))
                    continue;

                vao.AddAttribute(locations[att.Name], att.ElementCount, att.Type, false, stride, offset);
                offset += att.Stride;
            }

            stride = AttributeInstances.Sum(x => x.Stride);
            offset = 0;
            foreach (var att in AttributeInstances)
            {
                if (!locations.ContainsKey(att.Name))
                    continue;

                vao.AddInstancedAttribute(locations[att.Name], att.ElementCount, att.Type, false, stride, offset);
                offset += att.Stride;
            }
            vao.Initialize();
        }

        internal void UpdateInstancedVertexData(PtclAttributeBuffer[] buffer)
        {
            float[] vertexBuffer = GetInstancedVertexBuffer(buffer);

            GL.BindBuffer(BufferTarget.ArrayBuffer, InstancedVbo);
            GL.BufferData(BufferTarget.ArrayBuffer, vertexBuffer.Length * sizeof(float), vertexBuffer, BufferUsageHint.StaticDraw);
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
        }

        private float[] GetInstancedVertexBuffer(PtclAttributeBuffer[] instances)
        {
            List<float> vertexBuffer = new List<float>();
            foreach (var instance in instances)
            {
                foreach (var att in AttributeInstances)
                {
                    switch (att.Name)
                    {
                        case "v_inWldPos":
                            {
                                vertexBuffer.Add(instance.WorldPosition.X);
                                vertexBuffer.Add(instance.WorldPosition.Y);
                                vertexBuffer.Add(instance.WorldPosition.Z);
                                vertexBuffer.Add(instance.WorldPosition.W);
                            }
                            break;
                        case "v_inVec":
                            {
                                vertexBuffer.Add(instance.Vector.X);
                                vertexBuffer.Add(instance.Vector.Y);
                                vertexBuffer.Add(instance.Vector.Z);
                                vertexBuffer.Add(instance.Vector.W);
                            }
                            break;
                        case "v_inWldPosDf":
                            {
                                vertexBuffer.Add(instance.WorldPositionDif.X);
                                vertexBuffer.Add(instance.WorldPositionDif.Y);
                                vertexBuffer.Add(instance.WorldPositionDif.Z);
                                vertexBuffer.Add(1);
                            }
                            break;
                        case "v_inColor0":
                            vertexBuffer.Add(instance.Color0.X);
                            vertexBuffer.Add(instance.Color0.Y);
                            vertexBuffer.Add(instance.Color0.Z);
                            vertexBuffer.Add(instance.Color0.W);
                            break;
                        case "v_inColor1":
                            vertexBuffer.Add(instance.Color1.X);
                            vertexBuffer.Add(instance.Color1.Y);
                            vertexBuffer.Add(instance.Color1.Z);
                            vertexBuffer.Add(instance.Color1.W);
                            break;
                        case "v_inRandom":
                            vertexBuffer.Add(instance.Random.X);
                            vertexBuffer.Add(instance.Random.Y);
                            vertexBuffer.Add(instance.Random.Z);
                            vertexBuffer.Add(instance.Random.W);
                            break;
                        case "v_inScl":
                            vertexBuffer.Add(instance.ScaleCenter.X);
                            vertexBuffer.Add(instance.ScaleCenter.Y);
                            vertexBuffer.Add(instance.ScaleCenter.Z);
                            vertexBuffer.Add(instance.ScaleCenter.W);
                            break;
                        case "v_inRot":
                            vertexBuffer.Add(instance.Rotate.X);
                            vertexBuffer.Add(instance.Rotate.Y);
                            vertexBuffer.Add(instance.Rotate.Z);
                            vertexBuffer.Add(instance.Rotate.W);
                            break;
                        case "v_inEmtMat0":
                            vertexBuffer.Add(instance.EmissionMatrix[0].X);
                            vertexBuffer.Add(instance.EmissionMatrix[0].Y);
                            vertexBuffer.Add(instance.EmissionMatrix[0].Z);
                            vertexBuffer.Add(instance.EmissionMatrix[0].W);
                            break;
                        case "v_inEmtMat1":
                            vertexBuffer.Add(instance.EmissionMatrix[1].X);
                            vertexBuffer.Add(instance.EmissionMatrix[1].Y);
                            vertexBuffer.Add(instance.EmissionMatrix[1].Z);
                            vertexBuffer.Add(instance.EmissionMatrix[1].W);
                            break;
                        case "v_inEmtMat2":
                            vertexBuffer.Add(instance.EmissionMatrix[2].X);
                            vertexBuffer.Add(instance.EmissionMatrix[2].Y);
                            vertexBuffer.Add(instance.EmissionMatrix[2].Z);
                            vertexBuffer.Add(instance.EmissionMatrix[2].W);
                            break;
                        case "v_inTexAnim":
                            vertexBuffer.Add(instance.TextureAnim.X);
                            vertexBuffer.Add(instance.TextureAnim.Y);
                            vertexBuffer.Add(instance.TextureAnim.Z);
                            vertexBuffer.Add(instance.TextureAnim.W);
                            break;
                        case "v_inSubTexAnim":
                            vertexBuffer.Add(instance.SubTextureAnim.X);
                            vertexBuffer.Add(instance.SubTextureAnim.Y);
                            vertexBuffer.Add(instance.SubTextureAnim.Z);
                            vertexBuffer.Add(instance.SubTextureAnim.W);
                            break;
                    }
                }
            }
            return vertexBuffer.ToArray();
        }

        public virtual void Draw(GLContext context)
        {
        }

        public virtual void Draw(GLContext context, int numInstances)
        {
        }

        public class Attribute
        {
            public string Name { get; set; }
            public int Location { get; set; }
            public int ElementCount { get; set; }
            public int Offset { get; set; }
            public VertexAttribPointerType Type { get; set; } = VertexAttribPointerType.Float;

            public int Stride => ElementCount * 4;

            public Attribute(string name, int count)
            {
                Name = name;
                ElementCount = count;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK.Graphics.OpenGL;
using OpenTK;
using GLFrameworkEngine;

namespace EffectLibrary
{
    public class ParticleQuadRender : ParticleBaseMeshRender
    {
        Vector2[] positions = new Vector2[4]
        {
                    new Vector2(-0.5f, 0.5f),
                    new Vector2(-0.5f, -0.5f),
                    new Vector2(0.5f, 0.5f),
                    new Vector2(0.5f, -0.5f),
        };

        int[] Indices = new int[4] { 0,1,2,3 };

        public override void Init()
        {
            //Load vaos
            Vbo = GL.GenBuffer();
            InstancedVbo = GL.GenBuffer();
            vao = new VertexBufferObject(Vbo, null, InstancedVbo);

            InitAttributes();
            UpdateVAO();
            UpdateVertexData();
        }

        public override void UpdateInstances(PtclAttributeBuffer[] buffer)
        {
            UpdateVAO();
            UpdateInstancedVertexData(buffer);
        }

        public override void InitAttributes()
        {
            Attributes.Add(new Attribute("v_inPos", 4));
            Attributes.Add(new Attribute("v_inColor", 4));
            Attributes.Add(new Attribute("v_inIndex", 1) { Type = VertexAttribPointerType.UnsignedInt });

            InitInstancedAttributes();
        }

        public override float[] GetVertexBuffer()
        {
            List<float> vertexBuffer = new List<float>();

            int vertexCount = positions.Length;
            for (int i = 0; i < vertexCount; i++)
            {
                foreach (var att in Attributes)
                {
                    switch (att.Name)
                    {
                        case "v_inPos":
                            {
                                var position = positions[i];
                                vertexBuffer.Add(position.X);
                                vertexBuffer.Add(position.Y);
                                vertexBuffer.Add(0);
                                vertexBuffer.Add(0);
                            }
                            break;
                        case "v_inColor":
                            var color = new OpenTK.Vector4(1);
                            vertexBuffer.Add(color.X);
                            vertexBuffer.Add(color.Y);
                            vertexBuffer.Add(color.Z);
                            vertexBuffer.Add(color.W);
                            break;
                        case "v_inIndex":
                            {
                                vertexBuffer.Add(Convert.ToSingle(Indices[i]));
                            }
                            break;
                    }
                }
            }
            return vertexBuffer.ToArray();
        }

        public override void Draw(GLContext context)
        {
            vao.Enable(context.CurrentShader);
            vao.BindVertexArray();
            GL.DrawArrays(PrimitiveType.TriangleStrip, 0, 4);
            GL.BindVertexArray(0);

            vao.Disable(context.CurrentShader);
        }

        public override void Draw(GLContext context, int numInstances)
        {
            if (numInstances == 0)
                return;

            vao.Enable(context.CurrentShader);
            vao.BindVertexArray();
            GL.DrawArraysInstanced(PrimitiveType.TriangleStrip, 0, 4, numInstances);
            GL.BindVertexArray(0);
        }
    }
}

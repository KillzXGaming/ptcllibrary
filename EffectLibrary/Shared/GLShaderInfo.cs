﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GLFrameworkEngine;
using System.IO;

namespace EffectLibrary
{
    public class GLShaderInfo
    {
        public string FragPath;
        public string VertPath;

        public byte[] VertexConstants;
        public byte[] PixelConstants;

        public ShaderProgram Program;

        public List<string> UsedVertexStageUniforms = new List<string>();
        public List<string> UsedPixelStageUniforms = new List<string>();

        public void Reload()
        {
            Program = new ShaderProgram(
                   new FragmentShader(File.ReadAllText(FragPath)),
                   new VertexShader(File.ReadAllText(VertPath)));
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EffectLibrary
{
    public interface IShaderResource
    {
        VertexShaderKey VertexShaderKey { get; set; }
        FragmentShaderKey FragmentShaderKey { get; set; }

        ShaderWrapperBase CreatePlatformWrapper();
    }
}

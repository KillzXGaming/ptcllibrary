﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EffectLibrary
{
    public interface IFileHeader
    {
        IList<IEmitterSet> EmitterSetList { get; }
        IList<IPrimitive> PrimitiveList { get; }
        IList<IShaderResource> ShaderList { get;  }

        uint Version { get; set; }
        FileResourceFlags Flags { get; set; }
    }
}

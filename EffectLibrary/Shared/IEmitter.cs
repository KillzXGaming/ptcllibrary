﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Toolbox.Core;

namespace EffectLibrary
{
    public interface IEmitter
    {
        string Name { get; set; }

        bool IsVisible { get; set; } 

        STGenericTexture GetTextureWrapper(int index);
    }
}

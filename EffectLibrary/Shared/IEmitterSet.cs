﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;

namespace EffectLibrary
{
    public interface IEmitterSet
    {
        //Instance used for spawning emitter set data
        EmitterSet EmitterSetHandle { get; set; }

        public Matrix4 MatrixSRT { get; set; }

        IEnumerable<IEmitter> EmitterList { get; }

        string Name { get; set; }

        bool IsVisible { get; set; }

        uint UserData { get; set; }
    }
}

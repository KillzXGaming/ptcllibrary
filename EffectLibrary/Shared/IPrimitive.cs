﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Numerics;

namespace EffectLibrary
{
    public interface IPrimitive
    {
        bool HasNormals { get; }
        bool HasTexCoords { get; }
        bool HasColors { get; }

        uint GetElementCount();
        uint GetVertexCount();

        Vector4[] GetPositionData();
        Vector4[] GetNormalsData();
        Vector4[] GetColorData();
        Vector4[] GetTexCoordData();

        uint[] GetIndexBuffer();
    }
}

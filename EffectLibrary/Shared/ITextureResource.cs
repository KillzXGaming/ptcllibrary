﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Toolbox.Core;

namespace EffectLibrary
{
    public interface ITextureResource
    {
        STGenericTexture Handle { get; set; }

        bool HasData { get; }

        bool Initialized { get; }

        WrapMode WrapX { get; set; }
        WrapMode WrapY { get; set; }

        float LODMax { get; set; }
        float LODBias { get; set; }

        FilterMode FilterMode { get; set; }

        void CreateResourceHandle();
    }
}

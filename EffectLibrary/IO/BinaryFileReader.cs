﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.IO;
using Syroot.BinaryData;

namespace EffectLibrary
{
    public class BinaryFileReader : BinaryDataReader
    {
        public PTCL PTCL { get; set; }

        public bool IsSwitch { get; set; }

        internal BinaryFileReader(PTCL ptcl, Stream stream, bool isSwitch, bool leaveOpen = true)
             : base(stream, Encoding.ASCII, leaveOpen)
        {
            IsSwitch = isSwitch;
            PTCL = ptcl;
            ByteOrder = IsSwitch ? ByteOrder.LittleEndian : ByteOrder.BigEndian;
        }

        public void Execute() {
            PTCL.Read(this);
        }

        /// <summary>
        /// Reads a section at the current reader position.
        /// </summary>
        /// <typeparam name="T">The type of the <see cref="IResData"/> elements.</typeparam>
        /// <returns></returns>
        internal List<T> ReadList<T>(uint count) where T : IResData, new()
        {
            List<T> list = new List<T>();
            for (int i = 0; i < count; i++)
            {
                T instance = new T();
                instance.Read(this);
                list.Add(instance);
            }
            return list;
        }

        /// <summary>
        /// Reads a section at the current reader position.
        /// </summary>
        /// <typeparam name="T">The type of the <see cref="IResData"/> elements.</typeparam>
        /// <returns></returns>
        internal List<T> ReadList<T>(int count) where T : IResData, new()
        {
            List<T> list = new List<T>();
            for (int i = 0; i < count; i++)
            {
                T instance = new T();
                instance.Read(this);
                list.Add(instance);
            }
            return list;
        }

        /// <summary>
        /// Reads a section at the current reader position.
        /// </summary>
        /// <typeparam name="T">The type of the <see cref="IResData"/> elements.</typeparam>
        /// <returns></returns>
        internal T ReadSection<T>() where T : IResData, new()
        {
            T instance = new T();
            instance.Read(this);
            return instance;
        }

        /// <summary>
        /// Reads a signature consisting of 4 ASCII characters encoded as an <see cref="UInt32"/> and checks for
        /// validity.
        /// </summary>
        /// <param name="validSignature">A valid signature.</param>
        internal void CheckSignature(params string[] validSignature)
        {
            // Read the actual signature and compare it.
            string signature = ReadString(sizeof(uint), Encoding.ASCII);
            if (!validSignature.Contains(signature))
            {
                 throw new Exception($"Invalid signature, expected '{string.Join(",", validSignature)}' but got '{signature}' at position {Position}.");
            }
        }

        /// <summary>
        /// Reads a section after reading and seeking an offset value.
        /// </summary>
        /// <typeparam name="T">The type of the <see cref="IResData"/> elements.</typeparam>
        /// <returns></returns>
        internal T ReadSectionAtOffset<T>() where T : IResData, new()
        {
            var offset = ReadOffset();
            if (offset == 0)
                return new T();

            using (TemporarySeek(offset, SeekOrigin.Begin))
            {
                T instance = new T();
                instance.Read(this);
                return instance;
            }
        }

        public string ReadStringFixed(int length)
        {
            return ReadString(length).Replace("\0", string.Empty);
        }

        /// <summary>
        /// Reads an offset in the current reader position.
        /// </summary>
        internal long ReadOffset() {
            return (long)(IsSwitch ? ReadUInt64() : ReadUInt32());
        }

        internal void SeekBegin(long pos) {
            this.Seek(pos, SeekOrigin.Begin);
        }

        internal string ReadStringTable() {
           return ((WiiU.PTCL_Header)PTCL.FileHeader).ReadStringTable(this);
        }
    }
}

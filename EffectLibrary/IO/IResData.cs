﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EffectLibrary
{
    public interface IResData
    {
        void Read(BinaryFileReader reader);
        void Write(BinaryFileWriter writer);
    }
}

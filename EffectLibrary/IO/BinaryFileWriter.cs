﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Syroot.BinaryData;

namespace EffectLibrary
{
    public class BinaryFileWriter : BinaryDataWriter
    {
        public PTCL PTCL { get; set; }

        public bool IsSwitch { get; set; }

        private List<StringEntry> _savedStrings = new List<StringEntry>();

        internal BinaryFileWriter(PTCL ptcl, Stream stream, bool isSwitch, bool leaveOpen = false)
             : base(stream, Encoding.ASCII, leaveOpen)
        {
            IsSwitch = isSwitch;
            PTCL = ptcl;
            ByteOrder = IsSwitch ? ByteOrder.LittleEndian : ByteOrder.BigEndian;
        }

        public void Execute()
        {
            PTCL.Write(this);
        }

        /// <summary>
        /// Writes a signature consisting of 4 ASCII characters encoded as an <see cref="UInt32"/>.
        /// </summary>
        /// <param name="value">A valid signature.</param>
        internal void WriteSignature(string value) {
            Write(Encoding.ASCII.GetBytes(value));
        }

        public void WriteList(IEnumerable<IResData> resDataList)
        {
            foreach (var resData in resDataList)
                resData.Write(this);
        }

        public void WriteSection(IResData resData) {
            resData.Write(this);
        }

        public void WriteOffset(long targetPosition)
        {
            long pos = this.Position;
            using (TemporarySeek(targetPosition, SeekOrigin.Begin)) {
                Write((uint)pos);
            }
        }

        public void WriteStringTable()
        {
            long startPos = this.Position;

            Dictionary<string, uint> writtenStrings = new Dictionary<string, uint>();
            foreach (var stringEntry in _savedStrings)
            {
                uint offset = (uint)(this.Position - startPos);
                if (writtenStrings.ContainsKey(stringEntry.Name))
                    offset = writtenStrings[stringEntry.Name];
                else
                    Write(stringEntry.Name, BinaryStringFormat.ZeroTerminated);

                using (TemporarySeek(stringEntry.ofsPositon, SeekOrigin.Begin)) {
                    Write(offset);
                }
            }
        }

        public void SaveString(string name)
        {
            Write(0);
            _savedStrings.Add(new StringEntry()
            {
                Name = name,
                ofsPositon = this.Position - 4,
            });
        }

        class StringEntry
        {
            public string Name;
            public long ofsPositon;
        }
    }
}

﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using EffectLibrary;
using Toolbox.Core;
using Toolbox.Core.Collada;
using Newtonsoft.Json;
using System.Text;

namespace EffectLibrary.Test
{
    class Program
    {
        static void Main(string[] args)
        {
            var ptcl = new PTCL(args[0]);
            Console.Read();
        }

        static void PrintStaticBlock(WiiU.GX2VertexShader vertexShader)
        {
            Console.WriteLine($"PrintStaticBlock");

            using (var reader = new Toolbox.Core.IO.FileReader(File.ReadAllBytes("static_block.bin")))
            {
                foreach (var uniform in vertexShader.Uniforms.OrderBy(x => x.Offset)) {
                    if (uniform.BlockIndex == 0) {
                        if (uniform.Name.EndsWith("]"))
                            continue;

                        int numElements = 1;
                        if (uniform.Type == WiiU.GX2ShaderVarType.FLOAT4)
                            numElements = 4;
                        if (uniform.Type == WiiU.GX2ShaderVarType.FLOAT3)
                            numElements = 3;
                        if (uniform.Type == WiiU.GX2ShaderVarType.FLOAT2)
                            numElements = 2;
                        if (uniform.Type == WiiU.GX2ShaderVarType.MATRIX4X4)
                            numElements = 16;

                        reader.SeekBegin(uniform.Offset * 4);
                        float[] values = reader.ReadSingles((int)uniform.Count * numElements);
                        Console.WriteLine($"{uniform.Offset * 4} Uniform {uniform.Name} values {string.Join(",", values)}");
                    }
                }
            }
        }

        static string WriteShaderInfo(WiiU.GX2VertexShader vertex, WiiU.GX2PixelShader pixel)
        {
            StringBuilder builder = new StringBuilder();
            using (StringWriter writer = new StringWriter(builder))
            {
                writer.WriteLine("-------------VERTEX SHADER-----------------");
                foreach (var attribute in vertex.Attributes)
                {
                    writer.WriteLine($"{attribute.Name} {attribute.Location} {attribute.Type} {attribute.Count}");
                }
                for (int i = 0; i < vertex.UniformBlocks.Count; i++)
                {
                    writer.WriteLine($"{vertex.UniformBlocks[i].Name} {vertex.UniformBlocks[i].Offset} {vertex.UniformBlocks[i].Size}");

                    foreach (var uniform in vertex.Uniforms.OrderBy(x => x.Offset))
                    {
                        if (uniform.BlockIndex == i)
                            writer.WriteLine($"{uniform.Offset * 4} {uniform.Name} {uniform.Offset} {uniform.Type} {uniform.Count}");

                    }
                }
                foreach (var sampler in vertex.Samplers)
                {
                    writer.WriteLine($"{sampler.Name} {sampler.Location} {sampler.Type}");
                }

                writer.WriteLine("-------------PIXEL SHADER-----------------");
                for (int i = 0; i < pixel.UniformBlocks.Count; i++)
                {
                    writer.WriteLine($"{pixel.UniformBlocks[i].Name} {pixel.UniformBlocks[i].Offset} {pixel.UniformBlocks[i].Size}");

                    foreach (var uniform in pixel.Uniforms.OrderBy(x => x.Offset))
                    {
                        if (uniform.BlockIndex == i)
                            writer.WriteLine($"{uniform.Name} {uniform.Offset} {uniform.Type} {uniform.Count}");

                    }
                }
                foreach (var sampler in pixel.Samplers)
                {
                    writer.WriteLine($"{sampler.Name} {sampler.Location} {sampler.Type}");
                }

            }

            return builder.ToString();
        }

        static STGenericModel FromGeneric(WiiU.PTCL_Header header, WiiU.EmitterSet emitterSet)
        {
            STGenericModel model = new STGenericModel();
            foreach (var emitter in emitterSet.Emitters)
            {
                if (emitter.MeshType == MeshType.Primitive)
                {
                    if (emitter.PrimitiveIndex != -1)
                    {
                        var prim = header.Primitives[(int)emitter.PrimitiveIndex];
                        var mesh = ConvertPrimitive(prim);
                        mesh.Name = $"{emitterSet.Name}_{emitter.Name}";
                        model.Meshes.Add(mesh);
                    }
                }
            }
            return model;
        }

        static STGenericMesh ConvertPrimitive(WiiU.Primitive prim)
        {
            var mesh = new STGenericMesh();
            uint vertexCount = prim.PositionsInfo.ElementCount / 3;
            uint indexCount = prim.IndexInfo.ElementCount;

            for (int i = 0; i < vertexCount; i++)
            {
                var vertex = new STVertex();
                if (prim.HasTexCoords)
                    vertex.TexCoords = new OpenTK.Vector2[1];
                if (prim.HasColors)
                    vertex.Colors = new OpenTK.Vector4[1];

                vertex.Position = new OpenTK.Vector3(
                     prim.PositionsInfo.Data[i].X,
                     prim.PositionsInfo.Data[i].Y,
                     prim.PositionsInfo.Data[i].Z);

                if (prim.HasNormals)
                    vertex.Normal = new OpenTK.Vector3(
                 prim.NormalsInfo.Data[i].X,
                 prim.NormalsInfo.Data[i].Y,
                 prim.NormalsInfo.Data[i].Z);

                if (prim.HasTexCoords)
                    vertex.TexCoords[0] = new OpenTK.Vector2(
                 prim.TexCoordInfo.Data[i].X,
                 prim.TexCoordInfo.Data[i].Y);

                if (prim.HasColors)
                    vertex.Colors[0] = new OpenTK.Vector4(
                 prim.ColorsInfo.Data[i].X,
                 prim.ColorsInfo.Data[i].Y,
                 prim.ColorsInfo.Data[i].Z,
                 prim.ColorsInfo.Data[i].W);

                mesh.Vertices.Add(vertex);
            }

            var poly = new STPolygonGroup();
            mesh.PolygonGroups.Add(poly);

            for (int i = 0; i < indexCount; i++)
            {
                uint index = (uint)prim.IndexInfo.Data[i].X;
                poly.Faces.Add(index);
            }
            return mesh;
        }
    }
}

# PtclLibrary

An accurate PTCL rendering library for displaying and editing PTCL files real time.

Using the eft decompilation for rendering code https://github.com/open-ead/NW4F-Eft

Wiki for parsing information 

- https://zenith.miraheze.org/wiki/PTCL_(File_Format)
- https://github.com/kinnay/Nintendo-File-Formats/wiki/PTCL-File-Format

## Credits

- AboodXD/MasterVermilli0n for decomp and ptcl research.
- Kinnary for additional ptcl research.
